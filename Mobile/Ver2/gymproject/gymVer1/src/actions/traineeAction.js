import { showMessage } from 'react-native-flash-message';

import {
  getPersonalTrainee,
  getTrainersOfPT,
  getExercisesById,
  uppdateBmis,
  ptOfTraineeBeforeBooking,
  changePT,
  confirm,
} from '../api/TraineeApi';

import {
  getInfoPt,
  getInfoTrainee,
} from '../api/AuthApi';

import types from './actionTypes';
import AsyncStorageService from '../api/AsyncStorageService';


export const getPersonalTrainers = () => async (dispatch) => {
  // dispatch({type: types.GETTING_PT_INFO});

  const json = await getPersonalTrainee();
  console.log('getPersonalTrainers', json);

  // if (json?.id) {
  //   dispatch({type: types.GET_PT_INFO_SUCCESS, data: json});
  // } else {
  //   // showMessage({
  //   //   message: json?.message || 'Yêu cầu thất bại. Vui lòng thử lại sau',
  //   //   type: 'danger',
  //   //   duration: 1500,
  //   // })
  //   dispatch({type: types.GET_PT_INFO_FAILURE});
  // }
};

export const getTrainersOfPt = () => async (dispatch) => {
  dispatch({ type: types.GETTING_TRAINERS_BY_PT_ID });

  const json = await getTrainersOfPT();
  await AsyncStorageService.putUser('');

  if (json?.length >= 0) {
    dispatch({ type: types.GET_TRAINERS_BY_PT_ID_SUCCESS, data: json });
  } else {
    dispatch({ type: types.GET_TRAINERS_BY_PT_ID_FAILURE });
  }
};

// export const getExercisesByTrainerId = (id) => async (dispatch) => {
//   dispatch({ type: types.GETTING_EXERCISES_OF_TRAINEE });

//   const json = await getExercisesById(id);

//   if (json?.length >= 0) {
//     dispatch({ type: types.GET_EXERCISES_OF_TRAINEE_SUCCESS, data: json });
//   } else {
//     dispatch({ type: types.GET_EXERCISES_OF_TRAINEE_FAILURE });
//   }
// };
export const getExercisesByTrainerId = (id) => async (dispatch) => {
  dispatch({ type: types.GETTING_EXERCISES_OF_TRAINEE });
  const json = await getExercisesById(id);
  console.log('EXCERCISE: ', json)
  if (json?.error == true) {
    dispatch({ type: types.GET_EXERCISES_OF_TRAINEE_FAILURE });
  } else {
    dispatch({ type: types.GET_EXERCISES_OF_TRAINEE_SUCCESS, data: json });
  }
};
export const uppdateBmi = (payload) => async (dispatch) => {
  const json = await uppdateBmis(payload);
  if (json?.id) {
    dispatch({ type: types.FINISH_INPUT_BMI });
    showMessage({
      message: 'Update BMI Succesful',
      type: 'success',
      duration: 2000,
    });

    dispatch({ type: types.GETTING_PT_INFO });
    const json1 = await getInfoPt();
    console.log("getInfoPT: ", json)
    if (json1?.id) {
      dispatch({ type: types.GET_PT_INFO_SUCCESS, data: json1 });
    } else {
      dispatch({ type: types.GET_PT_INFO_FAILURE });
    }

    const json2 = await getInfoTrainee();
    if (json2?.id) {
      dispatch({ type: types.GET_PT_INFO_SUCCESS, data: json2 });
    } else {
      dispatch({ type: types.GET_PT_INFO_FAILURE });
    }

    meta.onSuccess(json);
  } else {
    showMessage({
      message: 'Request failed. Please try again later',
      type: 'warning',
      duration: 2000,
    });
  }
};
export const getPtOfTraineeBeforeBooking = (meta) => async (dispatch) => {
  const json = await ptOfTraineeBeforeBooking();
  meta.onSuccess(json);
};

export const changePt = (payload) => async (dispatch) => {
  const json = await changePT(payload);
  showMessage({
    message: 'Send Request Change Successfull',
    type: 'success',
    duration: 2000,
  });
};

export const confirmStatus = (payload, id, traineeId) => async (dispatch) => {
  const json = await confirm(payload, id);
  console.log('confirmStatus: ', json)
  if (json?.error == true) {
    showMessage({
      message: json.responseError.title,
      type: 'warning',
      duration: 2000,
    });
  } else {
    dispatch(getExercisesByTrainerId(traineeId));
    showMessage({
      message: "Status change successful",
      type: 'success',
      duration: 2000,
    });
  }
};