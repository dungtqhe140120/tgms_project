import moment from 'moment';

export const formatTodayByYMD = () => {
  const today = new Date();
  return moment(today).format('YYYY/MM/DD');
};

export const formatDate1 = date => {
  return moment(date).format('DD/MM/YYYY');
};

export const formatDate2 = date => {
  const dateMomentObject = moment(date, 'DD/MM/YYYY'); // 1st argument - string, 2nd argument - format
  const dateObject = dateMomentObject.toDate();
  return dateObject;
};

export const inputFormatDate = text => {
  text = text.replace(/[/]/g, '');
  const date = text.slice(0, 2);
  const month = text.slice(2, 4);
  const year = text.slice(4, 8);
  let dateTime = '';
  dateTime += `${date}`;
  dateTime += month ? `/${month}` : '';
  dateTime += year ? `/${year}` : '';
  return dateTime;
};

export const calculateTimeMessage = (date, time) => {
  const newListDate = date?.split('/');
  const newDate =
    newListDate?.length === 3
      ? `${newListDate[1]}/${newListDate[0]}/${newListDate[2]} ${time}`
      : null;
  let textResult = '';

  if (newDate?.length) {
    const textTime = moment(newDate).add(14, 'hours').from(moment());
    const lengthTextTime = textTime.length || 0;

    if (lengthTextTime > 0) {
      const arrayTextTime = textTime.split(' ');
      const lengthArrayTextTime = arrayTextTime.length;
      const timeFromArrayTextTime = arrayTextTime[lengthArrayTextTime - 2];

      console.warn(newDate, timeFromArrayTextTime);
      switch (timeFromArrayTextTime) {
        case 'seconds':
          textResult = 'Vài giây trước';
          break;

        case 'minutes':
          textResult = arrayTextTime[0] + ' phút trước';
          break;

        case 'hours':
          textResult = arrayTextTime[0] + ' giờ trước';
          break;

        case 'hour':
          textResult = '1 giờ trước';
          break;

        case 'an':
          textResult = '1 giờ trước';
          break;

        case 'days':
          textResult = arrayTextTime[0] + ' ngày trước';
          break;

        case 'months':
          textResult = arrayTextTime[0] + ' tháng trước';
          break;

        case 'years':
          textResult = arrayTextTime[0] + ' năm trước';
          break;

        default:
          textResult = textTime;
          break;
      }

      return textResult;
    }
  }
  return textResult;
};
