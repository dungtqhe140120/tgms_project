import moment from 'moment';

const emailRegex =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const phoneRegex = /((^(\+84|84|0|0084){1})(3|5|7|8|9))+([0-9]{8})$/;

class Validator {
  constructor() {
    this.emailRegex = emailRegex;
    this.phoneRegex = phoneRegex;
  }

  isEmpty(...data) {
    for (let i = 0; i < data.length; i++) {
      if (!data[i]) return true;
    }
    return false;
  }

  isEmail(email) {
    return this.emailRegex.test(email);
  }

  isPhoneNumber(phoneNumber) {
    return this.phoneRegex.test(phoneNumber);
  }

  isTrueDateFormat(format = 'DD-MM-YYYY', value) {
    const d = moment(value, format);
    if (d == null || !d.isValid()) return false;
    return true;
  }
}

export default new Validator();
