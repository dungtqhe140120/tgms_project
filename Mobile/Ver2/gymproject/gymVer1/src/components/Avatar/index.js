import React, {useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {launchImageLibrary} from 'react-native-image-picker';
import FastImage from 'react-native-fast-image';
import IonIcons from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import Images from '../../common/Images';

function Avatar({onChangeImage, editable, image}) {
  const [urlAvatar, setUrlAvatar] = useState(image);

  const chooseImage = async () => {
    const result = await launchImageLibrary({
      // includeBase64: true,
    });

    if (result.didCancel || result.errorCode) {
      return;
    }
    if (result?.assets.length === 1) {
      setUrlAvatar(result.assets[0]);
      onChangeImage(result.assets[0]);
    }
  };

  return (
    <View style={styles.container}>
      <View>
        {urlAvatar?.uri ? (
          <FastImage
            source={{
              uri: urlAvatar.uri,
            }}
            style={styles.image}
          />
        ) : (
          <FastImage source={Images.defaultAvatar} style={styles.image} />
        )}

        {editable && (
          <TouchableOpacity
            activeOpacity={0.9}
            style={styles.buttonChooseImage}
            onPress={chooseImage}>
            <IonIcons name="camera-outline" size={20} color="white" />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
}

export default Avatar;
