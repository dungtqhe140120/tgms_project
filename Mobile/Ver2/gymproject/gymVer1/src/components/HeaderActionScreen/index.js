import React, {PureComponent} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import IonIcons from 'react-native-vector-icons/Ionicons';
import FAIcons from 'react-native-vector-icons/FontAwesome';

import styles from './styles';

class HeaderActionScreen extends PureComponent {
  renderHeaderLeft = () => {
    const {
      goBack,
      colorIconBack,
      sizeIconBack,
      sizeIconDrawer,
      openDrawer,
      showEmptyView,
    } = this.props;
    if (sizeIconBack) {
      return (
        <TouchableOpacity style={styles.leftContainer} onPress={goBack}>
          <IonIcons
            name="chevron-back"
            size={sizeIconBack}
            color={colorIconBack}
          />
        </TouchableOpacity>
      );
    } else if (sizeIconDrawer) {
      return (
        <TouchableOpacity
          style={[styles.leftContainer, {paddingLeft: 10}]}
          onPress={openDrawer}>
          <FAIcons name="navicon" size={sizeIconDrawer} color={colorIconBack} />
        </TouchableOpacity>
      );
    } else if (showEmptyView) {
      return <View style={styles.leftContainer} />;
    } else {
      return null;
    }
  };

  render() {
    const {title, titleActionRight, onPressText, colorText} = this.props;

    return (
      <View style={styles.container}>
        {this.renderHeaderLeft()}

        <View style={styles.centerContainer}>
          {title?.length > 0 && (
            <Text style={[styles.textTitle, {color: colorText}]}>{title}</Text>
          )}
        </View>

        <View style={styles.rightContainer}>
          {titleActionRight?.length > 0 && (
            <TouchableOpacity onPress={onPressText} activeOpacity={0.9}>
              <Text style={[styles.textRightTitle, {color: colorText}]}>
                {titleActionRight}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}

export default HeaderActionScreen;
