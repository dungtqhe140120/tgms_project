import React from 'react';
import {Image, TouchableOpacity, View, Text} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/core';
import {useDispatch, useSelector} from 'react-redux';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';

// import ImageZoom from "../../components/ImageZoom";

import Images from '../../common/Images';
import Constants from '../../common/Constants';

// import {calculateTimeMessage} from '../../utils/DateFormat';
// import {getListMessages} from '../../actions/chatAction';

export const HeaderLeftChatRoom = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  // const friendReducer = useSelector((state) => state.fiendShip);

  // const { friend } = friendReducer;

  const onShowDetailUser = () => {
    // dispatch(
    //   getUserById(friend?.id, {
    //     onSuccess: () => {
    //       navigation.navigate("FriendScreen");
    //     },
    //   })
    // );
  };
  const onBack = () => {
    // dispatch(getListMessages());
    navigation.goBack();
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onBack}>
        <MCI name="chevron-left" size={40} />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={onShowDetailUser}
        style={styles.buttonAvatarDefault}>
        {/* {friend?.avatarUrl ? (
          <ImageZoom
            style={styles.avatarChatRoom}
            url={`${Constants.apiUrl}${friend.avatarUrl}`}
            useZoom={false}
            resizeMode="cover"
          />
        ) : ( */}
        <Image
          style={styles.avatarChatRoom}
          source={Images.defaultAvatar}
          resizeMode="cover"
        />
        {/* )} */}

        <View style={{marginLeft: 7}}>
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>
            {/* {friend?.fullName} */}
          </Text>
          <View style={styles.viewTimeOnScreen}>
            <Text style={styles.textTimeOnScreen}>
              {/* Hoạt động {calculateTimeMessage(friend?.lastOnline * 1000 || 0)} */}
            </Text>
            {/* {friend.idOnline && <View style={styles.dotActive} />} */}
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export const HeaderLeftNorMal = () => {
  const navigation = useNavigation();

  const onBack = () => {
    navigation.goBack();
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onBack}>
        <MCI name="chevron-left" size={40} />
      </TouchableOpacity>
    </View>
  );
};
