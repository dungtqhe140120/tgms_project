import { StyleSheet } from "react-native";
import Colors from "../../common/Colors";

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginBottom: 7,
    alignItems: "center",
    justifyContent: "center",
  },

  buttonAvatarDefault: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },

  buttonAvatar: {
    paddingLeft: 10,
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },

  avatarChatRoom: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },

  viewTimeOnScreen: {
    flexDirection: "row",
    alignItems: "center",
  },
  textTimeOnScreen: {
    fontSize: 12,
    opacity: 0.7,
    marginRight: 5,
  },
  dotActive: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: "green",
  },
});

export default styles;
