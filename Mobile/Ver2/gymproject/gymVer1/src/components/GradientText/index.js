import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import MaskedView from '@react-native-community/masked-view';
import {StyleSheet, Text} from 'react-native';

const GradientText = props => {
  return (
    <MaskedView maskElement={<Text {...props} />}>
      <LinearGradient
        start={{x: 1, y: 0}}
        end={{x: 0, y: 0}}
        colors={props.colors}
        {...props.gradient}
        locations={[1, 0.7, 0]}
        style={StyleSheet.absoluteFill}
      />
      <Text {...props} style={[props.style, styles.mask]} />
    </MaskedView>
  );
};

const styles = StyleSheet.create({
  mask: {
    opacity: 0,
  },
});

export default GradientText;
