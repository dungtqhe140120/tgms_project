import types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  exercises: [],
  myExercises: [],
  myDetailExercises: [],
};

const exercisesReducer = (state = initialState, action) => {
  const {type, data} = action;

  switch (type) {
    case types.GETTING_ROOM_EXERCISES:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_ROOM_EXERCISES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        exercises: data,
      };
    case types.GET_ROOM_EXERCISES_FAILURE:
      return {
        ...state,
        isFetching: false,
        exercises: [],
      };

    case types.GETTING_MY_EXERCISES:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_MY_EXERCISES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        myExercises: data,
      };
    case types.GET_MY_EXERCISES_FAILURE:
      return {
        ...state,
        isFetching: false,
        myExercises: [],
      };

    case types.ADDING_NEW_EXERCISES:
      return {
        ...state,
        isFetching: true,
      };
    case types.ADD_NEW_EXERCISES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        myExercises: [...state.myExercises, data],
      };
    case types.ADD_NEW_EXERCISES_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case types.ADDING_NEW_DETAIL_EXERCISES:
      return {
        ...state,
        isFetching: true,
      };
    case types.ADD_NEW_DETAIL_EXERCISES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        myDetailExercises: [...state.myDetailExercises, data],
      };
    case types.ADD_NEW_DETAIL_EXERCISES_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case types.CLEAR_FETCHING:
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
};

export default exercisesReducer;
