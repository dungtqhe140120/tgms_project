import types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  dataOnHomeOfTrainee: {},
};

const homeReducer = (state = initialState, action) => {
  const {type, data} = action;

  switch (type) {
    case types.GETTING_DATA_ON_HOME_BY_TRAINEE:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_DATA_ON_HOME_BY_TRAINEE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        dataOnHomeOfTrainee: data,
      };
    case types.GET_DATA_ON_HOME_BY_TRAINEE_FAILURE:
      return {
        ...state,
        dataOnHomeOfTrainee: {},
      };

    case types.CLEAR_FETCHING:
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
};

export default homeReducer;
