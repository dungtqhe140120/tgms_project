import axios from 'axios';
import {showMessage} from 'react-native-flash-message';

import AppConfig from '../common/AppConfig.json';

const APIInstant = axios.create();
const createAPINoToken = () => {
  APIInstant.interceptors.request.use(async (config) => {
    config.baseURL = AppConfig.api.url;
    config.headers = {
      'Content-Type': 'application/json',
      ...config.headers,
    };
    return config;
  });
  APIInstant.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      // const message = error?.response?.data?.message;
      // return Promise.reject(error);
      return {
        data: {
          error: true,
          errorCode: error?.response?.status || 0,
        },
      };
    },
  );
  return APIInstant;
};
const axiosClientWithoutToken = createAPINoToken();
function handleResult(api, generic) {
  return api.then((res) => handleResponse(res.data));
}
function handleResponse(data) {
  return Promise.resolve(data);
}
export const ApiClientWithoutToken = {
  get: (url, payload) =>
    handleResult(axiosClientWithoutToken.get(url, payload)),
  post: (url, payload) =>
    handleResult(axiosClientWithoutToken.post(url, payload)),
};
