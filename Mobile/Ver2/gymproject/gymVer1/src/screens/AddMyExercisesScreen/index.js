import React, {useState, useRef} from 'react';
import {SafeAreaView, Text, View, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useNavigation} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import {launchImageLibrary} from 'react-native-image-picker';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage} from 'react-native-flash-message';
import AntDIcons from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import {HeaderLeftNorMal} from '../../components/HeaderLeft';
import InputCustom from '../../components/InputCustom';
import Colors from '../../common/Colors';
import Validator from '../../utils/Validator';
import Constants from '../../common/Constants';
import {addNewMyExercises, uploadImage} from '../../actions';
import AsyncStorageService from '../../api/AsyncStorageService';

const AddMyExercisesScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const authReducer = useSelector((state) => state.auth);

  const refTitle = useRef(null);
  const refDescription = useRef(null);
  const refContent = useRef(null);

  const [image, setImage] = useState(null);
  const [error, setError] = useState({
    title: null,
    description: null,
    content: null,
  });

  const createFormData = (photo) => {
    const data = new FormData();

    data.append('file', {
      name: Constants.isIOS ? photo.filename : `${new Date()}.png`,
      type: Constants.isIOS ? 'jpeg' : 'image/jpeg',
      uri: Constants.isIOS ? photo?.uri.replace('file://', '') : photo.uri,
    });

    return data;
  };

  const chooseImage = async () => {
    const result = await launchImageLibrary({
      selectionLimit: 1,
    });

    if (result.didCancel || result.errorCode) {
      return;
    }
    if (result?.assets.length === 1) {
      setImage(result.assets[0]);
    }
  };

  const addNewExercises = async () => {
    const title = refTitle.current.value;
    const description = refDescription.current.value;
    // const content = refContent.current.value;

    const err = {title: null, description: null, content: null};
    let titleStatus = false;
    let descriptionStatus = false;
    let contentStatus = false;

    if (Validator.isEmpty(title)) {
      err.title = 'Title is required';
      titleStatus = true;
    }
    if (Validator.isEmpty(description)) {
      err.description = 'Description is required';
      descriptionStatus = true;
    }
    // if (Validator.isEmpty(content)) {
    //   err.content = 'Content is required';
    //   contentStatus = true;
    // }
    setError(err);

    if (image?.uri) {
      const formDataAvatar = createFormData(image);
      dispatch(
        uploadImage(formDataAvatar, {
          onSuccess: (url) => {
            if (
              !titleStatus &&
              !descriptionStatus &&
              !contentStatus &&
              authReducer.info?.id &&
              authReducer.role?.length > 0
            ) {
              const payload = {
                name: title,
                short_Description: description,
                // content,
                image: url,
              };
              dispatch(
                addNewMyExercises(payload, {
                  onSuccess: () => {
                    navigation.goBack();
                  },
                  onFailure: () => {},
                }),
              );
            }
          },
          onFailure: () => {},
        }),
      );
    } else {
      showMessage({
        message: 'Please select an image to upload',
        type: 'warning',
        duration: 2000,
      });
    }
  };

  // const sendData = async (payload) => {
  //   await AsyncStorageService.putUser(`${authReducer.info.id}`);
  //   await AsyncStorageService.putRole(`${authReducer.role[0].name}`);

  //   console.log('payload', payload)
  //   dispatch(
  //     addNewMyExercises(payload, {
  //       onSuccess: () => {
  //         navigation.goBack();
  //       },
  //       onFailure: () => {},
  //     }),
  //   );
  // };

  const focusInput = (type) => {
    switch (type) {
      case 'title':
        setError({...error, title: null});
        break;
      case 'description':
        setError({...error, description: null});
        break;
      case 'content':
        setError({...error, content: null});
        break;
      default:
        break;
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.headerAction}>
        <HeaderLeftNorMal />
      </View>

      <View style={styles.wrapContent}>
        <Text style={styles.labelText}>New Exercises</Text>
        <InputCustom
          id="title"
          ref={refTitle}
          placeholder="Input your title..."
          containerStyleProps={styles.inputContainer}
          useFocusInput
          hideIconLeft
          errorText={error.title}
          focusAction={focusInput}
        />
        <InputCustom
          id="description"
          ref={refDescription}
          placeholder="Input your short description..."
          containerStyleProps={styles.inputContainer}
          useFocusInput
          hideIconLeft
          errorText={error.description}
          focusAction={focusInput}
        />
        {/* <InputCustom
          id="content"
          ref={refContent}
          placeholder="Input your content..."
          containerStyleProps={styles.inputContainer}
          useFocusInput
          hideIconLeft
          errorText={error.content}
          focusAction={focusInput}
        /> */}

        <Text style={[styles.labelText, styles.mt]}>Select Image</Text>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.addImageButton}
          onPress={chooseImage}>
          {image?.uri ? (
            <FastImage
              style={styles.imageSelected}
              source={{uri: image.uri}}
              resizeMode="contain"
            />
          ) : (
            <AntDIcons name="plus" style={styles.addImageIcon} />
          )}
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.containerPlusButton}
          onPress={addNewExercises}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 0, y: 1}}
            colors={Colors.mainGradientButton}
            style={styles.linearGradient}>
            <Text style={styles.addText}>Add</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default AddMyExercisesScreen;
