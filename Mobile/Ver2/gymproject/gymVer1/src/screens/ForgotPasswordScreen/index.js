import {Text, View, TouchableOpacity} from 'react-native';
import React, {useRef} from 'react';
import Svg, {
  Path,
  Defs,
  Stop,
  LinearGradient as LinearGradientSVG,
} from 'react-native-svg';
import FastImage from 'react-native-fast-image';
import {showMessage} from 'react-native-flash-message';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import FAIcons from 'react-native-vector-icons/FontAwesome';

import styles from './styles';
import {trimString} from '../../utils/Function';
import Images from '../../common/Images';
import InputCustom from '../../components/InputCustom';
import HideKeyboard from '../../components/HideKeyboard';
import {HeaderLeftNorMal} from '../../components/HeaderLeft';
import {resetPassword} from '../../actions';

const ForgotPasswordScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const refPhoneOrEmail = useRef(null);
  // setBarStyle('dark-content');
  const goToInputOTPCode = () => {
    const strEmail = trimString(refPhoneOrEmail.current.value);
    if (strEmail.length > 0) {
      // navigation.navigate('InputOTPScreen', {
      //   phoneOrEmail: strEmail,
      // });
      dispatch(resetPassword(strEmail));
    } else {
      showMessage({
        message: 'Please input your phone or email address',
        type: 'warning',
        duration: 2000,
      });
    }
  };

  return (
    <HideKeyboard>
      <View style={styles.container}>
        <View style={styles.headerAction}>
          <HeaderLeftNorMal />
        </View>

        <View style={styles.contentView}>
          <View style={styles.imageLogoView}>
            <FastImage source={Images.logo} style={styles.imageLogo} />
          </View>
          <InputCustom
            ref={refPhoneOrEmail}
            placeholder="Email"
            checkIcon
            iconComponent={<FAIcons name="user-o" style={styles.inputIcon} />}
          />

          <TouchableOpacity
            style={styles.confirmButton}
            onPress={goToInputOTPCode}>
            <Text style={styles.confirmText}>Send</Text>
          </TouchableOpacity>
        </View>

        <Svg
          height="150%"
          width="150%"
          viewBox="0 0 20 20"
          style={styles.svgView}>
          <Defs>
            <LinearGradientSVG id="path" x1="0" y1="0" x2="1" y2="1">
              <Stop offset="0" stopColor="#8736e7" stopOpacity="1" />
              <Stop offset="0.5" stopColor="#b732a0" stopOpacity="1" />
              <Stop offset="1" stopColor="#e52b54" stopOpacity="1" />
            </LinearGradientSVG>
          </Defs>
          <Path d="M 0 8 C 5 11 12 5 16 9 V 13 H 0" fill="url(#path)" />
        </Svg>
      </View>
    </HideKeyboard>
  );
};

export default ForgotPasswordScreen;
