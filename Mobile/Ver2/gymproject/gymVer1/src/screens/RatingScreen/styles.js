import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth, ScreenHeight} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  reportItem: {
    borderColor:'red',
    marginTop: 40,
    marginHorizontal: 10,
  },

  headerItem: {
    backgroundColor: Colors.lightgrey,
    flexDirection: 'row',
    alignItems: 'center',
  },
  userImgItem: {
    height: 45,
    width: 45,
  },
  userNameItem: {
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 10,
  },

  starsItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'baseline',
    paddingVertical: 10,
    marginTop: 5,
    width: '100%',
  },
  starItem: {
    marginHorizontal: 5,
  },
  starImageItem: {
    width: 30,
    height: 30,
  },

  ratingInput: {
    height: 100,
    borderWidth: 1,
    borderColor: Colors.lightgrey,
    borderRadius: 4,
    marginTop: 10,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    textAlignVertical: 'top',
  },

  containerPlusButton: {
    marginTop: ScreenHeight(0.04),
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addText: {
    fontSize: 24,
    color: Colors.white,
  },
  linearGradient: {
    width: ScreenWidth(0.3),
    borderRadius: 8,
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
