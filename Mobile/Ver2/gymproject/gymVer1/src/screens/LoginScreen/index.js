import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import {showMessage} from 'react-native-flash-message';
import {useDispatch} from 'react-redux';
import OctIcons from 'react-native-vector-icons/Octicons';
import ZocialIcons from 'react-native-vector-icons/Zocial';
import EntypoIcons from 'react-native-vector-icons/Entypo';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import IonIcons from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import Images from '../../common/Images';
import Colors from '../../common/Colors';
import GradientText from '../../components/GradientText';
import HideKeyboard from '../../components/HideKeyboard';
import {loginWithPayload} from '../../actions';
import {trimString} from '../../utils/Function';

const LoginScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const handleLogin = () => {
    if (email.length === 0 || password.length === 0) {
      showMessage({
        message: 'Username or password is required',
        type: 'warning',
        duration: 2000,
      });
    } else {
      const payload = {
        username: trimString(email),
        password: trimString(password),
        rememberMe: false,
      };
      dispatch(
        loginWithPayload(payload, {
          onSuccess: () => {},
          onFailure: () => {},
        }),
      );
    }
  };

  return (
    <HideKeyboard>
      <View style={styles.container}>
        <Image style={styles.background} source={Images.bgLogin} />
        <SafeAreaView style={styles.content}>
          <Image source={Images.logo} style={styles.logo} />
          <GradientText
            colors={Colors.mainGradientButton}
            style={styles.textLoginStay}>
            <Text>Login to sty fit</Text>
          </GradientText>
          <Text style={styles.textDescriptionLogin}>SAVE MONEY WITH CLUB</Text>
          <Text style={styles.textDescriptionLogin}>MELCHE CARORP</Text>

          {/* Form */}
          <View style={styles.form}>
            <View style={styles.emailView}>
              <EntypoIcons style={styles.iconForm} name="email" />
              <View style={styles.inputEmailView}>
                <Text style={styles.label}>EMAIL</Text>
                <TextInput
                  style={styles.input}
                  placeholderTextColor="rgba(255,255,255,0.5)"
                  placeholder="example@example.com"
                  maxLength={50}
                  onChangeText={(text) => setEmail(text)}
                />
              </View>
            </View>
            <View style={styles.emailView}>
              <SimpleLineIcons style={styles.iconForm} name="lock" />
              <View style={styles.inputEmailView}>
                <Text style={styles.label}>PASSWORD</Text>
                <TextInput
                  style={styles.input}
                  placeholderTextColor="rgba(255,255,255,0.5)"
                  maxLength={50}
                  secureTextEntry={!showPassword}
                  onChangeText={(text) => setPassword(text)}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.5}
                style={styles.btnEye}
                onPress={() => setShowPassword(!showPassword)}>
                <IonIcons
                  name={showPassword ? 'eye' : 'eye-off'}
                  size={20}
                  color="white"
                />
              </TouchableOpacity>
            </View>
          </View>

          {/* Actions login */}
          <View style={styles.actionsLogin}>
            <LinearGradient
              start={{x: 1, y: 0}}
              end={{x: 0, y: 0}}
              colors={Colors.mainGradientButton}
              style={styles.linearGradient}>
              <TouchableOpacity
                onPress={handleLogin}
                style={styles.btnSignIn}
                activeOpacity={0.5}>
                <View style={styles.emptyViewSignInt} />
                <Text style={styles.textSignIn}>Sign In</Text>
                <OctIcons style={styles.iconNext} name="arrow-right" />
              </TouchableOpacity>
            </LinearGradient>
            <TouchableOpacity
              style={styles.btnForgotPassword}
              activeOpacity={0.5}
              onPress={() => navigation.navigate('ForgotPasswordScreen')}>
              <Text style={styles.textBtnForgotPassword}>Forgot password?</Text>
            </TouchableOpacity>
          </View>

          {/* Another actions */}
          <View style={styles.anotherActions}>
            <TouchableOpacity style={styles.anotherAction}>
              <ZocialIcons style={styles.iconAnother} name="facebook" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.anotherAction}>
              <ZocialIcons style={styles.iconAnother} name="twitter" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.anotherAction}>
              <ZocialIcons style={styles.iconAnother} name="google" />
            </TouchableOpacity>
          </View>
          <View style={styles.navSignUpView}>
            <View>
              <Text style={styles.textSignUp}>Don't have account?</Text>
            </View>
            <TouchableOpacity style={styles.btnSignUp}>
              <Text style={styles.textSignUp}> Sign up now</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>

        <View style={styles.layeredView} />
      </View>
    </HideKeyboard>
  );
};

export default LoginScreen;
