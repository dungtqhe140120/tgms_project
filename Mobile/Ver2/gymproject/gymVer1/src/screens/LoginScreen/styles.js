import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  layeredView: {
    zIndex: 1,
    backgroundColor: Colors.rgba6,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },

  background: {
    transform: [{scale: 2.1}, {translateX: 90}, {translateY: 40}],
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: 400,
    height: 600,
  },

  content: {
    flex: 1,
    alignItems: 'center',
    zIndex: 2,
  },

  logo: {
    width: ScreenWidth(0.36),
    height: ScreenWidth(0.24),
    marginTop: ScreenWidth(0.15),
  },

  textLoginStay: {
    fontWeight: 'bold',
    fontSize: 30,
    marginVertical: 7,
  },
  textDescriptionLogin: {
    fontSize: 15,
    fontWeight: 'bold',
    color: Colors.white,
  },

  form: {
    width: ScreenWidth(0.86),
    marginTop: ScreenWidth(0.1),
  },
  emailView: {
    borderBottomColor: Colors.white,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginBottom: 20,
  },
  iconForm: {
    color: Colors.white,
    fontSize: 30,
    marginRight: 7,
    paddingBottom: 12,
  },
  inputEmailView: {
    flex: 1,
  },
  label: {
    color: Colors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
  input: {
    paddingTop: 5,
    paddingBottom: 12,
    color: Colors.lightTextInputLogin,
  },
  btnEye: {
    bottom: 12,
  },

  actionsLogin: {
    width: ScreenWidth(0.86),
    marginTop: ScreenWidth(0.04),
    alignItems: 'center',
  },
  linearGradient: {
    width: ScreenWidth(0.86),
    borderRadius: 50,
    paddingVertical: 12,
  },
  btnSignIn: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  emptyViewSignInt: {
    width: 30,
  },
  textSignIn: {
    color: Colors.white,
    fontSize: 22,
    fontWeight: 'bold',
  },
  iconNext: {
    fontSize: 18,
    color: Colors.white,
    width: 30,
  },
  btnForgotPassword: {
    marginTop: 12,
  },
  textBtnForgotPassword: {
    color: Colors.white,
    fontSize: 16,
  },

  anotherActions: {
    marginTop:'20%',
    bottom: 0,
    marginBottom: ScreenWidth(0.18),
    flexDirection: 'row',
  },
  anotherAction: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginHorizontal: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconAnother: {
    color: Colors.white,
    fontSize: 16,
  },

  navSignUpView: {
    marginTop:'-15%',
    bottom: 0,
    marginBottom: ScreenWidth(0.1),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  btnSignUp: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSignUp: {
    color: Colors.white,
    fontSize: 16,
  },
});

export default styles;
