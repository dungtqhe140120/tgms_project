import { StyleSheet } from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const { ScreenWidth } = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  shadow: {
    shadowColor: 'black',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
  },
  containerView: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  textProcess: {
    fontSize: 18,
    fontWeight: '600',
    marginLeft: 2,
  },
  textTraineeProcess: {
    fontSize: 13,
    fontWeight: '600',
  },
  containerText: {
    flexDirection: 'row',
    marginTop: 30,
  },
  containerTraineeText: {
    flexDirection: 'row',
    marginTop: 18,
    justifyContent: 'space-around',
    width: '100%',
  },
  traineeInfoView: {
    marginTop: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  topText: {
    backgroundColor: 'white',
    width: '95%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 20,
    alignItems: 'center',
    height: '45%',
    marginBottom: '5%',
  },
  bottomText: {
    backgroundColor: 'white',
    width: '95%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 20,
    alignItems: 'center',
    height: '45%',
    marginTop: '5%',
  },
  leftText: {
    flex: 1,
    width: '90%',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    borderRadius: 20,
    elevation: 5,
    alignItems: 'center',
  },
  calendar: {
    fontSize: 20,
    fontWeight: '600',
    marginLeft: 10,
  },
  avatarView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  avatar: {
    height: 80,
    width: 80,
    borderRadius: 40,
    backgroundColor: 'blue',
    overflow: 'hidden',
  },
  avatarImage: {
    width: '100%',
    height: '100%',
  },
  headerContain: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  nameHeaderText: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  activityView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  activityText: {
    fontSize: 20,
    marginLeft: 7,
  },
  dayContainer: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  item: {
    height: 150,
    borderRadius: 20,
    marginHorizontal: 20,
    width: 300,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'black',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    marginVertical: 20,
    flexDirection: 'row',
  },
  itemContain: {
    width: '50%',
    height: '100%',
    padding: 20,
  },
  text: {
    color: 'white',
    fontSize: 18,
    fontWeight: '500',
    marginTop: 10,
  },
  textTitle: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
  },
  textTime: {
    color: 'white',
    fontSize: 18,
    fontWeight: '500',
  },
  footerContainer: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: 'white',
    width: '80%',
    height: 100,
    marginVertical: 20,
    borderRadius: 15,
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightCom: {
    width: '100%',
  },
  bottomText2: {
    backgroundColor: 'white',
    width: '100%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 20,
    alignItems: 'center',
    height: '100%',
    marginTop: '25%',
  },
});

export default styles;
