import {Text, View, TouchableOpacity, Image} from 'react-native';
import React, {useRef} from 'react';
import Svg, {
  Path,
  Defs,
  Stop,
  LinearGradient as LinearGradientSVG,
} from 'react-native-svg';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import {showMessage} from 'react-native-flash-message';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FAIcons from 'react-native-vector-icons/FontAwesome';

import styles from './styles';
import Images from '../../common/Images';
import InputCustom from '../../components/InputCustom';
import HideKeyboard from '../../components/HideKeyboard';
import {HeaderLeftNorMal} from '../../components/HeaderLeft';
import {updatePassword} from '../../actions';

const ChangePasswordScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const refPhoneOrEmail = useRef(null);
  const refPassword = useRef(null);
  const refConfirmPassword = useRef(null);

  const handleSend = () => {
    if (refPassword.current.value === refConfirmPassword.current.value) {
      dispatch(
        updatePassword(
          {
            currentPassword: refPhoneOrEmail.current.value,
            newPassword: refPassword.current.value,
          },
          {
            onSuccess: () => {
              navigation.goBack();
            },
          },
        ),
      );
    } else {
      showMessage({
        message: 'Password and Confirm Password is not compar',
        type: 'warning',
        duration: 2000,
      });
    }
  };

  return (
    <HideKeyboard>
      <View style={styles.container}>
        <View style={styles.headerAction}>
          <HeaderLeftNorMal />
        </View>

        <View style={styles.contentView}>
          <View style={styles.imageLogoView}>
            <Image source={Images.logo} style={styles.imageLogo} />
          </View>
          <InputCustom
            ref={refPhoneOrEmail}
            placeholder="Current Password"
            password
            iconComponent={<FAIcons name="user-o" style={styles.inputIcon} />}
            maxLength={50}
          />
          <InputCustom
            ref={refPassword}
            placeholder="Password"
            password
            iconComponent={
              <MaterialIcons name="lock-outline" style={styles.inputIcon} />
            }
            maxLength={50}
          />
          <InputCustom
            ref={refConfirmPassword}
            placeholder="Confirm Password"
            password
            iconComponent={
              <MaterialIcons name="lock-outline" style={styles.inputIcon} />
            }
            maxLength={50}
          />

          <TouchableOpacity style={styles.confirmButton} onPress={handleSend}>
            <Text style={styles.confirmText}>Send</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.forgotPasswordButton}
            onPress={() => navigation.navigate('ForgotPasswordScreen')}>
            <Text style={styles.forgotPasswordText}>Forgot password</Text>
          </TouchableOpacity>
        </View>

        <Svg
          height="150%"
          width="150%"
          viewBox="0 0 20 20"
          style={styles.svgView}>
          <Defs>
            <LinearGradientSVG id="path" x1="0" y1="0" x2="1" y2="1">
              <Stop offset="0" stopColor="#8736e7" stopOpacity="1" />
              <Stop offset="0.5" stopColor="#b732a0" stopOpacity="1" />
              <Stop offset="1" stopColor="#e52b54" stopOpacity="1" />
            </LinearGradientSVG>
          </Defs>
          <Path d="M 0 8 C 5 11 12 5 16 9 V 13 H 0" fill="url(#path)" />
        </Svg>
      </View>
    </HideKeyboard>
  );
};

export default ChangePasswordScreen;
