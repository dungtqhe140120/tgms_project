import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  wrapStar: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
  stars: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
    alignSelf: 'baseline',
    marginHorizontal: 20,
    paddingVertical: 10,
    borderColor: Colors.blue2,
    borderRadius: 4,
    borderWidth: 2,
    borderStyle: 'dashed',
    paddingHorizontal: 10,
  },
  starImage: {
    width: 30,
    height: 30,
  },
  star: {
    marginHorizontal: 7,
  },

  boxShadow: {
    shadowColor: '#000',
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 3,
    marginHorizontal: 10,
    marginTop: 20,
  },
  reportItem: {
    marginTop: 25,
    marginHorizontal: 10,
  },

  headerItem: {
    backgroundColor: Colors.lightgrey,
    flexDirection: 'row',
    alignItems: 'center',
  },
  userImgItem: {
    height: 45,
    width: 45,
  },
  userNameItem: {
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 10,
  },

  starsItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'baseline',
    paddingVertical: 10,
    marginLeft:'12%',
  },
  starItem: {
    marginHorizontal: 1,
  },
  starImageItem: {
    width: 12,
    height: 12,
  },
  createAtText: {
    fontSize: 13,
    opacity: 0.8,
    marginLeft:'12%',
  },

  starsItem2: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  starItem2: {
    marginHorizontal: 5,
  },
  starImageItem2: {
    width: 30,
    height: 30,
  },

  contentItem: {
    fontSize: 16,
    marginTop: 7,
    marginLeft:'12%',
  },

  emptyViewRating: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyTextRating: {
    fontSize: 24,
  },
});

export default styles;
