import React from 'react';
import {SafeAreaView, Text, View, ScrollView, Image} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useNavigation, useRoute} from '@react-navigation/native';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';
import Images from '../../common/Images';
import HeaderActionScreen from '../../components/HeaderActionScreen';

const DetailRoomExerciseScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();

  const data = route.params?.data || '';
  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        goBack={() => navigation.goBack()}
        sizeIconBack={30}
        title="News"
        colorText="black"
      />
      <ScrollView
        style={styles.containerScrollView}
        showsVerticalScrollIndicator={false}>
        <View style={styles.titleAndTime}>
          <Text style={styles.titleText}>{data?.title}</Text>
        </View>
        <Text style={styles.textShortDescription}>
          {data?.short_Description}
        </Text>
        <View style={styles.imageStep}>
          <Image
            resizeMode="contain"
            style={{ width: '100%', height: '100%' }}
            source={{ uri: data?.image }}
          />
        </View>
        <Text style={styles.stepText}>{data?.content}</Text>
      </ScrollView>
    </SafeAreaView>
  );
};

export default DetailRoomExerciseScreen;
