import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: 10,
  },
  containerScrollView: {
    flex: 1,
  },

  titleAndTime: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  titleText: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 15,
  },

  timeView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  timeIcon: {
    fontSize: 22,
    color: Colors.darkGrey,
  },
  timeText: {
    fontSize: 16,
    color: Colors.darkGrey,
    marginLeft: 2,
  },

  textShortDescription: {
    fontSize: 18,
    marginLeft: 2,
  },

  stepText: {
    fontSize: 22,
    fontWeight: '500',
    marginTop: 20,
  },
  imageStep: {
    width: ScreenWidth(0.86),
    height: ScreenWidth(0.7),
  },
});

export default styles;
