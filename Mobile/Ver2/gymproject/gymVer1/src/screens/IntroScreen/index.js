/* eslint-disable react-hooks/exhaustive-deps */
import {
  Text,
  View,
  ImageBackground,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useEffect} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {useDispatch} from 'react-redux';
import OctIcons from 'react-native-vector-icons/Octicons';

import styles from './styles';
import Images from '../../common/Images';
import Colors from '../../common/Colors';
import {setBarStyle} from '../../utils/StatusBar';
import {finishIntro} from '../../actions';

const IntroScreen = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    setBarStyle('light-content');
  }, []);

  const handleGetStarted = () => {
    dispatch(finishIntro());
  };

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.background} source={Images.bgIntro}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 0, y: 1}}
          colors={Colors.mainGradientButton}
          style={styles.linearGradient}>
          <SafeAreaView style={styles.content}>
            <Image source={Images.logo} style={styles.logo} />
            <Text style={styles.textCenter}>FITNESS</Text>

            <View style={styles.footer}>
              <TouchableOpacity
                style={styles.btnStarted}
                activeOpacity={0.5}
                onPress={handleGetStarted}>
                <Text style={styles.textStarted}>GET STARTED</Text>
                <OctIcons style={styles.iconNext} name="arrow-right" />
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </LinearGradient>
      </ImageBackground>
    </View>
  );
};

export default IntroScreen;
