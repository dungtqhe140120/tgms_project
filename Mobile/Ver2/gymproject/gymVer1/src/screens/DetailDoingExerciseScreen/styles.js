import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: 10,
  },

  listRoomExercises: {
    flex: 1,
    marginTop: 10,
  },

  stepTitleText: {
    fontSize: 24,
    fontWeight: '500',
    marginTop: 20,
  },
  stepText: {
    fontSize: 18,
    marginTop: 20,
  },
  imageStep: {
    width: ScreenWidth(0.86),
    height: ScreenWidth(0.7),
  },
});

export default styles;
