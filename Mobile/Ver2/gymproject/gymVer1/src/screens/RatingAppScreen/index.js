/* eslint-disable react-hooks/exhaustive-deps */
import {Text, View, SafeAreaView, TouchableOpacity} from 'react-native';
import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';

import styles from './styles';
import Images from '../../common/Images';
import {setBarStyle} from '../../utils/StatusBar';
import {finishIntro} from '../../actions';

const RatingAppScreen = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    setBarStyle('dark-content');
  }, []);

  const handleGetStarted = () => {
    dispatch(finishIntro());
  };

  return (
    <SafeAreaView style={styles.container}>
      <Text>Rating</Text>
    </SafeAreaView>
  );
};

export default RatingAppScreen;
