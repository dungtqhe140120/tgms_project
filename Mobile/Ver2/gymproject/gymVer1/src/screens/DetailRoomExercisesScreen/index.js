import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import AntDIcons from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import Images from '../../common/Images';
import Colors from '../../common/Colors';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import { getDoingExer, getDoingExercise, deleteDetailExcercise, getDetailExercise } from '../../actions';

const DetailRoomExercisesScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();

  const title = route.params?.title || '';
  const parentId = route.params?.parentId || '';
  const content = route.params?.data?.content || [];
  const showButtonAdd = route.params?.showButtonAdd || false;
  const showButtonSub = route.params?.showButtonSub || false;

  const goToTypeOfExercises = (item) => {
    const from = route.params?.from || '';
    if (from === 'MyExercisesScreen') {
      console.log("ITEM: ", item.id)
      dispatch(getDoingExer(item.id, {
        onSuccess: (data) => {
          navigation.navigate('AddDoingExercisesScreen', {
            id: item.id,
            data,
          });
        },
        onFailure: () => { },
      }));
    } else {
      dispatch(
        getDoingExercise(item.id, {
          onSuccess: (data) => {
            navigation.navigate('DetailDoingExerciseScreen', {
              title: item.title,
              data,
            });
          },
          onFailure: () => {
            navigation.navigate('DetailDoingExerciseScreen', {
              title: item.title,
              data: {},
            });
          },
        }),
      );
    }
  };


  const handleRemoveDetailExercise = (id) => {
    dispatch(
      deleteDetailExcercise(
        id,
        {
          onSuccess: () => {
            console.log("DELETE SUCCESS")
            dispatch(
              getDetailExercise(parentId, {
                onSuccess: (data) => {
                  navigation.navigate('DetailRoomExercisesScreen', {
                    data,
                    showButtonAdd: true,
                    showButtonSub: true,
                    title: title,
                    parentId: parentId,
                    from: 'MyExercisesScreen',
                  });
                },
                onFailure: () => { },
              }),
            );
          },
          onFailure: () => {
            console.log("DELETE FAIL")
          },
        }
      ),
    );
  };

  const addNewDoingExercises = () => {
    const parentId = route.params?.parentId || 0;
    navigation.navigate('AddDetailExercisesScreen', { parentId, title });
  };

  const renderItemDetailExercises = ({ item, index }) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={[styles.item, styles.bgGray]} // index % 2 === 1 &&
        onPress={() => goToTypeOfExercises(item)}>
        {item?.image?.length > 0 ? (
          <FastImage style={styles.itemImage} source={{ uri: item.image }} />
        ) : (
          <FastImage style={styles.itemImage} source={Images.chestMuscles} />
        )}
        <Text style={styles.itemText}>{item.title}</Text>

        {showButtonSub && (
          <TouchableOpacity
            style={[styles.minusButton]}
            onPress={() => handleRemoveDetailExercise(item.id)}>
            <AntDIcons name="minus" style={styles.itemIcon} />
          </TouchableOpacity>
        )
        }

      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        goBack={() => navigation.goBack()}
        sizeIconBack={30}
        title={title}
      />
      <View style={styles.imageHeaderView}>
        <FastImage
          style={styles.imageHeader}
          source={Images.headerDetailExercises}
        />
      </View>

      <FlatList
        data={content}
        keyExtractor={(item) => `${item.id}`}
        style={styles.listRoomExercises}
        renderItem={renderItemDetailExercises}
      />

      {showButtonAdd && (
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.containerPlusButton}
          onPress={addNewDoingExercises}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0, y: 1 }}
            colors={Colors.mainGradientButton}
            style={styles.linearGradient}>
            <View style={styles.addTextView}>
              <Text style={styles.addText}>Add</Text>
            </View>
            <View style={styles.plusButton}>
              <AntDIcons name="plus" style={styles.itemIcon} />
            </View>
          </LinearGradient>
        </TouchableOpacity>
      )}
    </SafeAreaView>
  );
};

export default DetailRoomExercisesScreen;
