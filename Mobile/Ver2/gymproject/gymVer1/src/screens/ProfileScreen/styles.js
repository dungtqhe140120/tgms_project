import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth, ScreenHeight} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.lineColor,
  },

  inputContainer: {
    marginTop: 20,
    paddingHorizontal: 0,
    marginHorizontal: 20,
    paddingVertical: Constants.isIOS ? 12 : 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 0,
    backgroundColor: Colors.lineColor,
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 2,
  },

  inputIcon: {
    fontSize: 24,
    color: Colors.darkGrey,
  },

  actionView: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  containerPlusButton: {
    marginTop: ScreenHeight(0.04),
    justifyContent: 'center',
    alignItems: 'center',
  },
  addText: {
    fontSize: 20,
    color: Colors.white,
  },
  linearGradient: {
    borderRadius: 8,
    padding: 8,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
