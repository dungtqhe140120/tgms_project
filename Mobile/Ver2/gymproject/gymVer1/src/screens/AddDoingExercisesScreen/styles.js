import { StyleSheet } from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const { ScreenWidth, ScreenHeight } = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  headerAction: {
    flexDirection: 'row',
    marginTop: 10,
  },

  chooseTypeButton: {
    marginLeft: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  chooseTypeText: {
    fontSize: 24,
  },

  wrapContent: {
    backgroundColor: Colors.rgba1,
    marginTop: 25,
    marginHorizontal: 20,
  },
  titleInput: {
    backgroundColor: Colors.lineColor,
    margin: 10,
    paddingHorizontal: 15,
  },

  containerPlusButton: {
    marginTop: '15%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  plusButton: {
    width: 150,
    height: 70,
    borderRadius: 25,
    backgroundColor: Colors.lightBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },

  addDoingIcon: {
    fontSize: 24,
    color: Colors.white,
  },

  btnSaveText: {
    fontSize: 24,
    color: Colors.white,
  },

  addImageButton: {
    width: '100%',
    height: ScreenHeight(0.3),
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageSelected: {
    width: '96%',
    height: ScreenHeight(0.25),
    marginTop: 10,
  },
  addImageIcon: {
    fontSize: 40,
    color: Colors.lightGrey,
  },

  itemStep: {
    marginTop: 10,
    marginBottom: 15,
    marginHorizontal: 20,
  },
  stepText: {
    fontSize: 24,
    fontWeight: '500',
  },
  imageStep: {
    marginTop: 10,
    width: '100%',
    height: ScreenWidth(0.7),
  },
  textAreaContainer: {
    borderColor: 'red',
    borderWidth: 1,
    margin: 10
  },
  textArea: {
    fontSize:20,
    height: 400,
    justifyContent: "flex-start",
    textAlignVertical: 'top'
  }
});

export default styles;
