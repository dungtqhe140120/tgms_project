import React, { useState } from 'react';
import {
  SafeAreaView,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Pressable,
  ScrollView,
  Alert
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import DatePicker from 'react-native-date-picker'
import moment from 'moment';
import { CheckBox } from 'react-native-elements'
import { showMessage } from 'react-native-flash-message';

import styles from './styles';
import Images from '../../common/Images';
import { getDetailExercise, getListExercises, getListMyExercises, confirmStatus } from '../../actions';
import HeaderActionScreen from '../../components/HeaderActionScreen';

const ExercisesOfTraineeScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const route = useRoute();
  const traineeReducer = useSelector((state) => state.trainee);
  const [date, setDate] = useState(new Date())
  const [open, setOpen] = useState(false)
  const authReducer = useSelector((state) => state.auth);
  const traineeId = route.params?.traineeId || 0;

  const { exercisesOfTrainee } = traineeReducer;

  const headerComponent = () => {
    return (
      <View style={styles.headerContainer}>
        {/* <View style={styles.sttTitleTable}>
          <Text style={styles.textTitleTable}>Time</Text>
        </View> */}
        <View style={styles.nameTitleTable}>
          <Text style={styles.textTitleTable}>Date</Text>
        </View>
        <View style={styles.exercise}>
          <Text style={styles.textTitleTable}>Exercise</Text>
        </View>
        <View style={styles.attended}>
          <Text style={styles.textTitleTable}>Status</Text>
        </View>
      </View>
    );
  };

  const renderItemDetailExercises = ({ item, index }) => {
    return (
      <View style={styles.item}>
        {/* <View style={styles.sttView}>
          <Text>{item.time}</Text>
        </View> */}
        <View style={styles.nameTitleTable}>
          <Text>{moment(item.calendarDate).format('DD-MM-YYYY')}</Text>
        </View>
        <TouchableOpacity style={styles.exercise} onPress={() => goToTypeOfExercises(item)}>
          <View style={styles.exercise}>
            <Text style={styles.itemTextExTitle} >{item.categoryName}</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.attended}>
          <CheckBox
            center
            checked={checkCheckedCheckbox(item) == true}
            onPress={() => alertBeforConfirm(item)}
          />
        </View>
      </View>
    );
  };
  const checkCheckedCheckbox = (item) => {
    if (authReducer.role[0].name === 'ROLE_PT' && item.ptConfirm == 1) {
      return true;
    }
    if (authReducer.role[0].name === 'ROLE_TRAINEE' && item.traineeConfirm == 1) {
      return true;
    }
    return false;
  }
  const alertBeforConfirm = (item) => {
    Alert.alert(
      "Are your sure?",
      "Are you sure you want to change the status?",
      [
        // The "Yes" button
        {
          text: "Yes",
          onPress: () => {
            onPressCheckbook(item)
          },
        },
        // The "No" button
        // Does nothing but dismiss the dialog when tapped
        {
          text: "No",
        },
      ]
    );
  }
  const onPressCheckbook = (item) => {
    if (checkCheckedCheckbox(item) == false) {
      let payload = {};
      if (authReducer.role[0].name === 'ROLE_TRAINEE') {
        payload = {
          calendarDate: item.calendarDate,
          traineeConfirm: 1,
        }
      }
      if (authReducer.role[0].name === 'ROLE_PT') {
        payload = {
          calendarDate: item.calendarDate,
          ptConfirm: 1,
        }
      }
      dispatch(confirmStatus(payload, item.id, traineeId));
    }else{
      showMessage({
        message: "Unable to edit status",
        type: 'warning',
        duration: 2000,
      });
    }
  }

  const goToTypeOfExercises = (item) => {
    dispatch(
      getDetailExercise(item.categoryId, {
        onSuccess: (data) => {
          navigation.navigate('DetailRoomExercisesScreen', {
            data,
            showButtonAdd: false,
            showButtonSub: false,
            title: item.categoryName,
            parentId: item.categoryId
          });
        },
        onFailure: () => { },
      }),
    );
  };

  const renderItemSeparatorComponent = () => {
    return <View style={styles.itemSeparatorComponent} />;
  };

  const add = (dateChoose) => {
    const calendarDate = moment(dateChoose).format('YYYY-MM-DD');
    console.log('calendarDate', calendarDate)
    dispatch(getListMyExercises());
    navigation.navigate('RoomExercisesScreen', {
      assign: true,
      traineeId,
      calendarDate,
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        goBack={() => navigation.goBack()}
        sizeIconBack={30}
        title=""
      />
      <View style={styles.imageHeaderView}>
        <FastImage
          style={styles.imageHeader}
          source={Images.headerDetailExercises}
        />
      </View>

      <View style={styles.traineeInfoView}>
        {/* <View style={styles.emptyInfoView} /> */}
        <View style={styles.nameInfoView}>
          <Text>
            {route.params?.traineeId != null
              && <Text style={styles.nameInfoText}>
                Name: {exercisesOfTrainee.nameTrainee}
              </Text>
            }
          </Text>
        </View>
        <View style={styles.bMIInfoView}>
          <Text>
            {route.params?.traineeId != null &&
              <Text style={styles.bMIInfoText}>
                BMI:{exercisesOfTrainee.bmi}
              </Text>
            }
          </Text>
        </View>
      </View>
      {headerComponent()}
      {exercisesOfTrainee?.nameTrainee &&

        <FlatList
          // pagingEnabled={true}
          // horizontal={true}
          // vertical
          data={exercisesOfTrainee.gymCalendarDTOS.content}
          style={styles.listRoomExercises}
          // ListHeaderComponent={headerComponent}
          renderItem={renderItemDetailExercises}
          ItemSeparatorComponent={renderItemSeparatorComponent}
        />

      }
      {route.params?.traineeId != null &&
        <View style={styles.containerPlusButton}>
          <Pressable style={styles.plusButton} onPress={() => setOpen(true)}>
            <Text style={styles.addDoingIcon}>+</Text>
          </Pressable>
          <DatePicker
            mode="date"
            modal
            open={open}
            date={date}
            onConfirm={(date) => {
              setOpen(false)
              // setDate(date)
              add(date)
            }}
            onCancel={() => {
              setOpen(false)
            }}
          />
        </View>
      }

    </SafeAreaView>
  );
};

export default ExercisesOfTraineeScreen;
