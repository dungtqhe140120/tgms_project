import {StyleSheet} from 'react-native';
import Colors from '../common/Colors';

const styles = StyleSheet.create({
  tabBar: {
    height: 70,
    bottom: 16,
    marginRight: 16,
    marginLeft: 16,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    elevation: 13,
  },
  circle: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.blue2,
    borderRadius: 25,
  },
  text: {
    fontSize: 10,
    textAlign: 'center',
    color: Colors.primary,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    borderRadius: 16,
  },

  // drawer
  bgAvatarDrawer: {
    padding: 20,
    marginTop: -5,
  },
  avatarDrawer: {
    height: 60,
    width: 60,
    borderRadius: 30,
    marginBottom: 10,
  },
  textUserDrawer: {
    color: Colors.white,
    fontSize: 18,
    marginVertical: 5,
    textShadowColor: Colors.rgba8,
    textShadowOffset: {
      width: -1,
      height: 1,
    },
    textShadowRadius: 10,
  },
  contentDrawer: {
    flex: 1,
    marginTop: 10,
  },
  drawerIcon: {
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  labelStyles: {
    marginLeft: -20,
    fontSize: 15,
    fontWeight: '500',
  },
});

export default styles;
