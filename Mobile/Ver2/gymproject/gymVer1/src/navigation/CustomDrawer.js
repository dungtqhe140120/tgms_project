import React from 'react';
import { View, Text, ImageBackground, Linking } from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import FastImage from 'react-native-fast-image';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';
import Images from '../common/Images';
import { logout } from '../actions';

const CustomDrawer = (props) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const authReducer = useSelector((state) => state.auth);
  const appReducer = useSelector((state) => state.app);

  const { appInfo } = appReducer;
  const { info } = authReducer;

  const navToPhone = () => {
    if (appInfo?.id) {
      Linking.openURL(`tel: ${appInfo.phoneNumber}`);
    }
  };

  const handleLogout = () => {
    dispatch(logout());
  };

  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        <ImageBackground
          source={Images.bgIntro}
          style={styles.bgAvatarDrawer}
          blurRadius={5}>
          {info?.avatar ? (
            <FastImage
              source={{ uri: info.avatar }}
              style={styles.avatarDrawer}
            />
          ) : (
            <FastImage
              source={Images.defaultAvatar}
              style={styles.avatarDrawer}
            />
          )}
          <Text style={styles.textUserDrawer}>{info?.email}</Text>
        </ImageBackground>
        <View style={styles.contentDrawer}>
          <DrawerItemList {...props} />
          <DrawerItem
            label="Support"
            labelStyle={styles.labelStyles}
            icon={({ color }) => (
              <View style={styles.drawerIcon}>
                <MCIcons
                  name="account-question-outline"
                  size={22}
                  color={color}
                />
              </View>
            )}
            onPress={navToPhone}
          />
          <DrawerItem
            label="Logout"
            labelStyle={styles.labelStyles}
            icon={({ color }) => (
              <View style={styles.drawerIcon}>
                <MCIcons name="logout" size={22} color={color} />
              </View>
            )}
            onPress={handleLogout}
          />
        </View>
      </DrawerContentScrollView>
    </View>
  );
};

export default CustomDrawer;
