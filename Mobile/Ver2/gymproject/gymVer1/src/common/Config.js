import Images from './Images';

export const intro = [
  // {
  //   key: 1,
  //   title: 'New Home',
  //   subTitle: 'Make renting more enjoyable',
  //   image: Images.intro_image_1,
  //   backgroundColor: '#59b2ab',
  // },
  // {
  //   key: 2,
  //   title: 'Rich community',
  //   subTitle: 'Get rid of the long loneliness',
  //   image: Images.intro_image_2,
  //   backgroundColor: '#febe29',
  // },
  // {
  //   key: 3,
  //   title: 'New features',
  //   subTitle: 'Renting at your fingertips',
  //   image: Images.intro_image_3,
  //   backgroundColor: '#22bcb5',
  // },
];

export const listRoomExercises = [
  {
    id: 0,
    name: 'Chest',
    image: Images.chestMuscles,
  },
  {
    id: 1,
    name: 'Back',
    image: Images.backMuscles,
  },
  {
    id: 2,
    name: 'Shoulders',
    image: Images.shouldersMuscles,
  },
  {
    id: 3,
    name: 'Biceps',
    image: Images.bicepsMuscles,
  },
  {
    id: 4,
    name: 'Triceps',
    image: Images.tricepsMuscles,
  },
  {
    id: 5,
    name: 'Abdominals',
    image: Images.abdominalsMuscles,
  },
  {
    id: 6,
    name: 'Legs',
    image: Images.legsMuscles,
  },
  {
    id: 7,
    name: 'Cardio',
    image: Images.cardioMuscles,
  },
  {
    id: 8,
    name: 'Personal',
    image: Images.personalMuscles,
  },
];

export const listDetailRoomExercises = [
  {
    id: 0,
    name: 'Barbell Flat Bench Press',
    image: Images.bFBPExercises,
  },
  {
    id: 1,
    name: 'Barbell Incline Chest Press',
    image: Images.bICPExercises,
  },
  {
    id: 2,
    name: 'Cable Chest Fly',
    image: Images.cCFExercises,
  },
  {
    id: 3,
    name: 'Chest Dips',
    image: Images.cDExercises,
  },
  {
    id: 4,
    name: 'Dumbbell Bench Press',
    image: Images.dBPExercises,
  },
  {
    id: 5,
    name: 'Dumbbell Flyes',
    image: Images.dFExercises,
  },
];
