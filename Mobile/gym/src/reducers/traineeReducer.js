import types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  totalTrainers: 0,
  trainers: [],
  exercisesOfTrainee: [],
};

const traineeReducer = (state = initialState, action) => {
  const {type, data} = action;

  switch (type) {
    case types.GETTING_TRAINERS_BY_PT_ID:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_TRAINERS_BY_PT_ID_SUCCESS:
      return {
        ...state,
        isFetching: false,
        totalTrainers: data.length,
        trainers: data,
      };
    case types.GET_TRAINERS_BY_PT_ID_FAILURE:
      return {
        ...state,
        isFetching: false,
        totalTrainers: 0,
        trainers: [],
      };

    case types.GETTING_EXERCISES_OF_TRAINEE:
      return {
        ...state,
        isFetching: true,
        exercisesOfTrainee: [],
      };
    case types.GET_EXERCISES_OF_TRAINEE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        exercisesOfTrainee: data,
      };
    case types.GET_EXERCISES_OF_TRAINEE_FAILURE:
      return {
        ...state,
        isFetching: false,
        exercisesOfTrainee: [],
      };

    case types.CLEAR_FETCHING:
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
};

export default traineeReducer;
