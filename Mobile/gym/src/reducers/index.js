// gkc_hash_code : 01EG2WW8VW4FBC0TPYY35WMMGG
import {persistCombineReducers} from 'redux-persist';
import FilesystemStorage from 'redux-persist-filesystem-storage';

import appReducer from './appReducer';
import authTokenReducer from './authTokenReducer';
import exercisesReducer from './exercisesReducer';
import calendarReducer from './calendarReducer';
import traineeReducer from './traineeReducer';
import homeReducer from './homeReducer';
import rateReducer from './rateReducer';

const config = {
  key: 'root',
  storage: FilesystemStorage,
  // blacklist: [], // no set redux again
  timeout: 0, // 60000 = 1 minute timeout
};

const reducers = persistCombineReducers(config, {
  app: appReducer,
  auth: authTokenReducer,
  exercises: exercisesReducer,
  calendar: calendarReducer,
  trainee: traineeReducer,
  home: homeReducer,
  rate: rateReducer,
});

export default reducers;
