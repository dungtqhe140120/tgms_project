import types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  totalTrainers: 0,
  trainers: [],
};

const calendarReducer = (state = initialState, action) => {
  const {type, data} = action;

  switch (type) {
    case types.GETTING_TRAINERS_BY_DATE:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_TRAINERS_BY_DATE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        totalTrainers: data.total,
        trainers: data.trainers,
      };
    case types.GET_TRAINERS_BY_DATE_FAILURE:
      return {
        ...state,
        isFetching: false,
        totalTrainers: 0,
        trainers: [],
      };

    case types.BOOKING_PT:
      return {
        ...state,
        isFetching: true,
      };
    case types.BOOK_PT_SUCCESS:
      return {
        ...state,
        isFetching: false,
      };
    case types.BOOK_PT_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case types.CLEAR_FETCHING:
      return {
        ...state,
        isFetching: false,
        totalTrainers: 0,
        trainers: [],
      };

    default:
      return state;
  }
};

export default calendarReducer;
