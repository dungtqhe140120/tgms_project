import types from '../actions/actionTypes';

const initialState = {
  isFetching: false,
  token: null,
  role: null,
  loginRole: null,
  info: {},
};

const authTokenReducer = (state = initialState, action) => {
  const {type, data} = action;

  switch (type) {
    case types.RESET_TOKEN:
      return {
        ...state,
        token: null,
      };

    case types.LOGIN:
      return {
        ...state,
        isFetching: true,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        isFetching: false,
        token: data.id_token,
        role: data.role,
        loginRole: data.login,
      };
    case types.LOGIN_FAILURE:
      return {
        ...state,
        isFetching: false,
        token: null,
        role: null,
        loginRole: null,
      };

    case types.GETTING_PT_INFO:
      return {
        ...state,
        isFetching: true,
      };
    case types.GET_PT_INFO_SUCCESS:
      return {
        ...state,
        isFetching: false,
        info: data,
      };
    case types.GET_PT_INFO_FAILURE:
      return {
        ...state,
        isFetching: false,
        info: {},
      };

    case types.UPDATING_INFO:
      return {
        ...state,
        isFetching: true,
      };
    case types.UPDATE_INFO_SUCCESS:
      return {
        ...state,
        isFetching: false,
        info: data,
      };
    case types.UPDATE_INFO_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case types.LOGOUT:
      return {
        ...state,
        isFetching: false,
        token: null,
        role: null,
        loginRole: null,
        info: {},
      };

    case types.CLEAR_FETCHING:
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
};

export default authTokenReducer;
