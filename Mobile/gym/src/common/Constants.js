import {Dimensions, Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';

const {width, height} = Dimensions.get('window');

const Constants = {
  appStoreId: '',
  bundleId: 'org.reactjs.native.example.GymApp',
  appsflyerKey: '',
  firebaseConfig: {},
  appFacebookId: '',
  useReactotron: false,
  deviceId: DeviceInfo.getUniqueId(),
  Language: 'en', // ar, en. Default to set redux. Only use first time
  VND: '₫',
  fontFamily: 'Lora-Regular',
  fontFamilyBold: 'Lora-Bold',
  fontHeader: 'verdana',
  fontHeaderAndroid: 'verdana',
  isoDateFormat: 'YYYY-MM-DD',
  displayDateFormat: 'DD-MM-YYYY',
  allCategorySlug: 'show-all-category',
  isIOS: Platform.OS === 'ios',
  Filter: {
    maxPrice: 1000000,
    minPrice: 0,
    defaultPrice: 50000,
    priceStep: 10000,
  },
  AsyncCode: {
    Intro: 'async.intro',
  },
  EmitCode: {
    Toast: 'toast',
  },
  Dimension: {
    ScreenWidth(percent = 1) {
      return Dimensions.get('window').width * percent;
    },
    ScreenHeight(percent = 1) {
      return Dimensions.get('window').height * percent;
    },
  },
  Window: {
    width,
    height,
    headerHeight: (65 * height) / 100,
    headerBannerAndroid: (55 * height) / 100,
    profileHeight: (45 * height) / 100,
  },
  PostImage: {
    small: 'small',
    medium: 'medium',
    medium_large: 'medium_large',
    large: 'large',
  },
  tagIdBanner: 273, // cat ID for Sticky Products
  pagingLimit: 16,
  fontText: {
    size: 16,
  },
  productAttributeColor: 'color',
};

export default Constants;
