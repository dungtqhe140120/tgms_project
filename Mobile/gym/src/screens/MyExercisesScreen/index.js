import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import AntDIcons from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import Colors from '../../common/Colors';
import Images from '../../common/Images';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import { getDetailExercise, removeExercise , getListMyExercises} from '../../actions';

const MyExercisesScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const exercisesReducer = useSelector((state) => state.exercises);

  const { myExercises } = exercisesReducer;

  const goToTypeOfExercises = (item) => {
    dispatch(
      getDetailExercise(item.id, {
        onSuccess: (data) => {
          navigation.navigate('DetailRoomExercisesScreen', {
            data,
            showButtonAdd: true,
            showButtonSub: true,
            title: item.title,
            parentId: item.id,
            from: 'MyExercisesScreen',
          });
        },
        onFailure: () => { },
      }),
    );
  };

  const handleRemoveExercise = (id) => {
    dispatch(
      removeExercise(
        id,
          {
          onSuccess: () => {
            dispatch(getListMyExercises());
          },
          onFailure: () => {},
        }
      ),
    );
  };

  const renderItemExercises = ({ item }) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={[styles.item, styles.bgGray]}
        onPress={() => goToTypeOfExercises(item)}>
        <Text style={styles.itemText}>{item?.title || item?.name}</Text>
        {item?.picture?.length > 5 || item?.image?.length > 5 ? (
          item?.image?.length > 5 ? (
            <FastImage style={styles.itemImage} source={{ uri: item.image }} />
          ) : (
            <FastImage style={styles.itemImage} source={Images.chestMuscles} />
          )
        ) : (
          <FastImage style={styles.itemImage} source={Images.chestMuscles} />
        )}
        <TouchableOpacity
          style={[styles.plusButton, styles.minusButton]}
          onPress={() => handleRemoveExercise(item.id)}>
          <AntDIcons name="minus" style={styles.itemIcon} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  const addNewExercises = () => {
    navigation.navigate('AddMyExercisesScreen');
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        title="Exercises"
        goBack={() => navigation.goBack()}
        sizeIconBack={28}
      />

      <FlatList
        data={myExercises}
        keyExtractor={(item) => `${item.id}`}
        style={styles.listRoomExercises}
        ListFooterComponent={<View style={styles.emptyItemView} />}
        renderItem={renderItemExercises}
      />

      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.containerPlusButton}
        onPress={addNewExercises}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          colors={Colors.mainGradientButton}
          style={styles.linearGradient}>
          <View style={styles.addTextView}>
            <Text style={styles.addText}>Add</Text>
          </View>
          <View style={styles.plusButton}>
            <AntDIcons name="plus" style={styles.itemIcon} />
          </View>
        </LinearGradient>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default MyExercisesScreen;
