import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth, ScreenHeight} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  headerAction: {
    flexDirection: 'row',
    marginTop: 10,
  },

  wrapContent: {
    alignItems: 'center',
  },

  mt: {
    marginTop: ScreenWidth(0.07),
  },
  labelText: {
    fontSize: 30,
  },
  searchInput: {
    backgroundColor: Colors.DirtyBackground,
    width: ScreenWidth(0.8),
    marginTop: 10,
    borderRadius: 30,
    paddingHorizontal: 20,
    textAlign: 'center',
  },
  inputContainer: {
    marginTop: 20,
    paddingHorizontal: 10,
    marginHorizontal: 20,
    paddingVertical: Constants.isIOS ? 12 : 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 0,
    backgroundColor: Colors.lineColor,
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 2,
    width: '90%',
  },

  linearGradient: {
    borderRadius: 8,
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerPlusButton: {
    marginTop: ScreenHeight(0.15),
    width: ScreenWidth(0.3),
  },
  addText: {
    fontSize: 24,
    color: Colors.white,
  },

  addImageButton: {
    width: ScreenWidth(0.94),
    height: ScreenHeight(0.22),
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageSelected: {
    width: ScreenWidth(0.94),
    height: ScreenHeight(0.3),
    marginTop: 70,
  },
  addImageIcon: {
    fontSize: 40,
    color: Colors.lightGrey,
  },
});

export default styles;
