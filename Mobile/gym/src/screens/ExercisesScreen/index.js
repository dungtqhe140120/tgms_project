import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import Images from '../../common/Images';
import {getListExercises, getListMyExercises, getExercisesByTrainerId} from '../../actions';
import AsyncStorageService from '../../api/AsyncStorageService';

const RoomExercisesScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const authReducer = useSelector((state) => state.auth);

  const {info, role} = authReducer;

  const goToRoomExercisesScreen = () => {
    dispatch(getListExercises());
    navigation.navigate('RoomExercisesScreen');
  };
  const goToMyExercisesScreen = async () => {
    dispatch(getListMyExercises());
    navigation.navigate('MyExercisesScreen');
  };
  const goToExercisesWithPTScreen = async () => {
    console.log("INFO: ",info)
    dispatch(getExercisesByTrainerId(info.id));
    navigation.navigate('ExercisesOfTraineeScreen', {
      traineeId: null,
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        contentContainerStyle={styles.scrollContainer}
        showsVerticalScrollIndicator={false}>
        <FastImage source={Images.logo} style={styles.imageLogo} />
        {/* CENTER EXCERCISE */}
        <TouchableOpacity
          style={styles.exercisesView}
          activeOpacity={0.7}
          onPress={goToRoomExercisesScreen}>
          <FastImage
            source={Images.bgExercisesAtGym}
            style={styles.exercisesImage}>
            <Text style={styles.exercisesText}>
              Center Exercise
            </Text>
            <View style={styles.layout} />
          </FastImage>
        </TouchableOpacity>

        {/* PT, TRAINEE */}
        {role?.length > 0 && role[0].name === 'ROLE_TRAINEE' ? (
          <TouchableOpacity
            style={styles.exercisesView}
            activeOpacity={0.7}
            onPress={goToExercisesWithPTScreen}>
            <FastImage
              source={Images.bgExercisesAtGymWithPT}
              style={styles.exercisesImage}>
              <Text style={styles.exercisesText}>Exercises with PT</Text>
              <View style={styles.layout} />
            </FastImage>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.exercisesView}
            activeOpacity={0.7}
            onPress={goToMyExercisesScreen}>
            <FastImage
              source={Images.bgExercisesAtHome}
              style={styles.exercisesImage}>
              <Text style={styles.exercisesText}>
                My Excercise
              </Text>
              <View style={styles.layout} />
            </FastImage>
          </TouchableOpacity>
        )}
        <View style={styles.emptyView} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default RoomExercisesScreen;
