import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import Images from '../../common/Images';
import {getExercisesByTrainerId} from '../../actions';
import HeaderActionScreen from '../../components/HeaderActionScreen';

const TrainersScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const traineeReducer = useSelector((state) => state.trainee);

  const {trainers} = traineeReducer;

  const getExercises = (item) => {
    console.log('getExercises', item);
    dispatch(getExercisesByTrainerId(item.id));
    navigation.navigate('ExercisesOfTraineeScreen', {
      traineeId: item.id,
      traineeName: item.name,
    });
  };

  const headerComponent = () => {
    return (
      <View style={styles.headerContainer}>
        <View style={styles.sttTitleTable}>
          <Text style={styles.textTitleTable}>STT</Text>
        </View>
        <View style={styles.nameTitleTable}>
          <Text style={styles.textTitleTable}>Name</Text>
        </View>
        <View style={styles.phoneTitleTable}>
          <Text style={styles.textTitleTable}>Phone</Text>
        </View>
      </View>
    );
  };

  const renderItemDetailExercises = ({item, index}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.75}
        style={styles.item}
        onPress={() => getExercises(item)}>
        <View style={styles.sttView}>
          <Text style={styles.itemText}>{index + 1}</Text>
        </View>
        <View style={styles.nameView}>
          <Text style={styles.itemText}>{item.name}</Text>
        </View>
        <View style={styles.phoneView}>
          <Text style={styles.itemText}>{item.phone}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderItemSeparatorComponent = () => {
    return <View style={styles.itemSeparatorComponent} />;
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        goBack={() => navigation.goBack()}
        sizeIconBack={30}
        title="List Trainees"
      />
      <View style={styles.imageHeaderView}>
        <FastImage
          style={styles.imageHeader}
          source={Images.headerDetailExercises}
        />
      </View>

      <FlatList
        data={trainers}
        keyExtractor={(item) => `${item.id}`}
        style={styles.listRoomExercises}
        ListHeaderComponent={headerComponent}
        renderItem={renderItemDetailExercises}
        ItemSeparatorComponent={renderItemSeparatorComponent}
      />
    </SafeAreaView>
  );
};

export default TrainersScreen;
