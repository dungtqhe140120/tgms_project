import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  imageHeaderView: {
    width: ScreenWidth(1),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -10,
  },
  imageHeader: {
    width: ScreenWidth(0.77),
    height: ScreenWidth(0.1),
  },

  headerContainer: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: Colors.sectionSeparatorColor,
    paddingVertical: 12,
  },
  sttTitleTable: {
    width: '25%',
    alignItems: 'center',
  },
  nameTitleTable: {
    width: '15%',
    alignItems: 'center',
  },
  phoneTitleTable: {
    width: '65%',
    alignItems: 'center',
  },
  textTitleTable: {
    fontSize: 16,
    fontWeight: 'bold',
  },

  listRoomExercises: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  sttView: {
    width: '25%',
    alignItems: 'center',
  },
  nameView: {
    width: '40%',
    paddingLeft: 10,
  },
  phoneView: {
    width: '40%',
    paddingLeft: 10,
  },

  itemSeparatorComponent: {
    height: 0.5,
    width: '100%',
    backgroundColor: Colors.border,
  },
});

export default styles;
