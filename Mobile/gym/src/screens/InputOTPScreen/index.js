import {Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import Svg, {
  Path,
  Defs,
  Stop,
  LinearGradient as LinearGradientSVG,
} from 'react-native-svg';
import {useNavigation, useRoute} from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FAIcons from 'react-native-vector-icons/FontAwesome';

import styles from './styles';
import Images from '../../common/Images';
import InputCustom from '../../components/InputCustom';
import {HeaderLeftNorMal} from '../../components/HeaderLeft';
import HideKeyboard from '../../components/HideKeyboard';

const InputOTPScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const valueInput = route.params?.phoneOrEmail || '';
  console.warn(route, valueInput);
  // setBarStyle('dark-content');
  return (
    <HideKeyboard>
      <View style={styles.container}>
        <View style={styles.headerAction}>
          <HeaderLeftNorMal />
        </View>
        <View style={styles.contentView}>
          <View style={styles.imageLogoView}>
            <Image source={Images.logo} style={styles.imageLogo} />
          </View>
          <InputCustom
            valueInput={valueInput}
            editable={false}
            placeholder="Phone/Email"
            checkIcon
            iconComponent={<FAIcons name="user-o" style={styles.inputIcon} />}
          />
          <InputCustom placeholder="OTP code" />

          <TouchableOpacity style={styles.confirmButton}>
            <Text style={styles.confirmText}>Send</Text>
          </TouchableOpacity>
        </View>

        <Svg
          height="150%"
          width="150%"
          viewBox="0 0 20 20"
          style={styles.svgView}>
          <Defs>
            <LinearGradientSVG id="path" x1="0" y1="0" x2="1" y2="1">
              <Stop offset="0" stopColor="#8736e7" stopOpacity="1" />
              <Stop offset="0.5" stopColor="#b732a0" stopOpacity="1" />
              <Stop offset="1" stopColor="#e52b54" stopOpacity="1" />
            </LinearGradientSVG>
          </Defs>
          <Path d="M 0 8 C 5 11 12 5 16 9 V 13 H 0" fill="url(#path)" />
        </Svg>
      </View>
    </HideKeyboard>
  );
};

export default InputOTPScreen;
