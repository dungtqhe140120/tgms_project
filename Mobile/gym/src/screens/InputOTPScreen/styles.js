import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;
const {height, width} = Constants.Window;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.sectionSeparatorColor,
  },

  headerAction: {
    position: 'absolute',
    top: ScreenWidth(0.05),
    left: 10,
  },

  contentView: {
    marginTop: ScreenWidth(0.22),
    marginHorizontal: ScreenWidth(0.1),
    zIndex: 2,
  },

  imageLogoView: {
    alignItems: 'center',
  },
  imageLogo: {
    width: ScreenWidth(0.6),
    height: ScreenWidth(0.42),
  },

  inputIcon: {
    fontSize: 18,
    color: Colors.sectionSeparatorColor,
  },

  confirmButton: {
    backgroundColor: '#8736e7',
    marginHorizontal: ScreenWidth(0.16),
    paddingVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40,
    marginTop: 40,
  },
  confirmText: {
    color: Colors.white,
    fontSize: 20,
  },

  forgotPasswordButton: {
    marginTop: 30,
    left: 30,
    alignItems: 'flex-end',
  },
  forgotPasswordText: {
    color: Colors.darkGrey,
    fontSize: 15,
  },

  svgView: {
    position: 'absolute',
    top: height * 0.2,
    left: 0,
    zIndex: 1,
  },
});

export default styles;
