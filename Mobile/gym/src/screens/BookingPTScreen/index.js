import React, {useRef, useState, useEffect} from 'react';
import {SafeAreaView, Text, View, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import SelectDropdown from 'react-native-select-dropdown';
import {showMessage} from 'react-native-flash-message';
import LinearGradient from 'react-native-linear-gradient';
import IIcons from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import Images from '../../common/Images';
import Colors from '../../common/Colors';
import HeaderActionScreen from '../../components/HeaderActionScreen';
import {
  getRegisterDayWithPt,
  getPriceByParams,
  getSlotTimeWithPt,
  getPtByParams,
  bookingPt,
} from '../../actions';
import {tempData} from '../../common/tempData';

const BookingPTScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [timeRegister, setTimeRegister] = useState([]);
  const [slot, setSlot] = useState([]);
  const [pt, setPT] = useState([]);
  const [price, setPrice] = useState('');
  const [date, setDate] = useState('');
  const [selectedPT, setSelectedPT] = useState(null);
  const [selectedSlot, setSelectedSlot] = useState(null);

  const [packageSe, setpackageSe] = useState("Package");
  const [slotSe, setSlotSe] = useState("Slot");
  const [ptSe, setPtSe] = useState("PT");


  useEffect(() => {
    dispatch(
      getRegisterDayWithPt({
        onSuccess: (data) => {
          setTimeRegister(data);
        },
      }),
    );
    dispatch(
      getSlotTimeWithPt({
        onSuccess: (data) => {
          setSlot(data);
        },
      }),
    );
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setDate(tempData.selectDate);
    });

    return unsubscribe;
  }, [navigation]);

  const navCalendar = () => {
    navigation.navigate('CalenderScreen', {from: 'BookingPTScreen'});
  };

  const onSelectItem = (item) => {
    if (tempData.selectDate) {
      setpackageSe(item)
      tempData.packageDate = item;
      dispatch(
        getPriceByParams(item, tempData.selectDate, {
          onSuccess: (data) => {
            setPrice(data);
          },
        }),
      );
    } else {
      showMessage({
        message: 'Please choose date!!',
        type: 'warning',
        duration: 2000,
      });
    }
  };

  const onSelectSlotItem = (item) => {
    console.log('onSelectSlotItem', item);
    if (tempData.selectDate) {
      setSlotSe(item.name);
      setSelectedSlot(item.id);
      dispatch(
        getPtByParams(item.id, tempData.selectDate, {
          onSuccess: (data) => {
            setPT(data);
          },
        }),
      );
    } else {
      showMessage({
        message: 'Please choose date!!',
        type: 'warning',
        duration: 2000,
      });
    }
  };

  const onSelectPTItem = (item) => {
    console.log('onSelectPTItem', item);
    setPtSe(item.full_name);
    setSelectedPT(item.id);
  };

  const bookPT = () => {
    if (
      selectedPT &&
      selectedSlot &&
      price &&
      tempData.packageDate &&
      tempData.selectDate
    ) {
      const payload = {
        ptId: selectedPT,
        slotId: selectedSlot,
        price: price,
        dateRegister: tempData.packageDate,
        registerDate: tempData.selectDate,
      };
      console.log("PAYLOAD BOOKING: ",payload)
      dispatch(bookingPt(payload));
    } else {
      showMessage({
        message: 'Please select all data!!',
        type: 'warning',
        duration: 2000,
      });
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        title="Booking PT"
        openDrawer={() => navigation.openDrawer()}
        sizeIconDrawer={20}
      />
      <View style={styles.imageHeaderView}>
        <FastImage
          style={styles.imageHeader}
          source={Images.headerDetailExercises}
        />
      </View>

      <View style={styles.content}>
        <View style={styles.headerContent}>
          <TouchableOpacity style={styles.inputContainer} onPress={navCalendar}>
            <Text style={styles.textPrice}>
              {date?.length > 0 ? date : 'Date'}
            </Text>
          </TouchableOpacity>

          <SelectDropdown
            data={timeRegister}
            onSelect={(selectedItem) => {
              onSelectItem(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem) => {
              return selectedItem;
            }}
            rowTextForSelection={(item) => {
              return item;
            }}
            renderCustomizedButtonChild={(selectedItem) => {
              return (
                <View style={styles.viewSelect}>
                  <Text style={styles.textSelect}>{packageSe}</Text>
                  <IIcons name="caret-down-sharp" style={styles.iconSelect} />
                </View>
              );
            }}
            buttonStyle={styles.buttonSelect}
          />

          <View style={styles.inputPrice}>
            <Text style={styles.textPrice}>{price}</Text>
          </View>
        </View>

        <View style={styles.bottomContent}>
          <SelectDropdown
            data={slot}
            onSelect={(selectedItem) => {
              onSelectSlotItem(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem) => {
              return selectedItem;
            }}
            rowTextForSelection={(item) => {
              return item.name;
            }}
            renderCustomizedButtonChild={(selectedItem) => {
              return (
                <View style={styles.viewSelect}>
                  <Text style={styles.textSelect}>{slotSe}</Text>
                  <IIcons name="caret-down-sharp" style={styles.iconSelect} />
                </View>
              );
            }}
            buttonStyle={styles.bottomButtonSelect}
          />

          <SelectDropdown
            data={pt}
            onSelect={(selectedItem) => {
              onSelectPTItem(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem) => {
              return selectedItem;
            }}
            rowTextForSelection={(item) => {
              return item.full_name;
            }}
            renderCustomizedButtonChild={(selectedItem) => {
              return (
                <View style={styles.viewSelect}>
                  <Text style={styles.textSelect}>{ptSe}</Text>
                  <IIcons name="caret-down-sharp" style={styles.iconSelect} />
                </View>
              );
            }}
            buttonStyle={styles.bottomButtonSelect}
          />
        </View>
      </View>

      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.containerPlusButton}
        onPress={bookPT}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 0, y: 1}}
          colors={Colors.mainGradientButton}
          style={styles.linearGradient}>
          <Text style={styles.addText}>Send Request</Text>
        </LinearGradient>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default BookingPTScreen;
