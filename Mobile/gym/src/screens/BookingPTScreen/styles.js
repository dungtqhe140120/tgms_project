import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth, ScreenHeight} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  searchInput: {
    paddingHorizontal: 10,
    fontSize: 17,
  },

  bgGray: {
    backgroundColor: Colors.DirtyBackground,
  },

  imageHeaderView: {
    width: ScreenWidth(1),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -10,
  },
  imageHeader: {
    width: ScreenWidth(0.77),
    height: ScreenWidth(0.1),
  },

  inputContainer: {
    marginTop: 0,
    paddingHorizontal: 0,
    marginHorizontal: 20,
    paddingVertical: Constants.isIOS ? 12 : 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 0,
    backgroundColor: 'transparent',
    borderBottomColor: Colors.primary,
    borderBottomWidth: 2,
    width: ScreenWidth(0.3),
  },

  inputPrice: {
    marginTop: 0,
    paddingHorizontal: 0,
    marginHorizontal: 20,
    paddingVertical: Constants.isIOS ? 12 : 0,
    backgroundColor: 'transparent',
    borderBottomColor: Colors.primary,
    borderBottomWidth: 2,
    width: ScreenWidth(0.3),
  },
  textPrice: {
    fontSize: 18,
  },

  headerContent: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  viewSelect: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textSelect: {
    fontSize: 18,
  },
  buttonSelect: {
    width: ScreenWidth(0.28),
  },

  bottomContent: {
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  bottomButtonSelect: {
    width: ScreenWidth(0.45),
  },

  containerPlusButton: {
    marginTop: ScreenHeight(0.04),
    justifyContent: 'center',
    alignItems: 'center',
  },
  addText: {
    fontSize: 20,
    color: Colors.white,
  },
  linearGradient: {
    borderRadius: 8,
    padding: 8,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
