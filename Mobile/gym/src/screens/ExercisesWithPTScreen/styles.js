import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  searchInput: {
    paddingHorizontal: 10,
    fontSize: 17,
  },

  bgGray: {
    backgroundColor: Colors.DirtyBackground,
  },

  listRoomExercises: {
    flex: 1,
  },
  emptyItemView: {
    height: ScreenWidth(0.1),
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: ScreenWidth(0.35),
    marginHorizontal: ScreenWidth(0.05),
    marginBottom: 20,
    borderRadius: 20,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    elevation: 5,
  },
  itemImage: {
    width: ScreenWidth(0.58),
    height: '100%',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
  },
  itemText: {
    fontSize: 26,
    position: 'absolute',
    left: 20,
    zIndex: 2,
  },

  headerContainer: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: Colors.sectionSeparatorColor,
    paddingVertical: 12,
  },
  sttTitleTable: {
    width: '25%',
    alignItems: 'center',
  },
  nameTitleTable: {
    width: '40%',
    alignItems: 'center',
  },
  phoneTitleTable: {
    width: '35%',
    alignItems: 'center',
  },
  textTitleTable: {
    fontSize: 16,
    fontWeight: 'bold',
  },

  listRoomExercises: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    marginVertical: 7,
  },
  sttView: {
    width: '25%',
    alignItems: 'center',
  },
  nameView: {
    width: '40%',
    marginLeft: 10,
  },
  phoneView: {
    width: '35%',
    marginLeft: 10,
  },

  itemSeparatorComponent: {
    height: 0.5,
    width: '100%',
    backgroundColor: Colors.border,
  },
});

export default styles;
