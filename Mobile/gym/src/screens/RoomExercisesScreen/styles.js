import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  searchInput: {
    paddingHorizontal: 10,
    fontSize: 17,
  },

  bgGray: {
    backgroundColor: Colors.DirtyBackground,
  },

  listRoomExercises: {
    flex: 1,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: ScreenWidth(0.35),
    marginHorizontal: ScreenWidth(0.05),
    marginBottom: 20,
    borderRadius: 20,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    elevation: 5,
  },
  itemImage: {
    width: ScreenWidth(0.45),
    height: ScreenWidth(0.34),
    marginRight:'1%',
    borderRadius:10,
  },
  plusButton: {
    width: 36,
    height: 36,
    borderRadius: 18,
    backgroundColor: Colors.lightBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  minusButton: {
    position: 'absolute',
    right: 10,
    zIndex: 2,
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  itemIcon: {
    fontSize: 26,
    color: Colors.white,
  },
  itemText: {
    fontSize: 26,
    position: 'absolute',
    left: 20,
    zIndex: 2,
  },
});

export default styles;
