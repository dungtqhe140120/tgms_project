import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  background: {
    flex: 1,
    zIndex: 1,
    alignItems: 'center',
  },

  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(255,255,255,0.25)',
  },

  logo: {
    width: ScreenWidth(0.72),
    height: ScreenWidth(0.48),
    marginTop: ScreenWidth(0.3),
  },

  BMITitle: {
    fontSize: 36,
    fontWeight: 'bold',
    marginLeft: -100,
    marginBottom: 15,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: 'white',
  },

  inputView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputText: {
    fontSize: 18,
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 5,
    textShadowColor: 'white',
  },
  inputContainer: {
    marginTop: 0,
    paddingHorizontal: 0,
    marginHorizontal: 20,
    paddingVertical: Constants.isIOS ? 12 : 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 0,
    backgroundColor: 'transparent',
    borderBottomColor: Colors.primary,
    borderBottomWidth: 2,
  },

  BMIView: {
    flexGrow: 1,
  },
  BMIText: {
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: -100,
    marginTop: 30,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    textShadowColor: 'white',
  },

  btn: {
    width: '90%',
    backgroundColor: Colors.primary,
    marginBottom: 40,
    paddingVertical: 15,
    borderRadius: 7,
    alignItems: 'center',
  },
  btnText: {
    fontSize: 20,
    color: Colors.white,
    fontWeight: 'bold',
  },
});

export default styles;
