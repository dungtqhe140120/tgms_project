import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  background: {
    flex: 1,
    zIndex: 1,
  },

  linearGradient: {
    flex: 1,
    zIndex: 2,
    opacity: 0.75,
  },

  logo: {
    width: ScreenWidth(0.36),
    height: ScreenWidth(0.24),
  },

  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  textCenter: {
    fontSize: 40,
    fontWeight: 'bold',
    color: Colors.white,
    marginTop: 10,
  },

  footer: {
    position: 'absolute',
    bottom: 40,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnStarted: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 30,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: Colors.white,
  },
  textStarted: {
    color: Colors.white,
    fontSize: 16,
    fontWeight: 'bold',
  },
  iconNext: {
    color: Colors.white,
    fontSize: 18,
    marginLeft: 10,
  },
});

export default styles;
