import {StyleSheet} from 'react-native';

import Colors from '../../common/Colors';
import Constants from '../../common/Constants';

const {ScreenWidth} = Constants.Dimension;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  imageHeaderView: {
    width: ScreenWidth(1),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -10,
  },
  imageHeader: {
    width: ScreenWidth(0.77),
    height: ScreenWidth(0.1),
  },

  traineeInfoView: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    marginVertical: 15,
  },
  emptyInfoView: {
    width: '30%',
  },
  nameInfoView: {
    width: '55%',
  },
  nameInfoText: {
    fontWeight: 'bold',
  },
  bMIInfoView: {
    width: '15%',
  },
  bMIInfoText: {
    fontWeight: 'bold',
  },

  headerContainer: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: Colors.sectionSeparatorColor,
    paddingVertical: 12,
  },
  sttTitleTable: {
    width: '25%',
    alignItems: 'center',
  },
  nameTitleTable: {
    width: '14%',
    alignItems: 'center',
  },
  phoneTitleTable: {
    width: '50%',
    alignItems: 'center',
  },
  textTitleTable: {
    fontSize: 16,
    fontWeight: 'bold',
  },

  listRoomExercises: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
  },
  sttView: {
    width: '25%',
    alignItems: 'center',
  },
  nameView: {
    width: '25%',
    alignItems: 'center',
  },
  phoneView: {
    width: '50%',
    marginLeft: 28,
  },
  itemTextExTitle: {
    color: Colors.blue,
    fontWeight: 'bold',
  },

  itemSeparatorComponent: {
    height: 0.5,
    width: '100%',
    backgroundColor: Colors.border,
  },

  containerPlusButton: {
    width: '100%',
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  plusButton: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: Colors.lightBlue,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:10,
  },
  addDoingIcon: {
    fontSize: 24,
    color: Colors.white,
  },
});

export default styles;
