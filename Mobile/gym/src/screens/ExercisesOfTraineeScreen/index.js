import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import AntDIcons from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import Images from '../../common/Images';
import { getDetailExercise, getListExercises, getListMyExercises } from '../../actions';
import HeaderActionScreen from '../../components/HeaderActionScreen';

const ExercisesOfTraineeScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const route = useRoute();
  const traineeReducer = useSelector((state) => state.trainee);

  const { exercisesOfTrainee } = traineeReducer;

  const navExercisesScreen = (item) => {
    dispatch(
      getDetailExercise(item.exID, {
        onSuccess: (data) => {
          console.log('navExercisesScreen', data);
          navigation.navigate('DetailRoomExerciseScreen', {
            data,
            showButtonAdd: false,
          });
        },
        onFailure: () => { },
      }),
    );
  };

  const headerComponent = () => {
    return (
      <View style={styles.headerContainer}>
        <View style={styles.sttTitleTable}>
          <Text style={styles.textTitleTable}>STT</Text>
        </View>
        <View style={styles.nameTitleTable}>
          <Text style={styles.textTitleTable}>Slot</Text>
        </View>
        <View style={styles.phoneTitleTable}>
          <Text style={styles.textTitleTable}>Exercise</Text>
        </View>
      </View>
    );
  };

  const renderItemDetailExercises = ({ item, index }) => {
    return (
      <View style={styles.item}>
        <View style={styles.sttView}>
          <Text>{index + 1}</Text>
        </View>
        <View style={styles.nameView}>
          <Text>{item.slotName}</Text>
        </View>
        {/* <View style={styles.phoneView}>
          <Text style={styles.itemTextExTitle} >{item.exTitle}</Text>
        </View> */}
        <TouchableOpacity style={styles.phoneView} onPress={() => goToTypeOfExercises(item)}>
          <View style={styles.phoneView}>
            <Text style={styles.itemTextExTitle} >{item.exTitle}</Text>
          </View>
        </TouchableOpacity>

      </View>
    );
  };

  const goToTypeOfExercises = (item) => {
    dispatch(
      getDetailExercise(item.exID, {
        onSuccess: (data) => {
          navigation.navigate('DetailRoomExercisesScreen', {
            data,
            showButtonAdd: false,
            showButtonSub: false,
            title: item.title,
            parentId: item.exID
          });
        },
        onFailure: () => { },
      }),
    );
  };

  const renderItemSeparatorComponent = () => {
    return <View style={styles.itemSeparatorComponent} />;
  };

  const add = () => {
    const traineeId = route.params?.traineeId || 0;

    dispatch(getListMyExercises());
    navigation.navigate('RoomExercisesScreen', {
      assign: true,
      traineeId,
    });

    dispatch(getListExercises());
    // navigation.navigate('RoomExercisesScreen', {
    //   assign: true,
    //   traineeId,
    // });
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderActionScreen
        goBack={() => navigation.goBack()}
        sizeIconBack={30}
        title=""
      />
      <View style={styles.imageHeaderView}>
        <FastImage
          style={styles.imageHeader}
          source={Images.headerDetailExercises}
        />
      </View>

      <View style={styles.traineeInfoView}>
        <View style={styles.emptyInfoView} />
        <View style={styles.nameInfoView}>
          <Text>
            {route.params?.traineeId != null
              && <Text style={styles.nameInfoText}>
                Trainee Name:{' '}
                {exercisesOfTrainee.length > 0 &&
                  exercisesOfTrainee[0].traineeName}
              </Text>
            }
          </Text>
        </View>
        <View style={styles.bMIInfoView}>
          <Text>
            {route.params?.traineeId != null &&
              <Text style={styles.bMIInfoText}>
                BMI:{' '}
                {exercisesOfTrainee.length > 0 && exercisesOfTrainee[0].bmiNumber}
              </Text>
            }
          </Text>
        </View>
      </View>

      <FlatList
        data={exercisesOfTrainee}
        keyExtractor={(item) => `${item.exID}`}
        style={styles.listRoomExercises}
        ListHeaderComponent={headerComponent}
        renderItem={renderItemDetailExercises}
        ItemSeparatorComponent={renderItemSeparatorComponent}
      />
      {route.params?.traineeId != null &&
        <View style={styles.containerPlusButton}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.plusButton}
            onPress={add}>
            <AntDIcons name="plus" style={styles.addDoingIcon} />
          </TouchableOpacity>
        </View>
      }

    </SafeAreaView>
  );
};

export default ExercisesOfTraineeScreen;
