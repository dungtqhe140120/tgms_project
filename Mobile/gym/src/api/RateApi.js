import { ApiClient } from './ApiService';

export const getRate = () => ApiClient.get('api/rates');

export const addNewRate = (payload) => ApiClient.put('api/rates', payload);

export const getRatesOfPt = (params) =>
  ApiClient.get(
    `api/rate-pts/pt/about-me?page=${params.page}&size=${params.size}`,
  );

export const getRateOfPtAboutRoom = () => ApiClient.get('api/rates/center');

export const addNewRateOfPt = (payload) =>
  ApiClient.put('api/rates/center', payload);

export const getPtProfileAndRating = () =>
  ApiClient.get('api/rate-pts/pt?page=0&size=1000000');
