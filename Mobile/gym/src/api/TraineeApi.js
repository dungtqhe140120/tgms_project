import { ApiClient } from './ApiService';

export const getPersonalTrainee = () => ApiClient.get('api/personal-trainers');

export const getTrainersOfPT = () => ApiClient.get('api/trainees-pt');

export const getExercisesById = (trainerId) =>
  ApiClient.get(
    `api/trainee-exercises/getExerciseTraineeByTrainee/${trainerId}`,
  );

export const uppdateBmis = (payload) =>
  ApiClient.post('api/bmis', payload);
