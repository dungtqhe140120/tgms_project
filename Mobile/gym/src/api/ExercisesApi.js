import { ApiClient } from './ApiService';

export const getExercises = () => ApiClient.get('api/gym-center-exercises');

export const getMyExercises = () => ApiClient.get('api/mine-exercises');

export const getExerciseById = (id) =>
  ApiClient.get(`api/detail-exercises/viewDetailExercise/${id}?size=10000000&sort=id,asc`);

export const addMyList = (id) =>
  ApiClient.put(`api/gym-center-excercises/${id}/add-my-list`);

export const getDoingExerciseById = (id) =>
  ApiClient.get(`api/detail-exercises/getDetailExerciseShowDTOByID/${id}`);

export const addMyExercises = (payload) =>
  ApiClient.post('api/categories', payload);

export const getDetailMyExercises = (id) =>
  ApiClient.post(`mine-excercises?id=${id}`);

export const addDoingExercise = (payload) =>
  ApiClient.post('api/mine-excercises/post', payload);

export const deleteExercises = (id) =>
  ApiClient.delete(`api/categories/deletePTCategory/${id}`);

export const addExerciseToMyExercises = (id) =>
  ApiClient.delete(`api/exercises/${id}`);

export const addDetailEx = (payload) =>
  ApiClient.post('api/detail-exercises', payload);

export const addDoingEx = (id, payload) =>
  ApiClient.put(`api/detail-exercises/update/${id}`, payload);

export const getDoingEx = (id) =>
  ApiClient.get(`api/detail-exercises/getContent/${id}`);

export const deleteDetailEx = (id) =>
  ApiClient.delete(`api/detail-exercises/${id}`);