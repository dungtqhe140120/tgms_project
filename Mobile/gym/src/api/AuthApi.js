import {ApiClient} from './ApiService';
import {ApiClientWithoutToken} from './ApiWithoutTokenService';

export const login = (payload) =>
  ApiClientWithoutToken.post('api/authenticate', payload);

export const changePassword = (payload) =>
  ApiClient.post('api/change-password', payload);

export const forgotPassword = (payload) =>
  ApiClient.post('api/forgot-password', payload);

export const confirmForgotPassword = (payload) =>
  ApiClient.post('api/confirm-forgot-password', payload);

export const resetPasswordWithEmail = (email) =>
  ApiClientWithoutToken.post(`api/account/reset-password/init?mail=${email}`);

export const getInfoPt = () =>
  ApiClient.get('api/personal-trainers/getCurrentPT');

export const updateInfoPt = (payload) =>
  ApiClient.put('api/personal-trainers/updateInformation', payload);

export const updatePasswordUser = (payload) =>
  ApiClient.post('api/account/change-password', payload);

export const getInfoTrainee = () =>
  ApiClient.get('api/trainees/getCurrentTrainee');

export const updateInfoTrainee = (payload) =>
  ApiClient.put('api/profiles/mine-profile', payload);
