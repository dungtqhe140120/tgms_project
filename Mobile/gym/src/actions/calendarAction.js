import { showMessage } from 'react-native-flash-message';

import {
  getTrainersByDate,
  assignExercises,
  getRegisterDayWithPT,
  getSlotTimeWithPT,
  getPrice,
  getPTByParams,
  bookingPT,
} from '../api/CalendarApi';
import types from './actionTypes';

// showMessage({
//   message: json?.message || 'Yêu cầu thất bại. Vui lòng thử lại sau',
//   type: 'danger',
//   duration: 1500,
// })

export const getCalendarTrainersByDate = (ptId, date) => async (dispatch) => {
  dispatch({ type: types.GETTING_TRAINERS_BY_DATE });

  const json = await getTrainersByDate(ptId, date);

  if (json?.content) {
    dispatch({
      type: types.GET_TRAINERS_BY_DATE_SUCCESS,
      data: {
        total: json.totalElements,
        trainers: json.content,
      },
    });
  } else {
    dispatch({ type: types.GET_TRAINERS_BY_DATE_FAILURE });
  }
};

export const bookingPt = (payload, meta) => async (dispatch) => {
  dispatch({ type: types.BOOKING_PT });

  const json = await bookingPT(payload, meta);

  if (json?.id) {
    dispatch({ type: types.BOOK_PT_SUCCESS });

    showMessage({
      message: 'Booking PT success',
      type: 'success',
      duration: 20000,
    });
  } else {
    dispatch({ type: types.BOOK_PT_FAILURE });
    showMessage({
      message: 'Booking PT failure',
      type: 'warning',
      duration: 20000,
    });
  }
};

export const assignExercisesForTrainee =
  (traineeID, categoryId, meta) => async (dispatch) => {
    const json = await assignExercises(traineeID, categoryId);

    if (json?.id) {
      meta.onSuccess();

      showMessage({
        message: 'Success',
        type: 'success',
        duration: 2000,
      });
    } else {
      showMessage({
        message: 'Request failed',
        type: 'warning',
        duration: 2000,
      });
    }
  };

export const getRegisterDayWithPt = (meta) => async (dispatch) => {
  const json = await getRegisterDayWithPT();

  if (json?.length >= 0) {
    meta.onSuccess(json);
  }
};

export const getSlotTimeWithPt = (meta) => async (dispatch) => {
  const json = await getSlotTimeWithPT();

  if (json?.content?.length >= 0) {
    meta.onSuccess(json.content);
  }
};

export const getPriceByParams =
  (registerDays, registerDate, meta) => async (dispatch) => {
    const json = await getPrice(registerDays, registerDate);
    if (json?.id) {
      meta.onSuccess(json.price);
    } else {
      showMessage({
        message: json.responseError.title,
        type: 'warning',
        duration: 2000,
      });
    }
  };

export const getPtByParams =
  (slotId, registerDate, meta) => async (dispatch) => {
    const json = await getPTByParams(slotId, registerDate);

    if (json?.length >= 0) {
      meta.onSuccess(json);
    } else {
      showMessage({
        message: 'Request failed',
        type: 'warning',
        duration: 2000,
      });
    }
  };

export const clearDataTrainers = () => async (dispatch) => {
  dispatch({ type: types.CLEAR_FETCHING });
};
