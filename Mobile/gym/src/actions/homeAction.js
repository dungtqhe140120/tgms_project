import {showMessage} from 'react-native-flash-message';

import types from './actionTypes';
import AsyncStorageService from '../api/AsyncStorageService';
import {
  getDataOnHomeScreen,
  getListPost,
  getDetailPost,
  getHomeByTraineeRole,
} from '../api/HomeApi';

export const getDataOnHome = (role) => async (dispatch) => {
  dispatch({type: types.GETTING_DATA_ON_HOME});

  const json = await getDataOnHomeScreen(role);
  console.log('getDataOnHome json', json);

  // if (json.status === 'SUCCESS') {
  // } else {
  //   // showMessage({
  //   //   message: json?.message || 'Yêu cầu thất bại. Vui lòng thử lại sau',
  //   //   type: 'danger',
  //   //   duration: 1500,
  //   // })
  //   dispatch({type: types.GET_DATA_ON_HOME_SUCCESS, data});
  //   dispatch({type: types.GET_DATA_ON_HOME_FAILURE});
  // }
};

export const getHomeByTrainee = () => async (dispatch) => {
  dispatch({type: types.GETTING_DATA_ON_HOME_BY_TRAINEE});
  const json = await getHomeByTraineeRole();
  if (json?.newsResources?.content?.length >= 0) {
    dispatch({type: types.GET_DATA_ON_HOME_BY_TRAINEE_SUCCESS, data: json});
  } else {
    dispatch({type: types.GET_DATA_ON_HOME_BY_TRAINEE_FAILURE});
  }
};

export const getPost = (succes) => async (dispatch) => {
  const json = await getListPost();
  succes(json);
};

export const getDetail = (id, meta) => async (dispatch) => {
  const json = await getDetailPost(id);
  meta.onSuccess(json);
};
