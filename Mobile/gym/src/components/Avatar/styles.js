import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  buttonChooseImage: {
    position: 'absolute',
    bottom: -5,
    right: 5,
    backgroundColor: '#ed3631',
    padding: 5,
    borderRadius: 15,
  },
});

export default styles;
