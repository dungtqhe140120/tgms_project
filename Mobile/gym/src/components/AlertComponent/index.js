import {Alert} from 'react-native';

const goBack = navigation => {
  if (navigation) {
    navigation.goBack();
  }
};

const AlertComponent = (title, body, navigation) => {
  Alert.alert(title, body, [
    {
      text: 'ok',
      onPress: () => goBack(navigation),
    },
  ]);
};

export {AlertComponent};
