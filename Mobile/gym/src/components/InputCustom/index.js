import React, {useState, forwardRef, useImperativeHandle} from 'react';
import {View, TextInput, Text} from 'react-native';
import AntDIcons from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import Colors from '../../common/Colors';

const InputCustom = forwardRef(
  (
    {
      containerStyleProps,
      placeholder,
      stylesProps,
      iconComponent,
      checkIcon,
      password,
      valueInput,
      setValueParent,
      editable,
      useFocusInput,
      iconRight,
      hideIconLeft,
      keyboardType,
      errorText,
      id,
      focusAction,
    },
    ref,
  ) => {
    const [value, setValue] = useState(valueInput || '');
    const [focusInput, setFocusInput] = useState(false);

    useImperativeHandle(ref, () => ({
      value,
    }));

    const renderLeftIconComponent = () => {
      if (iconComponent) {
        return <View style={styles.iconView}>{iconComponent}</View>;
      } else if (hideIconLeft) {
        return <View />;
      } else {
        return <View style={styles.emptyView} />;
      }
    };

    return (
      <View>
        <View
          style={[
            styles.container,
            containerStyleProps,
            useFocusInput && focusInput && styles.borderBottomStyle,
          ]}>
          {renderLeftIconComponent()}
          <TextInput
            style={[styles.localInputStyles, stylesProps]}
            placeholder={placeholder || ''}
            editable={editable}
            value={value}
            defaultValue={value}
            secureTextEntry={password || false}
            onChangeText={(text) => {
              setValue(text);
              setValueParent && setValueParent(text);
            }}
            onFocus={() => {
              setFocusInput(true);
              focusAction && focusAction(id);
            }}
            onBlur={() => setFocusInput(false)}
            keyboardType={keyboardType || 'default'}
          />
          <View style={styles.checkIconView}>
            {iconRight && <View style={styles.iconView}>{iconRight}</View>}
            {checkIcon ? (
              <View style={styles.iconView}>
                <AntDIcons size={16} name="checkcircle" color={Colors.green} />
              </View>
            ) : (
              <View style={styles.emptyView} />
            )}
          </View>
        </View>
        {errorText && <Text style={styles.textError}>{errorText}</Text>}
      </View>
    );
  },
);

export default InputCustom;
