/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as Animatable from 'react-native-animatable';
import { Text, TouchableOpacity, StyleSheet, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FoundationIcons from 'react-native-vector-icons/Foundation';
// import SplashScreen from "react-native-splash-screen";
// import NetInfo from "@react-native-community/netinfo";

// import ApiWorker from '../../common/ApiWorker';
import CustomDrawer from './CustomDrawer';

// Screens
import LoginScreen from '../screens/LoginScreen';
import ChangePasswordScreen from '../screens/ChangePasswordScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';
import InputOTPScreen from '../screens/InputOTPScreen';

// Exercises
import ExercisesScreen from '../screens/ExercisesScreen';
import RoomExercisesScreen from '../screens/RoomExercisesScreen';
import DetailRoomExercisesScreen from '../screens/DetailRoomExercisesScreen';
import DetailDoingExerciseScreen from '../screens/DetailDoingExerciseScreen';
import DetailRoomExerciseScreen from '../screens/DetailRoomExerciseScreen';
import MyExercisesScreen from '../screens/MyExercisesScreen';
import AddMyExercisesScreen from '../screens/AddMyExercisesScreen';
import AddDoingExercisesScreen from '../screens/AddDoingExercisesScreen';
import AddDetailExercisesScreen from '../screens/AddDetailExercisesScreen';
import ExercisesWithPTScreen from '../screens/ExercisesWithPTScreen';

// Trainers
import TrainersScreen from '../screens/TrainersScreen';
import ExercisesOfTraineeScreen from '../screens/ExercisesOfTraineeScreen';

// Other
// import RatingAppScreen from '../screens/RatingAppScreen';
import RatingScreen from '../screens/RatingScreen';
import AboutUsScreen from '../screens/AboutUsScreen';

import CalenderScreen from '../screens/calenderScreen';
import MyPtProfile from '../screens/MyPtProfile';
import BookingPTScreen from '../screens/BookingPTScreen';
import HomeScreen from '../screens/HomeScreen';
import ProfileScreen from '../screens/ProfileScreen';
import ReportsScreen from '../screens/ReportsScreen';

import Colors from '../common/Colors';
import Icon, { Icons } from '../common/Icons';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const AppStackNavigator = ({ role }) => {
  // const statusLogin = ApiWorker.getLoginAgain();
  console.log('role', role);
  const TabArr = [
    {
      route: 'Home',
      label: 'Home',
      type: Icons.Feather,
      icon: 'home',
      component: HomeScreen,
      color: Colors.primary,
      alphaClr: Colors.accentLight,
    },
    {
      route:
        role?.length > 0 && role[0].name === 'ROLE_PT' ? 'Calendar' : 'Booking',
      label:
        role?.length > 0 && role[0].name === 'ROLE_PT' ? 'Calendar' : 'Booking',
      type: Icons.Feather,
      icon: 'calendar',
      component: role?.length > 0 && role[0].name === 'ROLE_PT' ? CalenderScreen : BookingPTScreen,
      color: Colors.primary,
      alphaClr: Colors.accentLight,
    },
    {
      route: 'Exercises',
      label: 'Exercises',
      type: Icons.Feather,
      icon: 'file',
      component: ExercisesScreen,
      color: Colors.primary,
      alphaClr: Colors.accentLight,
    },
    {
      route: role?.length > 0 && role[0].name === 'ROLE_PT' ? 'Report' : 'Rating',
      label: 'Report',
      type: Icons.MaterialIcons,
      icon: 'rate-review',
      component:  role?.length > 0 && role[0].name === 'ROLE_PT' ? ReportsScreen : RatingScreen,
      color: Colors.primary,
      alphaClr: Colors.accentLight,
    },
    {
      route: 'Profile',
      label: 'Profile',
      type: Icons.Feather,
      icon: 'user',
      component: ProfileScreen,
      color: Colors.primary,
      alphaClr: Colors.accentLight,
    },
  ];

  function TabButton(props) {
    const { item, onPress, accessibilityState } = props;
    const focused = accessibilityState.selected;
    const viewRef = useRef(null);
    const textViewRef = useRef(null);

    useEffect(() => {
      if (focused) {
        // 0.3: { scale: .7 }, 0.5: { scale: .3 }, 0.8: { scale: .7 },
        viewRef.current.animate({ 0: { scale: 0 }, 1: { scale: 1 } });
        textViewRef.current.animate({ 0: { scale: 0 }, 1: { scale: 1 } });
      } else {
        viewRef.current.animate({ 0: { scale: 1 }, 1: { scale: 0 } });
        textViewRef.current.animate({ 0: { scale: 1 }, 1: { scale: 0 } });
      }
    }, [focused]);

    return (
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={1}
        style={[styles.container, { flex: focused ? 1 : 0.65 }]}>
        <View>
          <Animatable.View
            ref={viewRef}
            style={[
              StyleSheet.absoluteFillObject,
              { backgroundColor: item.color, borderRadius: 16 },
            ]}
          />
          <View
            style={[
              styles.btn,
              { backgroundColor: focused ? null : item.alphaClr },
            ]}>
            <Icon
              type={item.type}
              name={item.icon}
              color={focused ? Colors.white : Colors.blue2}
            />
            <Animatable.View ref={textViewRef}>
              {focused && (
                <Text
                  style={{
                    color: Colors.white,
                    paddingHorizontal: 8,
                  }}>
                  {item.label}
                </Text>
              )}
            </Animatable.View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  function AppStack() {
    return (
      <Tab.Navigator
        screenOptions={{
          headerShown: false,
          tabBarStyle: {
            height: 55,
            backgroundColor: Colors.backgroundLightGrey,
          },
        }}>
        {TabArr.map((item, index) => {
          return (
            <Tab.Screen
              key={index}
              name={item.route}
              component={item.component}
              options={{
                tabBarShowLabel: false,
                tabBarButton: (props) => <TabButton {...props} item={item} />,
              }}
            />
          );
        })}
      </Tab.Navigator>
    );
  }

  function Root() {
    return (
      <Drawer.Navigator
        drawerContent={(props) => <CustomDrawer {...props} />}
        screenOptions={{
          headerShown: false,
          drawerActiveBackgroundColor: Colors.primary,
          drawerActiveTintColor: Colors.white,
          drawerLabelStyle: {
            marginLeft: -20,
            fontSize: 15,
          },
        }}>
        <Drawer.Screen
          name="Home"
          component={AppStack}
          options={{
            drawerIcon: ({ color }) => (
              <View style={styles.drawerIcon}>
                <Ionicons name="home-outline" size={22} color={color} />
              </View>
            ),
          }}
        />
        <Drawer.Screen
          name="Rating"
          component={RatingScreen}
          options={{
            drawerIcon: ({ color }) => (
              <View style={styles.drawerIcon}>
                <FoundationIcons
                  name="clipboard-pencil"
                  size={22}
                  color={color}
                />
              </View>
            ),
          }}
        />
        <Drawer.Screen
          name="About Us"
          component={AboutUsScreen}
          options={{
            drawerIcon: ({ color }) => (
              <View style={styles.drawerIcon}>
                <Ionicons
                  name="information-circle-outline"
                  size={24}
                  color={color}
                />
              </View>
            ),
          }}
        />
      </Drawer.Navigator>
    );
  }

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          header: () => null,
        }}>
        <Stack.Screen
          name="Root"
          component={Root}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="RoomExercisesScreen"
          component={RoomExercisesScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="DetailRoomExercisesScreen"
          component={DetailRoomExercisesScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="DetailDoingExerciseScreen"
          component={DetailDoingExerciseScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="DetailRoomExerciseScreen"
          component={DetailRoomExerciseScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="MyExercisesScreen"
          component={MyExercisesScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AddMyExercisesScreen"
          component={AddMyExercisesScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AddDoingExercisesScreen"
          component={AddDoingExercisesScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AddDetailExercisesScreen"
          component={AddDetailExercisesScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ExercisesWithPTScreen"
          component={ExercisesWithPTScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ChangePasswordScreen"
          component={ChangePasswordScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ForgotPasswordScreen"
          component={ForgotPasswordScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="InputOTPScreen"
          component={InputOTPScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="TrainersScreen"
          component={TrainersScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ExercisesOfTraineeScreen"
          component={ExercisesOfTraineeScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="CalenderScreen"
          component={CalenderScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="MyPtProfile"
          component={MyPtProfile}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export const AppStackNavigatorWithoutToken = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          header: () => null,
        }}>
        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ForgotPasswordScreen"
          component={ForgotPasswordScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="InputOTPScreen"
          component={InputOTPScreen}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  tabBar: {
    height: 70,
    bottom: 16,
    marginRight: 16,
    marginLeft: 16,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    elevation: 13,
  },
  circle: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.blue2,
    borderRadius: 25,
  },
  text: {
    fontSize: 10,
    textAlign: 'center',
    color: Colors.primary,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    borderRadius: 16,
  },
  drawerIcon: {
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default React.memo(AppStackNavigator);
