export const isPersistedState = (stateName) => {
  const sessionState = sessionStorage.getItem(stateName);
  return sessionState && JSON.parse(sessionState);
};

export const formatYYYYMMdd = (date) => {
  let month = (date.getMonth() + 1).toString();
  let day = date.getDate().toString();
  let year = date.getFullYear();
  if (month.length < 2) {
    month = "0" + month;
  }
  if (day.length < 2) {
    day = "0" + day;
  }
  return [year, month, day].join("-");
};

export const checkDateLessThanToday = (dateTemp) => {
  if (!dateTemp) {
    return false;
  }
  let todayTemp = new Date();
  let today = new Date(formatYYYYMMdd(todayTemp));
  let date = new Date(formatYYYYMMdd(dateTemp));
  return date.getTime() < today.getTime();
};

export const checkDateMoreThanToday = (date) => {
  if (!date) {
    return false;
  }
  let today = new Date();
  return date.getTime() > today.getTime();
};
