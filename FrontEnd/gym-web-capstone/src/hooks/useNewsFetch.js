import React, { useEffect, useState } from "react";
import API from "../API";

const initialState = {
  page: 0,
  results: [],
  total_pages: 0,
  totol_results: 0,
};

export const useNewsEffect = () => {
  const [state, setState] = useState(initialState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const fetchNews = async (page) => {
    try {
      setError(false);
      setLoading(true);

      let endpoint = "http://101.99.6.31:8887/api/news";
      const results = await API.fetchData(endpoint);

      setState((prev) => ({
        results: results.data,
      }));
    } catch (error) {
      setError(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    setState(initialState);
    fetchNews();
  }, []);

  return {
    state,
    loading,
    error,
    setState,
  };
};
