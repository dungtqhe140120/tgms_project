import React, { useEffect, useState } from "react";
import API from "../API";

const initialState = {
  page: 0,
  results: [],
  total_pages: 0,
  totol_results: 0,
  page_size: 0,
};

export const usePersonalTrainersEffect = () => {
  const [state, setState] = useState(initialState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(25);

  const fetchPersonalTrainers = async (pageNo = 0, pageSize = 25) => {
    try {
      setError(false);
      setLoading(true);

      let endpoint = `http://101.99.6.31:8887/api/pt-inf-full?page=${pageNo}&size=${pageSize}`;
      const results = await API.fetchData(endpoint);
      const data = results.data;
      setState((prev) => ({
        page: data.number,
        results: data.content,
        total_pages: data.totalPages,
        totol_results: data.totalElements,
        page_size: data.size,
      }));
    } catch (error) {
      setError(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    setState(initialState);
    fetchPersonalTrainers(page, pageSize);
  }, [page, pageSize]);

  return {
    state,
    loading,
    error,
    setState,
    setPage,
    setPageSize,
  };
};
