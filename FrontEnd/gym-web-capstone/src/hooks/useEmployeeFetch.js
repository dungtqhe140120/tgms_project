import { useState, useEffect } from "react";
import API from "../API";

const initialState = {
  page: 0,
  results: [],
  total_pages: 0,
  totol_results: 0,
};

export const useEmployeeEffect = () => {
  const [state, setState] = useState(initialState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const fetchEmployees = async (page) => {
    try {
      setError(false);
      setLoading(true);

      let endpoint =
        "http://101.99.6.31:8887/api/v2/employees";

      const results = await API.fetchData(endpoint);
      setState((prev) => ({
        results: results.data,
      }));
    } catch (error) {
      setError(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    setState(initialState);
    fetchEmployees();
  }, []);

  return {
    state,
    loading,
    error,
    setState,
  };
};
