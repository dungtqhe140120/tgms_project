import axios from "axios";
import { LOGIN_URL } from "../../config";

import {
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT,
} from "./authTypes";

let config = {
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': 'true'
  }
}


export const login = (email, password) => async (dispatch) => {
  // dispath login request
  dispatch(loginRequest());
  try {
    //login request
    const response = await axios.post(LOGIN_URL, {
      username: email,
      password,
      rememberMe: false
    }, config);
    const data = await response.data;
    sessionStorage.setItem("auth",JSON.stringify(data));
    dispatch(loginSuccess(data));
    return true;
  } catch (error) {
    dispatch(loginFailure({message: 'Đăng nhập thất bại!'}));
    return false;
  }
};

export const logoutUser = () => {
  return (dispatch) => {
    dispatch(logoutRequest());
    sessionStorage.removeItem("auth")
  };
};

export const loginRequest = () => {
  return {
    type: LOGIN_REQUEST,
  };
};

export const loginSuccess = (authentication) => {
  return {
    type: LOGIN_SUCCESS,
    payload: authentication,
  };
};

export const loginFailure = (error) => {
  return {
    type: LOGIN_FAILURE,
    payload: error,
  };
};

export const logoutRequest = () => {
  return {
    type: LOGOUT,
  };
};
