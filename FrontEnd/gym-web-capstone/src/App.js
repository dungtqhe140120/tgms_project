import React from "react";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import { connect } from "react-redux";
import LoginPage from "./components/LoginPage/LoginPage";
import LandingPage from "./components/LandingPage/LandingPage";
import Home from "./pages/home/Home";
import "./App.css";
import TraineeList from "./pages/list/TraineeList";
import New from "./pages/new/New";
import { userInputs } from "./formSource";
import Layout from "./components/common/Layout";
import Unauthorized from "./pages/unauthorized/Unauthorized";
import RequireAuth from "./components/common/RequireAuth";
import NotificationManagement from "./pages/manage_notification/NotificationManagement";
import EmailList from "./components/notification/EmailList";
import Mail from "./components/notification/Mail";
import EmployeeList from "./pages/employee/EmployeeList";
import News from "./pages/news/News";
import Test from "./pages/news/Test";
import NewsDetail from "./pages/news/NewsDetail";
import EmployeeDetail from "./pages/employee/EmployeeDetail";
import PersonalTrainers from "./pages/personal-trainer/PersonalTrainers";
import PersonalTrainerDetail from "./pages/personal-trainer/PersonalTrainerDetail";
import ErrorPage from "./pages/error/ErrorPage";
import TraineeDetail from "./pages/trainee/TraineeDetail";
import Contractors from "./pages/contractor/Contractors";

function App({ auth }) {
  return (
    <>
      <Routes>
        <Route path="/">
          {/* public routes */}
          <Route index element={<LandingPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/" element={<LandingPage />} />
          <Route path="unauthorized" element={<Unauthorized />} />
          <Route path="/test" element={<Test />} />

          <Route
            element={
              <RequireAuth allowedRoles={["ROLE_EMPLOYEE", "ROLE_ADMIN"]} />
            }
          >
            <Route path="dashboard" element={<Layout />}>
              {/* <Route index element={<Home />} /> */}
              <Route index element={<TraineeList />} />
              {/* user */}
              <Route path="users">
                <Route index element={<TraineeList />} />
                <Route path=":traineeId" element={<TraineeDetail />} />
                <Route
                  path="new"
                  element={<New inputs={userInputs} title="Add New User" />}
                />
              </Route>

              {/* employee */}
              <Route element={<RequireAuth allowedRoles={["ROLE_ADMIN"]} />}>
                <Route path="employees">
                  <Route index element={<EmployeeList />} />
                  <Route path=":employeeId" element={<EmployeeDetail />} />
                </Route>
              </Route>

              {/* personal trainer */}
              <Route path="personal-trainer">
                <Route index element={<PersonalTrainers />} />
                <Route path=":ptId" element={<PersonalTrainerDetail />} />
              </Route>

              {/* <Route path="notification">
                <Route index element={<Contractors/>}/>
              </Route> */}

              {/* notification */}
              <Route path="notification" element={<NotificationManagement />}>
                <Route index element={<EmailList />} />
                <Route path="mail-list" element={<EmailList />} />
                <Route path="starred" element={<EmailList />} />
                <Route path="sent" element={<EmailList />} />
                <Route path="mail-detail/:mailId" element={<Mail />} />
              </Route>
              {/* transaction */}
              <Route path="transactions">
                <Route index element={<Contractors />} />
              </Route>
              {/* news */}
              <Route path="news">
                <Route index element={<News />} />
                <Route path=":newsId" element={<NewsDetail />} />
              </Route>
            </Route>
          </Route>
        </Route>
        <Route path="*" element={<p>There 's nothing here: 404!</p>} />
      </Routes>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
