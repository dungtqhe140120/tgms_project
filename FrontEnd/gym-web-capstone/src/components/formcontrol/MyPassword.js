import { TextField } from "@mui/material";

export default function MyPassword(props) {
  const {name, label, value, error, onChange } = props;

  return (
    <TextField
      variant="outlined"
      name={name}
      label={label}
      type="password"
      value={value}
      onChange={onChange}
      {...(error && { error: true, helperText: error })}
    />
  );
}
