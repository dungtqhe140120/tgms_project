import MyInput from "./MyInput";
import MyCheckbox from "./MyCheckbox";
import MyDatePicker from "./MyDatePicker";
import MyButton from "./MyButton";
import MySelect from "./MySelect";
import MyRadioGroup from "./MyRadioGroup";
import MyImageUpload from "./MyImageUpload";
import MyPassword from "./MyPassword";
import MyMultiInput from "./MyMultiInput";
import MySwitch from "./MySwitch";

const Controls = {
  MyInput,
  MyCheckbox,
  MyDatePicker,
  MyButton,
  MySelect,
  MyRadioGroup,
  MyImageUpload,
  MyPassword,
  MyMultiInput,
  MySwitch,
};

export default Controls;
