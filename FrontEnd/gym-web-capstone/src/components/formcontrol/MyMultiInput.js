import { TextField } from "@mui/material";

export default function MyMultiInput({
  name,
  label,
  value,
  error = null,
  onChange,
}) {
  return (
    <TextField
      label={label}
      name={name}
      multiline
      minRows={5}
      maxRows={10}
      value={value}
      onChange={onChange}
      {...(error && { error: true, helperText: error })}
    />
  );
}
