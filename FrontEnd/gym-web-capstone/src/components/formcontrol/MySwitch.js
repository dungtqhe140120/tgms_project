import { FormControlLabel, Switch } from "@mui/material";

export default function MySwitch({ name, label, value, onChange }) {
  return (
    <FormControlLabel
      control={<Switch checked={value} onChange={onChange} name={name} />}
      label={label}
    />
  );
}
