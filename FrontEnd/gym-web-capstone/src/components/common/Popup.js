import {
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Typography,
} from "@mui/material";
import './Popup.scss'
import CloseIcon from "@mui/icons-material/Close";

export default function Popup({ title, children, openPopup, setOpenPopup }) {
  return (
    <Dialog open={openPopup} maxWidth="md" className="dialog-wrapper">
      <DialogTitle className="dialog-title">
        <div style={{ display: "flex" }}>
          <Typography variant="h6" component="div" style={{ flexGrow: 1 }}>
            {title}
          </Typography>
          <Button
            onClick={() => {
              setOpenPopup(false);
            }}
          >
            <CloseIcon />
          </Button>
        </div>
      </DialogTitle>
      <DialogContent dividers>{children}</DialogContent>
    </Dialog>
  );
}
