import React, {useState} from "react";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";

const ExpandableCell = ({ value }) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <Box>
      {expanded ? value : value.slice(0, 50)}&nbsp;
      {value.length > 50 && (
        // eslint-disable-next-line jsx-a11y/anchor-is-valid
        <Link
          type="button"
          component="button"
          sx={{ fontSize: "inherit" }}
          onClick={() => setExpanded(!expanded)}
        >
          {expanded ? "view less" : "view more"}
        </Link>
      )}
    </Box>
  );
};

export default ExpandableCell;
