import React from "react";
import { Outlet } from "react-router-dom";
import Navbar from "../navbar/Navbar";
import Sidebar from "../sidebar/Sidebar";
import "./layout.scss";

const Layout = () => {
  return (
    <main className="dashboard-layout">
      <Sidebar />
      <div className="right-side-bar">
        {/* <Navbar /> */}
        <Outlet />
      </div>
    </main>
  );
};

export default Layout;
