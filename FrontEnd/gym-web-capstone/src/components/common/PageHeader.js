import React from "react";
import { Paper, Card, Typography } from "@mui/material";
import './pageHeader.scss'

export default function PageHeader({ title, subTitle, icon }) {
  return (
    <Paper elevation={0} square className="page-header-paper">
      <div className="pageHeader">
        <Card className="pageIcon">{icon}</Card>
        <div className="pageTitle">
          <Typography variant="h6" component="div">
            {title}
          </Typography>
          <Typography variant="subtitle2" component="div">
            {subTitle}
          </Typography>
        </div>
      </div>
    </Paper>
  );
}
