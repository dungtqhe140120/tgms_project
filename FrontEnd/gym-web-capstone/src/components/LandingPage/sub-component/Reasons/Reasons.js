import React from "react";
import image1 from "../../../../assets/image1.png";
import image2 from "../../../../assets/image2.png";
import image3 from "../../../../assets/image3.png";
import image4 from "../../../../assets/image4.png";
import nb from "../../../../assets/nb.png";
import adidas from "../../../../assets/adidas.png";
import nike from "../../../../assets/nike.png";
import tick from "../../../../assets/tick.png";
import "./reasons.scss";

const Reasons = () => {
  return (
    <div className="Reasons" id="reasons">
      <div className="left-r">
        <img src={image1} alt="" />
        <img src={image2} alt="" />
        <img src={image3} alt="" />
        <img src={image4} alt="" />
      </div>
      <div className="right-r">
        <span>Các lý do</span>

        <div>
          <span className="stroke-text">TẠI SAO </span>
          <span>NÊN CHỌN CHÚNG TÔI?</span>
        </div>

        <div className="details-r">
          <div>
            <img src={tick} alt=""></img>
            <span>HƠN 140+ HUẤN LUYỆN VIÊN</span>
          </div>
          <div>
            <img src={tick} alt=""></img>
            <span>TRỞ THÀNH PHIÊN BẢN TỐT NHẤT CỦA BẢN THÂN</span>
          </div>
          <div>
            <img src={tick} alt=""></img>
            <span>1 CHƯƠNG TRÌNH TẬP FREE CHO NGƯỜI MỚI</span>
          </div>
          <div>
            <img src={tick} alt=""></img>
            <span>BẠN ĐỒNG HÀNH ĐÁNG TIN CẬY</span>
          </div>
        </div>

        <span style={{
          color: "var(--gray)",
          fontWeight: "normal"
        }}>
          CÁC HÃNG KẾT NỐI
        </span>

        <div className="partners">
          <img src={nb} alt=""/>
          <img src={adidas} alt=""/>
          <img src={nike} alt=""/>
        </div>
      </div>
    </div>
  );
};

export default Reasons;
