import React from 'react'
import './header.scss'
import Logo from '../../../../assets/logo.png'

const Header = () => {
  return (
    <div className='header'>
        <img src={Logo} alt="" className='logo'/>
        <ul className='header-menu'>
            <li><a href='#hero'>Trang Chủ</a></li>
            <li><a href='#programs'>Chương trình</a></li>
            <li><a href='#reasons'>Chúng tôi</a></li>
            <li><a href='#join-us'>Tham gia</a></li>
        </ul>
    </div>
  )
}

export default Header