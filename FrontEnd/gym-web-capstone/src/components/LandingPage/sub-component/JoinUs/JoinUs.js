import React, { useRef } from "react";
import "./joinUs.scss";

const JoinUs = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();
  }

  return (
    <div className="Join" id="join-us">
      <div className="left-j">
        <hr />
        <div>
          <span className="stroke-text">BẠN ĐÃ SẴN SÀNG</span>
          <span>CẢI THIỆN</span>
        </div>
        <div>
          <span>BẢN THÂN</span>
          <span className="stroke-text">VỚI CHÚNG TÔI?</span>
        </div>
      </div>
      <div className="right-j">
        <form ref={form} action="" className="email-container" >
          <input
            type="email"
            name="user_email"
            placeholder="Nhập email của bạn"
          />
          <button className="btn btn-j">Tham gia ngay!</button>
        </form>
      </div>
    </div>
  );
};

export default JoinUs;
