import React from 'react'
import Footer from './sub-component/Footer/Footer'
import Hero from './sub-component/Hero/Hero'
import JoinUs from './sub-component/JoinUs/JoinUs'
import Programs from './sub-component/Programs/Programs'
import Reasons from './sub-component/Reasons/Reasons';
import './landingPage.scss'

const LandingPage = () => {
  return (
    <div className='landingPage'>
        <Hero/>
        <Programs/>
        <Reasons/>
        <JoinUs/>
        <Footer/>
    </div>
  )
}

export default LandingPage