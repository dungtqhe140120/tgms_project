import { IconButton } from "@mui/material";
import React, { useEffect, useState } from "react";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import MoveToInboxIcon from "@mui/icons-material/MoveToInbox";
import ErrorIcon from "@mui/icons-material/Error";
import DeleteIcon from "@mui/icons-material/Delete";
import EmailIcon from "@mui/icons-material/Email";
import WatchLaterIcon from "@mui/icons-material/WatchLater";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import LabelImportantIcon from "@mui/icons-material/LabelImportant";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { useNavigate, useParams } from "react-router-dom";
import UnfoldMoreIcon from "@mui/icons-material/UnfoldMore";
import PrintIcon from "@mui/icons-material/Print";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import "./mail.scss";

const Mail = () => {
  const navigation = useNavigate();
  const { mailId } = useParams();

  const [mail, setMail] = useState({});

  const mails = [
    {
      id: "1",
      title: "Email1",
      subject: "Hey i am email 1!!!",
      message: "This is a test",
      time: "10pm",
    },
    {
      id: "2",
      title: "Email1",
      subject: "Hey i am email 2!!!",
      message: "This is a test",
      time: "11pm",
    },
  ];

  useEffect(() => {
    const mailFind = mails.filter((mailElement) => mailElement.id == mailId);
    setMail(mailFind);
    console.log("current mail:", mail);
    // return () => {};
  }, [mailId]);

  return (
    <div className="mail content-notification">
      <div className="mail_tools">
        <div className="mail_toolsLeft">
          <IconButton onClick={() => navigation("/", { replace: true })}>
            <ArrowBackIcon />
          </IconButton>
          <IconButton>
            <MoveToInboxIcon />
          </IconButton>
          <IconButton>
            <ErrorIcon />
          </IconButton>
          <IconButton>
            <DeleteIcon />
          </IconButton>
          <IconButton>
            <EmailIcon />
          </IconButton>
          <IconButton>
            <WatchLaterIcon />
          </IconButton>
          <IconButton>
            <CheckCircleIcon />
          </IconButton>
          <IconButton>
            <LabelImportantIcon />
          </IconButton>
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        </div>
        <div className="mail_toolsRight">
          <IconButton>
            <UnfoldMoreIcon />
          </IconButton>
          <IconButton>
            <PrintIcon />
          </IconButton>
          <IconButton>
            <ExitToAppIcon />
          </IconButton>
        </div>
      </div>
      <div className="mail_body">
        <div className="mail_bodyHeader">
          <h2>{mail.subject}</h2>
          <LabelImportantIcon className="mail_important" />
          <p>Title</p>
          <p className="mail_time">{mail.time}</p>
        </div>

        <div className="mail_message">
          <p>{mail.message}</p>
        </div>
      </div>
    </div>
  );
};

export default Mail;
