import React from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
} from "@mui/material";

const UserProfileDetail = ({
  fullname,
  email,
  phone,
  address,
  gender,
  birthday,
}) => {
  console.log("Fullname UserProfileDetail:", fullname);

  return (
    <form autoComplete="off" noValidate>
      <Card>
        <CardHeader
          subheader="Thông tin bên dưới là chỉ đọc"
          title="Thông tin cá nhân"
        />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Họ Tên"
                value={fullname}
                variant="outlined"
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Email"
                value={email}
                variant="outlined"
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Số Điện Thoại"
                value={phone}
                variant="outlined"
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Ngày sinh"
                value={birthday}
                variant="outlined"
                InputProps={{
                  readOnly: true,
                }}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </form>
  );
};

export default UserProfileDetail;
