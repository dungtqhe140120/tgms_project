import React from "react";
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  Typography,
} from "@mui/material";

const UserProfile = ({ fullname, avatar, display1, display2 }) => {
  return (
    <Card>
      <CardContent>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Avatar
            src={avatar}
            sx={{
              height: 64,
              mb: 2,
              width: 64,
            }}
          />
          <Typography color="textPrimary" gutterBottom variant="h5">
            {fullname}
          </Typography>
          <Typography color="HighlightText" variant="body2">
            {display1}
          </Typography>
          <Typography color="textSecondary" variant="body2">
            {display2}
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
};

export default UserProfile;
