import React, { useState, useRef, useEffect } from "react";
import "./LoginPage.css";
import { connect } from "react-redux";
import { login } from "../../redux";
import { useNavigate, useLocation } from "react-router-dom";
import {CircularProgress} from "@mui/material";

const LoginPage = ({ auth, login }) => {
  let navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || "/dashboard";

  const emailRef = useRef();
  const errRef = useRef();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errMsg, setErrMsg] = useState("");

  useEffect(() => {
    emailRef.current.focus();
  }, []);

  useEffect(() => {
    setErrMsg("");
  }, [email, password]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let loginStatus = await login(email, password);
      if (loginStatus) {
        navigate(from, { replace: true });
      } else {
        setErrMsg(auth.error.message)
        errRef.current.focus();
      }
    } catch (err) {
      setErrMsg("No Server Response");
      errRef.current.focus();
    }
  };

  return (
    <div className="login">
      <p
        ref={errRef}
        className={errMsg ? "errmsg" : "offscreen"}
        aria-live="assertive"
      >
        {errMsg}
      </p>
      <form className="login_form" onSubmit={(e) => handleSubmit(e)}>
        <h1>Đăng nhập ở đây!</h1>
        <input
          ref={emailRef}
          type="text"
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button type="submit" className="submit_btn">
          {auth?.loading ? <CircularProgress/> : 'Đăng Nhập'}
        </button>
      </form>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (email, password) => dispatch(login(email, password)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
