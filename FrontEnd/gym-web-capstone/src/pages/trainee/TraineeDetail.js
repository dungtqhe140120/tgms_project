import React, {useEffect, useState} from "react";
import { useParams } from "react-router-dom";
import PageHeader from "../../components/common/PageHeader";
import { Paper } from "@mui/material";
import PeopleOutlineIcon from "@mui/icons-material/PeopleOutline";
import TraineeForm from "./TraineeForm";

const TraineeDetail = () => {
  const { traineeId } = useParams();
  const [title, setTitle] = useState("Thêm mới người tập");

  useEffect(() => {
    if (traineeId !== "add") {
      setTitle("Chỉnh sửa người tập");
    }
  }, []);

  return (
    <div>
      <PageHeader
        title={title}
        subTitle="Mẫu điền thông tin của người tập"
        icon={<PeopleOutlineIcon fontSize="large" />}
      />
      <Paper className="trainee-detail-paper page-child">
        <TraineeForm traineeId={traineeId} />
      </Paper>
    </div>
  );
};

export default TraineeDetail;
