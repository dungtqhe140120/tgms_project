import React, { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import { Grid } from "@mui/material";
import "react-toastify/dist/ReactToastify.css";
import Controls from "../../components/formcontrol/Controls";
import { MyForm, useMyForm } from "../employee/useFormEmployee";
import API from "../../API";
import {
  checkDateLessThanToday,
  checkDateMoreThanToday,
  formatYYYYMMdd,
} from "../../helpers";

const initialFValues = {
  id: 0,
  login: "",
  firstName: "",
  lastName: "",
  email: "",
  phone: "",
  address: "",
  gender: 1,
  password: "",
  dob: new Date(),
  expired_date: new Date(),
  image: null,
};

const genderItems = [
  { id: 1, title: "Nam" },
  { id: 0, title: "Nữ" },
];

const passwordRegex = new RegExp(
  "^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})"
);

const registerTrainee = async (values) => {
  let urlImage = null;
  if (values.image) {
    urlImage = await uploadImage(values.image);
  }

  let data = {
    login: values.login,
    firstName: values.firstName,
    lastName: values.lastName,
    email: values.email,
    imageUrl: urlImage,
    actived: "true",
    langKey: "vi",
    password: values.password,
    authorities: ["ROLE_TRAINEE"],
    gender: values.gender,
    birthDay: formatYYYYMMdd(values.dob),
    phone: values.phone,
    address: values.address,
  };

  let exp_date = formatYYYYMMdd(values.expired_date);

  return await API.postRequest(
    `http://101.99.6.31:8887/api/v2/register?expired_Date=${exp_date}`,
    data
  );
};

const updateTrainee = async (values, traineeId) => {
  let urlImage = values.imageUrl;
  if (values.image) {
    urlImage = await uploadImage(values.image);
  }
  let data = {
    id: traineeId,
    login: values.login,
    firstName: values.firstName,
    lastName: values.lastName,
    email: values.email,
    imageUrl: urlImage,
    gender: values.gender,
    birthDay: formatYYYYMMdd(values.dob),
    phone: values.phone,
    address: values.address,
    expireDate: formatYYYYMMdd(values.expired_date),
  };
  return await API.putRequest(
    `http://101.99.6.31:8887/api/v2/trainees/detail-profile/${traineeId}`,
    data
  );
};

const fetchTraineeById = async (traineeId) => {
  return await API.fetchData(
    `http://101.99.6.31:8887/api/v2/trainees/detail-profile/${traineeId}`
  );
};

const uploadImage = async (image_file) => {
  let formData = new FormData();
  formData.append("file", image_file);
  let result = await API.postForm(
    "http://101.99.6.31:8887/api/uploadImage",
    formData
  );
  return result.data;
};

const TraineeForm = ({ traineeId }) => {
  const [formData, setFormData] = useState(initialFValues);
  const [isAdd, setIsAdd] = useState(true);
  const [btnAction, setBtnAction] = useState("Thêm mới");

  useEffect(() => {
    if (traineeId !== "add") {
      setIsAdd(false);
      setBtnAction("Chỉnh sửa");
      (async () => {
        const result = await fetchTraineeById(traineeId);
        const data = result.data;
        let dataTemp = {
          ...data,
          dob: new Date(data.birthDay),
          expired_date: new Date(data.expireDate),
        };
        setFormData(dataTemp);
        setValues(dataTemp);
      })();
    } else {
      setIsAdd(true);
    }
  }, []);

  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("login" in fieldValues)
      temp.login = fieldValues.login ? "" : "Yêu cầu bạn nhập nickname.";
    if ("firstName" in fieldValues)
      temp.firstName = fieldValues.firstName ? "" : "Yêu cầu bạn nhập Họ.";
    if ("lastName" in fieldValues)
      temp.lastName = fieldValues.lastName ? "" : "Yêu cầu bạn nhập Tên.";
    if ("email" in fieldValues)
      if (!fieldValues.email) {
        temp.email = "Yêu cầu bạn nhập email.";
      } else {
        temp.email = /$^|.+@.+..+/.test(fieldValues.email)
          ? ""
          : "Email không hợp lệ.";
      }
    if ("password" in fieldValues)
      temp.password = passwordRegex.test(fieldValues.password)
        ? ""
        : "Mật khẩu chứa ít nhất 6 ký tự, 1 chữ số, 1 chữ hoa, 1 chữ thường, 1 ký tự đăc biệt.";
    if ("phone" in fieldValues)
      temp.phone =
        fieldValues.phone.length > 9 ? "" : "Số điện thoại cần ít nhất 10 số.";
    if ("dob" in fieldValues)
      temp.dob = checkDateLessThanToday(fieldValues.dob)
        ? ""
        : "Ngày sinh phải nhỏ hơn hôm nay.";
    if ("expired_date" in fieldValues) {
      temp.expired_date = checkDateMoreThanToday(fieldValues.expired_date)
        ? ""
        : "Ngày hết hạn phải lớn hôm hôm nay";
    }
    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };

  const { values, setValues, errors, setErrors, handleInputChange, resetForm } =
    useMyForm(formData, true, validate);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      if (isAdd) {
        // todo: do service
        registerTrainee(values)
          .then((result) => {
            resetForm();
            toast.success("Thêm mới nhân viên thành công!");
          })
          .catch((error) => {
            toast.error("Thêm mới nhân viên thất bại!");
          });
      } else {
        updateTrainee(values, traineeId)
          .then((result) => {
            toast.success("Chỉnh sửa huấn luyện viên thành công!");
            const data = result.data;
            let dataTemp = {
              ...data,
              dob: new Date(data.birthDay),
              expired_date: new Date(data.expireDate),
            };
            setFormData(dataTemp);
            setValues(dataTemp);
          })
          .catch((error) => {
            toast.error("Chỉnh sửa huấn luyện viên viên thất bại!");
          });
      }
    }
  };

  return (
    <>
      <ToastContainer />
      <MyForm>
        <Grid container>
          <Grid item xs={6}>
            {isAdd && (
              <Controls.MyInput
                name="login"
                label="Nickname"
                value={values.login}
                onChange={handleInputChange}
                error={errors.login}
              />
            )}
            <Controls.MyInput
              name="firstName"
              label="Họ"
              value={values.firstName}
              onChange={handleInputChange}
              error={errors.firstName}
            />
            <Controls.MyInput
              name="lastName"
              label="Tên"
              value={values.lastName}
              onChange={handleInputChange}
              error={errors.lastName}
            />
            <Controls.MyInput
              name="email"
              label="Email"
              value={values.email}
              onChange={handleInputChange}
              error={errors.email}
            />
            <Controls.MyInput
              name="phone"
              label="Số điện thoại"
              value={values.phone}
              onChange={handleInputChange}
              error={errors.phone}
            />
            <Controls.MyInput
              name="address"
              label="Địa chỉ"
              value={values.address}
              onChange={handleInputChange}
            />
          </Grid>
          <Grid item xs={6}>
            {isAdd && (
              <Controls.MyPassword
                name="password"
                label="Mật khẩu"
                value={values.password}
                onChange={handleInputChange}
                error={errors.password}
              />
            )}

            <Controls.MyRadioGroup
              name="gender"
              label="Giới tính"
              value={values.gender}
              onChange={handleInputChange}
              items={genderItems}
            />

            <Controls.MyDatePicker
              name="dob"
              label="Ngày sinh"
              value={values.dob}
              onChange={handleInputChange}
              error={errors.dob}
            />

            <Controls.MyDatePicker
              name="expired_date"
              label="Ngày account hết hạn"
              value={values.expired_date}
              onChange={handleInputChange}
              error={errors.expired_date}
            />

            <Controls.MyImageUpload
              name="image"
              label="Ảnh cá nhân"
              id="image-upload-employee"
              value={values.image}
              onChange={handleInputChange}
              imageLink={values.imageUrl}
              style={{
                paddingTop: "15px",
                paddingBottom: "15px",
              }}
            />
            <div>
              <Controls.MyButton
                type="submit"
                variant="contained"
                text={btnAction}
                color="primary"
                style={{ marginRight: "15px" }}
                onClick={handleSubmit}
              />
              <Controls.MyButton
                text="Hủy bỏ"
                color="warning"
                onClick={resetForm}
              />
            </div>
          </Grid>
        </Grid>
      </MyForm>
    </>
  );
};

export default TraineeForm;
