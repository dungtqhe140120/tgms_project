import React, { useState } from "react";
import { useEmployeeEffect } from "../../hooks/useEmployeeFetch";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { Link, useNavigate } from "react-router-dom";
import "./employeeList.scss";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import DeleteIcon from "@mui/icons-material/Delete";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Paper,
  Toolbar,
} from "@mui/material";
import API from "../../API";
import PageHeader from "../../components/common/PageHeader";
import Controls from "../../components/formcontrol/Controls";
import ErrorPage from "../error/ErrorPage";
import PersonNoImage from "../../assets/employee-no-image.webp";
import VisibilityIcon from "@mui/icons-material/Visibility";

const employeesColumns = [
  {
    field: "id",
    headerName: "ID",
    minWidth: 60,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "fullName",
    headerName: "Họ Tên",
    minWidth: 180,
    renderCell: (params) => {
      return (
        <div className="cellWithImg">
          <img
            className="cellImg"
            src={params?.row?.avatar || PersonNoImage}
            alt="avatar"
          />
          {params.row.fullName}
        </div>
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "gender",
    headerName: "Giới Tính",
    minWidth: 80,
    renderCell: (params) => {
      return params.row.gender === 1 ? (
        <span>Nam</span>
      ) : params.row.gender === 0 ? (
        <span>Nữ</span>
      ) : (
        <span>Chưa rõ</span>
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "birthday",
    headerName: "Ngày Sinh",
    minWidth: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "phone",
    headerName: "Số Điện Thoại",
    minWidth: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "email",
    headerName: "Email",
    minWidth: 220,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "address",
    headerName: "Địa Chỉ",
    minWidth: 150,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "joinDate",
    headerName: "Ngày Tham Gia",
    minWidth: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
];

const EmployeeList = () => {
  const navigation = useNavigate();

  const { state, loading, error, setState } = useEmployeeEffect();

  const [open, setOpen] = useState(false);
  const [currentId, setCurrentId] = useState("");

  const onDeleting = (id) => {
    setOpen(true);
    setCurrentId(id);
  };

  const handleDelete = async () => {
    API.delete(`http://101.99.6.31:8887/api/employees/${currentId}`)
      .then((response) => {
        setOpen(false);
        toast.success("Xóa nhân viên thành công!");
        const newState = {
          ...state,
          results: state.results.filter((item) => item.id !== currentId),
        };
        setState(newState);
      })
      .catch((error) => {
        setOpen(false);
        toast.error("Lỗi khi xóa nhân viên thất bại!");
      });
  };

  const handleClose = () => {
    setOpen(false);
  };

  const actionColumn = [
    {
      field: "action",
      headerName: "Thao Tác",
      minWidth: 100,
      renderCell: (params) => {
        return (
          <div className="cellAction">
            {/* <Link
              to={`/dashboard/employees/${params.row.id}`}
              style={{ textDecoration: "none" }}
            >
              <div className="viewButton">View</div>
            </Link> */}
            <Button onClick={() => {}}>
              <VisibilityIcon size="small" />
            </Button>
            <div
              className="deleteButton"
              onClick={() => onDeleting(params.row.id)}
            >
              <DeleteIcon size="small" />
            </div>
          </div>
        );
      },
      headerClassName: "header-mui-custome",
      cellClassName: "cell-mui-custome",
      flex: 1,
    },
  ];

  if (error) return <ErrorPage />;
  return (
    <div className="employee-list">
      <ToastContainer />
      <PageHeader
        title="Quản lý nhân viên"
        subTitle="Màn hình giúp thêm sửa xóa nhân viên"
        icon={<ManageAccountsIcon fontSize="large" />}
      />
      <Paper className="page-child">
        <Toolbar>
          <Controls.MyButton
            text="Thêm nhân viên mới"
            size="small"
            style={{
              marginLeft: "auto",
              paddingTop: "9px",
              paddingBottom: "9px",
            }}
            onClick={() => {
              navigation("/dashboard/employees/add", { replace: true });
            }}
          />
        </Toolbar>
        <Box
          sx={{
            height: 800,
            width: "100%",
            "& .header-mui-custome": {
              backgroundColor: "gray",
              color: "white",
              fontSize: "17px",
            },
            "& .cell-mui-custome": {
              fontSize: "13px",
            },
          }}
        >
          <DataGrid
            rows={state.results}
            columns={employeesColumns.concat(actionColumn)}
            loading={loading}
            getRowHeight={() => "auto"}
            disableSelectionOnClick
            localeText={{
              toolbarColumns: "Các trường hiển thị",
              toolbarFilters: "Tìm kiếm theo",
              toolbarDensity: "Độ rộng các dòng",
              toolbarExport: "Xuất file",
            }}
            components={{ Toolbar: GridToolbar }}
            getRowHeight={() => "auto"}
          />
          <Dialog open={open} keepMounted onClose={handleClose}>
            <DialogTitle>{"Xác nhận xóa?"}</DialogTitle>
            <DialogContent>
              Bạn có muốn xóa nhân viên mã {currentId}?
            </DialogContent>
            <DialogActions>
              <Button onClick={handleDelete}>Xóa</Button>
              <Button onClick={handleClose} style={{ color: "red" }}>
                Hủy bỏ
              </Button>
            </DialogActions>
          </Dialog>
        </Box>
      </Paper>
    </div>
  );
};

export default EmployeeList;
