import React, { useState } from "react";
import Header from "../../components/notification/Header";
import SidebarNotification from "../../components/notification/Sidebar-notification";
import "./notificationManament.scss";
import { Outlet } from "react-router-dom";
import SendEmail from "../../components/notification/SendEmail";

const NotificationManagement = () => {
  const [show, setShow] = useState(false);

  return (
    <div className="notification-management">
      <div className="notification-left-side">
        <SidebarNotification show={show} setShow={setShow} />
      </div>
      <div className="notification-right-side">
        <Header />
        <Outlet />
        {show && <SendEmail />}
      </div>
    </div>
  );
};

export default NotificationManagement;
