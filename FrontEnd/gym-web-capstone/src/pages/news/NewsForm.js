import { useEffect } from "react";
import Controls from "../../components/formcontrol/Controls";
import { MyForm, useMyForm } from "../employee/useFormEmployee";
import { Grid } from "@mui/material";

const initialFValues = {
  id: 0,
  title: "",
  short_description: "",
  content: "",
  image: null,
  image_url: "",
  is_display: true,
};

export default function NewsForm({ addOrEdit, recordForEdit }) {

  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("title" in fieldValues)
      temp.title = fieldValues.title ? "" : "Yêu cầu bạn nhập tiêu đề.";
    if ("short_description" in fieldValues)
      temp.short_description = fieldValues.short_description
        ? ""
        : "Yêu cầu bạn nhập mô tả ngắn.";
    if ("content" in fieldValues)
      temp.content = fieldValues.content ? "" : "Yêu cầu bạn nhập nội dung.";
    if ("image" in fieldValues)
      temp.image = (values.id!=0 || (values.id==0 && fieldValues.image)) ? "" : "Yêu cầu bạn nhập ảnh.";
    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };

  const { values, setValues, errors, setErrors, handleInputChange, resetForm } =
    useMyForm(initialFValues, true, validate);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      addOrEdit(values, resetForm);
    }
  };

  useEffect(() => {
    if (recordForEdit != null)
      setValues({
        ...recordForEdit,
        image: null,
        image_url: recordForEdit.image,
      });
  }, [recordForEdit]);

  return (
    <MyForm onSubmit={handleSubmit}>
      <Grid container>
        <Grid item xs={6}>
          <Controls.MyInput
            name="title"
            label="Tiêu đề"
            fullWidth
            value={values.title}
            onChange={handleInputChange}
            error={errors.title}
            style = {{width: 100}}
          />
          <Controls.MyMultiInput
            name="short_description"
            label="Mô tả ngắn"
            value={values.short_description}
            onChange={handleInputChange}
            error={errors.short_description}
          />
          <Controls.MyMultiInput
            name="content"
            label="Nội dung"
            value={values.content}
            onChange={handleInputChange}
            error={errors.content}
          />
        </Grid>
        <Grid item xs={6}>
          <Controls.MySwitch
            name="is_display"
            label="Hiển thị"
            value={values.is_display}
            onChange={handleInputChange}
          />
          <Controls.MyImageUpload
            name="image"
            label="Ảnh"
            id="image-upload-news"
            onChange={handleInputChange}
            value={values.image}
            imageLink={values.image_url}
            style={{
              paddingTop: "15px",
              paddingBottom: "15px",
            }}
            error={errors.image}
          />
          <Controls.MyButton
            type="submit"
            variant="contained"
            text="Thêm mới"
            color="primary"
            style={{ marginRight: "15px" }}
            onClick={handleSubmit}
          />
          <Controls.MyButton
            text="Hủy bỏ"
            color="warning"
            onClick={resetForm}
          />
        </Grid>
      </Grid>
    </MyForm>
  );
}
