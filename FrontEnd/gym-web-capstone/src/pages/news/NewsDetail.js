import React from "react";
import { useParams } from "react-router-dom";

const NewsDetail = () => {
  const { newsId } = useParams();

  return <div>NewsDetail ${newsId}</div>;
};

export default NewsDetail;
