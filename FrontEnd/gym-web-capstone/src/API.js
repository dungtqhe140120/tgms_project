import axios from "axios";
import { isPersistedState } from "./helpers";

const apiSettings = {
  fetchData: async (endpoint) => {
    const token = isPersistedState("auth")?.id_token;
    return await axios.get(endpoint, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  postRequest: async (endpoint, data) => {
    const token = isPersistedState("auth")?.id_token;
    return await axios.post(endpoint, data, {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    });
  },
  postForm: async (endpoint, formData) => {
    const token = isPersistedState("auth")?.id_token;
    return await axios({
      method: "post",
      url: endpoint,
      data: formData,
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "multipart/form-data",
      },
    });
  },
  delete: async (endpoint) => {
    const token = isPersistedState("auth")?.id_token;
    return await axios.delete(endpoint, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  putRequest: async (endpoint, data) => {
    const token = isPersistedState("auth")?.id_token;
    return await axios.put(endpoint, data, {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    });
  },
};

export default apiSettings;
