export const BASE_URL = "http://101.99.6.31:8887";

export const LOGIN_URL = `${BASE_URL}/api/authenticate`;
export const REGISTER_FCM = `${BASE_URL}/api/employees/fcm`;