import React, { useEffect, useState } from "react";
import PageHeader from "../../components/common/PageHeader";
import GavelIcon from "@mui/icons-material/Gavel";
import {
  Autocomplete,
  InputAdornment,
  Paper,
  Stack,
  TextField,
  Toolbar,
  Chip,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Box,
} from "@mui/material";
import "./contractors.scss";
import { useContractFetch } from "../../hooks/useContractFetch";
import {
  DataGrid,
  GridToolbarColumnsButton,
  GridToolbarContainer,
  GridToolbarExport,
} from "@mui/x-data-grid";
import Controls from "../../components/formcontrol/Controls";
import FitnessCenterIcon from "@mui/icons-material/FitnessCenter";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import ErrorPage from "../error/ErrorPage";
import DoneIcon from "@mui/icons-material/Done";
import CancelIcon from "@mui/icons-material/Cancel";
import PaymentIcon from "@mui/icons-material/Payment";
import HourglassTopIcon from "@mui/icons-material/HourglassTop";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import API from "./../../API";
import { BASE_URL } from "../../config";
import NoRow from "../../components/common/NoRow";
import DialogContractor from "./DialogContractor";

const contractorsColumns = [
  {
    field: "id",
    headerName: "ID",
    width: 80,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "traineeName",
    headerName: "Tên người tập",
    width: 180,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "ptName",
    headerName: "Tên huấn luyện viên",
    width: 180,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "registerDate",
    headerName: "Ngày bắt đầu",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "expireDate",
    headerName: "Ngày kêt thúc",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "packageRegister",
    headerName: "Gói tập",
    width: 50,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "confirmStatus",
    headerName: "Trạng Thái",
    width: 150,
    renderCell: (params) => {
      if (params.row.confirmStatus === 0) {
        return <Chip label="Đang chờ xử lý" icon={<HourglassTopIcon />} />;
      } else if (params.row.confirmStatus === 1) {
        return (
          <Chip
            label="Đã xác nhận"
            style={{ backgroundColor: "#a1f5a1" }}
            icon={<DoneIcon />}
          />
        );
      }
      return (
        <Chip
          label="Đã từ chối"
          style={{ backgroundColor: "#ff7a7a" }}
          icon={<CancelIcon />}
        />
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "confirmBy",
    headerName: "Người xác nhận",
    width: 150,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "confirmAt",
    headerName: "Ngày xác nhận",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
];

const updateConfirm = async (confirmOne) => {
  return API.putRequest(
    `${BASE_URL}/api/trainee-and-pts/${confirmOne?.traineeAndPtId}/confirm`,
    confirmOne
  );
};

const LIST_STATUS = [
  { value: 0, label: "Đang chờ xử lý", style: {} },
  {
    value: 1,
    label: "Đã xác nhận",
    style: { backgroundColor: "#a1f5a1" },
  },
  {
    value: 2,
    label: "Đã từ chối",
    style: { backgroundColor: "#ff7a7a" },
  },
];

const ConfirmStatusFilter = ({ value, onChange }) => {
  return (
    <Stack spacing={3} sx={{ width: 400 }}>
      <Autocomplete
        multiple
        size="small"
        options={LIST_STATUS}
        getOptionLabel={(option) => option.label}
        value={value}
        onChange={(event, newValue) => {
          onChange(newValue);
        }}
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              variant="outlined"
              size="small"
              style={option.style}
              label={option.label}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            label="Trạng Thái"
            placeholder="Trạng thái yêu cầu"
          />
        )}
      />
    </Stack>
  );
};

const ContractorToolbar = () => {
  return (
    <GridToolbarContainer>
      <GridToolbarColumnsButton />
      <GridToolbarExport />
    </GridToolbarContainer>
  );
};

const Contractors = () => {
  const {
    state,
    loading,
    error,
    page,
    pageSize,
    nameTrainee,
    statusConfirm,
    namePT,
    setState,
    setPage,
    setPageSize,
    setNameTrainee,
    setStatusConfirm,
    setNamePT,
    fetchContract,
  } = useContractFetch();

  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [currentAction, setCurrentAction] = useState(null);

  // Contractor Dialog
  const [displayContractorDialog, setDisplayContractorDialog] = useState(false);
  const [currentTransactionId, setCurrentTransactionId] = useState(0);

  const handleCloseContractDialog = () => {
    setDisplayContractorDialog(false);
  };

  useEffect(() => {
    document.title = "Xác nhận giao dịch";
  }, []);

  const doUpdate = async (confirmOne) => {
    updateConfirm(confirmOne)
      .then(({ data }) => {
        let index = state.results.findIndex((ele) => ele.id === data.id);
        let newResults = [...state.results];
        newResults[index] = {
          ...newResults[index],
          confirmStatus: data.confirm_status,
          confirmAt: data.confirm_date,
        };
        setState((prevState) => ({
          ...prevState,
          results: newResults,
        }));
        toast.success("Chỉnh sửa thành công!");
      })
      .catch((error) => {
        toast.error(error?.response?.data?.title || "Chỉnh sửa thất bại!");
      })
      .finally(() => {
        setOpen(false);
      });
  };

  const updateColumn = (contractors, confirmStatus) => {
    let newCurrentAction = {
      traineeAndPtId: contractors.id,
      confirmStatus: confirmStatus,
    };
    setCurrentAction(newCurrentAction);
    if (contractors.confirmStatus !== 0) {
      setTitle("Xác nhận ");
      setContent(
        "Bạn có muốn cập nhật thành " +
          (contractors.confirmStatus === 1 ? "Từ chối" : "Xác nhận")
      );
      setOpen(true);
      return;
    }
    doUpdate(newCurrentAction);
  };

  const handleConfirm = () => {
    doUpdate(currentAction);
  };

  const handleCancel = () => {
    setOpen(false);
  };

  const ChipUIs = ({params}) => {
    return params?.row?.confirmStatus === 0 ? (
      <Stack direction="row" spacing={1}>
        <Chip
          label="Xác nhận"
          icon={<DoneIcon />}
          onClick={() => {
            updateColumn(params.row, 1);
          }}
        />
        <Chip
          label="Từ chối"
          icon={<CancelIcon />}
          onClick={() => {
            updateColumn(params.row, 2);
          }}
        />
      </Stack>
    ) : params?.row?.confirmStatus === 1 ? (
      <Stack direction="row" spacing={1}>
        <Chip
          label="Từ chối"
          icon={<CancelIcon />}
          onClick={() => {
            updateColumn(params.row, 2);
          }}
        />
      </Stack>
    ) : (
      <Stack direction="row" spacing={1}>
        <Chip
          label="Xác nhận"
          icon={<DoneIcon />}
          onClick={() => {
            updateColumn(params.row, 1);
          }}
        />
      </Stack>
    );
  };

  const ViewTransaction = ({params}) => {
    return params?.row?.transactionId ? (
      <Button
        onClick={() => {
          setCurrentTransactionId(params?.row?.id);
          setDisplayContractorDialog(true);
        }}
      >
        <PaymentIcon size="small" />
      </Button>
    ) : (
      <></>
    );
  };

  const actionColumns = [
    {
      field: "action",
      headerName: "Thao tác",
      width: 220,
      renderCell: (params) => {
        return (
          <div className="cellAction">
            <ViewTransaction params={params}/>
            <ChipUIs params={params}/>
          </div>
        );
      },
      headerClassName: "header-mui-custome",
    },
  ];

  const handleClose = () => {
    setOpen(false);
  };

  const handleSearch = () => {
    if (page !== 0) {
      //trigger by hook
      setPage(0);
      return;
    }
    fetchContract(page, pageSize, nameTrainee, statusConfirm, namePT);
  };
  if (error) return <ErrorPage />;
  return (
    <>
      <ToastContainer />
      <PageHeader
        title="Quản lý giao dịch"
        subTitle="Màn giúp quản lý giao dịch giữa huấn luyện viên và người tập"
        icon={<GavelIcon fontSize="large" />}
      />
      <Paper className="page-child">
        <Toolbar>
          <Stack spacing={1} direction="row">
            <Controls.MyInput
              label="Tìm kiếm theo tên người tập"
              className="trainee-search-contractor"
              size="small"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <PersonOutlineIcon />
                  </InputAdornment>
                ),
              }}
              onChange={(event) => {
                setNameTrainee(event.target.value);
              }}
            />
            <Controls.MyInput
              label="Tìm kiếm theo tên huấn luyện viên"
              className="pt-search-contractor"
              size="small"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <FitnessCenterIcon />
                  </InputAdornment>
                ),
              }}
              onChange={(event) => {
                setNamePT(event.target.event);
              }}
            />
            <ConfirmStatusFilter
              value={statusConfirm}
              onChange={setStatusConfirm}
            />
            <Controls.MyButton
              text="Tìm kiếm"
              onClick={handleSearch}
              size="small"
            />
          </Stack>
        </Toolbar>

        <Box
          sx={{
            height: 600,
            width: "100%",
            "& .header-mui-custome": {
              backgroundColor: "gray",
              color: "white",
              fontSize: "17px",
            },
            "& .cell-mui-custome": {
              fontSize: "13px",
            },
          }}
        >
          <DataGrid
            rows={state.results}
            rowCount={state.totol_results}
            loading={loading}
            rowsPerPageOptions={[10, 25, 50]}
            pagination
            page={state.page}
            pageSize={state.page_size}
            paginationMode="server"
            onPageChange={(newPage) => setPage(newPage)}
            onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
            columns={contractorsColumns.concat(actionColumns)}
            localeText={{
              toolbarColumns: "Trường hiển thị",
              toolbarExport: "Xuất file",
            }}
            components={{
              Toolbar: ContractorToolbar,
              NoRowsOverlay: NoRow,
            }}
            disableSelectionOnClick
          />
          <Dialog open={open} keepMounted onClose={handleClose}>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>{content}</DialogContent>
            <DialogActions>
              <Button onClick={handleConfirm}>Xác nhận</Button>
              <Button onClick={handleCancel}>Hủy bỏ</Button>
            </DialogActions>
          </Dialog>

          <DialogContractor
            open={displayContractorDialog}
            handleClose={handleCloseContractDialog}
            transactionId={currentTransactionId}
          />
        </Box>
      </Paper>
    </>
  );
};

export default Contractors;
