import React, { useEffect, useState } from "react";
import { BASE_URL } from "../../config";
import API from "../../API";

export const useDialogContractor = (transactionId) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [transaction, setTransaction] = useState({});

  const fetchTransactionRegisterPt = async (transactionId) => {
    try {
      setError(false);
      setLoading(true);
      if (transactionId !== 0) {
        let endpoint = `${BASE_URL}/api/momo/register-pt/${transactionId}`;
        const result = await API.fetchData(endpoint);
        const data = result.data;
        setTransaction(data);
      }
    } catch (error) {
      setError(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchTransactionRegisterPt(transactionId);
  }, [transactionId]);

  return {
    transaction,
    loading,
    error,
  };
};
