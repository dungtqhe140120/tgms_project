import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CircularProgress,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
} from "@mui/material";
import React from "react";
import ErrorPage from "../error/ErrorPage";
import { useDialogContractor } from "./useDialogContractor";
import PersonNoImage from "../../assets/employee-no-image.webp";
import "./dialogContractor.scss";
import LogoMomo from "../../assets/logo-momo.png";

const DialogContractor = ({ open, handleClose, transactionId }) => {
  const { transaction, loading, error } = useDialogContractor(transactionId);

  if (loading) {
    return (
      <Box sx={{ display: "flex" }}>
        <CircularProgress />
      </Box>
    );
  }

  if (error) {
    return <ErrorPage />;
  }

  return (
    <>
      <Dialog maxWidth="lg" open={open} keepMounted onClose={handleClose}>
        <DialogTitle>
          <div className="logo-wrap">
            <img src={LogoMomo} alt="momo" className="logo-momo" />
            <span className="logo-title">Giao dịch Momo</span>
          </div>
        </DialogTitle>
        <DialogContent className="payment-container">
          <Box sx={{ flexGrow: 1, py: 8 }}>
            <Container maxWidth="lg">
              <Grid container spacing={3}>
                {/* Transaction Info */}
                <Grid item lg={12} md={12} xs={12}>
                  <Box
                    sx={{
                      "& .MuiTextField-root": { m: 1 },
                    }}
                  >
                    <TextField
                      fullWidth
                      label="Kết quả"
                      value={transaction?.transactionId || "Chưa rõ"}
                      variant="outlined"
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                    <TextField
                      fullWidth
                      label="Số Tiền"
                      value={transaction?.amount || "Chưa rõ"}
                      variant="outlined"
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                    <TextField
                      fullWidth
                      label="Thông Tin Thanh Toán"
                      value={transaction?.transactionInfo || "Chưa rõ"}
                      variant="outlined"
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                    <TextField
                      fullWidth
                      label="Kết quả"
                      value={transaction?.message || "Chưa rõ"}
                      variant="outlined"
                      InputProps={{
                        readOnly: true,
                      }}
                    />
                  </Box>
                </Grid>
                {/* Trainee Info */}
                <Grid item lg={6} md={12} xs={12}>
                  <Card>
                    <CardContent>
                      <Box
                        sx={{
                          alignItems: "center",
                          display: "flex",
                          flexDirection: "column",
                        }}
                      >
                        <Avatar
                          src={
                            transaction?.trainee?.profile?.avatar ||
                            PersonNoImage
                          }
                          sx={{
                            height: 84,
                            mb: 2,
                            width: 84,
                          }}
                        />
                      </Box>
                      <Box
                        sx={{
                          "& .MuiTextField-root": { m: 1 },
                        }}
                      >
                        <TextField
                          fullWidth
                          label="Họ Tên"
                          value={
                            transaction?.trainee?.profile?.fullname || "Chưa rõ"
                          }
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                        <TextField
                          fullWidth
                          label="Số Điện Thoại"
                          value={
                            transaction?.trainee?.profile?.phone || "Chưa rõ"
                          }
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                        <TextField
                          fullWidth
                          label="Địa Chỉ"
                          value={
                            transaction?.trainee?.profile?.address || "Chưa rõ"
                          }
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                        <TextField
                          fullWidth
                          label="Ngày Sinh"
                          value={
                            transaction?.trainee?.profile?.birthday || "Chưa rõ"
                          }
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Box>
                    </CardContent>
                  </Card>
                </Grid>
                {/* PersonalTrainer Info */}
                <Grid item lg={6} md={12} xs={12}>
                  <Card>
                    <CardContent>
                      <Box
                        sx={{
                          alignItems: "center",
                          display: "flex",
                          flexDirection: "column",
                        }}
                      >
                        <Avatar
                          src={
                            transaction?.personalTrainer?.profile?.avatar ||
                            PersonNoImage
                          }
                          sx={{
                            height: 84,
                            mb: 2,
                            width: 84,
                          }}
                        />
                      </Box>
                      <Box
                        sx={{
                          "& .MuiTextField-root": { m: 1 },
                        }}
                      >
                        <TextField
                          fullWidth
                          label="Họ Tên"
                          value={
                            transaction?.personalTrainer?.profile?.fullname ||
                            "Chưa rõ"
                          }
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                        <TextField
                          fullWidth
                          label="Số Điện Thoại"
                          value={
                            transaction?.personalTrainer?.profile?.phone ||
                            "Chưa rõ"
                          }
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                        <TextField
                          fullWidth
                          label="Địa Chỉ"
                          value={
                            transaction?.personalTrainer?.profile?.address ||
                            "Chưa rõ"
                          }
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                        <TextField
                          fullWidth
                          label="Ngày Sinh"
                          value={
                            transaction?.personalTrainer?.profile?.birthday ||
                            "Chưa rõ"
                          }
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Box>
                    </CardContent>
                  </Card>
                </Grid>
              </Grid>
            </Container>
          </Box>
        </DialogContent>
        <DialogActions>
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              p: 2,
            }}
          >
            <Button color="primary" variant="contained" onClick={handleClose}>
              Thoát
            </Button>
          </Box>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default DialogContractor;
