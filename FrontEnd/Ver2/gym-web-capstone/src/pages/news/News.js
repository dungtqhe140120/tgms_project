import React, { useState, useEffect } from "react";
import { useNewsEffect } from "../../hooks/useNewsFetch";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import "./news.scss";
import ExpandableCell from "../../components/common/ExpandableCell";
import PageHeader from "../../components/common/PageHeader";
import NewspaperIcon from "@mui/icons-material/Newspaper";
import { Box, Button, Paper, Toolbar } from "@mui/material";
import Controls from "../../components/formcontrol/Controls";
import Popup from "../../components/common/Popup";
import NewsForm from "./NewsForm";
import API from "../../API";
import { BASE_URL } from "../../config";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ErrorPage from "../error/ErrorPage";
import EditIcon from "@mui/icons-material/Edit";
import DeleteDialog from "../../components/common/DeleteDialog";
import DeleteIcon from "@mui/icons-material/Delete";

const newsColumns = [
  {
    field: "id",
    headerName: "ID",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "title",
    headerName: "Tiêu đề",
    width: 200,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "short_description",
    headerName: "Mô tả ngắn",
    width: 350,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "content",
    headerName: "Nội dung",
    width: 400,
    renderCell: (params) => {
      return <ExpandableCell {...params} />;
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "created_date",
    headerName: "Ngày tạo",
    width: 200,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "image",
    headerName: "Ảnh",
    width: 200,
    renderCell: (params) => {
      return (
        <img className="image-news" src={params.row.image} alt="Ảnh tiêu đề" />
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "is_display",
    headerName: "Hiển thị",
    type: "boolean",
    width: 70,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
];

const uploadImage = async (image_file) => {
  let formData = new FormData();
  formData.append("file", image_file);
  let result = await API.postForm(
    `${BASE_URL}/api/uploadImage`,
    formData
  );
  return result.data;
};

const News = () => {
  const [recordForEdit, setRecordForEdit] = useState(null);
  const [openPopup, setOpenPopup] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [contentDelete, setContentDelete] = useState("");
  const [currentNews, setCurrentNews] = useState(null);

  const { state, loading, error, setState } = useNewsEffect();

  useEffect(()=>{
    document.title="Bài viết"
  })

  const onDeleting = (news) => {
    setOpenDelete(true);
    setCurrentNews(news);
    setContentDelete(`Bạn có muốn xóa bài viết ${news.id}`);
  };

  const handleDelete = async (news) => {
    API.delete(`${BASE_URL}/api/news/${news.id}`)
      .then((response) => {
        let remainNews = state.results.filter((pt) => pt.id !== news.id);
        setState({ ...state, results: remainNews });
        toast.success("Xóa huấn luyện viên thành công!");
      })
      .catch((error) => {
        toast.error("Lỗi khi xóa huấn luyện viên thất bại!");
      })
      .finally(() => {
        setOpenDelete(false);
      });
  };

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const addOrEdit = (news, resetForm) => {
    if (news.id == 0) {
      // do insert data
      insertNews(news)
        .then((response) => {
          toast.success("Thêm mới bài viết thành công!");
          setOpenPopup(false);
          setRecordForEdit(null);
        })
        .catch((eror) => {
          toast.error("Thêm mới bài viết thất bại!");
        });
    } else {
      // do update data
      updateNews(news)
        .then(({ data }) => {
          toast.success("Chỉnh sửa bài viết thành công!");
          setOpenPopup(false);
          setRecordForEdit(null);

          let index = state.results.findIndex((ele) => ele.id === data.id);
          let newResults = [...state.results];
          newResults[index] = {
            ...data,
          };
          setState((prevState) => ({
            ...prevState,
            results: newResults,
          }));
        })
        .catch((eror) => {
          toast.error("Chỉnh sửa bài viết thất bại!");
        });
    }
    resetForm();
    setRecordForEdit(null);
    setOpenPopup(false);
    // update data in table
  };

  const openInPopup = (item) => {
    setRecordForEdit(item);
    setOpenPopup(true);
  };

  useEffect(() => {
    document.title = "Nhân Viên Gym";
  }, []);

  const insertNews = async (news) => {
    //need check required
    let urlImage = await uploadImage(news.image);
    let data = {
      title: news.title,
      short_description: news.short_description,
      content: news.content,
      created_date: "2022-08-09",
      image: urlImage,
      is_display: news.is_display ? 1 : 0,
    };

    return API.postRequest(`${BASE_URL}/api/news`, data);
  };

  // current do
  const updateNews = async (news) => {
    // notice data image not url
    let urlImage = news.image_url;
    if (news.image) {
      urlImage = await uploadImage(recordForEdit.image);
    }

    let data = {
      id: news.id,
      title: news.title,
      short_description: news.short_description,
      content: news.content,
      image: urlImage,
      created_date: "2022-08-09",
      is_display: news.display ? 1 : 0,
    };

    return await API.putRequest(
      `${BASE_URL}/api/news/${news.id}`,
      data
    );
  };

  const actionColumn = [
    {
      field: "action",
      headerName: "Action",
      width: 200,
      renderCell: (params) => {
        return (
          <div className="cellAction">
            <Button onClick={() => openInPopup(params.row)}>
              <EditIcon size="small" />
            </Button>
            <div
              className="deleteButton"
              onClick={() => onDeleting(params.row)}
            >
              <DeleteIcon size="small" />
            </div>
          </div>
        );
      },
      headerClassName: "header-mui-custome",
      cellClassName: "cell-mui-custome",
    },
  ];

  if (error) return <ErrorPage />;
  return (
    <div className="news-container">
      <ToastContainer />
      <PageHeader
        title="Bài viết"
        subTitle="Trang quản lý bài viết"
        icon={<NewspaperIcon fontSize="large" />}
      />
      <Paper className="news-page-content">
        <Toolbar>
          <Controls.MyButton
            text="Thêm tin mới"
            style={{
              marginLeft: "auto",
            }}
            size="small"
            onClick={() => {
              setOpenPopup(true);
              setRecordForEdit(null);
            }}
          />
        </Toolbar>
        <Box
          sx={{
            height: 800,
            width: "100%",
            "& .header-mui-custome": {
              backgroundColor: "gray",
              color: "white",
              fontSize: "17px",
            },
            "& .cell-mui-custome": {
              fontSize: "13px",
            },
          }}
        >
          <DataGrid
            rows={state.results}
            columns={newsColumns.concat(actionColumn)}
            getEstimatedRowHeight={() => 100}
            getRowHeight={() => "auto"}
            loading={loading}
            autoHeight={false}
            localeText={{
              toolbarColumns: "Các trường hiển thị",
              toolbarFilters: "Tìm kiếm theo",
              toolbarDensity: "Độ rộng các dòng",
              toolbarExport: "Xuất file",
            }}
            components={{ Toolbar: GridToolbar }}
            sx={{
              "&.MuiDataGrid-root--densityCompact .MuiDataGrid-cell": {
                py: 1,
              },
              "&.MuiDataGrid-root--densityStandard .MuiDataGrid-cell": {
                py: "10px",
              },
              "&.MuiDataGrid-root--densityComfortable .MuiDataGrid-cell": {
                py: "12px",
              },
              fontSize: "1rem",
            }}
            disableSelectionOnClick
          />
        </Box>
      </Paper>
      <Popup
        title="Form thêm mới tin"
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
      >
        <NewsForm recordForEdit={recordForEdit} addOrEdit={addOrEdit} />
      </Popup>
      <DeleteDialog
        open={openDelete}
        title="Xác nhận delete?"
        content={contentDelete}
        handleClose={handleCloseDelete}
        handleDelete={() => {
          handleDelete(currentNews);
        }}
      />
    </div>
  );
};

export default News;
