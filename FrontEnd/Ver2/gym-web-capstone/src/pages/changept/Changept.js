import React, { useState, useEffect } from "react";
import { useChangePt } from "../../hooks/useChangePt";
import {
  Autocomplete,
  InputAdornment,
  Paper,
  Stack,
  TextField,
  Toolbar,
  Chip,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Box,
} from "@mui/material";
import HourglassTopIcon from "@mui/icons-material/HourglassTop";
import DoneIcon from "@mui/icons-material/Done";
import CancelIcon from "@mui/icons-material/Cancel";
import ChangeCircleIcon from "@mui/icons-material/ChangeCircle";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import FitnessCenterIcon from "@mui/icons-material/FitnessCenter";
import {
  DataGrid,
  GridToolbarColumnsButton,
  GridToolbarContainer,
  GridToolbarExport,
} from "@mui/x-data-grid";
import ErrorPage from "../error/ErrorPage";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import PageHeader from "../../components/common/PageHeader";
import Controls from "../../components/formcontrol/Controls";
import NoRow from "../../components/common/NoRow";
import API from "../../API";
import { BASE_URL } from "../../config";

const LIST_STATUS = [
  { value: 0, label: "Đang chờ xử lý", style: {} },
  {
    value: 1,
    label: "Đã xác nhận",
    style: { backgroundColor: "#a1f5a1" },
  },
  {
    value: 2,
    label: "Đã từ chối",
    style: { backgroundColor: "#ff7a7a" },
  },
];

const ConfirmStatusFilter = ({ value, onChange }) => {
  return (
    <Stack spacing={3} sx={{ width: 400 }}>
      <Autocomplete
        multiple
        size="small"
        options={LIST_STATUS}
        getOptionLabel={(option) => option.label}
        value={value}
        onChange={(event, newValue) => {
          onChange(newValue);
        }}
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              variant="outlined"
              size="small"
              style={option.style}
              label={option.label}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            label="Trạng Thái"
            placeholder="Trạng thái yêu cầu"
          />
        )}
      />
    </Stack>
  );
};

const changePtColumns = [
  {
    field: "id",
    headerName: "ID",
    width: 80,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "traineeName",
    headerName: "Tên người tập",
    width: 180,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "ptName",
    headerName: "Tên huấn luyện viên",
    width: 180,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "registerDate",
    headerName: "Ngày bắt đầu",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "expireDate",
    headerName: "Ngày kêt thúc",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "packageRegister",
    headerName: "Gói tập",
    width: 50,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "confirmStatus",
    headerName: "Trạng Thái",
    width: 150,
    renderCell: (params) => {
      if (params.row.confirmStatus === 0) {
        return <Chip label="Đang chờ xử lý" icon={<HourglassTopIcon />} />;
      } else if (params.row.confirmStatus === 1) {
        return (
          <Chip
            label="Đã xác nhận"
            style={{ backgroundColor: "#a1f5a1" }}
            icon={<DoneIcon />}
          />
        );
      }
      return (
        <Chip
          label="Đã từ chối"
          style={{ backgroundColor: "#ff7a7a" }}
          icon={<CancelIcon />}
        />
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "confirmBy",
    headerName: "Người xác nhận",
    width: 150,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "confirmAt",
    headerName: "Ngày xác nhận",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
  {
    field: "reasonChange",
    headerName: "Lý do",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
  },
];

const updateConfirm = async (confirmOne) => {
  return API.putRequest(
    `${BASE_URL}/api/trainee-and-pt-drafts/${confirmOne?.id}/confirm`,
    confirmOne
  );
};

const ChangePtToolbar = () => {
  return (
    <GridToolbarContainer>
      <GridToolbarColumnsButton />
      <GridToolbarExport />
    </GridToolbarContainer>
  );
};

const ContractorToolbar = () => {
  return (
    <GridToolbarContainer>
      <GridToolbarColumnsButton />
      <GridToolbarExport />
    </GridToolbarContainer>
  );
};

const Changept = () => {
  const {
    state,
    loading,
    error,
    page,
    pageSize,
    nameTrainee,
    statusConfirm,
    namePT,
    setState,
    setPage,
    setPageSize,
    setNameTrainee,
    setStatusConfirm,
    setNamePT,
    fetchChangeRequest,
  } = useChangePt();

  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [currentAction, setCurrentAction] = useState(null);

  useEffect(() => {
    document.title = "Thay đổi huấn luyện viên";
  });

  const doUpdate = async (confirmOne) => {
    updateConfirm(confirmOne)
      .then(({ data }) => {
        let index = state.results.findIndex((ele) => ele.id === data.id);
        let newResults = [...state.results];
        newResults[index] = {
          ...newResults[index],
          confirmStatus: data.confirmStatus,
          confirmAt: data.confirmDate,
        };
        setState((prevState) => ({
          ...prevState,
          results: newResults,
        }));
        toast.success("Chỉnh sửa thành công!");
      })
      .catch((error) => {
        toast.error(error?.response?.data?.title || "Chỉnh sửa thất bại!");
      })
      .finally(() => {
        setOpen(false);
      });
  };

  const updateColumn = (contractors, confirmStatus) => {
    let newCurrentAction = {
      id: contractors.id,
      confirmStatus: confirmStatus,
    };
    setCurrentAction(newCurrentAction);
    if (contractors.confirmStatus !== 0) {
      setTitle("Xác nhận ");
      setContent(
        "Bạn có muốn cập nhật thành " +
          (contractors.confirmStatus === 1 ? "Từ chối" : "Xác nhận")
      );
      setOpen(true);
      return;
    }
    doUpdate(newCurrentAction);
  };

  const handleConfirm = () => {
    doUpdate(currentAction);
  };

  const handleCancel = () => {
    setOpen(false);
  };

  const ChipUIs = ({ params }) => {
    return params?.row?.confirmStatus === 0 ? (
      <Stack direction="row" spacing={1}>
        <Chip
          label="Xác nhận"
          icon={<DoneIcon />}
          onClick={() => {
            updateColumn(params.row, 1);
          }}
        />
        <Chip
          label="Từ chối"
          icon={<CancelIcon />}
          onClick={() => {
            updateColumn(params.row, 2);
          }}
        />
      </Stack>
    ) : params?.row?.confirmStatus === 1 ? (
      <Stack direction="row" spacing={1}>
        <Chip
          label="Từ chối"
          icon={<CancelIcon />}
          onClick={() => {
            updateColumn(params.row, 2);
          }}
        />
      </Stack>
    ) : (
      <Stack direction="row" spacing={1}>
        <Chip
          label="Xác nhận"
          icon={<DoneIcon />}
          onClick={() => {
            updateColumn(params.row, 1);
          }}
        />
      </Stack>
    );
  };

  const actionColumns = [
    {
      field: "action",
      headerName: "Thao tác",
      width: 220,
      renderCell: (params) => {
        return (
          <div className="cellAction">
            <ChipUIs params={params} />
          </div>
        );
      },
      headerClassName: "header-mui-custome",
    },
  ];

  const handleClose = () => {
    setOpen(false);
  };

  const handleSearch = () => {
    if (page !== 0) {
      //trigger by hook
      setPage(0);
      return;
    }
    fetchChangeRequest(page, pageSize, nameTrainee, statusConfirm, namePT);
  };

  if (error) return <ErrorPage />;

  return (
    <>
      <ToastContainer />
      <PageHeader
        title="Quản lý giao dịch"
        subTitle="Màn giúp quản lý giao dịch giữa huấn luyện viên và người tập"
        icon={<ChangeCircleIcon fontSize="large" />}
      />
      <Paper className="page-child">
        <Toolbar>
          <Stack spacing={1} direction="row">
            <Controls.MyInput
              label="Tìm kiếm theo tên người tập"
              className="trainee-search-contractor"
              size="small"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <PersonOutlineIcon />
                  </InputAdornment>
                ),
              }}
              onChange={(event) => {
                setNameTrainee(event.target.value);
              }}
            />
            <Controls.MyInput
              label="Tìm kiếm theo tên huấn luyện viên"
              className="pt-search-contractor"
              size="small"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <FitnessCenterIcon />
                  </InputAdornment>
                ),
              }}
              onChange={(event) => {
                setNamePT(event.target.value);
              }}
            />
            <ConfirmStatusFilter
              value={statusConfirm}
              onChange={setStatusConfirm}
            />
            <Controls.MyButton
              text="Tìm kiếm"
              onClick={handleSearch}
              size="small"
            />
          </Stack>
        </Toolbar>

        <Box
          sx={{
            height: 600,
            width: "100%",
            "& .header-mui-custome": {
              backgroundColor: "gray",
              color: "white",
              fontSize: "17px",
            },
            "& .cell-mui-custome": {
              fontSize: "13px",
            },
          }}
        >
          <DataGrid
            rows={state.results}
            rowCount={state.totol_results}
            loading={loading}
            rowsPerPageOptions={[10, 25, 50]}
            pagination
            page={state.page}
            pageSize={state.page_size}
            paginationMode="server"
            onPageChange={(newPage) => setPage(newPage)}
            onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
            columns={changePtColumns.concat(actionColumns)}
            localeText={{
              toolbarColumns: "Trường hiển thị",
              toolbarExport: "Xuất file",
            }}
            components={{
              Toolbar: ContractorToolbar,
              NoRowsOverlay: NoRow,
            }}
            disableSelectionOnClick
          />
          <Dialog open={open} keepMounted onClose={handleClose}>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>{content}</DialogContent>
            <DialogActions>
              <Button onClick={handleConfirm}>Xác nhận</Button>
              <Button onClick={handleCancel}>Hủy bỏ</Button>
            </DialogActions>
          </Dialog>
        </Box>
      </Paper>
    </>
  );
};

export default Changept;
