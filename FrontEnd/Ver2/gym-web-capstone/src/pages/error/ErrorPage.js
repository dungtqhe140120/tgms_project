import React from "react";
import './errorPage.scss'

const ErrorPage = () => {
  return (
    <div id="notfound">
      <div className="notfound">
        <div className="notfound-404">
          <h3>Oops!</h3>
          <h1>
            <span>#</span>
            <span>4</span>
            <span>0</span>
            <span>4</span>
          </h1>
        </div>
        <h2>Xin lỗi, Trang này hiện đang lỗi</h2>
      </div>
    </div>
  );
};

export default ErrorPage;
