import React, { useEffect, useState } from "react";
import { Grid } from "@mui/material";
import { MyForm, useMyForm } from "./useFormEmployee";
import Controls from "../../components/formcontrol/Controls";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import API from "../../API";
import { BASE_URL } from "../../config";
import { checkDateLessThanToday, formatYYYYMMdd } from "../../helpers";

const initialFValues = {
  id: 0,
  login: "",
  firstName: "",
  lastName: "",
  email: "",
  mobile: "",
  address: "",
  password: "",
  gender: 1,
  dob: new Date(),
  image: null,
};

const genderItems = [
  { id: 1, title: "Nam" },
  { id: 0, title: "Nữ" },
];

const passwordRegex = new RegExp(
  "^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})"
);

const endpoint = `${BASE_URL}/api/v2/register?expired_Date`;

const registerEmployee = async (values) => {
  let urlImage = null;
  if (values.image) {
    urlImage = await uploadImage(values.image);
  }

  let data = {
    login: values.login,
    firstName: values.firstName,
    lastName: values.lastName,
    email: values.email,
    imageUrl: urlImage,
    actived: "true",
    langKey: "vi",
    password: values.password,
    authorities: ["ROLE_EMPLOYEE"],
    gender: values.gender,
    birthDay: formatYYYYMMdd(values.dob),
    phone: values.mobile,
    address: values.address,
  };

  return await API.postRequest(endpoint, data);
};

const uploadImage = async (image_file) => {
  let formData = new FormData();
  formData.append("file", image_file);
  let result = await API.postForm(
    `${BASE_URL}/api/uploadImage`,
    formData
  );
  return result.data;
};

const fetchEmployeeById = (employeeId) => {
  return API.fetchData(`${BASE_URL}/api/`);
};

const EmployeeForm = ({ employeeId }) => {
  const [btnAction, setBtnAction] = useState("Thêm mới");
  const [formData, setFormData] = useState(initialFValues);

  useEffect(() => {
    document.title = "Thêm mới nhân viên";
    if (employeeId !== "add") {
      setBtnAction("Chỉnh sửa");
      (async () => {
        const result = await fetchEmployeeById(employeeId);
        const data = result.data;
        setFormData(data);
      })();
    }
  }, []);

  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("login" in fieldValues)
      temp.login = fieldValues.login ? "" : "Yêu cầu bạn nhập nickname.";
    if ("firstName" in fieldValues)
      temp.firstName = fieldValues.firstName ? "" : "Yêu cầu bạn nhập họ.";
    if ("lastName" in fieldValues)
      temp.lastName = fieldValues.lastName ? "" : "Yêu cầu bạn nhập tên.";
    if ("email" in fieldValues)
      if (!fieldValues.email) {
        temp.email = "Yêu cầu bạn nhập email.";
      } else {
        temp.email = /$^|.+@.+..+/.test(fieldValues.email)
          ? ""
          : "Email không hợp lệ.";
      }
    if ("password" in fieldValues)
      temp.password = passwordRegex.test(fieldValues.password)
        ? ""
        : "Mật khẩu chứa ít nhất 6 ký tự, 1 chữ số, 1 chữ hoa, 1 chữ thường, 1 ký tự đăc biệt.";
    if ("mobile" in fieldValues)
      temp.mobile =
        fieldValues.mobile.length > 9 ? "" : "Số điện thoại cần ít nhất 10 số.";
    if ("dob" in fieldValues)
      temp.dob = checkDateLessThanToday(fieldValues.dob)
        ? ""
        : "Ngày sinh phải nhỏ hơn hôm nay.";
    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };

  const { values, setValues, errors, setErrors, handleInputChange, resetForm } =
    useMyForm(formData, true, validate);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      // todo: do service
      registerEmployee(values)
        .then((result) => {
          resetForm();
          toast.success("Thêm mới nhân viên thành công!");
        })
        .catch((error) => {
          toast.error(
            error?.response?.data?.title || "Thêm mới nhân viên thất bại!"
          );
        });
    }
  };

  return (
    <>
      <ToastContainer />
      <MyForm>
        <Grid container>
          <Grid item xs={6}>
            <Controls.MyInput
              name="login"
              label="Nickname"
              value={values.login}
              onChange={handleInputChange}
              error={errors.login}
            />
            <Controls.MyInput
              name="firstName"
              label="Họ"
              value={values.firstName}
              onChange={handleInputChange}
              error={errors.firstName}
            />
            <Controls.MyInput
              name="lastName"
              label="Tên"
              value={values.lastName}
              onChange={handleInputChange}
              error={errors.lastName}
            />
            <Controls.MyInput
              name="email"
              label="Email"
              value={values.email}
              onChange={handleInputChange}
              error={errors.email}
            />
            <Controls.MyInput
              name="mobile"
              label="Số điện thoại"
              value={values.mobile}
              onChange={handleInputChange}
              error={errors.mobile}
            />
            <Controls.MyInput
              name="address"
              label="Địa chỉ"
              value={values.address}
              onChange={handleInputChange}
            />
          </Grid>
          <Grid item xs={6}>
            <Controls.MyPassword
              name="password"
              label="Mật khẩu"
              value={values.password}
              onChange={handleInputChange}
              error={errors.password}
            />

            <Controls.MyRadioGroup
              name="gender"
              label="Giới tính"
              value={values.gender}
              onChange={handleInputChange}
              items={genderItems}
            />

            <Controls.MyDatePicker
              name="dob"
              label="Ngày sinh"
              value={values.dob}
              onChange={handleInputChange}
            />

            <Controls.MyImageUpload
              name="image"
              label="Ảnh cá nhân"
              id="image-upload-employee"
              value={values.image}
              onChange={handleInputChange}
              style={{
                paddingTop: "15px",
                paddingBottom: "15px",
              }}
            />
            <div>
              <Controls.MyButton
                type="submit"
                variant="contained"
                text={btnAction}
                color="primary"
                style={{ marginRight: "15px" }}
                onClick={handleSubmit}
              />
              <Controls.MyButton
                text="Hủy bỏ"
                color="warning"
                onClick={resetForm}
              />
            </div>
          </Grid>
        </Grid>
      </MyForm>
    </>
  );
};

export default EmployeeForm;
