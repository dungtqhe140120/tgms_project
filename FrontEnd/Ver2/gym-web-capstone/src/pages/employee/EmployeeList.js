import React, { useState } from "react";
import { useEmployeeEffect } from "../../hooks/useEmployeeFetch";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { Link, useNavigate } from "react-router-dom";
import "./employeeList.scss";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import DeleteIcon from "@mui/icons-material/Delete";
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  Paper,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import API from "../../API";
import PageHeader from "../../components/common/PageHeader";
import Controls from "../../components/formcontrol/Controls";
import ErrorPage from "../error/ErrorPage";
import PersonNoImage from "../../assets/employee-no-image.webp";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { BASE_URL } from "../../config";

const employeesColumns = [
  {
    field: "id",
    headerName: "ID",
    minWidth: 60,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "fullName",
    headerName: "Họ Tên",
    minWidth: 180,
    renderCell: (params) => {
      return (
        <div className="cellWithImg">
          <img
            className="cellImg"
            src={params?.row?.avatar || PersonNoImage}
            alt="avatar"
          />
          {params.row.fullName}
        </div>
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "gender",
    headerName: "Giới Tính",
    minWidth: 80,
    renderCell: (params) => {
      return params.row.gender === 1 ? (
        <span>Nam</span>
      ) : params.row.gender === 0 ? (
        <span>Nữ</span>
      ) : (
        <span>Chưa rõ</span>
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "birthday",
    headerName: "Ngày Sinh",
    minWidth: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "phone",
    headerName: "Số Điện Thoại",
    minWidth: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "email",
    headerName: "Email",
    minWidth: 220,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "address",
    headerName: "Địa Chỉ",
    minWidth: 150,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "joinDate",
    headerName: "Ngày Tham Gia",
    minWidth: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
];

const EmployeeList = () => {
  const navigation = useNavigate();

  const { state, loading, error, setState } = useEmployeeEffect();

  const [open, setOpen] = useState(false);
  const [currentId, setCurrentId] = useState("");
  const [openDetail, setOpenDetail] = useState(false);
  const [currentEmployee, setCurrentEmployee] = useState({});

  const onDeleting = (id) => {
    setOpen(true);
    setCurrentId(id);
  };

  const handleCloseDetail = () => {
    setOpenDetail(false);
  };

  const handleDelete = async () => {
    API.delete(`${BASE_URL}/api/employees/${currentId}`)
      .then((response) => {
        setOpen(false);
        toast.success("Xóa nhân viên thành công!");
        const newState = {
          ...state,
          results: state.results.filter((item) => item.id !== currentId),
        };
        setState(newState);
      })
      .catch((error) => {
        setOpen(false);
        toast.error("Lỗi khi xóa nhân viên thất bại!");
      });
  };

  const handleClose = () => {
    setOpen(false);
  };

  const actionColumn = [
    {
      field: "action",
      headerName: "Thao Tác",
      minWidth: 100,
      renderCell: (params) => {
        return (
          <div className="cellAction">
            <Button
              onClick={() => {
                setOpenDetail(true);
                setCurrentEmployee(params.row);
              }}
            >
              <VisibilityIcon size="small" />
            </Button>
            <div
              className="deleteButton"
              onClick={() => onDeleting(params.row.id)}
            >
              <DeleteIcon size="small" />
            </div>
          </div>
        );
      },
      headerClassName: "header-mui-custome",
      cellClassName: "cell-mui-custome",
      flex: 1,
    },
  ];

  if (error) return <ErrorPage />;
  return (
    <div className="employee-list">
      <ToastContainer />
      <PageHeader
        title="Quản lý nhân viên"
        subTitle="Màn hình giúp thêm sửa xóa nhân viên"
        icon={<ManageAccountsIcon fontSize="large" />}
      />
      <Paper className="page-child">
        <Toolbar>
          <Controls.MyButton
            text="Thêm nhân viên mới"
            size="small"
            style={{
              marginLeft: "auto",
              paddingTop: "9px",
              paddingBottom: "9px",
            }}
            onClick={() => {
              navigation("/dashboard/employees/add", { replace: true });
            }}
          />
        </Toolbar>
        <Box
          sx={{
            height: 800,
            width: "100%",
            "& .header-mui-custome": {
              backgroundColor: "gray",
              color: "white",
              fontSize: "17px",
            },
            "& .cell-mui-custome": {
              fontSize: "13px",
            },
          }}
        >
          <DataGrid
            rows={state.results}
            columns={employeesColumns.concat(actionColumn)}
            loading={loading}
            getRowHeight={() => "auto"}
            disableSelectionOnClick
            localeText={{
              toolbarColumns: "Các trường hiển thị",
              toolbarFilters: "Tìm kiếm theo",
              toolbarDensity: "Độ rộng các dòng",
              toolbarExport: "Xuất file",
            }}
            components={{ Toolbar: GridToolbar }}
            getRowHeight={() => "auto"}
          />
          <Dialog open={open} keepMounted onClose={handleClose}>
            <DialogTitle>{"Xác nhận xóa?"}</DialogTitle>
            <DialogContent>
              Bạn có muốn xóa nhân viên mã {currentId}?
            </DialogContent>
            <DialogActions>
              <Button onClick={handleDelete}>Xóa</Button>
              <Button onClick={handleClose} style={{ color: "red" }}>
                Hủy bỏ
              </Button>
            </DialogActions>
          </Dialog>
          <DialogDetailEmployee
            open={openDetail}
            handleCloseDetail={handleCloseDetail}
            currentEmployee={currentEmployee}
          />
        </Box>
      </Paper>
    </div>
  );
};

const DialogDetailEmployee = ({ open, handleCloseDetail, currentEmployee }) => {
  let genderDetail =
    currentEmployee?.gender === 1
      ? "Nam"
      : currentEmployee?.gender === 0
      ? "Nữ"
      : "Chưa rõ";

  return (
    <>
      <Dialog maxWidth="md" open={open} keepMounted onClose={handleCloseDetail}>
        <DialogTitle>
          Thông tin cá nhân chi tiết của nhân viên
          <Divider />
        </DialogTitle>
        <DialogContent>
          <Box sx={{ flexGrow: 1, py: 8 }}>
            <Container maxWidth="md">
              <Grid item lg={4} md={6} xs={12}>
                <Card>
                  <CardContent>
                    <Box
                      sx={{
                        alignItems: "center",
                        display: "flex",
                        flexDirection: "column",
                      }}
                    >
                      <Avatar
                        src={currentEmployee?.avatar || PersonNoImage}
                        sx={{
                          height: 84,
                          mb: 2,
                          width: 84,
                        }}
                      />
                      <Typography color="HighlightText">
                        Ho Ten ow Day
                      </Typography>
                    </Box>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item lg={8} md={6} xs={12}>
                <Card>
                  <CardHeader title="Thông tin" />
                  <Divider />
                  <CardContent>
                    <Grid container spacing={3}>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Họ Tên"
                          value={currentEmployee?.fullName || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Giới Tính"
                          value={genderDetail}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Số Điện Thoại"
                          value={currentEmployee?.phone || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Email"
                          value={currentEmployee?.email || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Địa Chỉ"
                          value={currentEmployee?.address || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Ngày Sinh"
                          value={currentEmployee?.birthday || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Ngày Tham Gia"
                          value={currentEmployee?.joinDate || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            </Container>
          </Box>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default EmployeeList;
