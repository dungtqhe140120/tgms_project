import React, { useState } from "react";
import { useParams } from "react-router-dom";
import "./employeeDetail.scss";
import { Paper } from "@mui/material";
import PageHeader from "../../components/common/PageHeader";
import PeopleOutlineIcon from "@mui/icons-material/PeopleOutline";
import EmployeeForm from "./EmployeeForm";

const EmployeeDetail = () => {
  const { employeeId } = useParams();

  return (
    <div className="employee-detail-container">
      <PageHeader
        title="Thêm mới nhân viên"
        subTitle="Mẫu thêm mới nhân viên"
        icon={<PeopleOutlineIcon fontSize="large" />}
      />
      <Paper className="pageContent">
        <EmployeeForm employeeId={employeeId} />
      </Paper>
    </div>
  );
};

export default EmployeeDetail;
