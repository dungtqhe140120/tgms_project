import React, { useState } from "react";
import { usePersonalTrainersEffect } from "../../hooks/usePersonalTrainers";
import "./personalTrainer.scss";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { Link, useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import API from "../../API";
import { BASE_URL } from "../../config";
import SportsGymnasticsIcon from "@mui/icons-material/SportsGymnastics";
import DeleteIcon from "@mui/icons-material/Delete";
import VisibilityIcon from "@mui/icons-material/Visibility";
import EditIcon from "@mui/icons-material/Edit";
import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Pagination,
  Paper,
  Toolbar,
} from "@mui/material";
import Rate from "../../components/rate/Rate";
import ErrorPage from "../error/ErrorPage";
import PageHeader from "../../components/common/PageHeader";
import Controls from "../../components/formcontrol/Controls";
import PersonNoImage from "../../assets/employee-no-image.webp";
import UserProfileDialog from "../../components/profile/UserProfileDialog";
import NoRow from "../../components/common/NoRow";
import DeleteDialog from "../../components/common/DeleteDialog";

const personalTrainerColumns = [
  {
    field: "id",
    headerName: "ID",
    width: 60,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "full_name",
    headerName: "Họ Tên",
    width: 180,
    renderCell: (params) => {
      return (
        <div className="cellWithImg">
          <img
            className="cellImg"
            src={params?.row?.avatar || PersonNoImage}
            alt="avatar"
          />
          {params.row.full_name}
        </div>
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "gender",
    headerName: "Giới Tính",
    minWidth: 80,
    renderCell: (params) => {
      return params.row.gender === 1 ? (
        <span>Nam</span>
      ) : params.row.gender === 0 ? (
        <span>Nữ</span>
      ) : (
        <span>Chưa rõ</span>
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "birthday",
    headerName: "Ngày Sinh",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "phone",
    headerName: "Số Điện Thoại",
    width: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "email",
    headerName: "Email",
    width: 220,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "address",
    headerName: "Địa Chỉ",
    width: 150,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "experience",
    headerName: "Năm kinh nghiệm",
    width: 150,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "short_description",
    headerName: "Mô tả ngắn",
    width: 150,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
];

const PAGE_SIZE = 2;

const fetchRate = async (ptId, page, size) => {
  let endpoint = `${BASE_URL}/api/pt-inf-full/${ptId}/current-trainees?page=${page}&size=${size}`;
  return API.fetchData(endpoint);
};

const PersonalTrainers = () => {
  const navigation = useNavigate();

  const { state, loading, error, setState, setPage, setPageSize } =
    usePersonalTrainersEffect();
  const [rateOfPTs, setRateOfPTs] = useState(null);
  const [open, setOpen] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [contentDelete, setContentDelete] = useState("");
  const [currentPt, setCurrentPt] = useState(null);

  const onDeleting = (ptInfo) => {
    setOpenDelete(true);
    setCurrentPt(ptInfo);
    setContentDelete(`Bạn có muốn xóa huấn luyện viên ${ptInfo.full_name}?`);
  };

  const handleDelete = async (news) => {
    API.delete(`${BASE_URL}/api/personal-trainers/${news.id}`)
      .then((response) => {
        let remainPt = state.results.filter((pt) => pt.id !== news.id);
        setState({ ...state, results: remainPt });
        toast.success("Xóa nhân viên thành công!");
      })
      .catch((error) => {
        toast.error("Xóa nhân viên thất bại!");
      })
      .finally(() => {
        setOpenDelete(false);
      });
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const showRateOfPt = async (ptInfo, page = 0) => {
    setCurrentPt(ptInfo);
    fetchRate(ptInfo.id, page, PAGE_SIZE)
      .then((result) => {
        setRateOfPTs(result.data);
        setOpen(true);
      })
      .catch((error) => {
        toast.error(
          "Lỗi xảy ra khi cố gắng lấy thông tin đánh giá của huấn luyện viên!"
        );
      });
  };

  const handlePageChangeRate = (event, newPage) => {
    fetchRate(currentPt.id, newPage - 1, PAGE_SIZE)
      .then((result) => {
        setRateOfPTs(result.data);
        setOpen(true);
      })
      .catch((error) => {
        toast.error(
          "Lỗi xảy ra khi cố gắng lấy thông tin đánh giá của huấn luyện viên!"
        );
      });
  };

  const actionColumn = [
    {
      field: "action",
      headerName: "Thao Tác",
      width: 200,
      renderCell: (params) => {
        return (
          <div className="cellAction">
            <Button onClick={() => showRateOfPt(params.row, 0)}>
              <VisibilityIcon size="small" />
            </Button>
            <Link to={`/dashboard/personal-trainer/${params.row.id}`} replace>
              <EditIcon size="small" />
            </Link>
            <div
              className="deleteButton"
              onClick={() => onDeleting(params.row)}
            >
              <DeleteIcon size="small" />
            </div>
          </div>
        );
      },
      headerClassName: "header-mui-custome",
      cellClassName: "cell-mui-custome",
    },
  ];

  if (error) return <ErrorPage />;
  return (
    <>
      <ToastContainer />
      <PageHeader
        title="Quản lý huấn luyện viên"
        subTitle="Màn hình giúp quản lý huấn luyện viên"
        icon={<SportsGymnasticsIcon fontSize="large" />}
      />
      <Paper className="page-child">
        <Toolbar>
          <Controls.MyButton
            text="Thêm huấn lyện viên mới"
            style={{ marginLeft: "auto" }}
            onClick={() => {
              navigation("/dashboard/personal-trainer/add", { replace: false });
            }}
          />
        </Toolbar>
        <Box
          sx={{
            height: 800,
            width: "100%",
            "& .header-mui-custome": {
              backgroundColor: "gray",
              color: "white",
              fontSize: "17px",
            },
            "& .cell-mui-custome": {
              fontSize: "13px",
            },
          }}
        >
          <DataGrid
            rows={state.results}
            rowCount={state.totol_results}
            loading={loading}
            rowsPerPageOptions={[10, 25, 50]}
            pagination
            page={state.page}
            pageSize={state.page_size}
            paginationMode="server"
            onPageChange={(newPage) => setPage(newPage)}
            onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
            columns={personalTrainerColumns.concat(actionColumn)}
            disableSelectionOnClick
            localeText={{
              toolbarColumns: "Các trường hiển thị",
              toolbarFilters: "Tìm kiếm theo",
              toolbarDensity: "Độ rộng các dòng",
              toolbarExport: "Xuất file",
            }}
            components={{ Toolbar: GridToolbar }}
          />
          <UserProfileDialog
            open={open}
            title="View Ìno"
            userprofile={currentPt}
            handleClose={handleClose}
          >
            <RateView
              rateOfPTs={rateOfPTs}
              handlePageChangeRate={handlePageChangeRate}
            />
          </UserProfileDialog>
          <DeleteDialog
            open={openDelete}
            title="Xác nhận delete?"
            content={contentDelete}
            handleClose={handleCloseDelete}
            handleDelete={() => {
              handleDelete(currentPt);
            }}
          />
        </Box>
      </Paper>
    </>
  );
};

const RateView = ({ rateOfPTs, handlePageChangeRate }) => {
  if (rateOfPTs?.content.length === 0) {
    return (
      <Card>
        <CardHeader subheader="Các đánh giá của pt" title="Rate PT" />
        <Divider />
        <CardContent>
          <NoRow />
        </CardContent>
      </Card>
    );
  }
  return (
    <Card>
      <CardHeader subheader="Các đánh giá của pt" title="Danh sách người tập và đánh giá của họ" />
      <Divider />
      <CardContent>
        <Grid container>
          {rateOfPTs?.content?.map((rateOne) => (
            <Grid item width={"100%"} key={rateOne.id}>
              <Rate
                rator={rateOne.traineeName}
                avatar={PersonNoImage}
                date={rateOne.rateAt}
                comment={rateOne.ratePT}
                rateStar={rateOne.rateStar}
              />
            </Grid>
          ))}
        </Grid>
      </CardContent>
      <CardActionArea>
        <Pagination
          count={rateOfPTs?.totalPages || 0}
          page={(rateOfPTs?.pageable?.pageNumber || 0) + 1}
          onChange={handlePageChangeRate}
          size="small"
          style={{ margin: "auto" }}
        />
      </CardActionArea>
    </Card>
  );
};

export default PersonalTrainers;
