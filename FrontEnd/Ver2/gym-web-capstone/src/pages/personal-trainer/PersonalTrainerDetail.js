import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import PageHeader from "../../components/common/PageHeader";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import { Grid, Paper, TextField } from "@mui/material";
import { MyForm, useMyForm } from "../employee/useFormEmployee";
import Controls from "../../components/formcontrol/Controls";
import API from "../../API";
import { BASE_URL } from "../../config";
import { checkDateLessThanToday, formatYYYYMMdd } from "../../helpers";

const initialFValues = {
  id: 0,
  login: "",
  firstName: "",
  lastName: "",
  email: "",
  phone: "",
  address: "",
  gender: 1,
  password: "",
  experience: 0,
  shortDescription: "",
  dob: new Date(),
};

const genderItems = [
  { id: 1, title: "Nam" },
  { id: 0, title: "Nữ" },
];

const passwordRegex = new RegExp(
  "^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})"
);

const registerTrainee = async (values) => {
  let urlImage = null;
  if (values.image) {
    urlImage = await uploadImage(values.image);
  }

  let data = {
    login: values.login,
    firstName: values.firstName,
    lastName: values.lastName,
    email: values.email,
    imageUrl: urlImage,
    actived: "false",
    langKey: "vi",
    password: values.password,
    authorities: ["ROLE_PT"],
    gender: values.gender,
    birthDay: formatYYYYMMdd(values.dob),
    phone: values.phone,
    address: values.address,
    experience: values.experience,
    shortDescription: values.shortDescription,
  };

  return await API.postRequest(`${BASE_URL}/api/v2/register`, data);
};

const updatePersonalTrainer = async (values, ptId) => {
  let urlImage = values.imageUrl;
  if (values.image) {
    urlImage = await uploadImage(values.image);
  }

  let data = {
    id: ptId,
    firstName: values.firstName,
    lastName: values.lastName,
    email: values.email,
    imageUrl: urlImage,
    gender: values.gender,
    birthDay: formatYYYYMMdd(values.dob),
    phone: values.phone,
    address: values.address,
    experience: values.experience,
    shortDescription: values.shortDescription,
  };

  return await API.putRequest(
    `${BASE_URL}/api/pt-inf-full/${ptId}`,
    data
  );
};

const uploadImage = async (image_file) => {
  let formData = new FormData();
  formData.append("file", image_file);
  let result = await API.postForm(
    `${BASE_URL}/api/uploadImage`,
    formData
  );
  return result.data;
};

const fetchPersonalTrainerById = async (ptId) => {
  return await API.fetchData(`${BASE_URL}/api/pt-inf-full/${ptId}`);
};

const PersonalTrainerDetail = () => {
  const [formData, setFormData] = useState(initialFValues);
  const { ptId } = useParams();
  const [isAdd, setIsAdd] = useState(true);
  const [btnAction, setBtnAction] = useState("Thêm mới");

  useEffect(() => {
    if (ptId !== "add") {
      setIsAdd(false);
      setBtnAction("Chỉnh sửa");
      (async () => {
        const result = await fetchPersonalTrainerById(ptId);
        const data = result.data;
        let dataTemp = {
          ...data,
          dob: new Date(data.birthDay),
        };
        setFormData(dataTemp);
        setValues(dataTemp);
      })();
    } else {
      setIsAdd(true);
    }
  }, []);

  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("login" in fieldValues)
      temp.login = fieldValues.login ? "" : "Yêu cầu bạn nhập nickname.";
    if ("firstName" in fieldValues)
      temp.firstName = fieldValues.firstName ? "" : "Yêu cầu bạn nhập Họ.";
    if ("lastName" in fieldValues)
      temp.lastName = fieldValues.lastName ? "" : "Yêu cầu bạn nhập Tên.";
    if ("email" in fieldValues)
      if (!fieldValues.email) {
        temp.email = "Yêu cầu bạn nhập email.";
      } else {
        temp.email = /$^|.+@.+..+/.test(fieldValues.email)
          ? ""
          : "Email không hợp lệ.";
      }
    if ("password" in fieldValues)
      temp.password = passwordRegex.test(fieldValues.password)
        ? ""
        : "Mật khẩu chứa ít nhất 6 ký tự, 1 chữ số, 1 chữ hoa, 1 chữ thường, 1 ký tự đăc biệt.";
    if ("phone" in fieldValues)
      temp.phone =
        fieldValues.phone.length > 9 ? "" : "Số điện thoại cần ít nhất 10 số.";
    if ("dob" in fieldValues)
      temp.dob = checkDateLessThanToday(fieldValues.dob)
        ? ""
        : "Ngày sinh phải nhỏ hơn hôm nay.";
    if ("experience" in fieldValues)
      temp.experience =
        fieldValues.experience >= 0 ? "" : "Số năm kinh nghiệm không thể âm.";
    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };

  const { values, setValues, errors, setErrors, handleInputChange, resetForm } =
    useMyForm(formData, true, validate);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      if (isAdd) {
        registerTrainee(values)
          .then((result) => {
            resetForm();
            toast.success("Thêm mới huấn luyện viên thành công!");
          })
          .catch((error) => {
            toast.error("Thêm mới huấn luyện viên viên thất bại!");
          });
      } else {
        updatePersonalTrainer(values, ptId)
          .then((result) => {
            toast.success("Chỉnh sửa huấn luyện viên thành công!");
            const data = result.data;
            let dataTemp = {
              ...data,
              dob: new Date(data.birthDay),
            };
            setFormData(dataTemp);
            setValues(dataTemp);
          })
          .catch((error) => {
            toast.error(
              error?.response?.data?.title || "Chỉnh sửa huấn luyện viên viên thất bại!"
            );
          });
      }
    }
  };

  return (
    <>
      <ToastContainer />
      <PageHeader
        title="Màn thêm huấn luyện viên"
        subTitle="Màn thêm huấn luyện viên"
        icon={<ManageAccountsIcon fontSize="large" />}
      />
      <Paper className="page-child">
        <MyForm>
          <Grid container>
            <Grid item xs={6}>
              {isAdd && (
                <Controls.MyInput
                  name="login"
                  label="Nickname"
                  value={values.login}
                  onChange={handleInputChange}
                  error={errors.login}
                />
              )}
              <Controls.MyInput
                name="firstName"
                label="Họ"
                value={values.firstName}
                onChange={handleInputChange}
                error={errors.firstName}
              />
              <Controls.MyInput
                name="lastName"
                label="Tên"
                value={values.lastName}
                onChange={handleInputChange}
                error={errors.lastName}
              />
              <Controls.MyInput
                name="email"
                label="Email"
                value={values.email}
                onChange={handleInputChange}
                error={errors.email}
              />
              <Controls.MyInput
                name="phone"
                label="Số điện thoại"
                value={values.phone}
                onChange={handleInputChange}
                error={errors.phone}
              />
              <Controls.MyInput
                name="address"
                label="Địa chỉ"
                value={values.address}
                onChange={handleInputChange}
              />
              <Controls.MyInput
                name="experience"
                label="Năm kinh nghiệm"
                value={values.experience}
                onChange={handleInputChange}
                type="number"
                error={errors.experience}
              />
            </Grid>
            <Grid item xs={6}>
              {isAdd && (
                <Controls.MyPassword
                  name="password"
                  label="Mật khẩu"
                  value={values.password}
                  onChange={handleInputChange}
                  error={errors.password}
                />
              )}

              <Controls.MyRadioGroup
                name="gender"
                label="Giới tính"
                value={values.gender}
                onChange={handleInputChange}
                items={genderItems}
              />

              <Controls.MyDatePicker
                name="dob"
                label="Ngày sinh"
                value={values.dob}
                onChange={handleInputChange}
                error={errors.dob}
              />

              <Controls.MyMultiInput
                name="shortDescription"
                label="Mô tả ngắn"
                value={values.shortDescription}
                onChange={handleInputChange}
              />

              <Controls.MyImageUpload
                name="image"
                label="Ảnh cá nhân"
                id="image-upload-trainee"
                value={values.image}
                onChange={handleInputChange}
                imageLink={values.imageUrl}
                style={{
                  paddingTop: "15px",
                  paddingBottom: "15px",
                }}
              />
              <div>
                <Controls.MyButton
                  type="submit"
                  variant="contained"
                  text={btnAction}
                  color="primary"
                  style={{ marginRight: "15px" }}
                  onClick={handleSubmit}
                />
                <Controls.MyButton
                  text="Hủy bỏ"
                  color="warning"
                  onClick={resetForm}
                />
              </div>
            </Grid>
          </Grid>
        </MyForm>
      </Paper>
    </>
  );
};

export default PersonalTrainerDetail;
