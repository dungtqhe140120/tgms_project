import React, { useState } from "react";
import "./list.scss";
import PageHeader from "../../components/common/PageHeader";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Container,
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  InputAdornment,
  Paper,
  Stack,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import Controls from "../../components/formcontrol/Controls";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { useTraineeEffect } from "../../hooks/useTraineeFetch";
import { Link, useNavigate } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import DeleteDialog from "../../components/common/DeleteDialog";
import API from "../../API";
import { BASE_URL } from "../../config";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ErrorPage from "../error/ErrorPage";
import DeleteIcon from "@mui/icons-material/Delete";
import VisibilityIcon from "@mui/icons-material/Visibility";
import EditIcon from "@mui/icons-material/Edit";
import PersonNoImage from "../../assets/employee-no-image.webp";

const traineeColumns = [
  {
    field: "id",
    headerName: "ID",
    minWidth: 60,
    maxWidth: 60,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "full_name",
    headerName: "Họ Tên",
    minWidth: 150,
    renderCell: (params) => {
      return (
        <div className="cellWithImg">
          <img
            className="cellImg"
            src={params?.row?.avatar || PersonNoImage}
            alt="avatar"
          />
          {params.row.full_name}
        </div>
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "gender",
    headerName: "Giới Tính",
    minWidth: 80,
    renderCell: (params) => {
      return params.row.gender === 1 ? (
        <span>Nam</span>
      ) : params.row.gender === 0 ? (
        <span>Nữ</span>
      ) : (
        <span>Chưa rõ</span>
      );
    },
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "phone",
    headerName: "Số Điện Thoại",
    minWidth: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "email",
    headerName: "Email",
    minWidth: 220,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "address",
    headerName: "Địa Chỉ",
    minWidth: 100,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "created_date",
    headerName: "Ngày Tham Gia",
    minWidth: 70,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
  {
    field: "expired_date",
    headerName: "Ngày Tài Khoản Hết Hạn",
    minWidth: 70,
    headerClassName: "header-mui-custome",
    cellClassName: "cell-mui-custome",
    flex: 1,
  },
];

const TraineeList = () => {
  const navigation = useNavigate();
  const [currentTrainee, setCurrentTrainee] = useState({});

  const actionColumn = [
    {
      field: "action",
      headerName: "Thao Tác",
      width: 150,
      renderCell: (params) => {
        return (
          <div className="cellAction">
            <Button
              onClick={() => {
                setOpenDetail(true);
                setCurrentTrainee(params.row);
              }}
            >
              <VisibilityIcon size="small" />
            </Button>
            <Link to={`/dashboard/users/${params.row.id}`} replace={false}>
              <EditIcon size="small" />
            </Link>
            <div
              className="deleteButton"
              onClick={() => onDeleting(params.row.id)}
            >
              <DeleteIcon size="small" />
            </div>
          </div>
        );
      },
      headerClassName: "header-mui-custome",
      cellClassName: "cell-mui-custome",
      flex: 1,
    },
  ];

  const {
    state,
    loading,
    error,
    page,
    pageSize,
    search,
    setState,
    setPage,
    setPageSize,
    setSearch,
    fetchTrainee,
  } = useTraineeEffect();

  const [open, setOpen] = useState(false);
  const [contentDelete, setContentDelete] = useState("");
  const [currentId, setCurrentId] = useState("");
  const [openDetail, setOpenDetail] = useState(false);

  const handleCloseDetail = () => {
    setOpenDetail(false);
  };

  const handleDelete = async (traineeId) => {
    API.delete(`${BASE_URL}/api/trainees/${traineeId}`)
      .then((response) => {
        let remainTrainee = state.results.filter(
          (trainee) => trainee.id !== traineeId
        );
        setState({ ...state, results: remainTrainee });
        toast.success("Xóa nhân viên thành công!");
      })
      .catch((error) => {
        toast.error("Lỗi khi xóa nhân viên thất bại!");
      })
      .finally(() => {
        setOpen(false);
      });
  };

  const onDeleting = (id) => {
    setOpen(true);
    setCurrentId(id);
    setContentDelete(`Bạn có muốn xóa người tập mã ${id}?`);
  };

  const handleSearch = () => {
    if (page !== 0) {
      //trigger by hook
      setPage(0);
      return;
    }
    fetchTrainee(page, pageSize, search);
  };

  const handleClose = () => {
    setOpen(false);
  };

  if (error) return <ErrorPage />;
  return (
    <>
      <ToastContainer />
      <PageHeader
        title="Quản lý người tập"
        subTitle="Màn giúp quản lý người tập, xem thông tin chi tiết"
        icon={<ManageAccountsIcon fontSize="large" />}
      />
      <Paper className="trainees-page-content page-child">
        <Toolbar>
          <Stack spacing={1} direction="row" width={"100%"}>
            <Controls.MyInput
              label="Tìm kiếm theo tên"
              className="trainee-search"
              size="small"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
              onChange={(event) => {
                setSearch(event.target.value);
              }}
            />
            <Controls.MyButton
              text="Tìm kiếm"
              onClick={handleSearch}
              size="small"
            />
            <Controls.MyButton
              text="Thêm người tập mới"
              style={{ marginLeft: "auto" }}
              size="small"
              onClick={() => {
                navigation("/dashboard/users/add", { replace: true });
              }}
            />
          </Stack>
        </Toolbar>
        <Box
          sx={{
            height: 800,
            width: "100%",
            "& .header-mui-custome": {
              backgroundColor: "gray",
              color: "white",
              fontSize: "17px",
            },
            "& .cell-mui-custome": {
              fontSize: "13px",
            },
          }}
        >
          <DataGrid
            rows={state.results}
            rowCount={state.totol_results}
            loading={loading}
            rowsPerPageOptions={[10, 25, 50]}
            pagination
            page={state.page}
            pageSize={state.page_size}
            paginationMode="server"
            onPageChange={(newPage) => setPage(newPage)}
            onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
            columns={traineeColumns.concat(actionColumn)}
            disableSelectionOnClick
            localeText={{
              toolbarColumns: "Các trường hiển thị",
              toolbarFilters: "Tìm kiếm theo",
              toolbarDensity: "Độ rộng các dòng",
              toolbarExport: "Xuất file",
            }}
            components={{ Toolbar: GridToolbar }}
          />
          <DeleteDialog
            open={open}
            title="Xác nhận delete?"
            content={contentDelete}
            handleClose={handleClose}
            handleDelete={() => {
              handleDelete(currentId);
            }}
          />
          <DialogDetailTrainee
            open={openDetail}
            handleCloseDetail={handleCloseDetail}
            currentTrainee={currentTrainee}
          />
        </Box>
      </Paper>
    </>
  );
};

const DialogDetailTrainee = ({ open, handleCloseDetail, currentTrainee }) => {
  let genderDetail =
    currentTrainee?.gender === 1
      ? "Nam"
      : currentTrainee?.gender === 0
      ? "Nữ"
      : "Chưa rõ";

  return (
    <>
      <Dialog maxWidth="md" open={open} keepMounted onClose={handleCloseDetail}>
        <DialogTitle>
          Thông tin cá nhân chi tiết của người tập
          <Divider />
        </DialogTitle>
        <DialogContent>
          <Box sx={{ flexGrow: 1, py: 8 }}>
            <Container maxWidth="md">
              <Grid item lg={4} md={6} xs={12}>
                <Card>
                  <CardContent>
                    <Box
                      sx={{
                        alignItems: "center",
                        display: "flex",
                        flexDirection: "column",
                      }}
                    >
                      <Avatar
                        src={currentTrainee?.avatar || PersonNoImage}
                        sx={{
                          height: 84,
                          mb: 2,
                          width: 84,
                        }}
                      />
                      <Typography color="HighlightText">
                        Ho Ten ow Day
                      </Typography>
                    </Box>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item lg={8} md={6} xs={12}>
                <Card>
                  <CardHeader title="Thông tin" />
                  <Divider />
                  <CardContent>
                    <Grid container spacing={3}>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Họ Tên"
                          value={currentTrainee?.full_name || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Giới Tính"
                          value={genderDetail}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Số Điện Thoại"
                          value={currentTrainee?.phone || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Email"
                          value={currentTrainee?.email || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Địa Chỉ"
                          value={currentTrainee?.address || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Ngày Tạo"
                          value={currentTrainee?.created_date || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Ngày Hết Hạn"
                          value={currentTrainee?.expired_date || "Chưa rõ"}
                          variant="outlined"
                          InputProps={{
                            readOnly: true,
                          }}
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            </Container>
          </Box>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default TraineeList;
