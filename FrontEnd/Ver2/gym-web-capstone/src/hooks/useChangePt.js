import React, { useState, useEffect } from "react";
import { BASE_URL } from "../config";
import API from "../API";

const initialState = {
  page: 0,
  results: [],
  total_pages: 0,
  totol_results: 0,
  page_size: 0,
};

export const useChangePt = () => {
  const [state, setState] = useState(initialState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(25);
  const [nameTrainee, setNameTrainee] = useState("");
  const [statusConfirm, setStatusConfirm] = useState([]);
  const [namePT, setNamePT] = useState("");

  const fetchChangeRequest = async (
    pageNo = 0,
    pageSize = 25,
    nameTrainee = "",
    statusConfirm = [],
    namePT = ""
  ) => {
    try {
      setError(false);
      setLoading(true);

      let statusConfirmQuery = statusConfirm
        .map((option) => option.value)
        .join();
      let endpoint = `${BASE_URL}/api/trainee-and-pt-drafts/filters?nameTrainee=${nameTrainee}&statusConfirm=${statusConfirmQuery}&namePT=${namePT}&page=${pageNo}&size=${pageSize}`;
      const results = await API.fetchData(endpoint);
      const data = results.data;
      setState((prev) => ({
        page: data.number,
        results: data.content,
        total_pages: data.totalPages,
        totol_results: data.totalElements,
        page_size: data.size,
      }));
    } catch (error) {
      setError(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    setState(initialState);
    fetchChangeRequest(page, pageSize, nameTrainee, statusConfirm, namePT);
  }, [page, pageSize]);

  return {
    state,
    loading,
    error,
    page,
    pageSize,
    nameTrainee,
    statusConfirm,
    namePT,
    setState,
    setPage,
    setPageSize,
    setNameTrainee,
    setStatusConfirm,
    setNamePT,
    fetchChangeRequest,
  };
};
