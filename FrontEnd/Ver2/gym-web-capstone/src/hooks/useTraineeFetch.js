import React, { useEffect, useState } from "react";
import API from "../API";
import { BASE_URL } from "../config";

const initialState = {
  page: 0,
  results: [],
  total_pages: 0,
  totol_results: 0,
  page_size: 0,
};

export const useTraineeEffect = () => {
  const [state, setState] = useState(initialState);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(25);
  const [search, setSearch] = useState("");

  const fetchTrainee = async (pageNo = 0, pageSize = 25, search = "") => {
    console.log("doing featch with search", search);
    try {
      setError(false);
      setLoading(true);

      let endpoint = `${BASE_URL}/api/v2/trainees/getAllDisplayTrainees?page=${pageNo}&size=${pageSize}&nameTrainee=${search}`;
      const results = await API.fetchData(endpoint);
      const data = results.data;
      setState((prev) => ({
        page: data.number,
        results: data.content,
        total_pages: data.totalPages,
        totol_results: data.totalElements,
        page_size: data.size,
      }));
    } catch (error) {
      setError(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    setState(initialState);
    fetchTrainee(page, pageSize);
  }, [page, pageSize]);

  return {
    state,
    loading,
    error,
    page,
    pageSize,
    search,
    setState,
    setPage,
    setPageSize,
    setSearch,
    fetchTrainee,
  };
};
