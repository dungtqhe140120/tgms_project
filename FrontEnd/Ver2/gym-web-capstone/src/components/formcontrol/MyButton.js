import React from "react";
import Button from "@mui/material/Button";

export default function MyButton({ text, size, color, variant, onClick, style }) {

  return (
    <Button
      onClick={onClick}
      size={size || "large"}
      variant={variant || "contained"}
      color={color || "primary"}
      style={style || {}}
    >
      {text}
    </Button>
  );
}
