import no_image from "../../assets/no-image.jpg";

export default function MyImageUpload(props) {
  const {
    name,
    id,
    onChange,
    style,
    value,
    label,
    imageLink = no_image,
    error,
  } = props;

  const convertToDefEventPara = (name, value) => ({
    target: {
      name,
      value,
    },
  });

  return (
    <div className="formInput" style={style}>
      <label
        htmlFor={id}
        style={{
          fontSize: "15px",
        }}
      >
        {label}:
        <img
          className="image-upload"
          style={{
            height: "150px",
          }}
          src={value ? URL.createObjectURL(value) : imageLink || no_image}
          alt=""
        />
      </label>
      <input
        type="file"
        id={id}
        onChange={(e) => {
          onChange(convertToDefEventPara(name, e.target.files[0]));
        }}
        style={{ display: "none" }}
      />
      {error && <p style={{ color: "red" }}>{error}</p>}
    </div>
  );
}
