import { TextField } from "@mui/material";

export default function MyMultiInput({
  name,
  label,
  value,
  error = null,
  onChange,
  ...other
}) {
  return (
    <TextField
      label={label}
      name={name}
      multiline
      minRows={5}
      maxRows={10}
      value={value}
      onChange={onChange}
      {...other}
      {...(error && { error: true, helperText: error })}
    />
  );
}
