import React, { useState } from "react";
import Checkbox from "@mui/material/Checkbox";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

const AutoCompleteCheckboxes = ({ label, options, onHandleChange }) => {
  const [selectedValues, setSelectedValues] = useState([]);
  const handleOnCheckUpdate = (event, newValue) => {
    setSelectedValues(newValue);
    onHandleChange(newValue);
  };

  return (
    <Autocomplete
      multiple
      id="checkboxes-tags-demo"
      options={options}
      disableCloseOnSelect
      onChange={(event, newValue) => {
        handleOnCheckUpdate(event, newValue);
      }}
      renderOption={(props, option, { selected }) => (
        <li {...props}>
          <Checkbox
            icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
            checkedIcon={<CheckBoxIcon fontSize="small" />}
            style={{ marginRight: 8 }}
            checked={selected}
          />
          {option}
        </li>
      )}
      style={{ width: 150, margin: "8px" }}
      renderInput={(params) => (
        <TextField {...params} label={label} placeholder="Favorites" />
      )}
    />
  );
};

export default AutoCompleteCheckboxes;
