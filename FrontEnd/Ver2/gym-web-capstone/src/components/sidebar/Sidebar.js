import { Dashboard } from "@mui/icons-material";
import NotificationNoneIcon from "@mui/icons-material/NotificationsNone";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import InsertChartIcon from "@mui/icons-material/InsertChart";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import NewspaperIcon from "@mui/icons-material/Newspaper";
import SportsGymnasticsIcon from "@mui/icons-material/SportsGymnastics";
import ChangeCircleIcon from "@mui/icons-material/ChangeCircle";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import "./sidebar.scss";
import { logoutUser } from "../../redux";
import { connect } from "react-redux";

const Sidebar = ({ auth, logout }) => {
  let navigate = useNavigate();

  const logoutHandler = async (e) => {
    e.preventDefault();
    await logout();
    navigate("/login", { replace: true });
  };

  let shouldDisplay =
    auth?.user?.role[0]?.name === "ROLE_ADMIN" ? "block" : "none";

  return (
    <div className="sidebar">
      <div className="top">
        <Link to="/dashboard" style={{ textDecoration: "none" }} replace={false}>
          <span className="logo">Quản lý Gym</span>
        </Link>
      </div>
      <hr />
      <div className="center">
        <ul>
          {/* <p className="title">Màn</p>
          <Link to="/dashboard" className="link-nav">
            <li>
              <Dashboard className="icon" />
              <span>Dashboard</span>
            </li>
          </Link> */}
          <p className="title">Quản Lý</p>
          <Link
            to="/dashboard/employees"
            className="link-nav"
            style={{ display: shouldDisplay }}
            replace={false}
          >
            <li>
              <ManageAccountsIcon className="icon" />
              <span>Nhân Viên</span>
            </li>
          </Link>
          <Link
            to="/dashboard/users"
            className="link-nav"
            style={{ textDecoration: "none" }}
            replace={false}
          >
            <li>
              <PersonOutlineIcon className="icon" />
              <span>Người Tập</span>
            </li>
          </Link>
          <Link
            to="/dashboard/personal-trainer"
            className="link-nav"
            style={{ textDecoration: "none" }}
            replace={false}
          >
            <li>
              <SportsGymnasticsIcon className="icon" />
              <span>Huấn luyện viên</span>
            </li>
          </Link>
          <p className="title">Tiện Ích</p>
          <Link to="/dashboard/transactions" className="link-nav" replace={false}>
            <li>
              <InsertChartIcon className="icon" />
              <span>Giao dịch</span>
            </li>
          </Link>
          <Link to="/dashboard/change-pt" className="link-nav" replace={false}>
            <li>
              <ChangeCircleIcon className="icon" />
              <span>Yêu cầu đổi HLV</span>
            </li>
          </Link>
          {/* <Link to="/dashboard/notification" className="link-nav">
            <li>
              <NotificationNoneIcon className="icon" />
              <span>Thông báo</span>
            </li>
          </Link> */}
          <Link to="/dashboard/news" className="link-nav" replace={false}>
            <li>
              <NewspaperIcon className="icon" />
              <span>Bài Viết</span>
            </li>
          </Link>
          {/* <p className="title">Cá Nhân</p>
          <li>
            <AccountCircleIcon className="icon" />
            <span>Trang Cá Nhân</span>
          </li> */}
          <li onClick={logoutHandler}>
            <ExitToAppIcon className="icon" />
            <span>Đăng Xuất</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logoutUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
