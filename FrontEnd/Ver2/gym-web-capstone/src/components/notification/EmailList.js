import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { Checkbox, IconButton } from "@mui/material";
import RedoIcon from "@mui/icons-material/Redo";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import KeyboardHideIcon from "@mui/icons-material/KeyboardHide";
import SettingsIcon from "@mui/icons-material/Settings";
import React from "react";
import "./emailList.scss";
import EmailRow from "./EmailRow";

const EmailList = () => {
  return (
    <div className="emailList content-notification">
      <div className="emailList-settings">
        <div className="emailList-settingsLeft">
          <Checkbox />
          <IconButton>
            <ArrowDropDownIcon />
          </IconButton>
          <IconButton>
            <RedoIcon />
          </IconButton>
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        </div>
        <div className="emailList_settingsRight">
          <IconButton>
            <ChevronLeftIcon />
          </IconButton>
          <IconButton>
            <ChevronRightIcon />
          </IconButton>
          <IconButton>
            <KeyboardHideIcon />
          </IconButton>
          <IconButton>
            <SettingsIcon />
          </IconButton>
        </div>
      </div>
      <div className="emailList_list">
        <EmailRow
          id="1"
          title="Email1"
          subject="Hey i am email 1!!!"
          description="This is a test"
          time="10pm"
        />
        <EmailRow
          id="2"
          title="Twitch"
          subject="Hey i am email 2!!!"
          description="This is a test"
          time="11pm"
        />
      </div>
    </div>
  );
};

export default EmailList;
