import {IconButton} from '@mui/material'
import React from 'react'
import './header.scss'
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

const Header = () => {
  return (
    <div className='header'>
        <div className='header-left'>
            <IconButton>
                <MenuIcon/>
            </IconButton>
        </div>
        <div className='header-middle'>
            <SearchIcon/>
            <input placeholder='Search email' type='text'/>
            <ArrowDropDownIcon className='header-input-carel'/>
        </div>
        <div className='header-right'>

        </div>
    </div>
  )
}

export default Header