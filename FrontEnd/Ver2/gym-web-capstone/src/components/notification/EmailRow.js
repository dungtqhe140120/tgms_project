import { Checkbox, IconButton } from "@mui/material";
import React from "react";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import LabelImportantIcon from "@mui/icons-material/LabelImportant";
import "./emailRow.scss";
import { useNavigate } from "react-router-dom";

const EmailRow = ({ id, title, subject, description, time }) => {
  const navigate = useNavigate();
  return (
    <div
      onClick={() => {
        navigate(`/dashboard/notification/mail-detail/${id}`, { replace: true });
      }}
      className="emailRow"
    >
      <div className="emailRow-options">
        <Checkbox />
        <IconButton>
          <StarBorderIcon />
        </IconButton>
        <IconButton>
          <LabelImportantIcon />
        </IconButton>
      </div>
      <div className="emailRow-title">{title}</div>
      <div className="emailRow-message">
        <h4>
          {subject} <span className="emailRow-description">{description}</span>
        </h4>
      </div>
      <div className="emailRow-time">{time}</div>
    </div>
  );
};

export default EmailRow;
