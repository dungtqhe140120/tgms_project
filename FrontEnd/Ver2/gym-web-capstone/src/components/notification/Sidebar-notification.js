import React from "react";
import "./sidebar-notification.scss";
import AddIcon from "@mui/icons-material/Add";
import { Button } from "@mui/material";
import SidebarOption from "./SidebarOption";
import InboxIcon from "@mui/icons-material/Inbox";
import StarIcon from "@mui/icons-material/Star";
import NearMeIcon from "@mui/icons-material/NearMe";
import { Link } from "react-router-dom";

const SidebarNotification = ({show, setShow}) => {

  const handleComposeClick = () => {
    if (!show) {
      setShow(true);
    }
  }

  return (
    <div className="sidebar">
      <Button
        startIcon={<AddIcon fontSize="large" />}
        className="sidebar-compose"
        onClick={handleComposeClick}
      >
        Compose
      </Button>

      <Link to="mail-list">
        <SidebarOption
          Icon={InboxIcon}
          title="Inbox"
          number={54}
          selected={true}
          replace
        />
      </Link>
      <Link to="starred">
        <SidebarOption Icon={StarIcon} title="Starred" number={54} />
      </Link>
      <Link to="sent">
        <SidebarOption Icon={NearMeIcon} title="Sent" number={54} />
      </Link>
    </div>
  );
};

export default SidebarNotification;
