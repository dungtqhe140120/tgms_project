import {Rating} from '@mui/material'
import React from 'react'
import './rate.scss'

const Rate = ({rator, avatar, date, comment, rateStar}) => {
  return (
    <div className='rate-one'>
      <img src={avatar} alt="" className='avatar-rate'/>
      <h4 className='rator-name'>{rator}</h4>
      <span>- {date}</span>
      <br/>
      <p>{comment}</p>
      {rateStar && <Rating name="read-only" value={rateStar} precision={0.5} readOnly />}
    </div>
  )
}

export default Rate