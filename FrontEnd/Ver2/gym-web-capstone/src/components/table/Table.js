import React from "react";
import "./table.scss";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

const rows = [
  {
    id: 1143155,
    employeeName: "Employe1",
    employeeId: 1,
    employeeImg: "https://m.media-amazon.com/images/I/31JaiPXYI8L._AC_UY327_FMwebp_QL65_.jpg",
    createdDate: "15-3-2022",
    startDate: "15-3-2022",
    endDate: "15-7-2022",
    money: 3000000,
  },
  {
    id: 1143156,
    employeeName: "Employe1",
    employeeId: 1,
    employeeImg: "https://m.media-amazon.com/images/I/31JaiPXYI8L._AC_UY327_FMwebp_QL65_.jpg",
    createdDate: "15-7-2022",
    startDate: "15-7-2022",
    endDate: "15-10-2022",
    money: 3000000,
  }
];

const List = () => {
  return (
    <TableContainer component={Paper} className="table">
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell className="tableCell">ID</TableCell>
            <TableCell className="tableCell">Nhân viên đăng ký</TableCell>
            <TableCell className="tableCell">Ngày đăng ký</TableCell>
            <TableCell className="tableCell">Ngày bắt đầu</TableCell>
            <TableCell className="tableCell">Ngày kết thúc</TableCell>
            <TableCell className="tableCell">Tiền thanh toán</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows &&
            rows.map((row) => (
              <TableRow key={row.id}>
                <TableCell className="tableCell">{row.id}</TableCell>
                <TableCell className="tableCell">
                  <div className="cellWrapper">
                    <img src={row.employeeImg} alt="" className="image" />
                    {row.employeeName}
                  </div>
                </TableCell>
                <TableCell className="tableCell">{row.createdDate}</TableCell>
                <TableCell className="tableCell">{row.startDate}</TableCell>
                <TableCell className="tableCell">{row.endDate}</TableCell>
                <TableCell className="tableCell">{row.money}</TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default List;
