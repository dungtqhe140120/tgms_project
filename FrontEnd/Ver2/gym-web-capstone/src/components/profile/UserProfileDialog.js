import React from "react";
import {
  Box,
  Button,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
} from "@mui/material";
import UserProfile from "./UserProfile";
import UserProfileDetail from "./UserProfileDetail";

const UserProfileDialog = ({
  open,
  title,
  userprofile,
  handleClose,
  children,
}) => {
  
  return (
    <>
      <Dialog maxWidth="lg" open={open} keepMounted onClose={handleClose}>
        <DialogTitle>
          Thông tin cá nhân chi tiết
          <Divider />
        </DialogTitle>
        <DialogContent>
          <Box sx={{ flexGrow: 1, py: 8 }}>
            <Container maxWidth="lg">
              <Grid container spacing={3}>
                <Grid item lg={4} md={6} xs={12}>
                  <UserProfile
                    fullname={userprofile?.full_name || "Tên chưa rõ"}
                    avatar={userprofile?.avatar || ""}
                    display1={
                      userprofile?.experience || "Số năm kinh nghiệm chưa rõ"
                    }
                    display2={
                      userprofile?.short_description || "Không có mô tả"
                    }
                  />
                </Grid>
                <Grid item lg={8} md={6} xs={12}>
                  <UserProfileDetail
                    fullname={userprofile?.full_name || "Tên chưa rõ"}
                    email={userprofile?.email || "Email chưa rõ"}
                    phone={userprofile?.phone || "Số điện thoại chưa rõ"}
                    gender={userprofile?.gender || "Giới tính chưa rõ"}
                    birthday={userprofile?.birthday || "Ngày sinh chưa rõ"}
                  />
                </Grid>
              </Grid>
            </Container>
            <Container maxWidth="lg">{children}</Container>
          </Box>
        </DialogContent>
        <DialogActions>
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              p: 2,
            }}
          >
            <Button color="primary" variant="contained" onClick={handleClose}>
              Thoát
            </Button>
          </Box>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default UserProfileDialog;
