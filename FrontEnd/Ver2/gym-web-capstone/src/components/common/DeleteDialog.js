import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";

const DeleteDialog = ({ open, title, content, handleDelete, handleClose }) => {
  return (
    <>
      <Dialog open={open} keepMounted onClose={handleClose}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>{content}</DialogContent>
        <DialogActions>
          <Button onClick={handleDelete}>Xóa</Button>
          <Button onClick={handleClose} style={{ color: "red" }}>
            Hủy bỏ
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default DeleteDialog;
