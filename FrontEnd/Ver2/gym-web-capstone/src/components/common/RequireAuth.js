import React from "react";
import { useLocation, Navigate, Outlet } from "react-router-dom";
import { connect } from "react-redux";
import { isPersistedState } from "../../helpers";
import {loginSuccess} from "../../redux";

const RequireAuth = ({ auth, allowedRoles, loginBySession }) => {
  const location = useLocation();

  const authSession = isPersistedState("auth");
  if (authSession && !auth?.user?.role) {
    loginBySession(authSession);
  }

  const shouldAllow = 
    isPersistedState("auth")?.role &&
    allowedRoles?.includes(isPersistedState("auth")?.role[0].name);

  return shouldAllow ? (
    <Outlet />
  ) : isPersistedState("auth") ? (
    <Navigate to="/unauthorized" state={{ from: location }} replace />
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  );
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginBySession: (authentication) => dispatch(loginSuccess(authentication))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RequireAuth);
