import React, { useEffect } from "react";
import { Outlet } from "react-router-dom";
import Navbar from "../navbar/Navbar";
import Sidebar from "../sidebar/Sidebar";
import "./layout.scss";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import PersonNoImage from "../../assets/employee-no-image.webp";
import { Box, Grid } from "@mui/material";

const Layout = () => {

  return (
    <main className="dashboard-layout">
      <Sidebar />
      <div className="right-side-bar">
        {/* <Navbar /> */}
        <ToastContainer />
        <Outlet />
      </div>
    </main>
  );
};

export default Layout;
