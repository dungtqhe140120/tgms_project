import React from "react";
import Header from "../Header/Header";
import "./hero.scss";
import hero_image from "../../../../assets/hero_image.png";
import hero_image_back from "../../../../assets/hero_image_back.png";
import Heart from "../../../../assets/heart.png";
import Calories from "../../../../assets/calories.png";

const Hero = () => {
  return (
    <div className="hero" id="hero">

      <div className="blur hero-blur"></div>
      <div className="left-h">
        <Header />

        {/* the best ad */}
        <div className="the-best-ad">
          <span>Top trung tâm fitness hàng đầu</span>
        </div>

        {/* Hero Heading */}
        <div className="hero-text">
          <div>
            <span className="stroke-text">Biến giấc mơ </span>
            <span>trở thành</span>
          </div>
          <div>
            <span>sự thật</span>
          </div>
          <div>
            <span>
              Đến đây bạn sẽ được sử dụng những tiện ích tốt nhất giúp bạn nâng cao sức khỏe cải thiện vóc dáng!
            </span>
          </div>
        </div>
        <div className="figures">
          <div>
            <span>+140</span>
            <span>Huấn luyện viên</span>
          </div>
          <div>
            <span>+978</span>
            <span>Người đã tập</span>
          </div>
          <div>
            <span>+50</span>
            <span>Chương trình tập</span>
          </div>
        </div>
      </div>
      <div className="right-h">
        <button className="btn">tham gia ngay!</button>

        <div className="heart-rate">
          <img src={Heart} alt="" />
          <span>Heart Rate</span>
          <span>116 bpm</span>
        </div>

        {/* hero images */}
        <img src={hero_image} alt="" className="hero-image" />
        <img src={hero_image_back} alt="" className="hero-image-back" />
        {/* calories */}
        <div className="calories">
          <img src={Calories} alt="" />
          <div>
            <span>Calories Burned</span>
            <span>220 kcal</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hero;
