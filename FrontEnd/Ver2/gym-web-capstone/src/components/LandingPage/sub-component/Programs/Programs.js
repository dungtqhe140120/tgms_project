import React from "react";
import "./programs.scss";
import { programsData } from "../../../../data/programsData";

const Programs = () => {
  return (
    <div className="Programs" id="programs">
      {/* header */}
      <div className="program-header">
        <span className="stroke-text">Khám phá các</span>
        <span>Chương trình</span>
        <span className="stroke-text">đánh thức cơ thể</span>
      </div>

      <div className="program-categories">
        {programsData.map((program) => (
          <div className="category">
            {program.image}
            <span>{program.heading}</span>
            <span>{program.details}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Programs;
