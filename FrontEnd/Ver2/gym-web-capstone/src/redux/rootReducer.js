import { combineReducers } from "redux";
import authReducer from "./auth/authReducer.js";
import userReducer from "./user/userReducer.js";

const rootReducer = combineReducers({
  user: userReducer,
  auth: authReducer,
});

export default rootReducer;
