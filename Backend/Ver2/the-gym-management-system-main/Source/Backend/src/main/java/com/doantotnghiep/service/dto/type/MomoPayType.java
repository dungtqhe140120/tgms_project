package com.doantotnghiep.service.dto.type;

import com.doantotnghiep.utils.AppUtils;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 *
 */
public enum MomoPayType {
    WEB_APP,
    APP,
    QR,
    MINI_APP,
    EMPTY;

    @JsonCreator
    public static MomoPayType forValue(String value) {
        return AppUtils.createMomoPayTypeByValue(value);
    }
}
