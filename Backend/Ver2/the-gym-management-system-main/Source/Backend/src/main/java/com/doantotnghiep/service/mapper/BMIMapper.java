package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.BMI;
import com.doantotnghiep.service.dto.BMIDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link BMI} and its DTO {@link BMIDTO}.
 */
@Mapper(componentModel = "spring")
public interface BMIMapper extends EntityMapper<BMIDTO, BMI> {}
