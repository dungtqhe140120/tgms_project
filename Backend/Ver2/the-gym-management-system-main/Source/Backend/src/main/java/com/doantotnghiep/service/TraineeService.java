package com.doantotnghiep.service;

import com.doantotnghiep.domain.Employee;
import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.domain.Trainee;
import com.doantotnghiep.service.dto.PersonalTrainerFullDTO;
import com.doantotnghiep.service.dto.TraineeDTO;

import java.util.Optional;

import com.doantotnghiep.service.dto.TraineeFullDTO;
import com.doantotnghiep.service.dto.display.TraineeDisplayDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqTraineeFullDTO;
import com.doantotnghiep.service.dto.requestDTO.TraineeDetailDTO;
import com.doantotnghiep.service.resources.TraineeInfFullResources;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.Trainee}.
 */
public interface TraineeService {
    /**
     * Save a trainee.
     *
     * @param traineeDTO the entity to save.
     * @return the persisted entity.
     */
    TraineeDTO save(TraineeDTO traineeDTO);

    /**
     * Partially updates a trainee.
     *
     * @param traineeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TraineeDTO> partialUpdate(TraineeDTO traineeDTO);

    /**
     * Get all the trainees.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TraineeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" trainee.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TraineeDTO> findOne(Long id);

    Optional<TraineeInfFullResources> findFullInfTrainee (Long traineeId);

    /**
     * Delete the "id" trainee.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Trainee saveTrainee(Trainee trainee);

    TraineeFullDTO getOneTraineeFullDTOByUser(Long user_id);

    Boolean getExistTrainee(Long user_id);

    Trainee findTraineeByUserID(Long user_id);

    ReqTraineeFullDTO getOneReqTraineeFullDTOByUser(Long user_id);

    Page<TraineeDisplayDTO> getAllListTraineeDisplayDTO(Pageable pageable);

    Page<TraineeDisplayDTO> getAllTraineeDisplayDTOByName(Pageable pageable, String traineeName);

    Trainee findTraineeByID(Long id);

    TraineeDetailDTO getTraineeDetailDto(Long id);

    TraineeDetailDTO updateTraineeDetail(Long id, TraineeDetailDTO traineeDetailDTO);
}
