package com.doantotnghiep.repository;

import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.service.dto.PersonalTrainerFullDTO;
import com.doantotnghiep.service.dto.requestDTO.PersonalTraineeUpdateDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import liquibase.pro.packaged.Q;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data SQL repository for the PersonalTrainer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonalTrainerRepository extends JpaRepository<PersonalTrainer, Long> {

    @Query(value = "select new com.doantotnghiep.service.dto.PersonalTrainerFullDTO(pt.id, pt.experience, pt.short_description, pt.created_date, p.address, p.avatar, p.birthday, p.email, p.fullname, p.gender, p.is_active, p.modified_by, p.modified_date, p.objective, p.phone, p.first_login) from  PersonalTrainer   pt inner join Profile p  on p.id=pt.profile.id where p.user.id= ?1")
    PersonalTrainerFullDTO getOnePersonalTrainerFullDTOByUser(Long user_id);

    @Query(value = "select  count(pt.id) from  PersonalTrainer   pt inner join Profile p  on p.id=pt.profile.id where p.user.id= ?1")
    Integer countPTByUserID(Long user_id);

    @Query(value = "select pt.* from personal_trainer pt inner join jhi_profile p on pt.profile_id=p.id inner join jhi_user u on u.id=p.user_id where u.id=?1", nativeQuery = true)
    PersonalTrainer getPTByUserID(Long user_id);

    @Query(value = "select new com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO(pt.id, pt.experience, pt.short_description, p.address, p.avatar, p.birthday, p.email, p.fullname, p.gender, p.objective, p.phone) from PersonalTrainer  pt inner join Profile p on pt.profile.id=p.id inner join User u on u.id=p.user.id where u.id= ?1")
    ReqPTFullDTO getReqPTFullDTOByUserID(Long user_id);

    @Query(value = "select new com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO(pt.id, pt.experience, " +
        "pt.short_description, p.address, p.avatar, p.birthday, p.email, p.fullname, p.gender, p.objective, p.phone)" +
        " from PersonalTrainer  pt inner join Profile p on pt.profile.id=p.id where pt.id not in :ids")
    List<ReqPTFullDTO> getPTsFreeSlot(@Param("ids") List<Long> ids);

    @Query(value = "select new com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO(pt.id, pt.experience, " +
        "pt.short_description, p.address, p.avatar, p.birthday, p.email, p.fullname, p.gender, p.objective, p.phone)" +
        " from PersonalTrainer  pt inner join Profile p on pt.profile.id=p.id")
    List<ReqPTFullDTO> getPTs();

    @Query(value = "select new com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO(pt.id, pt.experience, " +
        "pt.short_description, p.address, p.avatar, p.birthday, p.email, p.fullname, p.gender, p.objective, p.phone)" +
        " from PersonalTrainer  pt inner join Profile p on pt.profile.id=p.id WHERE pt.id = :id")
    Optional<ReqPTFullDTO> getPTById(@Param("id") Long id);

    @Query(value = "SELECT t.id " +
        "FROM PersonalTrainer t INNER JOIN Profile p ON p.id = t.profile.id " +
        "WHERE p.user.id = :userId ")
    Long getPTId(@Param("userId") Long userId);

    @Query("select new com.doantotnghiep.service.dto.requestDTO.PersonalTraineeUpdateDTO(" +
        "pt.id," +
        "u.login," +
        "u.firstName," +
        "u.lastName," +
        "u.email," +
        "u.imageUrl," +
        "pro.gender," +
        "pro.birthday," +
        "pro.phone," +
        "pro.address," +
        "pt.experience," +
        "pt.short_description) " +
        "from PersonalTrainer pt " +
        "inner join Profile pro on pt.profile.id=pro.id " +
        "inner join User u on pro.user.id=u.id WHERE pt.id = :ptId")
    Optional<PersonalTraineeUpdateDTO> getPersonalTrainerDTOById(@Param("ptId") Long ptId);
    PersonalTrainer getPersonalTrainerById(Long id);

    @Query(value = "SELECT p.avatar " +
        "FROM PersonalTrainer pt INNER JOIN Profile p ON p.id = pt.profile.id " +
        "WHERE pt.id = :ptId ")
    String getImagePT(@Param("ptId") Long ptId);

    @Query("SELECT pt FROM PersonalTrainer pt JOIN FETCH pt.profile WHERE pt.id = :ptId")
    Optional<PersonalTrainer> findPersonalTrainerJoinFetch(@Param("ptId") Long ptId);
}
