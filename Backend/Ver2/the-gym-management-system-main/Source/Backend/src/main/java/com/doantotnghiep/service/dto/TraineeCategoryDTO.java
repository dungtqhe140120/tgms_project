package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.doantotnghiep.domain.TraineeCategory} entity.
 */
public class TraineeCategoryDTO implements Serializable {

    private Long id;

    private LocalDate modify_date;
    private Long assign_by;

    public Long getAssign_by() {
        return assign_by;
    }

    public void setAssign_by(Long assign_by) {
        this.assign_by = assign_by;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getModify_date() {
        return modify_date;
    }

    public void setModify_date(LocalDate modify_date) {
        this.modify_date = modify_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TraineeCategoryDTO)) {
            return false;
        }

        TraineeCategoryDTO traineeCategoryDTO = (TraineeCategoryDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, traineeCategoryDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TraineeCategoryDTO{" +
            "id=" + getId() +
            "}";
    }
}
