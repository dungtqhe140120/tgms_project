package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.UserGuide;
import com.doantotnghiep.service.dto.UserGuideDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserGuide} and its DTO {@link UserGuideDTO}.
 */
@Mapper(componentModel = "spring")
public interface UserGuideMapper extends EntityMapper<UserGuideDTO, UserGuide> {}
