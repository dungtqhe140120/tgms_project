package com.doantotnghiep.specification;

import com.doantotnghiep.domain.GymCalendar;
import com.doantotnghiep.service.dto.requestDTO.GymCalendarFilter;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
public class GymCalendarSpecification implements Specification<GymCalendar>{
    private final GymCalendarFilter filter;

    @Override
    public Predicate toPredicate(Root<GymCalendar> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        final List<Predicate> predicates = new ArrayList<>();

        if (!Objects.isNull(filter.getTraineeId())) {
            predicates.add(root.get("traineeId").in(filter.getTraineeId()));
        }

        if (!Objects.isNull(filter.getCalendarDateFrom())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("calendarDate"), filter.getCalendarDateFrom()));
        }

        if (!Objects.isNull(filter.getCalendarDateTo())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("calendarDate"), filter.getCalendarDateTo()));
        }
        query.orderBy(criteriaBuilder.asc(root.get("calendarDate")));
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
