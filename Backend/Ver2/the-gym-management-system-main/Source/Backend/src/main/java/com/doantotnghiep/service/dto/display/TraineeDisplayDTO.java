package com.doantotnghiep.service.dto.display;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

public class TraineeDisplayDTO implements Serializable {

    private Long id;
    private String full_name;
    private String avatar;
    private Integer gender;
    private String phone;
    private String email;
    private String address;
    private LocalDate created_date;
    private LocalDate expired_date;

    public TraineeDisplayDTO() {
    }

    public TraineeDisplayDTO(Long id, String full_name, String avatar, Integer gender, String phone, String email, String address, LocalDate created_date, LocalDate expired_date) {
        this.id = id;
        this.full_name = full_name;
        this.avatar = avatar;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.created_date = created_date;
        this.expired_date = expired_date;
    }

    public TraineeDisplayDTO(Long id, String full_name, String avatar, Integer gender, String phone, String email, String address, Instant created_date, LocalDate expired_date) {
        this.id = id;
        this.full_name = full_name;
        this.avatar = avatar;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
       LocalDate localDate= LocalDateTime.ofInstant(created_date, ZoneOffset.UTC).toLocalDate();
        this.created_date = localDate;
        this.expired_date = expired_date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public LocalDate getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(LocalDate expired_date) {
        this.expired_date = expired_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TraineeDisplayDTO that = (TraineeDisplayDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(full_name, that.full_name) && Objects.equals(avatar, that.avatar) && Objects.equals(gender, that.gender) && Objects.equals(phone, that.phone) && Objects.equals(email, that.email) && Objects.equals(address, that.address) && Objects.equals(created_date, that.created_date) && Objects.equals(expired_date, that.expired_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, full_name, avatar, gender, phone, email, address, created_date, expired_date);
    }

    @Override
    public String toString() {
        return "TraineeDisplayDTO{" +
            "id=" + id +
            ", full_name='" + full_name + '\'' +
            ", avatar='" + avatar + '\'' +
            ", gender=" + gender +
            ", phone='" + phone + '\'' +
            ", email='" + email + '\'' +
            ", address='" + address + '\'' +
            ", created_date=" + created_date +
            ", expired_date=" + expired_date +
            '}';
    }
}
