package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.UserGuide;
import com.doantotnghiep.repository.UserGuideRepository;
import com.doantotnghiep.service.UserGuideService;
import com.doantotnghiep.service.dto.UserGuideDTO;
import com.doantotnghiep.service.mapper.UserGuideMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UserGuide}.
 */
@Service
@Transactional
public class UserGuideServiceImpl implements UserGuideService {

    private final Logger log = LoggerFactory.getLogger(UserGuideServiceImpl.class);

    private final UserGuideRepository userGuideRepository;

    private final UserGuideMapper userGuideMapper;

    public UserGuideServiceImpl(UserGuideRepository userGuideRepository, UserGuideMapper userGuideMapper) {
        this.userGuideRepository = userGuideRepository;
        this.userGuideMapper = userGuideMapper;
    }

    @Override
    public UserGuideDTO save(UserGuideDTO userGuideDTO) {
        log.debug("Request to save UserGuide : {}", userGuideDTO);
        UserGuide userGuide = userGuideMapper.toEntity(userGuideDTO);
        userGuide = userGuideRepository.save(userGuide);
        return userGuideMapper.toDto(userGuide);
    }

    @Override
    public UserGuideDTO update(UserGuideDTO userGuideDTO) {
        log.debug("Request to save UserGuide : {}", userGuideDTO);
        UserGuide userGuide = userGuideMapper.toEntity(userGuideDTO);
        userGuide = userGuideRepository.save(userGuide);
        return userGuideMapper.toDto(userGuide);
    }

    @Override
    public Optional<UserGuideDTO> partialUpdate(UserGuideDTO userGuideDTO) {
        log.debug("Request to partially update UserGuide : {}", userGuideDTO);

        return userGuideRepository
            .findById(userGuideDTO.getId())
            .map(existingUserGuide -> {
                userGuideMapper.partialUpdate(existingUserGuide, userGuideDTO);

                return existingUserGuide;
            })
            .map(userGuideRepository::save)
            .map(userGuideMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserGuideDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserGuides");
        return userGuideRepository.findAll(pageable).map(userGuideMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserGuideDTO> findOne(Long id) {
        log.debug("Request to get UserGuide : {}", id);
        return userGuideRepository.findById(id).map(userGuideMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserGuide : {}", id);
        userGuideRepository.deleteById(id);
    }
}
