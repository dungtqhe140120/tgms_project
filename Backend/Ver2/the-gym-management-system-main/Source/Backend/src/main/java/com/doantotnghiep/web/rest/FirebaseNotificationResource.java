package com.doantotnghiep.web.rest;


import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.doantotnghiep.domain.Authority;
import com.doantotnghiep.domain.Profile;
import com.doantotnghiep.repository.ProfileRepository;
import com.doantotnghiep.service.FirebaseNotificationService;
import com.doantotnghiep.service.dto.firebase.Notice;
import com.google.firebase.messaging.BatchResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping("/api")
@Slf4j
@RequiredArgsConstructor
public class FirebaseNotificationResource {
    private final FirebaseNotificationService firebaseNotificationService;

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @PostMapping("/push-notification")
    public BatchResponse sendNotification(@RequestBody Notice notice) {
        return firebaseNotificationService.sendNotification(notice);
    }
}
