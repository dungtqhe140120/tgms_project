package com.doantotnghiep.service.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class GymPriceDTO {
    private Long id;

    private Long registerDays;

    private Long price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRegisterDays() {
        return registerDays;
    }

    public void setRegisterDays(Long registerDays) {
        this.registerDays = registerDays;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
