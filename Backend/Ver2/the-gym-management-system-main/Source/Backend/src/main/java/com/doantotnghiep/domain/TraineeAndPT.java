package com.doantotnghiep.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A TraineeAndPT.
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "trainee_andpt")
public class TraineeAndPT implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "expired_date")
    private LocalDate expiredDate;

    @NotNull
    @Column(name = "registered_date", nullable = false)
    private LocalDate registeredDate;

    @Column(name = "price")
    private Long price;

    @Min(value = 0)
    @Max(value = 2)
    @Column(name = "confirmed")
    private Integer confirmStatus;

    @Column(name = "confirmed_date")
    private LocalDate confirmDate;

    @Column(name = "confirmed_by")
    private String confirmBy;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnoreProperties(value = {"TraineeAndPT"}, allowSetters = true)
    private Trainee trainee;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnoreProperties(value = {"TraineeAndPT"}, allowSetters = true)
    private PersonalTrainer personalTrainer;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnoreProperties(value = {"TraineeAndPT"}, allowSetters = true)
    private SlotTime slotTime;

    @Column(name = "transaction_id")
    private Long transactionId;
    // jhipster-needle-entity-add-field - JHipster will add fields here
}
