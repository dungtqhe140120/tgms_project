package com.doantotnghiep.web.rest;

import com.doantotnghiep.domain.Contacts;
import com.doantotnghiep.service.ContactsService;
import com.doantotnghiep.service.dto.ContactsDTO;
import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import com.doantotnghiep.service.dto.requestDTO.ContactsFilter;
import com.doantotnghiep.service.dto.requestDTO.ReqCreateTraineeAndPT;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Objects;

@RestController
@RequestMapping("/api")
public class ContactsController {

    private static final String ENTITY_NAME = "Contacts";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactsService contactsService;

    public ContactsController(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @GetMapping("/contacts")
    public ResponseEntity<Page<ContactsDTO>> getContacts(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        ContactsFilter filter
    ) {
        var contacts = contactsService.getAll(filter, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), contacts);
        return ResponseEntity.ok().headers(headers).body(contacts);
    }


    @PostMapping("/contacts")
    public ResponseEntity<ContactsDTO> createContacts(@RequestBody ContactsDTO request) throws URISyntaxException {
        request.setCreatedAt(LocalDate.now());
        var result = contactsService.save(request);
        return ResponseEntity
            .created(new URI("/api/contacts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @PutMapping("/contacts/{id}")
    public ResponseEntity<ContactsDTO> updateTraineeAndPT(
        @PathVariable(value = "id") final Long id,
        @RequestBody ContactsDTO request
    ) throws URISyntaxException {
        if (request.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, request.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }
        ContactsDTO contactsDTO = contactsService.getContactsById(id);
        contactsDTO.setChecked(true);
        contactsDTO.setCheckedAt(LocalDate.now());
        contactsDTO.setNote(request.getNote());
        contactsDTO.setStatus(request.getStatus());
        ContactsDTO result = contactsService.save(contactsDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactsDTO.getId().toString()))
            .body(result);
    }
}
