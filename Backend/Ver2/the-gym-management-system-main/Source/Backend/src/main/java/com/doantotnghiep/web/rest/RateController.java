package com.doantotnghiep.web.rest;

import com.doantotnghiep.service.RateService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.AboutUSDTO;
import com.doantotnghiep.service.dto.RateCenterDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqRate;
import com.doantotnghiep.service.dto.response.ResponseRate;
import com.doantotnghiep.service.mappers2.HomeMapper2;
import com.doantotnghiep.service.resources.HomeTraineeEntity;
import com.doantotnghiep.service.resources.HomeTraineeResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RateController {
    private static final String ENTITY_NAME = "RateController";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RateService rateService;
    private final UserService userService;
    public RateController(RateService rateService, UserService userService){
        this.userService = userService;
        this.rateService = rateService;
    }

    @GetMapping("/rates")
    public ResponseEntity<ReqRate> getRate() {
        Long userId = userService.getCurrentUserLogin().getId();
        var reqRate = Optional.of(rateService.getRateByUserId(userId));
        return ResponseUtil.wrapOrNotFound(reqRate);
    }

    @PutMapping ("/rates")
    public ResponseEntity<ResponseRate> saveRate(
        @RequestBody ReqRate request
    ) {
        Long userId = userService.getCurrentUserLogin().getId();
        var response = Optional.of(rateService.save(request, userId));
        return ResponseUtil.wrapOrNotFound(response);
    }

    @PutMapping ("/rates/center")
    public ResponseEntity<RateCenterDTO> saveRateCenter(
        @RequestBody RateCenterDTO request
    ) {
        Long userId = userService.getCurrentUserLogin().getId();
        var response = Optional.of(rateService.saveRateCenter(request, userId));
        return ResponseUtil.wrapOrNotFound(response);
    }

    @GetMapping("/rates/center")
    public ResponseEntity<RateCenterDTO> getRateCenter() {
        Long userId = userService.getCurrentUserLogin().getId();
        var reqRate = Optional.ofNullable(rateService.getRateCenter(userId));
        return ResponseUtil.wrapOrNotFound(reqRate);
    }
}
