package com.doantotnghiep.momo.retrofit;

import com.doantotnghiep.momo.model.TransactionConfirmParam;
import com.doantotnghiep.momo.model.TransactionConfirmResponse;
import com.doantotnghiep.momo.model.TransactionCreateParam;
import com.doantotnghiep.momo.model.TransactionCreateResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MomoAPI {
    @POST("v2/gateway/api/create")
    Call<TransactionCreateResponse> createTransaction(@Body TransactionCreateParam transactionCreateParam);

    @POST("/v2/gateway/api/query")
    Call<TransactionConfirmResponse> confirmTransaction(@Body TransactionConfirmParam transactionConfirmParam);
}
