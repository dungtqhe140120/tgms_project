package com.doantotnghiep.service.dto.form;

import javax.validation.constraints.NotBlank;

/**
 *
 */
public record UpdateFcmForm(
    @NotBlank(message = "Fcm is mandatory")
    String fcm) {
}
