package com.doantotnghiep.service;

import com.doantotnghiep.service.resources.HomeTraineeEntity;
import com.doantotnghiep.service.resources.HomeTraineeResource;
import org.springframework.data.domain.Pageable;

public interface HomeService {
    HomeTraineeEntity getHomeTrainee(@org.springdoc.api.annotations.ParameterObject Pageable pageable, Long traineeId);
}
