package com.doantotnghiep.service.mappers2;

import com.doantotnghiep.domain.TraineeAndPT;
import com.doantotnghiep.domain.TraineeAndPTDraft;
import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import com.doantotnghiep.service.resources.TraineeAndPtDraftResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public abstract class TraineeAndPTDraftMapper2 {
    public static final TraineeAndPTDraftMapper2 INSTANCE = Mappers.getMapper(TraineeAndPTDraftMapper2.class);

    public abstract TraineeAndPtDraftResource toResource(TraineeAndPTDraft entity);

    public abstract List<TraineeAndPtDraftResource> toResources(List<TraineeAndPTDraft> entities);

    public abstract TraineeAndPTDraft toEntity(TraineeAndPtDraftResource resource);

    public abstract List<TraineeAndPTDraft> toEntities(List<TraineeAndPtDraftResource> resources);

    @Mapping(target = "traineeAndPtId", source = "id")
    @Mapping(target = "traineeId", source = "trainee.id")
    @Mapping(target = "ptId", source = "personalTrainer.id")
    @Mapping(target = "slotId", source = "slotTime.id")
    public abstract TraineeAndPTDraft toEntity(TraineeAndPT traineeAndPT);
}
