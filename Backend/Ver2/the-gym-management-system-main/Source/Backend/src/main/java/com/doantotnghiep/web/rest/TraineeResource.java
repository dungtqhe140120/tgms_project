package com.doantotnghiep.web.rest;

import com.doantotnghiep.domain.Profile;
import com.doantotnghiep.domain.Trainee;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.repository.PersonalTrainerRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.ProfileService;
import com.doantotnghiep.service.TraineeService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.TraineeDTO;
import com.doantotnghiep.service.dto.TraineeFullDTO;
import com.doantotnghiep.service.dto.display.TraineeDisplayDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqTraineeFullDTO;
import com.doantotnghiep.service.dto.requestDTO.TraineeDetailDTO;
import com.doantotnghiep.service.resources.TraineeInfFullResources;
import com.doantotnghiep.service.resources.TraineeInfShortResource;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.util.StringUtils;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.Trainee}.
 */
@RestController
@RequestMapping("/api")
public class TraineeResource {

    private final Logger log = LoggerFactory.getLogger(TraineeResource.class);

    private static final String ENTITY_NAME = "trainee";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TraineeService traineeService;

    private final TraineeRepository traineeRepository;
    private final UserService userService;

    private final PersonalTrainerRepository personalTrainerRepository;

    private final ProfileService profileService;

    public TraineeResource(TraineeService traineeService, TraineeRepository traineeRepository, UserService userService,
                           ProfileService profileService, PersonalTrainerRepository personalTrainerRepository) {
        this.traineeService = traineeService;
        this.traineeRepository = traineeRepository;
        this.userService = userService;
        this.profileService = profileService;
        this.personalTrainerRepository = personalTrainerRepository;
    }

    /**
     * {@code POST  /trainees} : Create a new trainee.
     *
     * @param traineeDTO the traineeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new traineeDTO, or with status {@code 400 (Bad Request)} if the trainee has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/trainees")
    public ResponseEntity<TraineeDTO> createTrainee(@RequestBody TraineeDTO traineeDTO) throws URISyntaxException {
        log.debug("REST request to save Trainee : {}", traineeDTO);
        if (traineeDTO.getId() != null) {
            throw new BadRequestAlertException("A new trainee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TraineeDTO result = traineeService.save(traineeDTO);
        return ResponseEntity
            .created(new URI("/api/trainees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /trainees/:id} : Updates an existing trainee.
     *
     * @param id         the id of the traineeDTO to save.
     * @param traineeDTO the traineeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated traineeDTO,
     * or with status {@code 400 (Bad Request)} if the traineeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the traineeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/trainees/{id}")
    public ResponseEntity<TraineeDTO> updateTrainee(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TraineeDTO traineeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Trainee : {}, {}", id, traineeDTO);
        if (traineeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, traineeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!traineeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TraineeDTO result = traineeService.save(traineeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /trainees/:id} : Partial updates given fields of an existing trainee, field will ignore if it is null
     *
     * @param id         the id of the traineeDTO to save.
     * @param traineeDTO the traineeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated traineeDTO,
     * or with status {@code 400 (Bad Request)} if the traineeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the traineeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the traineeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/trainees/{id}", consumes = {"application/json", "application/merge-patch+json"})
    public ResponseEntity<TraineeDTO> partialUpdateTrainee(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TraineeDTO traineeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Trainee partially : {}, {}", id, traineeDTO);
        if (traineeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, traineeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!traineeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TraineeDTO> result = traineeService.partialUpdate(traineeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /trainees} : get all the trainees.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of trainees in body.
     */
    @GetMapping("/trainees")
    public ResponseEntity<List<TraineeDTO>> getAllTrainees(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Trainees");
        Page<TraineeDTO> page = traineeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /trainees-pt} : get all the trainees of pt.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of trainees in body.
     */
    @GetMapping("/trainees-pt")
    public ResponseEntity<List<TraineeInfShortResource>> getTraineesOfPT() {
        log.debug("REST request to get a list Trainees of PT");
        Long userId = userService.getCurrentUserLogin().getId();
        Long ptId = personalTrainerRepository.getPTId(userId);
        if (ptId == null) {
            throw new BadRequestAlertException("PT not exit", ENTITY_NAME, "PT not exit");
        }
        List<TraineeInfShortResource> traineeInfShortResources = traineeRepository.findTraineeOfPT(ptId, 1);
        return ResponseEntity.ok().body(traineeInfShortResources);
    }

    /**
     * {@code GET  /trainees/:id} : get the "id" trainee.
     *
     * @param id the id of the traineeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the traineeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/trainees/{id}")
    public ResponseEntity<TraineeDTO> getTrainee(@PathVariable Long id) {
        log.debug("REST request to get Trainee : {}", id);
        Optional<TraineeDTO> traineeDTO = traineeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(traineeDTO);
    }

    @GetMapping("/trainees/{id}/full-inf")
    public ResponseEntity<TraineeInfFullResources> getTraineeV2(@PathVariable Long id) {
        log.debug("REST request to get Trainee : {}", id);
        Optional<TraineeInfFullResources> fullInfTrainee = traineeService.findFullInfTrainee(id);
        return ResponseUtil.wrapOrNotFound(fullInfTrainee);
    }

    /**
     * {@code DELETE  /trainees/:id} : delete the "id" trainee.
     *
     * @param id the id of the traineeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/trainees/{id}")
    public ResponseEntity<Void> deleteTrainee(@PathVariable Long id) {
        log.debug("REST request to delete Trainee : {}", id);
        traineeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping("/trainees/getCurrentTrainee")
    public ResponseEntity<TraineeFullDTO> getCurrentTrainee() {
        log.debug("REST request to get getCurrentPersonalTrainer : {}");
        User currentUser = userService.getCurrentUserLogin();
        TraineeFullDTO traineeFullDTO = traineeService.getOneTraineeFullDTOByUser(currentUser.getId());
        return ResponseEntity.ok().body(traineeFullDTO);
    }

    @PutMapping("/trainees/updateInformation")
    public ResponseEntity<ReqTraineeFullDTO> updatePTInformation(@RequestBody ReqTraineeFullDTO reqTraineeFullDTO) {
        log.debug("REST request to get updatePTInformation : {}");
        if (reqTraineeFullDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        log.info("alooo: " + reqTraineeFullDTO);
        ReqTraineeFullDTO traineeFullDTO = null;
        User currentUser = userService.getCurrentUserLogin();

        if (traineeService.getExistTrainee(currentUser.getId())) {

            //update Trainee
            Trainee trainee = traineeService.findTraineeByUserID(currentUser.getId());
            trainee.setShort_Description(reqTraineeFullDTO.getShort_description());
            Trainee updatedTrainee = traineeService.saveTrainee(trainee);
            log.debug("updatedTrainee : " + updatedTrainee);

            //update Profile
            Profile profile = profileService.findProfileByUserID(currentUser.getId());
            profile.setAddress(reqTraineeFullDTO.getAddress());
            profile.setAvatar(reqTraineeFullDTO.getAvatar());
            profile.setBirthday(reqTraineeFullDTO.getBirthday());
            profile.setEmail(reqTraineeFullDTO.getEmail());
            profile.setGender(reqTraineeFullDTO.getGender());
            profile.setFullname(reqTraineeFullDTO.getFull_name());
            profile.setModified_by(currentUser.getId());
            profile.setModified_date(LocalDate.now());
            profile.setObjective(reqTraineeFullDTO.getObjective());
            profile.setPhone(reqTraineeFullDTO.getPhone());
            profile.setFirst_login(0);
            Profile updatedProfile = profileService.saveProfile(profile);
            log.debug("updatedProfile : " + updatedProfile);

            traineeFullDTO = traineeService.getOneReqTraineeFullDTOByUser(currentUser.getId());

        } else {
            throw new BadRequestAlertException("PT is not exist", ENTITY_NAME, "PTisNotExist");
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, reqTraineeFullDTO.getId().toString()))
            .body(traineeFullDTO);
    }

    @GetMapping("/trainees/getAllDisplayTrainees")
    public ResponseEntity<List<TraineeDisplayDTO>> getAllDisplayTrainees(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of getAllDisplayTrainees");
        Page<TraineeDisplayDTO> page = traineeService.getAllListTraineeDisplayDTO(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @GetMapping("/v2/trainees/getAllDisplayTrainees")
    public ResponseEntity<Page<TraineeDisplayDTO>> getAllDisplayTraineesV2(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(value = "nameTrainee", required = false) String nameTrainee
    ) {
        Page<TraineeDisplayDTO> page;
        if (StringUtils.isEmpty(nameTrainee)) {
            page = traineeService.getAllListTraineeDisplayDTO(pageable);
        } else {
            page = traineeService.getAllTraineeDisplayDTOByName(pageable, nameTrainee.trim());
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @GetMapping("v2/trainees/detail-profile/{traineeId}")
    public TraineeDetailDTO getTraineeDetailById(@PathVariable(value = "traineeId") final Long id){
        return traineeService.getTraineeDetailDto(id);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @PutMapping("v2/trainees/detail-profile/{traineeId}")
    public TraineeDetailDTO updateTraineeDetail(@PathVariable(value = "traineeId") final Long id,
                                                @RequestBody final TraineeDetailDTO traineeDetailDTO) {
        return traineeService.updateTraineeDetail(id, traineeDetailDTO);
    }
}
