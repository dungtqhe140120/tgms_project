package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.RateCenter;
import com.doantotnghiep.service.dto.RateCenterDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link RateCenter} and its DTO {@link RateCenterDTO}.
 */
@Mapper(componentModel = "spring")
public interface RateCenterMapper extends EntityMapper<RateCenterDTO, RateCenter> {}
