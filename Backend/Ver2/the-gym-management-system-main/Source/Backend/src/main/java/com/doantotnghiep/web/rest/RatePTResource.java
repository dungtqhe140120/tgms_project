package com.doantotnghiep.web.rest;

import com.doantotnghiep.repository.RatePTRepository;
import com.doantotnghiep.service.RatePTService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.RatePTDTO;
import com.doantotnghiep.service.resources.RateAboutMeResources;
import com.doantotnghiep.service.resources.RatePTResources;
import com.doantotnghiep.service.resources.TraineeRatePTResource;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.RatePT}.
 */
@RestController
@RequestMapping("/api")
public class RatePTResource {

    private final Logger log = LoggerFactory.getLogger(RatePTResource.class);

    private static final String ENTITY_NAME = "ratePT";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RatePTService ratePTService;

    private final RatePTRepository ratePTRepository;
    private final UserService userService;

    public RatePTResource(RatePTService ratePTService, RatePTRepository ratePTRepository, UserService userService) {
        this.ratePTService = ratePTService;
        this.ratePTRepository = ratePTRepository;
        this.userService = userService;
    }

    /**
     * {@code POST  /rate-pts} : Create a new ratePT.
     *
     * @param ratePTDTO the ratePTDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ratePTDTO, or with status {@code 400 (Bad Request)} if the ratePT has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rate-pts")
    public ResponseEntity<RatePTDTO> createRatePT(@RequestBody RatePTDTO ratePTDTO) throws URISyntaxException {
        log.debug("REST request to save RatePT : {}", ratePTDTO);
        if (ratePTDTO.getId() != null) {
            throw new BadRequestAlertException("A new ratePT cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RatePTDTO result = ratePTService.save(ratePTDTO);
        return ResponseEntity
            .created(new URI("/api/rate-pts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rate-pts/:id} : Updates an existing ratePT.
     *
     * @param id        the id of the ratePTDTO to save.
     * @param ratePTDTO the ratePTDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ratePTDTO,
     * or with status {@code 400 (Bad Request)} if the ratePTDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ratePTDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rate-pts/{id}")
    public ResponseEntity<RatePTDTO> updateRatePT(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RatePTDTO ratePTDTO
    ) throws URISyntaxException {
        log.debug("REST request to update RatePT : {}, {}", id, ratePTDTO);
        if (ratePTDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ratePTDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ratePTRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RatePTDTO result = ratePTService.update(ratePTDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ratePTDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /rate-pts/:id} : Partial updates given fields of an existing ratePT, field will ignore if it is null
     *
     * @param id        the id of the ratePTDTO to save.
     * @param ratePTDTO the ratePTDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ratePTDTO,
     * or with status {@code 400 (Bad Request)} if the ratePTDTO is not valid,
     * or with status {@code 404 (Not Found)} if the ratePTDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the ratePTDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/rate-pts/{id}", consumes = {"application/json", "application/merge-patch+json"})
    public ResponseEntity<RatePTDTO> partialUpdateRatePT(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RatePTDTO ratePTDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update RatePT partially : {}, {}", id, ratePTDTO);
        if (ratePTDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ratePTDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ratePTRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RatePTDTO> result = ratePTService.partialUpdate(ratePTDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ratePTDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /rate-pts} : get all the ratePTS.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ratePTS in body.
     */
    @GetMapping("/rate-pts")
    public ResponseEntity<Page<RatePTDTO>> getAllRatePTS(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of RatePTS");
        Page<RatePTDTO> page = ratePTService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/rate-pts/pt")
    public ResponseEntity<RatePTResources> getAllRatePT(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        Long userId = userService.getCurrentUserLogin().getId();
        RatePTResources ratePTResource = ratePTService.findAllByPT(pageable, userId);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ratePTResource));
    }

    @GetMapping("/rate-pts/pt/about-me")
    public ResponseEntity<RateAboutMeResources> getRatePTByPT(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        Long userId = userService.getCurrentUserLogin().getId();
        var traineeRatePTResources = ratePTService.findByPT(pageable, userId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), traineeRatePTResources.getTraineeRatePTResource());
        return ResponseEntity.ok().headers(headers).body(traineeRatePTResources);
    }

    /**
     * {@code GET  /rate-pts/:id} : get the "id" ratePT.
     *
     * @param id the id of the ratePTDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ratePTDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rate-pts/{id}")
    public ResponseEntity<RatePTDTO> getRatePT(@PathVariable Long id) {
        log.debug("REST request to get RatePT : {}", id);
        Optional<RatePTDTO> ratePTDTO = ratePTService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ratePTDTO);
    }

    /**
     * {@code DELETE  /rate-pts/:id} : delete the "id" ratePT.
     *
     * @param id the id of the ratePTDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rate-pts/{id}")
    public ResponseEntity<Void> deleteRatePT(@PathVariable Long id) {
        log.debug("REST request to delete RatePT : {}", id);
        ratePTService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
