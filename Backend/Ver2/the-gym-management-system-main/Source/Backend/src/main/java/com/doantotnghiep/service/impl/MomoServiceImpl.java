package com.doantotnghiep.service.impl;

import java.util.Objects;
import java.util.Optional;

import com.doantotnghiep.domain.MomoTransaction;
import com.doantotnghiep.domain.TraineeAndPT;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.momo.config.MomoProperties;
import com.doantotnghiep.momo.constants.RequestType;
import com.doantotnghiep.momo.exception.MomoException;
import com.doantotnghiep.momo.model.PaymentResponse;
import com.doantotnghiep.momo.model.TransactionConfirmParam;
import com.doantotnghiep.momo.model.TransactionConfirmResponse;
import com.doantotnghiep.momo.model.TransactionCreateParam;
import com.doantotnghiep.momo.model.TransactionCreateResponse;
import com.doantotnghiep.momo.model.form.ExtraData;
import com.doantotnghiep.momo.retrofit.MomoClient;
import com.doantotnghiep.momo.utils.Encoder;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.service.MomoService;
import com.doantotnghiep.service.MomoTransactionService;
import com.doantotnghiep.service.dto.form.RegisterPTAndTraineeForm;
import com.doantotnghiep.service.dto.type.MomoTransactionStatus;
import com.doantotnghiep.service.dto.type.TransactionType;
import com.doantotnghiep.service.mappers2.MomoTransactionMapper2;
import com.doantotnghiep.service.resources.MomoPTAndTraineeRegisterResource;
import com.doantotnghiep.utils.AppUtils;
import com.doantotnghiep.utils.JsonUtils;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class MomoServiceImpl implements MomoService {

    private final static int CODE_TRANSACTION_SUCCESS = 0;
    private final static String MOMO_IPN_URL = "api/momo/ipnUrl";
    private final static int USER_MOMO_CANCEL = 1006;

    private final MomoTransactionService momoTransactionService;
    private final MomoProperties momoProperties;
    private final MomoClient momoClient;
    private final TraineeAndPTRepository traineeAndPTRepository;

    private final MomoTransactionMapper2 momoTransactionMapper2;

    @Override
    public TransactionCreateResponse sendRequestCreate(TransactionType transactionType, User user, String baseUrl, RegisterPTAndTraineeForm form) {
        MomoTransaction entity = momoTransactionService.initializeMomoTransaction(form, user);
        ExtraData extraData = new ExtraData("", entity.getId());

        TransactionCreateParam transactionCreateParam =
            generateTransactionCreateParam(baseUrl, entity, extraData, form);

        try {
            return momoClient.sendRequestCreate(transactionCreateParam);
        } catch (
            MomoException momoEx) {
            entity.setStatus(MomoTransactionStatus.ERROR);
            throw momoEx;
        } catch (
            Exception e) {
            log.error("Exception when send request create momo!", e);
            entity.setStatus(MomoTransactionStatus.ERROR);
            throw new RuntimeException(e);
        }
    }

    @Override
    public TransactionCreateParam generateTransactionCreateParam(String baseUrl, MomoTransaction entity, ExtraData extraData, RegisterPTAndTraineeForm form) {
        return TransactionCreateParam.builder()
            .partnerCode(momoProperties.partnerCode())
            .partnerName("")
            .storeId(momoProperties.storeId())
            .requestType(RequestType.CAPTURE_WALLET)
            .ipnUrl(baseUrl + MOMO_IPN_URL)
            .redirectUrl(form.getRedirectUrl())
            .orderId(entity.getId() + "")
            .requestId(entity.getRequestId())
            .amount(entity.getAmount())
            .lang("vi")
            .orderInfo(entity.getTransactionInfo())
            .extraData(Encoder.encode64ExtraData(extraData))
            .build();
    }

    @Override
    public boolean checkTransaction(PaymentResponse paymentResponse) throws Exception {
        TransactionConfirmParam transactionConfirmParam = TransactionConfirmParam.builder()
            .partnerCode(momoProperties.partnerCode())
            .requestId(paymentResponse.requestId())
            .orderId(paymentResponse.orderId())
            .lang("vi")
            .build();
        TransactionConfirmResponse response = momoClient.sendConfirmTransaction(transactionConfirmParam);
        return Objects.nonNull(response)
            && CODE_TRANSACTION_SUCCESS == response.getResultCode();
    }

    @Override
    @Transactional
    public void updateStatusTransaction(PaymentResponse paymentResponse) {
        MomoTransaction entity = momoTransactionService.findById(Long.valueOf(paymentResponse.orderId()))
            .orElseThrow(() -> {
                log.info("Cannot find momo transaction by id: {}", paymentResponse.orderId());
                throw new RuntimeException(String.format("Cannot find momo transaction by id: %s", paymentResponse.orderId()));
            });

        if (USER_MOMO_CANCEL == paymentResponse.resultCode()) {
            log.info("Giao dịch không thành công do bạn vừa hủy: {}", paymentResponse);
            updateMomoTransaction(paymentResponse, entity, MomoTransactionStatus.CANCEL);
            return;
        }

        boolean validTransaction;
        try {
            validTransaction = checkTransaction(paymentResponse);
        } catch (
            Exception e) {
            entity.setStatus(MomoTransactionStatus.ERROR);
            throw new RuntimeException(e);
        }

        switch (entity.getType()) {
            case REGISTER_PT -> {
                MomoTransactionStatus status = validTransaction
                    ? MomoTransactionStatus.DONE
                    : MomoTransactionStatus.ERROR;
                updateMomoTransaction(paymentResponse, entity, status);
            }
            case REGISTER_ROOM -> {
                throw new UnsupportedOperationException("This operation not support yet.");
            }
            default -> throw new RuntimeException("TransactionType not support");
        }
    }

    private void updateMomoTransaction(PaymentResponse paymentResponse,
                                              MomoTransaction entity,
                                              MomoTransactionStatus status) {
        final String[] IGNORE_PROPERTIES_PR_MTE = new String[] {
            "orderId", "requestId", "amount", "orderInfo", "orderType", "responseBody"
        };
        BeanUtils.copyProperties(paymentResponse, entity, IGNORE_PROPERTIES_PR_MTE);
        entity.setStatus(status);
        entity.setResponseBody(JsonUtils.toString(paymentResponse));
        entity.setTransactionId(paymentResponse.transId());
        entity.setPayType(AppUtils.createMomoPayTypeByValue(paymentResponse.payType()));
        momoTransactionService.save(entity);
    }

    @Scheduled(cron = "@hourly")
    @Transactional
    public void expireMomoTransaction() {
        momoTransactionService.updateStatusExpired();
    }

    @Override
    public MomoPTAndTraineeRegisterResource getMomoRegisterTraineeAndPT(Long traineeAndPTId) {
        TraineeAndPT traineeAndPT = traineeAndPTRepository.findByIdAlreadyFetch(traineeAndPTId)
            .orElseThrow(()->{
                throw new BadRequestAlertException("Không tìm thấy đăng ký người tập và huấn luyên viên với giao dịch ID: " + traineeAndPTId, "traineeAndPt", "inidvalid");
            });
        MomoTransaction entity = momoTransactionService.findById(traineeAndPT.getTransactionId())
            .orElseThrow(()->{
                throw new BadRequestAlertException("Không tìm thấy giao dịch MoMo ID: " + traineeAndPT.getTransactionId(), "momo_transaction", "inidvalid");
            });
        MomoPTAndTraineeRegisterResource resource = momoTransactionMapper2.toResource(entity);
        resource.setTrainee(traineeAndPT.getTrainee());
        resource.setPersonalTrainer(traineeAndPT.getPersonalTrainer());
        return resource;
    }
}
