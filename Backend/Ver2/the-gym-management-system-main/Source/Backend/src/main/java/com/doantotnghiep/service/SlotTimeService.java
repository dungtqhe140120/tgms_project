package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.SlotTimeDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.SlotTime}.
 */
public interface SlotTimeService {
    /**
     * Save a slotTime.
     *
     * @param slotTimeDTO the entity to save.
     * @return the persisted entity.
     */
    SlotTimeDTO save(SlotTimeDTO slotTimeDTO);

    /**
     * Updates a slotTime.
     *
     * @param slotTimeDTO the entity to update.
     * @return the persisted entity.
     */
    SlotTimeDTO update(SlotTimeDTO slotTimeDTO);

    /**
     * Partially updates a slotTime.
     *
     * @param slotTimeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<SlotTimeDTO> partialUpdate(SlotTimeDTO slotTimeDTO);

    /**
     * Get all the slotTimes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SlotTimeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" slotTime.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SlotTimeDTO> findOne(Long id);

    /**
     * Delete the "id" slotTime.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
