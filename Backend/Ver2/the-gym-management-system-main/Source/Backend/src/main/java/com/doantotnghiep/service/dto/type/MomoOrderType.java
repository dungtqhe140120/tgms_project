package com.doantotnghiep.service.dto.type;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
public enum MomoOrderType {
    @JsonProperty("momo_wallet")
    MOMO_WALLET
}
