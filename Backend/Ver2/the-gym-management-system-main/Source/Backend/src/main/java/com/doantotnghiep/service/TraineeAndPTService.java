package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import com.doantotnghiep.service.dto.display.CalendarPTDisplayDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqCreateTraineeAndPT;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.dto.requestDTO.TraineeAndPTFilter;
import com.doantotnghiep.service.resources.TraineeAndPTResources;
import com.doantotnghiep.service.resources.TraineeAndPtDraftInfResource;
import com.doantotnghiep.service.resources.TraineeAndPtDraftResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.TraineeAndPT}.
 */
public interface TraineeAndPTService {
//    /**
//     * Save a traineeAndPT.
//     *
//     * @param traineeAndPTDTO the entity to save.
//     * @return the persisted entity.
//     */
//    TraineeAndPTDTO save(TraineeAndPTDTO traineeAndPTDTO);

    TraineeAndPTDTO saveV2(ReqCreateTraineeAndPT request);

//    /**
//     * Partially updates a traineeAndPT.
//     *
//     * @param traineeAndPTDTO the entity to update partially.
//     * @return the persisted entity.
//     */
//    Optional<TraineeAndPTDTO> partialUpdate(TraineeAndPTDTO traineeAndPTDTO);
//
//    /**
//     * Get all the traineeAndPTS.
//     *
//     * @param pageable the pagination information.
//     * @return the list of entities.
//     */
//    Page<TraineeAndPTDTO> findAll(Pageable pageable);
//
//    /**
//     * Get the "id" traineeAndPT.
//     *
//     * @param id the id of the entity.
//     * @return the entity.
//     */
//    Optional<TraineeAndPTDTO> findOne(Long id);
//
//    /**
//     * Delete the "id" traineeAndPT.
//     *
//     * @param id the id of the entity.
//     */
//    void delete(Long id);

    List<ReqPTFullDTO> getPTsFreeSlot(Long slotId, LocalDate registerDate);
    Page<TraineeAndPTResources> getListTraineeAndPTByFilter(TraineeAndPTFilter traineeAndPTFilter, Pageable pageable);
    Page<TraineeAndPtDraftInfResource> getListTraineeAndPtDraftsByFilter(TraineeAndPTFilter traineeAndPTFilter, Pageable pageable);
    List<CalendarPTDisplayDTO> getListCalendarPTDisplayDTO(Pageable pageable, Long pt_id, String date);

    TraineeAndPTDTO confirm(TraineeAndPTDTO traineeAndPTDTO, Long confirmBy);

    TraineeAndPTDTO confirmDraft(TraineeAndPtDraftResource request, Long confirmBy);
    Optional<TraineeAndPTDTO> getCurrentTraineeAndPT(Long traineeId);
}
