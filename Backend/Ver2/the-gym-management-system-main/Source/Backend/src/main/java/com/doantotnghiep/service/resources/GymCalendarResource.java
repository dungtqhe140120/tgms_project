package com.doantotnghiep.service.resources;

import com.doantotnghiep.service.dto.GymCalendarDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GymCalendarResource {
    private String nameTrainee;
    private Float bmi;
    private Page<GymCalendarDTO> gymCalendarDTOS;
}
