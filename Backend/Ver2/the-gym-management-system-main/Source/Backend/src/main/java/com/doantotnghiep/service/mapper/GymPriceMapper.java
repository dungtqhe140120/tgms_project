package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.GymPrice;
import com.doantotnghiep.service.dto.GymPriceDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface GymPriceMapper extends EntityMapper<GymPriceDTO, GymPrice>{
}
