package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.TraineeCategory;
import com.doantotnghiep.repository.TraineeCategoryRepository;
import com.doantotnghiep.service.TraineeCategoryService;
import com.doantotnghiep.service.dto.TraineeCategoryDTO;
import com.doantotnghiep.service.dto.display.CategoryTraineePTDisplayDTO;
import com.doantotnghiep.service.mapper.TraineeCategoryMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TraineeCategory}.
 */
@Service
@Transactional
public class TraineeCategoryServiceImpl implements TraineeCategoryService {

    private final Logger log = LoggerFactory.getLogger(TraineeCategoryServiceImpl.class);

    private final TraineeCategoryRepository traineeCategoryRepository;

    private final TraineeCategoryMapper traineeCategoryMapper;

    public TraineeCategoryServiceImpl(TraineeCategoryRepository traineeCategoryRepository, TraineeCategoryMapper traineeCategoryMapper) {
        this.traineeCategoryRepository = traineeCategoryRepository;
        this.traineeCategoryMapper = traineeCategoryMapper;
    }

    @Override
    public TraineeCategoryDTO save(TraineeCategoryDTO traineeCategoryDTO) {
        log.debug("Request to save TraineeCategory : {}", traineeCategoryDTO);
        TraineeCategory traineeCategory = traineeCategoryMapper.toEntity(traineeCategoryDTO);
        traineeCategory = traineeCategoryRepository.save(traineeCategory);
        return traineeCategoryMapper.toDto(traineeCategory);
    }

    @Override
    public TraineeCategoryDTO update(TraineeCategoryDTO traineeCategoryDTO) {
        log.debug("Request to save TraineeCategory : {}", traineeCategoryDTO);
        TraineeCategory traineeCategory = traineeCategoryMapper.toEntity(traineeCategoryDTO);
        // no save call needed as we have no fields that can be updated
        return traineeCategoryMapper.toDto(traineeCategory);
    }

    @Override
    public Optional<TraineeCategoryDTO> partialUpdate(TraineeCategoryDTO traineeCategoryDTO) {
        log.debug("Request to partially update TraineeCategory : {}", traineeCategoryDTO);

        return traineeCategoryRepository
            .findById(traineeCategoryDTO.getId())
            .map(existingTraineeCategory -> {
                traineeCategoryMapper.partialUpdate(existingTraineeCategory, traineeCategoryDTO);

                return existingTraineeCategory;
            })
            // .map(traineeCategoryRepository::save)
            .map(traineeCategoryMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TraineeCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TraineeCategories");
        return traineeCategoryRepository.findAll(pageable).map(traineeCategoryMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TraineeCategoryDTO> findOne(Long id) {
        log.debug("Request to get TraineeCategory : {}", id);
        return traineeCategoryRepository.findById(id).map(traineeCategoryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TraineeCategory : {}", id);
        traineeCategoryRepository.deleteById(id);
    }

    @Override
    public Page<CategoryTraineePTDisplayDTO> getCategoryTraineeByTrainee(Pageable pageable, Long trainee_id) {
        log.debug("Request to getDetailExerciseTraineeByTrainee : {}", trainee_id);
        return traineeCategoryRepository.getCategoryTraineeByTrainee(pageable,trainee_id);
    }

    @Override
    public TraineeCategory saveTraineeCategory(TraineeCategory traineeCategory) {
        log.debug("Request to saveTraineeCategory : {}", traineeCategory);
        return traineeCategoryRepository.save(traineeCategory);
    }

}
