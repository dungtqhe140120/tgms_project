package com.doantotnghiep.service.dto.requestDTO;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class ReqTraineeFullDTO implements Serializable {
    private Long id;
    private String short_description;
    private String address;
    private String avatar;
    private LocalDate birthday;
    private String email;
    private String full_name;
    private Integer gender;
    private String objective;
    private String phone;

    public ReqTraineeFullDTO() {
    }

    public ReqTraineeFullDTO(Long id, String short_description, String address, String avatar, LocalDate birthday, String email, String full_name, Integer gender, String objective, String phone) {
        this.id = id;
        this.short_description = short_description;
        this.address = address;
        this.avatar = avatar;
        this.birthday = birthday;
        this.email = email;
        this.full_name = full_name;
        this.gender = gender;
        this.objective = objective;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReqTraineeFullDTO that = (ReqTraineeFullDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(short_description, that.short_description) && Objects.equals(address, that.address) && Objects.equals(avatar, that.avatar) && Objects.equals(birthday, that.birthday) && Objects.equals(email, that.email) && Objects.equals(full_name, that.full_name) && Objects.equals(gender, that.gender) && Objects.equals(objective, that.objective) && Objects.equals(phone, that.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, short_description, address, avatar, birthday, email, full_name, gender, objective, phone);
    }

    @Override
    public String toString() {
        return "ReqTraineeFullDTO{" +
            "id=" + id +
            ", short_description='" + short_description + '\'' +
            ", address='" + address + '\'' +
            ", avatar='" + avatar + '\'' +
            ", birthday=" + birthday +
            ", email='" + email + '\'' +
            ", full_name='" + full_name + '\'' +
            ", gender=" + gender +
            ", objective='" + objective + '\'' +
            ", phone='" + phone + '\'' +
            '}';
    }
}
