package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.Employee;
import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.domain.SlotTime;
import com.doantotnghiep.domain.TraineeAndPT;
import com.doantotnghiep.domain.TraineeAndPTDraft;
import com.doantotnghiep.repository.EmployeeRepository;
import com.doantotnghiep.repository.PersonalTrainerRepository;
import com.doantotnghiep.repository.SlotTimeRepository;
import com.doantotnghiep.repository.TraineeAndPTDraftRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.service.TraineeAndPTService;
import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import com.doantotnghiep.service.dto.display.CalendarPTDisplayDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqCreateTraineeAndPT;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.dto.requestDTO.TraineeAndPTFilter;
import com.doantotnghiep.service.mappers2.TraineeAndPTMapper2;
import com.doantotnghiep.service.resources.TraineeAndPTResources;
import com.doantotnghiep.service.resources.TraineeAndPtDraftInfResource;
import com.doantotnghiep.service.resources.TraineeAndPtDraftResource;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Service
 * Implementation
 * for
 * managing
 * {@link
 * TraineeAndPT}.
 */
@Service
@Transactional
@AllArgsConstructor
public class TraineeAndPTServiceImpl implements TraineeAndPTService {

    private final Logger log = LoggerFactory.getLogger(TraineeAndPTServiceImpl.class);

    private final TraineeAndPTRepository traineeAndPTRepository;

    private final PersonalTrainerRepository personalTrainerRepository;

    private final TraineeAndPTDraftRepository traineeAndPTDraftRepository;

    private final SlotTimeRepository slotTimeRepository;

    private final EmployeeRepository employeeRepository;

    private static final Integer STATUS_WAIT_FOR_CONFIRM = 0;
    private static final Integer STATUS_CONFIRMED = 1;
    private static final Set<Integer> STATUS_CONFIRMS = Set.of(STATUS_CONFIRMED, STATUS_WAIT_FOR_CONFIRM);

    //    @Autowired
    private final JdbcTemplate jdbcTemplate;

//    @Override
//    public TraineeAndPTDTO save(TraineeAndPTDTO traineeAndPTDTO) {
//        log.debug("Request to save TraineeAndPT : {}", traineeAndPTDTO);
//        TraineeAndPT traineeAndPT = traineeAndPTMapper.toEntity(traineeAndPTDTO);
//        traineeAndPT = traineeAndPTRepository.save(traineeAndPT);
//        return traineeAndPTMapper.toDto(traineeAndPT);
//    }

    @Override
    public TraineeAndPTDTO saveV2(ReqCreateTraineeAndPT request) {
        log.debug("Request to save TraineeAndPT : {}", request);
        TraineeAndPT traineeAndPT = TraineeAndPTMapper2.INSTANCE.toEntity(request);
        traineeAndPT.setExpiredDate(traineeAndPT.getRegisteredDate().plusDays(request.getDateRegister()));
        traineeAndPT.setConfirmStatus(0);
        if(!Objects.isNull(traineeAndPT.getTransactionId())){
            traineeAndPT.setConfirmStatus(1);
        }
        traineeAndPT = traineeAndPTRepository.save(traineeAndPT);
        return TraineeAndPTMapper2.INSTANCE.toDTO(traineeAndPT);
    }

//    @Override
//    public Optional<TraineeAndPTDTO> partialUpdate(TraineeAndPTDTO traineeAndPTDTO) {
//        log.debug("Request to partially update TraineeAndPT : {}", traineeAndPTDTO);
//
//        return traineeAndPTRepository
//            .findById(traineeAndPTDTO.getId())
//            .map(existingTraineeAndPT -> {
//                traineeAndPTMapper.partialUpdate(existingTraineeAndPT, traineeAndPTDTO);
//
//                return existingTraineeAndPT;
//            })
//            .map(traineeAndPTRepository::save)
//            .map(traineeAndPTMapper::toDto);
//    }

//    @Override
//    @Transactional(readOnly = true)
//    public Page<TraineeAndPTDTO> findAll(Pageable pageable) {
//        log.debug("Request to get all TraineeAndPTS");
//        return traineeAndPTRepository.findAll(pageable).map(traineeAndPTMapper::toDto);
//    }

//    @Override
//    @Transactional(readOnly = true)
//    public Optional<TraineeAndPTDTO> findOne(Long id) {
//        log.debug("Request to get TraineeAndPT : {}", id);
//        return traineeAndPTRepository.findById(id).map(traineeAndPTMapper::toDto);
//    }

//    @Override
//    public void delete(Long id) {
//        log.debug("Request to delete TraineeAndPT : {}", id);
//        traineeAndPTRepository.deleteById(id);
//    }

    @Override
    public List<CalendarPTDisplayDTO> getListCalendarPTDisplayDTO(Pageable pageable, Long pt_id, String date) {
//        return traineeAndPTRepository.getListCalendarPTDisplayDTO(pageable, pt_id, date);
        StringBuilder sql = new StringBuilder("select b.id, p.fullname , st.name as slot from trainee_andpt a inner join trainee b " +
            "on a.trainee_id=b.id inner join jhi_profile p on p.id=b.profile_id inner join slot_time st on st.id=a.slot_time_id inner " +
            "join personal_trainer pt on a.personal_trainer_id=pt.id \n" +
            "where  pt.id=? and to_date(?,'dd-mm-yyyy') >= a.registered_date " +
            "and to_date(?,'dd-mm-yyyy') <= a.expired_Date and a.confirmed=1  " +
            " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");
        log.info("final sql: " + sql.toString());
        return jdbcTemplate.query(sql.toString(), new Object[] {pt_id, date, date, pageable.getPageNumber() * pageable.getPageSize(), pageable.getPageSize()}, (rs, rowNum) ->
            new CalendarPTDisplayDTO(
                rs.getLong(1),
                rs.getString(2),
                rs.getString(3)
            ));
    }

    @Override
    public TraineeAndPTDTO confirm(TraineeAndPTDTO traineeAndPTDTO, Long confirmBy) {

        TraineeAndPT traineeAndPT = traineeAndPTRepository.getById(traineeAndPTDTO.getTraineeAndPtId());
        traineeAndPT.setConfirmBy(confirmBy.toString());
        traineeAndPT.setConfirmDate(LocalDate.now());
        traineeAndPT.setConfirmStatus(traineeAndPTDTO.getConfirmStatus());
        return TraineeAndPTMapper2.INSTANCE.toDTO(traineeAndPTRepository.save(traineeAndPT));
    }

    @Override
    public TraineeAndPTDTO confirmDraft(TraineeAndPtDraftResource request, Long confirmBy) {
        TraineeAndPTDTO result;
        TraineeAndPTDraft entityDraft = traineeAndPTDraftRepository.getById(request.getId());
        entityDraft.setConfirmBy(confirmBy.toString());
        entityDraft.setConfirmDate(LocalDate.now());
        entityDraft.setConfirmStatus(request.getConfirmStatus());
        result = TraineeAndPTMapper2.INSTANCE.toDTO(traineeAndPTDraftRepository.save(entityDraft));

        if (entityDraft.getConfirmStatus().equals(STATUS_CONFIRMED)) {
            TraineeAndPT entity = traineeAndPTRepository.getById(entityDraft.getTraineeAndPtId());
            entity.setConfirmBy(confirmBy.toString());
            entity.setConfirmDate(LocalDate.now());
            entity.setConfirmStatus(STATUS_CONFIRMED);
            SlotTime slotTime = slotTimeRepository.getById(entityDraft.getSlotId());
            entity.setSlotTime(slotTime);
            PersonalTrainer personalTrainer = personalTrainerRepository.getPersonalTrainerById(entityDraft.getPtId());
            entity.setPersonalTrainer(personalTrainer);
            result = TraineeAndPTMapper2.INSTANCE.toDTO(traineeAndPTRepository.save(entity));
        }
        return result;
    }

    @Override
    public Optional<TraineeAndPTDTO> getCurrentTraineeAndPT(Long traineeId) {
        TraineeAndPTDTO dto;
        var entityDraft = traineeAndPTDraftRepository.
            getTraineeAndPTDraftByTraineeIdAndExpiredDateGreaterThanEqualAndConfirmStatus(traineeId, LocalDate.now(), STATUS_WAIT_FOR_CONFIRM).
            map(TraineeAndPTMapper2.INSTANCE::toDTO);
        if (entityDraft.isPresent()) {
            dto = entityDraft.get();
        } else {
            var entity = traineeAndPTRepository
                .findTraineeAndPTByTraineeIdAndExpiredDateGreaterThanEqualAndConfirmStatusIn(traineeId, LocalDate.now(), STATUS_CONFIRMS).
                map(TraineeAndPTMapper2.INSTANCE::toDTO);
            dto = entity.isEmpty() ? null : entity.get();
        }
        if (dto != null && dto.getPtId() != null) {
            dto.setImagePt(personalTrainerRepository.getImagePT(dto.getPtId()));
        }

        return Optional.ofNullable(dto);
    }

    @Override
    public List<ReqPTFullDTO> getPTsFreeSlot(Long slotId, LocalDate registerDate) {
        List<TraineeAndPT> listPTsNotFreeSlot = traineeAndPTRepository.findTraineeAndPTSBySlotTimeIdAndExpiredDateIsAfter(slotId, registerDate);
        List<Long> ptIds = listPTsNotFreeSlot.stream().map(traineeAndPT -> traineeAndPT.getPersonalTrainer().getId()).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(ptIds)) {
            return personalTrainerRepository.getPTs();
        }
        return personalTrainerRepository.getPTsFreeSlot(ptIds);
    }

    @Override
    public Page<TraineeAndPTResources> getListTraineeAndPTByFilter(TraineeAndPTFilter traineeAndPTFilter, Pageable pageable) {
        var traineeAndPTFilter1 = traineeAndPTFilter.convertFilter();
        Page<TraineeAndPTResources> listTraineeAndPT = traineeAndPTRepository.getListTraineeByFilter(pageable, traineeAndPTFilter1.getNamePT(),
            traineeAndPTFilter1.getNameTrainee(), traineeAndPTFilter1.getStatusConfirm());

        Set<Long> employeeIds = listTraineeAndPT.get()
            .map(TraineeAndPTResources::getConfirmBy)
            .filter(Objects::nonNull)
            .map(Long::valueOf).collect(Collectors.toSet());

        List<Employee> employees = employeeRepository.findByIdIn(new ArrayList<>(employeeIds));
        Map<Long, Employee> mapIdEmployees = employees.stream()
            .collect(Collectors.toMap(Employee::getId, Function.identity()));
        listTraineeAndPT.getContent().forEach(traineeAndPTResources -> {
            String oldConfirmBy = traineeAndPTResources.getConfirmBy();
            String newConfirmBy = Objects.isNull(oldConfirmBy)
                ? "" : "1".equals(oldConfirmBy)
                ? "admin" : mapIdEmployees.containsKey(Long.valueOf(oldConfirmBy))
                ? mapIdEmployees.get(Long.valueOf(oldConfirmBy)).getProfile().getFullname() : "Không rõ";
            traineeAndPTResources.setConfirmBy(newConfirmBy);
        });

        return listTraineeAndPT;
    }

    @Override
    public Page<TraineeAndPtDraftInfResource> getListTraineeAndPtDraftsByFilter(TraineeAndPTFilter traineeAndPTFilter, Pageable pageable) {
        var traineeAndPTFilter1 = traineeAndPTFilter.convertFilter();

        Page<TraineeAndPtDraftInfResource> listByFilter = traineeAndPTDraftRepository.getListByFilter(pageable, traineeAndPTFilter1.getNamePT(),
            traineeAndPTFilter1.getNameTrainee(), traineeAndPTFilter1.getStatusConfirm());

        Set<Long> employeeIds = listByFilter.get()
            .map(TraineeAndPtDraftInfResource::getConfirmBy)
            .filter(Objects::nonNull)
            .map(Long::valueOf).collect(Collectors.toSet());
        List<Employee> employees = employeeRepository.findByIdIn(new ArrayList<>(employeeIds));
        Map<Long, Employee> mapIdEmployees = employees.stream()
                .collect(Collectors.toMap(Employee::getId, Function.identity()));
        listByFilter.getContent().forEach(traineeAndPtDraftInfResource -> {
            String oldConfirmBy = traineeAndPtDraftInfResource.getConfirmBy();
            String newConfirmBy = Objects.isNull(oldConfirmBy)
                ? "" : "1".equals(oldConfirmBy)
                ? "admin" : mapIdEmployees.containsKey(Long.valueOf(oldConfirmBy))
                ? mapIdEmployees.get(Long.valueOf(oldConfirmBy)).getProfile().getFullname() : "Không rõ";
            traineeAndPtDraftInfResource.setConfirmBy(newConfirmBy);
        });

        return listByFilter;
    }
}
