package com.doantotnghiep.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * A Trainee.
 */
@Entity
@Table(name = "trainee")
public class Trainee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "short_description")
    private String short_Description;

    @Column(name = "expired_date")
    private LocalDate expired_date;
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @NotNull
    @JoinColumn(unique = true)
    private Profile profile;

    @OneToMany(mappedBy = "trainee",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "trainee"}, allowSetters = true)
    private Set<TraineeAndPT> traineeAndPTS = new HashSet<>();

    @OneToMany(mappedBy = "trainee",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "trainee"}, allowSetters = true)
    private Set<TraineeExercise> traineeExercises = new HashSet<>();

    @OneToMany(mappedBy = "trainee",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "trainee"}, allowSetters = true)
    private Set<TraineeCategory> traineeCategories = new HashSet<>();

    @OneToMany(mappedBy = "trainee",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "trainee"}, allowSetters = true)
    private Set<BMI> bmis = new HashSet<>();


    // jhipster-needle-entity-add-field - JHipster will add fields here


    public LocalDate getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(LocalDate expired_date) {
        this.expired_date = expired_date;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Long getId() {
        return this.id;
    }

    public Trainee id(Long id) {
        this.setId(id);
        return this;
    }

    public Set<BMI> getBmis() {
        return bmis;
    }

    public void setBmis(Set<BMI> bmis) {
        this.bmis = bmis;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<TraineeAndPT> getTraineeAndPTS() {
        return traineeAndPTS;
    }
    public Set<TraineeCategory> getTraineeCategories() {
        return traineeCategories;
    }

    public void setTraineeCategories(Set<TraineeCategory> traineeCategories) {
        this.traineeCategories = traineeCategories;
    }

    public String getShort_Description() {
        return this.short_Description;
    }

    public Trainee short_Description(String short_Description) {
        this.setShort_Description(short_Description);
        return this;
    }

    public void setShort_Description(String short_Description) {
        this.short_Description = short_Description;
    }

    public Set<TraineeExercise> getTraineeExercises() {
        return this.traineeExercises;
    }

    public void setTraineeExercises(Set<TraineeExercise> traineeExercises) {
        if (this.traineeExercises != null) {
            this.traineeExercises.forEach(i -> i.setTrainee(null));
        }
        if (traineeExercises != null) {
            traineeExercises.forEach(i -> i.setTrainee(this));
        }
        this.traineeExercises = traineeExercises;
    }

    public Trainee traineeExercise(Set<TraineeExercise> traineeExercises) {
        this.setTraineeExercises(traineeExercises);
        return this;
    }

    public Trainee addTraineeExercise(TraineeExercise traineeExercise) {
        this.traineeExercises.add(traineeExercise);
        traineeExercise.setTrainee(this);
        return this;
    }

    public Trainee removeTraineeExercise(TraineeExercise traineeExercise) {
        this.traineeExercises.remove(traineeExercise);
        traineeExercise.setTrainee(null);
        return this;
    }

    public Set<TraineeAndPT> getTraineeAndPT() {
        return this.traineeAndPTS;
    }

    public void setTraineeAndPTS(Set<TraineeExercise> traineeExercises) {
        if (this.traineeAndPTS != null) {
            this.traineeAndPTS.forEach(i -> i.setTrainee(null));
        }
        if (traineeAndPTS != null) {
            traineeAndPTS.forEach(i -> i.setTrainee(this));
        }
        this.traineeAndPTS = traineeAndPTS;
    }

    public Trainee traineeAndPT(Set<TraineeAndPT> traineeAndPTS) {
        this.traineeAndPT(traineeAndPTS);
        return this;
    }

    public Trainee addTraineeAndPT(TraineeAndPT traineeAndPT) {
        this.traineeAndPTS.add(traineeAndPT);
        traineeAndPT.setTrainee(this);
        return this;
    }

    public Trainee removeTraineeAndPT(TraineeAndPT traineeAndPT) {
        this.traineeAndPTS.remove(traineeAndPT);
        traineeAndPT.setTrainee(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Trainee)) {
            return false;
        }
        return id != null && id.equals(((Trainee) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Trainee{" +
            "id=" + getId() +
            ", short_Description='" + getShort_Description() + "'" +
            "}";
    }
}
