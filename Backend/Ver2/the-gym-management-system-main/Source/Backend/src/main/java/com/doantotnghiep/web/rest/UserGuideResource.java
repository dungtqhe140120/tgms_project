package com.doantotnghiep.web.rest;

import com.doantotnghiep.repository.UserGuideRepository;
import com.doantotnghiep.service.UserGuideService;
import com.doantotnghiep.service.dto.UserGuideDTO;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.UserGuide}.
 */
@RestController
@RequestMapping("/api")
public class UserGuideResource {

    private final Logger log = LoggerFactory.getLogger(UserGuideResource.class);

    private static final String ENTITY_NAME = "userGuide";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserGuideService userGuideService;

    private final UserGuideRepository userGuideRepository;

    public UserGuideResource(UserGuideService userGuideService, UserGuideRepository userGuideRepository) {
        this.userGuideService = userGuideService;
        this.userGuideRepository = userGuideRepository;
    }

    /**
     * {@code POST  /user-guides} : Create a new userGuide.
     *
     * @param userGuideDTO the userGuideDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userGuideDTO, or with status {@code 400 (Bad Request)} if the userGuide has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-guides")
    public ResponseEntity<UserGuideDTO> createUserGuide(@RequestBody UserGuideDTO userGuideDTO) throws URISyntaxException {
        log.debug("REST request to save UserGuide : {}", userGuideDTO);
        if (userGuideDTO.getId() != null) {
            throw new BadRequestAlertException("A new userGuide cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserGuideDTO result = userGuideService.save(userGuideDTO);
        return ResponseEntity
            .created(new URI("/api/user-guides/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-guides/:id} : Updates an existing userGuide.
     *
     * @param id the id of the userGuideDTO to save.
     * @param userGuideDTO the userGuideDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userGuideDTO,
     * or with status {@code 400 (Bad Request)} if the userGuideDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userGuideDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-guides/{id}")
    public ResponseEntity<UserGuideDTO> updateUserGuide(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserGuideDTO userGuideDTO
    ) throws URISyntaxException {
        log.debug("REST request to update UserGuide : {}, {}", id, userGuideDTO);
        if (userGuideDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userGuideDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userGuideRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserGuideDTO result = userGuideService.update(userGuideDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userGuideDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /user-guides/:id} : Partial updates given fields of an existing userGuide, field will ignore if it is null
     *
     * @param id the id of the userGuideDTO to save.
     * @param userGuideDTO the userGuideDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userGuideDTO,
     * or with status {@code 400 (Bad Request)} if the userGuideDTO is not valid,
     * or with status {@code 404 (Not Found)} if the userGuideDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the userGuideDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/user-guides/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<UserGuideDTO> partialUpdateUserGuide(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserGuideDTO userGuideDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserGuide partially : {}, {}", id, userGuideDTO);
        if (userGuideDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userGuideDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userGuideRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserGuideDTO> result = userGuideService.partialUpdate(userGuideDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userGuideDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /user-guides} : get all the userGuides.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userGuides in body.
     */
    @GetMapping("/user-guides")
    public ResponseEntity<List<UserGuideDTO>> getAllUserGuides(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of UserGuides");
        Page<UserGuideDTO> page = userGuideService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-guides/:id} : get the "id" userGuide.
     *
     * @param id the id of the userGuideDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userGuideDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-guides/{id}")
    public ResponseEntity<UserGuideDTO> getUserGuide(@PathVariable Long id) {
        log.debug("REST request to get UserGuide : {}", id);
        Optional<UserGuideDTO> userGuideDTO = userGuideService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userGuideDTO);
    }

    /**
     * {@code DELETE  /user-guides/:id} : delete the "id" userGuide.
     *
     * @param id the id of the userGuideDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-guides/{id}")
    public ResponseEntity<Void> deleteUserGuide(@PathVariable Long id) {
        log.debug("REST request to delete UserGuide : {}", id);
        userGuideService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
