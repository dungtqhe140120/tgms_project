package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.Category;
import com.doantotnghiep.repository.CategoryRepository;
import com.doantotnghiep.service.CategoryService;
import com.doantotnghiep.service.dto.CategoryDTO;
import com.doantotnghiep.service.dto.display.CategoryDisplayDTO;
import com.doantotnghiep.service.mapper.CategoryMapper;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Category}.
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public CategoryDTO save(CategoryDTO categoryDTO) {
        log.debug("Request to save Category : {}", categoryDTO);
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryRepository.save(category);
        return categoryMapper.toDto(category);
    }

    @Override
    public CategoryDTO update(CategoryDTO categoryDTO) {
        log.debug("Request to save Category : {}", categoryDTO);
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryRepository.save(category);
        return categoryMapper.toDto(category);
    }

    @Override
    public Optional<CategoryDTO> partialUpdate(CategoryDTO categoryDTO) {
        log.debug("Request to partially update Category : {}", categoryDTO);

        return categoryRepository
            .findById(categoryDTO.getId())
            .map(existingCategory -> {
                categoryMapper.partialUpdate(existingCategory, categoryDTO);

                return existingCategory;
            })
            .map(categoryRepository::save)
            .map(categoryMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Categories");
        return categoryRepository.findAll(pageable).map(categoryMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CategoryDTO> findOne(Long id) {
        log.debug("Request to get Category : {}", id);
        return categoryRepository.findById(id).map(categoryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Category : {}", id);
        categoryRepository.deleteById(id);
    }

    @Override
    public Page<CategoryDisplayDTO> getCenterCategoryDisplayDTO(Pageable pageable) {
        log.debug("Request to getCenterCategoryDisplayDTO : {}");
        return categoryRepository.getCenterCategoryDisplayDTO(pageable);
    }

    @Override
    public Page<CategoryDisplayDTO> getPTCategoryDisplayDTO(Pageable pageable, Long user_id) {
        log.debug("Request to getPTCategoryDisplayDTO : {}", user_id);
        return categoryRepository.getPTCategoryDisplayDTO(pageable, user_id);
    }

    @Override
    public void deletePTCategory(Long user_id, Long id) {
        log.debug("Request to deletePTCategory : {}", id);
        categoryRepository.deletePTCategory(user_id, id);
    }

    @Override
    public Category findCategoryByID(Long id) {
        log.debug("Request to findCategoryByID : {}", id);
        return categoryRepository.findCategoriesByID(id);
    }
}
