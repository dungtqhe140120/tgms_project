package com.doantotnghiep.service.dto.display;

import java.io.Serializable;
import java.util.Objects;

public class DetailContentDisplayDTO implements Serializable {
    private String content;

    public DetailContentDisplayDTO() {
    }

    public DetailContentDisplayDTO(String content) {
        this.content = content;
    }

    public String getContent() {

        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetailContentDisplayDTO that = (DetailContentDisplayDTO) o;
        return Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content);
    }

    @Override
    public String toString() {
        return content;
    }
}
