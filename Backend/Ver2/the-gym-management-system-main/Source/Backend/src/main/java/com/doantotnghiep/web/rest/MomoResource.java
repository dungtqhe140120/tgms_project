package com.doantotnghiep.web.rest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.doantotnghiep.momo.model.PaymentResponse;
import com.doantotnghiep.momo.model.TransactionCreateResponse;
import com.doantotnghiep.service.MomoService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.form.RegisterPTAndTraineeForm;
import com.doantotnghiep.service.dto.type.TransactionType;
import com.doantotnghiep.service.resources.MomoPTAndTraineeRegisterResource;
import com.doantotnghiep.utils.AppUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("api/momo")
public class MomoResource {
    private final MomoService momoService;
    private final UserService userService;

    @PreAuthorize("hasRole('ROLE_TRAINEE')")
    @PostMapping("/register/pt-and-trainee")
    public ResponseEntity<Object> test(HttpServletRequest request,
                                       @RequestBody @Valid RegisterPTAndTraineeForm form) {
        String baseUrl = AppUtils.getBaseUrl(request);
        TransactionCreateResponse response = momoService.sendRequestCreate(
            TransactionType.REGISTER_PT,
            userService.getCurrentUserLogin(),
            baseUrl,
            form);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/ipnUrl")
    public ResponseEntity<?> ipnUrl(@RequestBody PaymentResponse paymentParam) {
        log.info(" 'ipnUrl' -------> {}", paymentParam);
        momoService.updateStatusTransaction(paymentParam);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @GetMapping("/register-pt/{traineeAndPTId}")
    public MomoPTAndTraineeRegisterResource getMomoTransaction(@PathVariable("traineeAndPTId") Long traineeAndPTId) {
        return momoService.getMomoRegisterTraineeAndPT(traineeAndPTId);
    }
}
