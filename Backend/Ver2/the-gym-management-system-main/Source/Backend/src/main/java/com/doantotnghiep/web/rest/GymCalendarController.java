package com.doantotnghiep.web.rest;

import com.doantotnghiep.service.GymCalendarService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.GymCalendarDTO;
import com.doantotnghiep.service.dto.GymPriceDTO;
import com.doantotnghiep.service.dto.requestDTO.GymCalendarConfirmRequest;
import com.doantotnghiep.service.dto.requestDTO.GymCalendarFilter;
import com.doantotnghiep.service.resources.GymCalendarResource;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class GymCalendarController {
//    @Value("${jhipster.clientApp.name}")
    private static final String applicationName = "doAnApp";

    private static final String ENTITY_NAME = "GymCalendar";

    private final GymCalendarService gymCalendarService;
    private final UserService userService;

    @PostMapping("/gym-calendars")
    public ResponseEntity<GymCalendarDTO> create(@RequestBody GymCalendarDTO request) throws URISyntaxException {
        if (request.getId() != null) {
            throw new BadRequestAlertException("A new gym-calendar cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (request.getCalendarDate().isBefore(LocalDate.now())) {
            throw new BadRequestAlertException("Bạn không thể lên lịch tập với thời gian biểu này", ENTITY_NAME, "ExpiredTime");
        }
        Long userId = userService.getCurrentUserLogin().getId();
        request.setCreatedBy(userId);
        GymCalendarDTO result = gymCalendarService.save(request);
        return ResponseEntity
            .created(new URI("/api/gym-calendars/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/gym-calendars")
    public ResponseEntity<GymCalendarResource> getCalendarTrainee(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        GymCalendarFilter filter
        ) {
        return ResponseEntity.ok().body(gymCalendarService.getCalendarTrainee(pageable, filter));
    }

    @PutMapping("/gym-calendars/{id}/confirm")
    public ResponseEntity<GymCalendarDTO> confirm(
        @PathVariable(value = "id") final Long id,
        @RequestBody GymCalendarConfirmRequest request
    ) {
        if (request.getCalendarDate().isAfter(LocalDate.now())) {
            throw new BadRequestAlertException("Bạn không thể điểm danh trước ngày tập", ENTITY_NAME, "Invalid");
        }
        var calendar = gymCalendarService.confirm(request, id);
        return ResponseEntity.ok().body(calendar);
    }
}
