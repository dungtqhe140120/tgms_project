package com.doantotnghiep.service;

import com.doantotnghiep.domain.Category;
import com.doantotnghiep.service.dto.CategoryDTO;

import java.util.List;
import java.util.Optional;

import com.doantotnghiep.service.dto.display.CategoryDisplayDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.Category}.
 */
public interface CategoryService {
    /**
     * Save a category.
     *
     * @param categoryDTO the entity to save.
     * @return the persisted entity.
     */
    CategoryDTO save(CategoryDTO categoryDTO);

    /**
     * Updates a category.
     *
     * @param categoryDTO the entity to update.
     * @return the persisted entity.
     */
    CategoryDTO update(CategoryDTO categoryDTO);

    /**
     * Partially updates a category.
     *
     * @param categoryDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CategoryDTO> partialUpdate(CategoryDTO categoryDTO);

    /**
     * Get all the categories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CategoryDTO> findAll(Pageable pageable);

    /**
     * Get the "id" category.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CategoryDTO> findOne(Long id);

    /**
     * Delete the "id" category.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Page<CategoryDisplayDTO> getCenterCategoryDisplayDTO(Pageable pageable);
    Page<CategoryDisplayDTO> getPTCategoryDisplayDTO(Pageable pageable, Long user_id);

    @Transactional
    @Modifying
    void deletePTCategory(Long user_id, Long id);

    Category findCategoryByID(Long id);

}
