package com.doantotnghiep.momo.config;

import java.lang.annotation.Annotation;

import com.doantotnghiep.momo.exception.BodyError;
import com.doantotnghiep.momo.retrofit.MomoAPI;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import okhttp3.ResponseBody;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 *
 */
@Configuration
@RequiredArgsConstructor
public class MomoConfig {
    private final MomoProperties momoProperties;

    public ObjectMapper momoObjectMapper(){
        return new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    @Bean
    public Retrofit retrofit() {
        return new Retrofit.Builder()
            .addConverterFactory(JacksonConverterFactory.create(momoObjectMapper()))
            .baseUrl(momoProperties.endpoint())
            .build();
    }

    @Bean
    public Converter<ResponseBody, BodyError> converterMomoError() {
        Retrofit retrofit = retrofit();
        return retrofit.responseBodyConverter(BodyError.class, new Annotation[0]);
    }

    @Bean
    public MomoAPI momoAPI() {
        Retrofit retrofit = retrofit();
        return retrofit.create(MomoAPI.class);
    }
}
