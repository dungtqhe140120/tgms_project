package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.GymCalendar;
import com.doantotnghiep.repository.BMIRepository;
import com.doantotnghiep.repository.CategoryRepository;
import com.doantotnghiep.repository.GymCalendarRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.GymCalendarService;
import com.doantotnghiep.service.dto.GymCalendarDTO;
import com.doantotnghiep.service.dto.requestDTO.GymCalendarConfirmRequest;
import com.doantotnghiep.service.dto.requestDTO.GymCalendarFilter;
import com.doantotnghiep.service.mappers2.GymCalendarMapper2;
import com.doantotnghiep.service.resources.GymCalendarResource;
import com.doantotnghiep.specification.GymCalendarSpecification;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Set;

@Service
@Transactional
@AllArgsConstructor
public class GymCalendarImpl implements GymCalendarService {
    private final GymCalendarRepository gymCalendarRepository;
    private final CategoryRepository categoryRepository;
    private final TraineeRepository traineeRepository;
    private final BMIRepository bmiRepository;

    private final TraineeAndPTRepository traineeAndPTRepository;

    private static final Integer STATUS_CONFIRMED = 1;
    private static final Set<Integer> STATUS_CONFIRMS = Set.of(STATUS_CONFIRMED);

    @Override
    public GymCalendarDTO save(GymCalendarDTO request) {
        var entityExit = gymCalendarRepository.
            getGymCalendarByCalendarDateAndTraineeIdAndCreatedBy(request.getCalendarDate(), request.getTraineeId(), request.getCreatedBy());
        if (entityExit.isPresent()){
            throw new BadRequestAlertException("Bạn đã có lịch tập trong ngày hôm nay", "GymCalendar", "exists");
        }

        var category = categoryRepository.getById(request.getCategoryId());
        request.setCategoryName(category.getName());

        var traineeAndPT = traineeAndPTRepository.findTraineeAndPTByTraineeIdAndExpiredDateGreaterThanEqualAndConfirmStatusIn(
            request.getTraineeId(), request.getCalendarDate(), STATUS_CONFIRMS);
        if (traineeAndPT.isPresent()){
            request.setTime(traineeAndPT.get().getSlotTime().getName());
        }
        request.setPtConfirm(0);
        request.setTraineeConfirm(0);
        GymCalendar entity = GymCalendarMapper2.INSTANCE.toEntity(request);
        return GymCalendarMapper2.INSTANCE.toDTO(gymCalendarRepository.save(entity));
    }

    @Override
    public GymCalendarResource getCalendarTrainee(Pageable pageable, GymCalendarFilter filter) {
        GymCalendarResource gymCalendarResource = new GymCalendarResource();
        var trainee = traineeRepository.getTraineeByID(filter.getTraineeId());
        gymCalendarResource.setNameTrainee(trainee.getProfile().getFullname());
        var bmi = bmiRepository.findTopByTraineeIdOrderByCreatedAtDesc(filter.getTraineeId());
        if (bmi.isPresent()){
            gymCalendarResource.setBmi(bmi.get().getBmi_number());
        }
        var calendar = gymCalendarRepository.findAll(new GymCalendarSpecification(filter), pageable).
            map(GymCalendarMapper2.INSTANCE :: toDTO);
        gymCalendarResource.setGymCalendarDTOS(calendar);
        return gymCalendarResource;
    }

    @Override
    public GymCalendarDTO confirm(GymCalendarConfirmRequest request, Long id) {
        var calendar = gymCalendarRepository.getById(id);
        if (!Objects.isNull(request.getPtConfirm())){
            calendar.setPtConfirm(request.getPtConfirm());
        }
        if (!Objects.isNull(request.getTraineeConfirm())){
            calendar.setTraineeConfirm(request.getTraineeConfirm());
        }
        var result = gymCalendarRepository.save(calendar);
        return GymCalendarMapper2.INSTANCE.toDTO(result);
    }


}
