package com.doantotnghiep.momo.utils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Formatter;
import java.util.Map;
import java.util.stream.Collectors;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.doantotnghiep.momo.config.MomoProperties;
import com.doantotnghiep.momo.exception.MomoException;
import com.doantotnghiep.momo.model.form.FormSignature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

/**
 *
 */
@Slf4j
public class Encoder {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public Encoder() {
    }

    private static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        Formatter formatter = new Formatter(sb);
        byte[] var3 = bytes;
        int var4 = bytes.length;

        for (int var5 = 0; var5 < var4; ++var5) {
            byte b = var3[var5];
            formatter.format("%02x", b);
        }

        return sb.toString();
    }

    public static String signHmacSHA256(String data, String secretKey) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(secretKeySpec);
        byte[] rawHmac = mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
        return toHexString(rawHmac);
    }

    public static <T extends FormSignature> String generateSignature(Object param, MomoProperties moMoProperties, Class<T> clazz)
        throws Exception {
        T t = clazz.getDeclaredConstructor(null).newInstance();
        BeanUtils.copyProperties(param, t);
        t.setAccessKey(moMoProperties.accessKey());
        Map<String, Object> parameters =
            OBJECT_MAPPER.convertValue(t, new TypeReference<>() {});
        return generateSignature(parameters, moMoProperties);
    }

    public static String generateSignature(Map<String, Object> parameters, MomoProperties moMoProperties) {
//        parameters.put()
        String parameterString = parameters.keySet().stream()
            .sorted(String::compareTo)
            .map(key -> key + "=" + parameters.get(key))
            .collect(Collectors.joining("&"));
        try {
            return signHmacSHA256(parameterString, moMoProperties.secretKey());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new MomoException("Fail when generate signature!");
        }
    }

    public static <T> String encode64ExtraData(T element) {
        try {
            String jsonStr = new ObjectMapper().writeValueAsString(element);
            return encode64(jsonStr);
        } catch (
            JsonProcessingException e) {
            log.error("Error when encode extraData: {}", e.getMessage());
            throw new MomoException("Error when encode extraData!");
        }
    }

    public static <T> T decode64ExtraData(String encodedStr, Class<T> clazz) {
        if (Strings.isEmpty(encodedStr)) {
            return null;
        }
        String jsonStr = decode64(encodedStr);
        try {
            return new ObjectMapper().readValue(jsonStr, clazz);
        } catch (JsonProcessingException e) {
            log.error("Error when decode extraData: {}", e.getMessage());
            throw new MomoException("Error when decode extraData!");
        }
    }

    private static String decode64(String s) {
        try {
            byte[] valueDecoded = Base64.getDecoder().decode(s.getBytes());
            return new String(valueDecoded);
        } catch (Exception e) {
            log.error("Error when decode64: {}", s);
            throw new MomoException("Error when decode extraData!");
        }
    }

    private static String encode64(String s) {
        byte[] bytesEncoded = Base64.getEncoder().encode(s.getBytes());
        return new String(bytesEncoded);
    }

    public static String generateURL(String baseUrl, Map<String, String> parameter) {
        if (CollectionUtils.isEmpty(parameter)) {
            return baseUrl;
        }
        return parameter.keySet().stream()
            .map(key -> key + "=" + encodeValue(parameter.get(key)))
            .collect(Collectors.joining("&", baseUrl, ""));
    }

    private static String encodeValue(String value) {
        return URLEncoder.encode(value, StandardCharsets.UTF_8);
    }
}
