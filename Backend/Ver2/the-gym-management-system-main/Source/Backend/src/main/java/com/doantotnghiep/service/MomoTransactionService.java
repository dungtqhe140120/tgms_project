package com.doantotnghiep.service;

import java.util.Optional;

import com.doantotnghiep.domain.MomoTransaction;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.service.dto.form.RegisterPTAndTraineeForm;
import com.doantotnghiep.service.dto.type.TransactionType;

public interface MomoTransactionService {
    MomoTransaction initializeMomoTransaction(RegisterPTAndTraineeForm form, User user);

    Optional<MomoTransaction> findById(Long id);

    Integer updateStatusExpired();

    MomoTransaction save(MomoTransaction entity);

}
