package com.doantotnghiep.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A DTO for the {@link com.doantotnghiep.domain.TraineeAndPT} entity.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TraineeAndPTDTO implements Serializable {

    private Long traineeAndPtId;

    private String type;

    private LocalDate expiredDate;

    private LocalDate registeredDate;

    private Long price;

    private Integer confirmStatus;

    private LocalDate confirmDate;

    private String confirmBy;

    private Long traineeId;

    private Long ptId;

    private Long slotId;

    private String imagePt;

    private Long transactionId;
}
