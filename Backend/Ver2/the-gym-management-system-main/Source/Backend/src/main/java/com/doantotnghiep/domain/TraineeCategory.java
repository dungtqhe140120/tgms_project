package com.doantotnghiep.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * A TraineeCategory.
 */
@Entity
@Table(name = "trainee_category")
public class TraineeCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "modify_date")
    private LocalDate modify_date;

    @Column(name = "assign_by")
    private Long assign_by;
    @Column(name = "assign_date")
    private LocalDate assign_Date;

    @Column(name = "modified_by")
    private Long modified_by;

    @Column(name = "modified_date")
    private LocalDate modified_Date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = {"TraineeCategory"}, allowSetters = true)
    private Trainee trainee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = {"traineeCategory"}, allowSetters = true)
    private Category category;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public TraineeCategory id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getModify_date() {
        return modify_date;
    }

    public void setModify_date(LocalDate modify_date) {
        this.modify_date = modify_date;
    }

    public Trainee getTrainee() {
        return trainee;
    }

    public void setTrainee(Trainee trainee) {
        this.trainee = trainee;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getAssign_by() {
        return assign_by;
    }

    public void setAssign_by(Long assign_by) {
        this.assign_by = assign_by;
    }

    public LocalDate getAssign_Date() {
        return assign_Date;
    }

    public void setAssign_Date(LocalDate assign_Date) {
        this.assign_Date = assign_Date;
    }

    public Long getModified_by() {
        return modified_by;
    }

    public void setModified_by(Long modified_by) {
        this.modified_by = modified_by;
    }

    public LocalDate getModified_Date() {
        return modified_Date;
    }

    public void setModified_Date(LocalDate modified_Date) {
        this.modified_Date = modified_Date;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TraineeCategory)) {
            return false;
        }
        return id != null && id.equals(((TraineeCategory) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TraineeCategory{" +
            "id=" + getId() +
            "}";
    }
}
