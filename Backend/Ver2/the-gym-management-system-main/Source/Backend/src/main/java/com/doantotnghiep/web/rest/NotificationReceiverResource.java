package com.doantotnghiep.web.rest;

import com.doantotnghiep.repository.NotificationReceiverRepository;
import com.doantotnghiep.service.NotificationReceiverService;
import com.doantotnghiep.service.dto.NotificationReceiverDTO;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.NotificationReceiver}.
 */
@RestController
@RequestMapping("/api")
public class NotificationReceiverResource {

    private final Logger log = LoggerFactory.getLogger(NotificationReceiverResource.class);

    private static final String ENTITY_NAME = "notificationReceiver";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NotificationReceiverService notificationReceiverService;

    private final NotificationReceiverRepository notificationReceiverRepository;

    public NotificationReceiverResource(
        NotificationReceiverService notificationReceiverService,
        NotificationReceiverRepository notificationReceiverRepository
    ) {
        this.notificationReceiverService = notificationReceiverService;
        this.notificationReceiverRepository = notificationReceiverRepository;
    }

    /**
     * {@code POST  /notification-receivers} : Create a new notificationReceiver.
     *
     * @param notificationReceiverDTO the notificationReceiverDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new notificationReceiverDTO, or with status {@code 400 (Bad Request)} if the notificationReceiver has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/notification-receivers")
    public ResponseEntity<NotificationReceiverDTO> createNotificationReceiver(
        @Valid @RequestBody NotificationReceiverDTO notificationReceiverDTO
    ) throws URISyntaxException {
        log.debug("REST request to save NotificationReceiver : {}", notificationReceiverDTO);
        if (notificationReceiverDTO.getId() != null) {
            throw new BadRequestAlertException("A new notificationReceiver cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NotificationReceiverDTO result = notificationReceiverService.save(notificationReceiverDTO);
        return ResponseEntity
            .created(new URI("/api/notification-receivers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /notification-receivers/:id} : Updates an existing notificationReceiver.
     *
     * @param id the id of the notificationReceiverDTO to save.
     * @param notificationReceiverDTO the notificationReceiverDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notificationReceiverDTO,
     * or with status {@code 400 (Bad Request)} if the notificationReceiverDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the notificationReceiverDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/notification-receivers/{id}")
    public ResponseEntity<NotificationReceiverDTO> updateNotificationReceiver(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody NotificationReceiverDTO notificationReceiverDTO
    ) throws URISyntaxException {
        log.debug("REST request to update NotificationReceiver : {}, {}", id, notificationReceiverDTO);
        if (notificationReceiverDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, notificationReceiverDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!notificationReceiverRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        NotificationReceiverDTO result = notificationReceiverService.save(notificationReceiverDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, notificationReceiverDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /notification-receivers/:id} : Partial updates given fields of an existing notificationReceiver, field will ignore if it is null
     *
     * @param id the id of the notificationReceiverDTO to save.
     * @param notificationReceiverDTO the notificationReceiverDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notificationReceiverDTO,
     * or with status {@code 400 (Bad Request)} if the notificationReceiverDTO is not valid,
     * or with status {@code 404 (Not Found)} if the notificationReceiverDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the notificationReceiverDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/notification-receivers/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<NotificationReceiverDTO> partialUpdateNotificationReceiver(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody NotificationReceiverDTO notificationReceiverDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update NotificationReceiver partially : {}, {}", id, notificationReceiverDTO);
        if (notificationReceiverDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, notificationReceiverDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!notificationReceiverRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<NotificationReceiverDTO> result = notificationReceiverService.partialUpdate(notificationReceiverDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, notificationReceiverDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /notification-receivers} : get all the notificationReceivers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of notificationReceivers in body.
     */
    @GetMapping("/notification-receivers")
    public List<NotificationReceiverDTO> getAllNotificationReceivers() {
        log.debug("REST request to get all NotificationReceivers");
        return notificationReceiverService.findAll();
    }

    /**
     * {@code GET  /notification-receivers/:id} : get the "id" notificationReceiver.
     *
     * @param id the id of the notificationReceiverDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the notificationReceiverDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/notification-receivers/{id}")
    public ResponseEntity<NotificationReceiverDTO> getNotificationReceiver(@PathVariable Long id) {
        log.debug("REST request to get NotificationReceiver : {}", id);
        Optional<NotificationReceiverDTO> notificationReceiverDTO = notificationReceiverService.findOne(id);
        return ResponseUtil.wrapOrNotFound(notificationReceiverDTO);
    }

    /**
     * {@code DELETE  /notification-receivers/:id} : delete the "id" notificationReceiver.
     *
     * @param id the id of the notificationReceiverDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/notification-receivers/{id}")
    public ResponseEntity<Void> deleteNotificationReceiver(@PathVariable Long id) {
        log.debug("REST request to delete NotificationReceiver : {}", id);
        notificationReceiverService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
