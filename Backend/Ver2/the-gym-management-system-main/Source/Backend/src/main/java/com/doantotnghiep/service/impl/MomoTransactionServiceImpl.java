package com.doantotnghiep.service.impl;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import com.doantotnghiep.domain.MomoTransaction;
import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.repository.MomoTransactionRepository;
import com.doantotnghiep.repository.PersonalTrainerRepository;
import com.doantotnghiep.service.MomoTransactionService;
import com.doantotnghiep.service.dto.form.RegisterPTAndTraineeForm;
import com.doantotnghiep.service.dto.type.MomoOrderType;
import com.doantotnghiep.service.dto.type.MomoTransactionStatus;
import com.doantotnghiep.service.dto.type.TransactionType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class MomoTransactionServiceImpl implements MomoTransactionService {

    private final MomoTransactionRepository momoTransactionRepository;
    private final PersonalTrainerRepository personalTrainerRepository;

    @Override
    @Transactional
    public MomoTransaction initializeMomoTransaction(RegisterPTAndTraineeForm form, User user) {
        PersonalTrainer pt = personalTrainerRepository.getPersonalTrainerById(form.getPtId());
        User ptUser = pt.getProfile().getUser();

        MomoTransaction entity = new MomoTransaction();
        entity.setAmount(form.getAmount());
        entity.setOrderType(MomoOrderType.MOMO_WALLET);
        entity.setUserId(user.getId());
        entity.setStatus(MomoTransactionStatus.NEW);
        entity.setType(TransactionType.REGISTER_PT);
        entity.setUserName(user.getFirstName() + " " + user.getLastName());
        entity.setPtName(ptUser.getFirstName() + " " + ptUser.getLastName());
        return momoTransactionRepository.save(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MomoTransaction> findById(Long id) {
        return momoTransactionRepository.findById(id);
    }

    @Override
    @Transactional
    public Integer updateStatusExpired() {
        return momoTransactionRepository.updateStatusExpire(Instant.now().minus(30L, ChronoUnit.MINUTES));
    }

    @Override
    public MomoTransaction save(MomoTransaction entity) {
        return momoTransactionRepository.save(entity);
    }
}
