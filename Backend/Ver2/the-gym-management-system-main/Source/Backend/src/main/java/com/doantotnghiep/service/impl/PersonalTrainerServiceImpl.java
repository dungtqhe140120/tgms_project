package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.domain.Profile;
import com.doantotnghiep.repository.PersonalTrainerRepository;
import com.doantotnghiep.repository.ProfileRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.repository.UserRepository;
import com.doantotnghiep.service.PersonalTrainerService;
import com.doantotnghiep.service.dto.PersonalTrainerDTO;
import com.doantotnghiep.service.dto.PersonalTrainerFullDTO;
import com.doantotnghiep.service.dto.requestDTO.PersonalTraineeUpdateDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.mapper.PersonalTrainerMapper;

import java.util.List;
import java.util.Optional;

import com.doantotnghiep.service.resources.Trainee2PTResources;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PersonalTrainer}.
 */
@Service
@Transactional
@AllArgsConstructor
public class PersonalTrainerServiceImpl implements PersonalTrainerService {

    private final Logger log = LoggerFactory.getLogger(PersonalTrainerServiceImpl.class);

    private final PersonalTrainerRepository personalTrainerRepository;

    private final PersonalTrainerMapper personalTrainerMapper;

    private final TraineeAndPTRepository traineeAndPTRepository;

    private final ProfileRepository profileRepository;

    private final UserRepository userRepository;

    @Override
    public PersonalTrainerDTO save(PersonalTrainerDTO personalTrainerDTO) {
        log.debug("Request to save PersonalTrainerDTO : {}", personalTrainerDTO);
        PersonalTrainer personalTrainer = personalTrainerMapper.toEntity(personalTrainerDTO);
        personalTrainer = personalTrainerRepository.save(personalTrainer);
        return personalTrainerMapper.toDto(personalTrainer);
    }

    @Override
    public Optional<PersonalTrainerDTO> partialUpdate(PersonalTrainerDTO personalTrainerDTO) {
        log.debug("Request to partially update PersonalTrainer : {}", personalTrainerDTO);

        return personalTrainerRepository
            .findById(personalTrainerDTO.getId())
            .map(existingPersonalTrainer -> {
                personalTrainerMapper.partialUpdate(existingPersonalTrainer, personalTrainerDTO);

                return existingPersonalTrainer;
            })
            .map(personalTrainerRepository::save)
            .map(personalTrainerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PersonalTrainerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PersonalTrainers");
        return personalTrainerRepository.findAll(pageable).map(personalTrainerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PersonalTrainerDTO> findOne(Long id) {
        log.debug("Request to get PersonalTrainer : {}", id);
        return personalTrainerRepository.findById(id).map(personalTrainerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PersonalTrainer : {}", id);
        personalTrainerRepository.deleteById(id);
    }

    @Override
    public PersonalTrainer savePersonalTrainer(PersonalTrainer personalTrainer) {
        log.debug("Request to delete PersonalTrainer : {}", personalTrainer);
        return personalTrainerRepository.save(personalTrainer);
    }

    @Override
    public PersonalTrainerFullDTO getOnePersonalTrainerFullDTOByUser(Long user_id) {
        log.debug("Request to getOnePersonalTrainerFullDTOByUser : {}", user_id);
        return personalTrainerRepository.getOnePersonalTrainerFullDTOByUser(user_id);
    }

    @Override
    public ReqPTFullDTO getOneReqPTFullDTOByUser(Long user_id) {
        log.debug("Request to getOneReqPTFullDTOByUser : {}", user_id);
        return personalTrainerRepository.getReqPTFullDTOByUserID(user_id);
    }

    @Override
    public Boolean getExistPT(Long user_id) {
        log.info("count PT by user: "+personalTrainerRepository.countPTByUserID(user_id));
        if (personalTrainerRepository.countPTByUserID(user_id) != 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public PersonalTrainer findPTByUserID(Long user_id) {
        log.debug("Request to findPTByUserID : {}", user_id);
        return personalTrainerRepository.getPTByUserID(user_id);
    }

    @Override
    public Page<ReqPTFullDTO> getPTInfFulls(Pageable pageable) {
        List<ReqPTFullDTO> reqPTFullDTOS =personalTrainerRepository.getPTs();
        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), reqPTFullDTOS.size());
        final Page<ReqPTFullDTO> page = new PageImpl<>(reqPTFullDTOS.subList(start, end), pageable, reqPTFullDTOS.size());
        return page;
    }

    @Override
    public Page<Trainee2PTResources> getCurrentTraineeOfPT(Long ptId, Pageable pageable) {
        return traineeAndPTRepository.getCurrentListTraineeByPTId(pageable, ptId);
    }

    @Override
    public Optional<PersonalTraineeUpdateDTO> getPTById(Long id) {
        return personalTrainerRepository.getPersonalTrainerDTOById(id);
    }

    @Override
    public void updateFullPT(PersonalTraineeUpdateDTO request) {
        var pt = personalTrainerRepository.getPersonalTrainerById(request.getId());
        pt.setExperience(request.getExperience());
        pt.setShort_description(request.getShortDescription());
        personalTrainerRepository.save(pt);

        var profilePT = profileRepository.getProfilesById(pt.getProfile().getId());
        profilePT.setEmail(request.getEmail());
        profilePT.setAvatar(request.getImageUrl());
        profilePT.setGender(request.getGender());
        profilePT.setBirthday(request.getBirthDay());
        profilePT.setPhone(request.getPhone());
        profilePT.setAddress(request.getAddress());
        profilePT.setFullname(request.getFirstName() + " " + request.getLastName());
        profileRepository.save(profilePT);

        var user = userRepository.getUserById(profilePT.getUser().getId());
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        userRepository.save(user);
    }

}
