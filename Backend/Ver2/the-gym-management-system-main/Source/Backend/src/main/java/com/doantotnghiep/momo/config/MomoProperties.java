package com.doantotnghiep.momo.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 *
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "momo")
public record MomoProperties(
    String endpoint,
    String accessKey,
    String partnerCode,
    String secretKey,
    String partnerName,
    String storeId) {

}
