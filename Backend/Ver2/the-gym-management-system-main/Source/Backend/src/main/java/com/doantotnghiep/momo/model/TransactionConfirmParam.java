package com.doantotnghiep.momo.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 *
 */
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TransactionConfirmParam {
    private String partnerCode;
    private String requestId;
    private String orderId;
    private String lang;
    private String signature;
}
