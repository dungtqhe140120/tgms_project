package com.doantotnghiep.momo.model;

public record PaymentResponse(
    String partnerCode,
    String orderId, // Note orderId equivalent transactionId in DB
    String requestId,
    Long amount,
    String orderInfo,
    String orderType,
    String transId,
    Integer resultCode,
    String message,
    String payType,
    String responseTime,
    String extraData,
    String signature
) {
}
