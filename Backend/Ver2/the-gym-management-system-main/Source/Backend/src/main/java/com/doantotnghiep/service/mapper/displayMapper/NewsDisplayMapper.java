package com.doantotnghiep.service.mapper.displayMapper;


import com.doantotnghiep.domain.News;
import com.doantotnghiep.service.dto.display.NewsDisplayDTO;
import com.doantotnghiep.service.mapper.EntityMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface NewsDisplayMapper extends EntityMapper<NewsDisplayDTO, News> {

}
