package com.doantotnghiep.repository;

import com.doantotnghiep.domain.User;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import liquibase.pro.packaged.Q;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findOneByActivationKey(String activationKey);
    List<User> findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant dateTime);
    Optional<User> findOneByResetKey(String resetKey);
//    Optional<User> findOneByEmailIgnoreCase(String email);


    Optional<User> findOneByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesByEmailIgnoreCase(String email);

    Page<User> findAllByIdNotNullAndActivatedIsTrue(Pageable pageable);
    Optional<User> findOneByEmailIgnoreCase(String email);

    @Query(value = "select * from jhi_user a where a.email = ?1", nativeQuery = true)
    Optional<User> findOneByEmail(String email);

    User getUserById(Long id);

}
