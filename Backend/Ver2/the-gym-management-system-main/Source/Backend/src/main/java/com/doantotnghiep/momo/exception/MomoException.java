package com.doantotnghiep.momo.exception;

import lombok.Data;

/**
 *
 */
@Data
public class MomoException extends RuntimeException{
    private BodyError bodyError;

    public MomoException(String message) {
        super(message);
    }

    public MomoException(BodyError bodyError) {
        this.bodyError = bodyError;
    }
}
