package com.doantotnghiep.web.rest;

import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.domain.Trainee;
import com.doantotnghiep.repository.EmployeeRepository;
import com.doantotnghiep.repository.PersonalTrainerRepository;
import com.doantotnghiep.repository.TraineeAndPTDraftRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.dto.HotFixTraineeAndPTDTO;
import com.doantotnghiep.service.TraineeAndPTService;
import com.doantotnghiep.service.TraineeAndPtDraftService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.HotFixTraineeAndPTDTO2;
import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import com.doantotnghiep.service.dto.display.CalendarPTDisplayDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqCreateTraineeAndPT;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.dto.requestDTO.TraineeAndPTFilter;
import com.doantotnghiep.service.mappers2.TraineeAndPTMapper2Bean;
import com.doantotnghiep.service.resources.TraineeAndPTResources;
import com.doantotnghiep.service.resources.TraineeAndPtDraftInfResource;
import com.doantotnghiep.service.resources.TraineeAndPtDraftResource;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.TraineeAndPT}.
 */
@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class TraineeAndPTController {

    private final Logger log = LoggerFactory.getLogger(TraineeAndPTController.class);

    private static final String ENTITY_NAME = "traineeAndPT";

    private static final String applicationName = "doAnApp";

    private final TraineeAndPTService traineeAndPTService;

    private final TraineeAndPtDraftService traineeAndPtDraftService;
    private final TraineeAndPTRepository traineeAndPTRepository;
    private final TraineeAndPTDraftRepository traineeAndPTDraftRepository;

    private final TraineeRepository traineeRepository;

    private final EmployeeRepository employeeRepository;

    private final UserService userService;

    private final TraineeAndPTMapper2Bean traineeAndPTMapper2Bean;

    private final PersonalTrainerRepository personalTrainerRepository;

    @PostMapping("/trainee-and-pts")
    public ResponseEntity<TraineeAndPTDTO> createTraineeAndPT(
        @Valid @RequestBody ReqCreateTraineeAndPT request
    )
        throws URISyntaxException {
        log.debug("REST request to save TraineeAndPT : {}", request);
        Long userId = userService.getCurrentUserLogin().getId();
        Long traineeId = traineeRepository.getTraineeId(userId);
        request.setTraineeId(traineeId);
        request.setType(request.getDateRegister().toString());
        TraineeAndPTDTO result = traineeAndPTService.saveV2(request);
        return ResponseEntity
            .created(new URI("/api/trainee-and-pts/" + result.getTraineeAndPtId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getTraineeAndPtId().toString()))
            .body(result);
    }

    @PreAuthorize("hasRole('ROLE_TRAINEE')")
    @PostMapping("/trainee-and-pts/draft")
    public ResponseEntity<TraineeAndPtDraftResource> createTraineeAndPTDraft(
        @Valid @RequestBody TraineeAndPtDraftResource request
    )
        throws URISyntaxException {
        log.debug("REST request to save TraineeAndPT : {}", request);
        Long userId = userService.getCurrentUserLogin().getId();
        Long traineeId = traineeRepository.getTraineeId(userId);
        request.setTraineeId(traineeId);
        TraineeAndPtDraftResource result = traineeAndPtDraftService.save(request);
        return ResponseEntity
            .created(new URI("/api/trainee-and-pts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

//    /**
//     * {@code PUT  /trainee-and-pts/:id} : Updates an existing traineeAndPT.
//     *
//     * @param id              the id of the traineeAndPTDTO to save.
//     * @param traineeAndPTDTO the traineeAndPTDTO to update.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated traineeAndPTDTO,
//     * or with status {@code 400 (Bad Request)} if the traineeAndPTDTO is not valid,
//     * or with status {@code 500 (Internal Server Error)} if the traineeAndPTDTO couldn't be updated.
//     * @throws URISyntaxException if the Location URI syntax is incorrect.
//     */
//    @PutMapping("/trainee-and-pts/{id}")
//    public ResponseEntity<TraineeAndPTDTO> updateTraineeAndPT(
//        @PathVariable(value = "id", required = false) final Long id,
//        @Valid @RequestBody TraineeAndPTDTO traineeAndPTDTO
//    ) throws URISyntaxException {
//        log.debug("REST request to update TraineeAndPT : {}, {}", id, traineeAndPTDTO);
//        if (traineeAndPTDTO.getId() == null) {
//            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
//        }
//        if (!Objects.equals(id, traineeAndPTDTO.getId())) {
//            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
//        }
//
//        if (!traineeAndPTRepository.existsById(id)) {
//            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
//        }
//
//        TraineeAndPTDTO result = traineeAndPTService.save(traineeAndPTDTO);
//        return ResponseEntity
//            .ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeAndPTDTO.getId().toString()))
//            .body(result);
//    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @PutMapping("/trainee-and-pts/{id}/confirm")
    public ResponseEntity<TraineeAndPTResources> confirmTraineeAndPT(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TraineeAndPTDTO traineeAndPTDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TraineeAndPT : {}, {}", id, traineeAndPTDTO);
        if (traineeAndPTDTO.getTraineeAndPtId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, traineeAndPTDTO.getTraineeAndPtId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!traineeAndPTRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        Long userId = userService.getCurrentUserLogin().getId();
        Long confirmBy = employeeRepository.getEmployeeId(userId);
        if (confirmBy == null) {
            confirmBy = 1l;
        }
        TraineeAndPTDTO result = traineeAndPTService.confirm(traineeAndPTDTO, confirmBy);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeAndPTDTO.getTraineeAndPtId().toString()))
            .body(traineeAndPTMapper2Bean.toResource(result));
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @PutMapping("/trainee-and-pt-drafts/{id}/confirm")
    public ResponseEntity<HotFixTraineeAndPTDTO> confirmTraineeAndPTDraft(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TraineeAndPtDraftResource request
    ) throws URISyntaxException {
        log.debug("REST request to update TraineeAndPT : {}, {}", id, request);
        if (request.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, request.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!traineeAndPTDraftRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        Long userId = userService.getCurrentUserLogin().getId();
        Long confirmBy = employeeRepository.getEmployeeId(userId);
        if (confirmBy == null) {
            confirmBy = 1l;
        }

        TraineeAndPTDTO result = traineeAndPTService.confirmDraft(request, confirmBy);
        HotFixTraineeAndPTDTO hotFixTraineeAndPTDTO = new HotFixTraineeAndPTDTO();
        BeanUtils.copyProperties(result, hotFixTraineeAndPTDTO);
        hotFixTraineeAndPTDTO.setId(id);

        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getTraineeAndPtId().toString()))
            .body(hotFixTraineeAndPTDTO);
    }

//    /**
//     * {@code PATCH  /trainee-and-pts/:id} : Partial updates given fields of an existing traineeAndPT, field will ignore if it is null
//     *
//     * @param id              the id of the traineeAndPTDTO to save.
//     * @param traineeAndPTDTO the traineeAndPTDTO to update.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated traineeAndPTDTO,
//     * or with status {@code 400 (Bad Request)} if the traineeAndPTDTO is not valid,
//     * or with status {@code 404 (Not Found)} if the traineeAndPTDTO is not found,
//     * or with status {@code 500 (Internal Server Error)} if the traineeAndPTDTO couldn't be updated.
//     * @throws URISyntaxException if the Location URI syntax is incorrect.
//     */
//    @PatchMapping(value = "/trainee-and-pts/{id}", consumes = {"application/json", "application/merge-patch+json"})
//    public ResponseEntity<TraineeAndPTDTO> partialUpdateTraineeAndPT(
//        @PathVariable(value = "id", required = false) final Long id,
//        @NotNull @RequestBody TraineeAndPTDTO traineeAndPTDTO
//    ) throws URISyntaxException {
//        log.debug("REST request to partial update TraineeAndPT partially : {}, {}", id, traineeAndPTDTO);
//        if (traineeAndPTDTO.getId() == null) {
//            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
//        }
//        if (!Objects.equals(id, traineeAndPTDTO.getId())) {
//            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
//        }
//
//        if (!traineeAndPTRepository.existsById(id)) {
//            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
//        }
//
//        Optional<TraineeAndPTDTO> result = traineeAndPTService.partialUpdate(traineeAndPTDTO);
//
//        return ResponseUtil.wrapOrNotFound(
//            result,
//            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeAndPTDTO.getId().toString())
//        );
//    }
//
//    /**
//     * {@code GET  /trainee-and-pts} : get all the traineeAndPTS.
//     *
//     * @param pageable the pagination information.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of traineeAndPTS in body.
//     */
//    @GetMapping("/trainee-and-pts")
//    public ResponseEntity<List<TraineeAndPTDTO>> getAllTraineeAndPTS(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
//        log.debug("REST request to get a page of TraineeAndPTS");
//        Page<TraineeAndPTDTO> page = traineeAndPTService.findAll(pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }

    @GetMapping("/trainee-and-pts/filters")
    public ResponseEntity<Page<TraineeAndPTResources>> getAllTraineeAndPTSFilter(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        TraineeAndPTFilter filter) {
        Page<TraineeAndPTResources> page = traineeAndPTService.getListTraineeAndPTByFilter(filter, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/trainee-and-pt-drafts/filters")
    public ResponseEntity<Page<TraineeAndPtDraftInfResource>> getAllTraineeAndPtDraftsFilter(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        TraineeAndPTFilter filter) {
        Page<TraineeAndPtDraftInfResource> page = traineeAndPTService.getListTraineeAndPtDraftsByFilter(filter, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

//    /**
//     * {@code GET  /trainee-and-pts/:id} : get the "id" traineeAndPT.
//     *
//     * @param id the id of the traineeAndPTDTO to retrieve.
//     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the traineeAndPTDTO, or with status {@code 404 (Not Found)}.
//     */
//    @GetMapping("/trainee-and-pts/{id}")
//    public ResponseEntity<TraineeAndPTDTO> getTraineeAndPT(@PathVariable Long id) {
//        log.debug("REST request to get TraineeAndPT : {}", id);
//        Optional<TraineeAndPTDTO> traineeAndPTDTO = traineeAndPTService.findOne(id);
//        return ResponseUtil.wrapOrNotFound(traineeAndPTDTO);
//    }
//
//    /**
//     * {@code DELETE  /trainee-and-pts/:id} : delete the "id" traineeAndPT.
//     *
//     * @param id the id of the traineeAndPTDTO to delete.
//     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
//     */
//    @DeleteMapping("/trainee-and-pts/{id}")
//    public ResponseEntity<Void> deleteTraineeAndPT(@PathVariable Long id) {
//        log.debug("REST request to delete TraineeAndPT : {}", id);
//        traineeAndPTService.delete(id);
//        return ResponseEntity
//            .noContent()
//            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
//            .build();
//    }

    @GetMapping("/trainees-and-pt/slot/{slotId}")
    public ResponseEntity<List<ReqPTFullDTO>> getFreePTs(
        @RequestParam(value = "register-date") LocalDate registerDate,
        @PathVariable Long slotId
    ) {
        log.debug("REST request to get a list PT have free slot");
        List<ReqPTFullDTO> pTsFreeSlot = traineeAndPTService.getPTsFreeSlot(slotId, registerDate);
        return ResponseEntity.ok().body(pTsFreeSlot);
    }

    @PreAuthorize("hasRole('ROLE_TRAINEE')")
    @GetMapping("/trainees-and-pt/trainee")
    public ResponseEntity<HotFixTraineeAndPTDTO2> getCurrentTraineeAndPT() {
        Long userId = userService.getCurrentUserLogin().getId();
        Long traineeId = traineeRepository.getTraineeId(userId);
        var dto = traineeAndPTService.getCurrentTraineeAndPT(traineeId);
        if (dto.isEmpty()) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TraineeAndPTDTO traineeAndPTDTO = dto.get();
        Optional<PersonalTrainer> ptFullProfile = personalTrainerRepository.findPersonalTrainerJoinFetch(traineeAndPTDTO.getPtId());
        HotFixTraineeAndPTDTO2 hotfix = new HotFixTraineeAndPTDTO2();
        BeanUtils.copyProperties(traineeAndPTDTO, hotfix);
        hotfix.setPtName(ptFullProfile.isPresent() ? ptFullProfile.get().getProfile().getFullname() : "Chưa rõ");
        return ResponseEntity.ok(hotfix);
    }

    @GetMapping("/trainee-and-pts/getCalendarPT/{id}")
    public ResponseEntity<Page<CalendarPTDisplayDTO>> getTraineeAndPT(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @PathVariable Long id, @RequestParam("date") String date
    ) {
        log.debug("REST request to get CalendarPTDisplayDTO : {}", id);
        List<CalendarPTDisplayDTO> ls = traineeAndPTService.getListCalendarPTDisplayDTO(pageable, id, date);
        log.debug("dateFind: " + date);
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), ls.size());
        Page<CalendarPTDisplayDTO> page = new PageImpl<>(ls.subList(start, end), pageable, ls.size());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }
}
