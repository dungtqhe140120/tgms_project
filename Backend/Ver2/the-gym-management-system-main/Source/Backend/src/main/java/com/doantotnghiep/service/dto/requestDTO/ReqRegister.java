package com.doantotnghiep.service.dto.requestDTO;

import com.doantotnghiep.web.rest.vm.ManagedUserVM;

import java.time.LocalDate;

public class ReqRegister extends ManagedUserVM {
    private Integer gender;
    private LocalDate birthDay;
    private String phone;
    private String address;
    private String experience;
    private String shortDescription;

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
}
