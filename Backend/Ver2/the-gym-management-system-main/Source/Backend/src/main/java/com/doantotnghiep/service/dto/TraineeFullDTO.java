package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class TraineeFullDTO implements Serializable {

    private Long id;

    private String short_description;

    private String address;
    private String avatar;
    private LocalDate birthday;
    private String email;
    private String full_name;
    private Integer gender;
    private Integer isActive;
    private Long modified_by;
    private LocalDate modified_dated;
    private String objective;
    private String phone;
    private Integer first_login;

    public TraineeFullDTO(Long id, String short_description,  String address, String avatar, LocalDate birthday, String email, String full_name, Integer gender, Integer isActive, Long modified_by, LocalDate modified_dated, String objective, String phone, Integer first_login) {
        this.id = id;
        this.short_description = short_description;
        this.address = address;
        this.avatar = avatar;
        this.birthday = birthday;
        this.email = email;
        this.full_name = full_name;
        this.gender = gender;
        this.isActive = isActive;
        this.modified_by = modified_by;
        this.modified_dated = modified_dated;
        this.objective = objective;
        this.phone = phone;
        this.first_login = first_login;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Long getModified_by() {
        return modified_by;
    }

    public void setModified_by(Long modified_by) {
        this.modified_by = modified_by;
    }

    public LocalDate getModified_dated() {
        return modified_dated;
    }

    public void setModified_dated(LocalDate modified_dated) {
        this.modified_dated = modified_dated;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getFirst_login() {
        return first_login;
    }

    public void setFirst_login(Integer first_login) {
        this.first_login = first_login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TraineeFullDTO that = (TraineeFullDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(short_description, that.short_description)  && Objects.equals(address, that.address) && Objects.equals(avatar, that.avatar) && Objects.equals(birthday, that.birthday) && Objects.equals(email, that.email) && Objects.equals(full_name, that.full_name) && Objects.equals(gender, that.gender) && Objects.equals(isActive, that.isActive) && Objects.equals(modified_by, that.modified_by) && Objects.equals(modified_dated, that.modified_dated) && Objects.equals(objective, that.objective) && Objects.equals(phone, that.phone) && Objects.equals(first_login, that.first_login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, short_description,  address, avatar, birthday, email, full_name, gender, isActive, modified_by, modified_dated, objective, phone, first_login);
    }

    @Override
    public String toString() {
        return "TraineeFullDTO{" +
            "id=" + id +
            ", short_description='" + short_description + '\'' +
            ", address='" + address + '\'' +
            ", avatar='" + avatar + '\'' +
            ", birthday=" + birthday +
            ", email='" + email + '\'' +
            ", full_name='" + full_name + '\'' +
            ", gender=" + gender +
            ", isActive=" + isActive +
            ", modified_by=" + modified_by +
            ", modified_dated=" + modified_dated +
            ", objective='" + objective + '\'' +
            ", phone='" + phone + '\'' +
            ", first_login=" + first_login +
            '}';
    }
}
