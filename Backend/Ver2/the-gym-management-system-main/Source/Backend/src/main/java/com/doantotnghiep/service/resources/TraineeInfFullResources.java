package com.doantotnghiep.service.resources;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
public class TraineeInfFullResources {
    private Long id;
    private String fullName;
    private String avatar;
    private Integer gender;
    private String phone;
    private String email;
    private String address;
    private LocalDate createdAt;
    private LocalDate expiredAccountAt;
    private LocalDate registerAt;
    private LocalDate expiredRegisterPTAt;
    private String numberDayRegister;
    private Long ptId;
    private String fullNamePt;
    private Long pricePT;
    private String avatarPT;
}
