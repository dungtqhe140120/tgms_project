package com.doantotnghiep.service.resources;

import org.springframework.data.domain.Page;

import java.util.List;

public class HomeTraineeEntity {
    private Long ptId;
    private BMIResource bmiResource;
    private Page<NewsResource> newsResources;

    public Long getPtId() {
        return ptId;
    }

    public void setPtId(Long ptId) {
        this.ptId = ptId;
    }

    public BMIResource getBmiResource() {
        return bmiResource;
    }

    public void setBmiResource(BMIResource bmiResource) {
        this.bmiResource = bmiResource;
    }

    public Page<NewsResource> getNewsResources() {
        return newsResources;
    }

    public void setNewsResources(Page<NewsResource> newsResources) {
        this.newsResources = newsResources;
    }
}
