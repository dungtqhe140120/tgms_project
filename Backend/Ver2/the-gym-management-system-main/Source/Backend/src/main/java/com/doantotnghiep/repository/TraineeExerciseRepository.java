package com.doantotnghiep.repository;

import com.doantotnghiep.domain.TraineeExercise;
import com.doantotnghiep.service.dto.display.ExerciseTraineePTDisplayDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TraineeExercise entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TraineeExerciseRepository extends JpaRepository<TraineeExercise, Long> {

    @Query(value = "select new com.doantotnghiep.service.dto.display.ExerciseTraineePTDisplayDTO(e.id, e.title, p.fullname,  s.name, b.height)  from Exercise e inner join TraineeExercise te   on te.exercise.id=e.id inner join Trainee t on te.trainee.id=t.id inner join Profile p on t.profile.id=p.id \n" +
        "inner join BMI b on b.trainee.id=t.id inner join TraineeAndPT tp on t.id=tp.trainee.id inner join SlotTime s on s.id=tp.slotTime.id  where t.id=?1")
    Page<ExerciseTraineePTDisplayDTO> getDetailExerciseTraineeByTrainee(Pageable pageable, Long trainee_id);
}
