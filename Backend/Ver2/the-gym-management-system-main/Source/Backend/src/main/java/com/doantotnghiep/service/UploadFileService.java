package com.doantotnghiep.service;

import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {

    String uploadFile(MultipartFile multipartFile, String name);
}
