package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.doantotnghiep.domain.Trainee} entity.
 */
public class TraineeDTO implements Serializable {

    private Long id;

    private String short_Description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShort_Description() {
        return short_Description;
    }

    public void setShort_Description(String short_Description) {
        this.short_Description = short_Description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TraineeDTO)) {
            return false;
        }

        TraineeDTO traineeDTO = (TraineeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, traineeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TraineeDTO{" +
            "id=" + getId() +
            ", short_Description='" + getShort_Description() + "'" +
            "}";
    }
}
