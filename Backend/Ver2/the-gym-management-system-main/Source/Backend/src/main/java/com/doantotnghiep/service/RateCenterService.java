package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.RateCenterDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.RateCenter}.
 */
public interface RateCenterService {
    /**
     * Save a rateCenter.
     *
     * @param rateCenterDTO the entity to save.
     * @return the persisted entity.
     */
    RateCenterDTO save(RateCenterDTO rateCenterDTO);

    /**
     * Updates a rateCenter.
     *
     * @param rateCenterDTO the entity to update.
     * @return the persisted entity.
     */
    RateCenterDTO update(RateCenterDTO rateCenterDTO);

    /**
     * Partially updates a rateCenter.
     *
     * @param rateCenterDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RateCenterDTO> partialUpdate(RateCenterDTO rateCenterDTO);

    /**
     * Get all the rateCenters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RateCenterDTO> findAll(Pageable pageable);

    /**
     * Get the "id" rateCenter.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RateCenterDTO> findOne(Long id);

    /**
     * Delete the "id" rateCenter.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
