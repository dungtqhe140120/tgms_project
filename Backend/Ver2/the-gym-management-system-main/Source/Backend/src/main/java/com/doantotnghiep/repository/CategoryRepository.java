package com.doantotnghiep.repository;

import com.doantotnghiep.domain.Category;
import com.doantotnghiep.service.dto.display.CategoryDisplayDTO;
import liquibase.pro.packaged.Q;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query("select new com.doantotnghiep.service.dto.display.CategoryDisplayDTO(c.id, c.name, c.image) from Category c where c.isCenter=1")
    Page<CategoryDisplayDTO> getCenterCategoryDisplayDTO(Pageable pageable);

    @Query("select new com.doantotnghiep.service.dto.display.CategoryDisplayDTO(c.id, c.name, c.image) from Category c inner join User u on c.created_by=u.id where c.isCenter=0 and u.id=?1")
    Page<CategoryDisplayDTO> getPTCategoryDisplayDTO(Pageable pageable, Long user_id);

    @Modifying
    @Query(value = "delete from category c where c.is_center=0 and c.created_by=?1 and c.id=?2 ", nativeQuery = true)
    void deletePTCategory(Long user_id, Long category_id);

    @Query(value = "select * from category where id=?1", nativeQuery = true)
    Category findCategoriesByID(Long id);
}
