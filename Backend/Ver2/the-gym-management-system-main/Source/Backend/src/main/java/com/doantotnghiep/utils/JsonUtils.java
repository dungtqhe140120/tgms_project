package com.doantotnghiep.utils;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.experimental.UtilityClass;

/**
 *
 */
@UtilityClass
public class JsonUtils {
    private static final ObjectMapper MAPPER = new ObjectMapper()
        .registerModule(new JavaTimeModule())
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .setSerializationInclusion(JsonInclude.Include.NON_NULL);

    public static String toString(Object object) {
        try{
            return MAPPER.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T toObject(String str, Class<T> type) {
        try{
            return MAPPER.readValue(str, type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, Object> toMap(Object object) {
        return MAPPER.convertValue(object, new TypeReference<Map<String, Object>>() {});
    }

    public static JsonNode toJsonNode(Object object) {
        return MAPPER.valueToTree(object);
    }
}
