package com.doantotnghiep.service;

import com.doantotnghiep.domain.MomoTransaction;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.momo.model.PaymentResponse;
import com.doantotnghiep.momo.model.TransactionCreateParam;
import com.doantotnghiep.momo.model.TransactionCreateResponse;
import com.doantotnghiep.momo.model.form.ExtraData;
import com.doantotnghiep.service.dto.form.RegisterPTAndTraineeForm;
import com.doantotnghiep.service.dto.type.TransactionType;
import com.doantotnghiep.service.resources.MomoPTAndTraineeRegisterResource;

public interface MomoService {
    TransactionCreateResponse sendRequestCreate(TransactionType transactionType, User user, String baseUrl, RegisterPTAndTraineeForm form);
    TransactionCreateParam generateTransactionCreateParam(String baseUrl, MomoTransaction entity, ExtraData extraData, RegisterPTAndTraineeForm form);
    boolean checkTransaction(PaymentResponse paymentResponse)throws Exception;
    void updateStatusTransaction(PaymentResponse paymentResponse);

    void expireMomoTransaction();

    MomoPTAndTraineeRegisterResource getMomoRegisterTraineeAndPT(Long traineeAndPTId);
}
