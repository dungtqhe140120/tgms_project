package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.TraineeAndPTDraft;
import com.doantotnghiep.repository.TraineeAndPTDraftRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.service.TraineeAndPtDraftService;
import com.doantotnghiep.service.mappers2.TraineeAndPTDraftMapper2;
import com.doantotnghiep.service.resources.TraineeAndPtDraftResource;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@Transactional
@AllArgsConstructor
public class TraineeAndPtDraftImpl implements TraineeAndPtDraftService {
    private final TraineeAndPTRepository traineeAndPTRepository;
    private final TraineeAndPTDraftRepository traineeAndPTDraftRepository;
    private static final Integer STATUS_WAIT_FOR_CONFIRM = 0;

    @Override
    public TraineeAndPtDraftResource save(TraineeAndPtDraftResource request) {
        TraineeAndPTDraft entity = new TraineeAndPTDraft();
        var entityDraft = traineeAndPTDraftRepository.
            getTraineeAndPTDraftByTraineeIdAndExpiredDateGreaterThanEqualAndConfirmStatus(request.getTraineeId(), LocalDate.now(), STATUS_WAIT_FOR_CONFIRM);
        if (entityDraft.isPresent()) {
            entity = entityDraft.get();
        } else {
            entity = TraineeAndPTDraftMapper2.INSTANCE.toEntity(traineeAndPTRepository.findTraineeAndPTById(request.getTraineeAndPtId()));
        }
        entity.setPtId(request.getPtId());
        entity.setSlotId(request.getSlotId());
        entity.setConfirmStatus(0);
        entity.setConfirmBy(null);
        entity.setConfirmDate(null);
        entity.setReasonChange(request.getReasonChange());
        return TraineeAndPTDraftMapper2.INSTANCE.toResource(traineeAndPTDraftRepository.save(entity));
    }
}
