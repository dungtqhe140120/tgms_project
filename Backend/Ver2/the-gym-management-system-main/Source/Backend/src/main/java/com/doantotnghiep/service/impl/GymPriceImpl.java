package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.GymPrice;
import com.doantotnghiep.repository.GymPriceRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.GymPriceService;
import com.doantotnghiep.service.dto.GymPriceDTO;
import com.doantotnghiep.service.mapper.GymPriceMapper;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class GymPriceImpl implements GymPriceService {

    private final GymPriceRepository gymPriceRepository;
    private final GymPriceMapper gymPriceMapper;
    private final TraineeRepository traineeRepository;
    private final TraineeAndPTRepository traineeAndPTRepository;

    public GymPriceImpl(GymPriceRepository gymPriceRepository, GymPriceMapper gymPriceMapper,
                        TraineeRepository traineeRepository, TraineeAndPTRepository traineeAndPTRepository) {
        this.gymPriceRepository = gymPriceRepository;
        this.gymPriceMapper = gymPriceMapper;
        this.traineeRepository = traineeRepository;
        this.traineeAndPTRepository = traineeAndPTRepository;
    }

    @Override
    public GymPriceDTO save(GymPriceDTO gymPriceDTO) {
        GymPrice gymPrice = gymPriceMapper.toEntity(gymPriceDTO);
        return gymPriceMapper.toDto(gymPriceRepository.save(gymPrice));
    }

    @Override
    public GymPriceDTO getGymPrice(Long registerDays, LocalDate registerDate, Long userId) {
        var trainee = traineeRepository.getTraineeByUserID(userId);
        var expireDates = traineeAndPTRepository.getExpireDayByTraineeIdAndRegistered_Date(trainee.getId(), registerDate);
        if (!CollectionUtils.isEmpty(expireDates)){
            throw new BadRequestAlertException("You have had PT", "GymPrice", "Can't register");
        }
        if (registerDate.plusDays(registerDays).isAfter(trainee.getExpired_date())){
            throw new BadRequestAlertException("Date expire PT is after expire account!", "GymPrice", "Can't register");
        }
        return gymPriceMapper.toDto(gymPriceRepository.getGymPricesByRegisterDays(registerDays));
    }

    @Override
    public List<Long> getRegisterDays() {
        var listPackage = gymPriceRepository.getRegisterDays();
        Collections.sort(listPackage);
        return listPackage;
    }
}
