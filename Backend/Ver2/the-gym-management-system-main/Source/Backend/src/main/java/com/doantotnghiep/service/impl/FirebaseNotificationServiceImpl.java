package com.doantotnghiep.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.doantotnghiep.service.FirebaseNotificationService;
import com.doantotnghiep.service.dto.firebase.Notice;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.SendResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class FirebaseNotificationServiceImpl implements FirebaseNotificationService {

    private static final Logger log = LoggerFactory.getLogger(FirebaseNotificationServiceImpl.class);

    private final FirebaseMessaging firebaseMessaging;

    public FirebaseNotificationServiceImpl(FirebaseMessaging firebaseMessaging) {
        this.firebaseMessaging = firebaseMessaging;
    }

    @Override
    public BatchResponse sendNotification(Notice notice) {
        List<String> registrationTokens = notice.getRegistrationTokens();
        Notification notification = Notification.builder()
            .setTitle(notice.getSubject())
            .setBody(notice.getContent())
            .setImage(notice.getImage())
            .build();

        MulticastMessage message = MulticastMessage.builder()
            .addAllTokens(registrationTokens)
            .setNotification(notification)
            .putAllData(notice.getData())
            .build();

        BatchResponse batchResponse = null;
        try {
            batchResponse = firebaseMessaging.sendMulticast(message);
        } catch (
            FirebaseMessagingException e) {
            log.info("Firebase error {}", e.getMessage());
        }
        if (Objects.nonNull(batchResponse) && batchResponse.getFailureCount() > 0) {
            List<SendResponse> responses = batchResponse.getResponses();
            List<SendResponse> failedTokens =
                responses.stream()
                    .filter(sendResponse -> !sendResponse.isSuccessful())
                    .collect(Collectors.toList());
            log.info("List of tokens that caused failures: " + failedTokens);
        }
        return batchResponse;
    }
}
