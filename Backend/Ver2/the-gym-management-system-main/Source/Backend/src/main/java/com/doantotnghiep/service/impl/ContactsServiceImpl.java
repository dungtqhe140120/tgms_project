package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.Contacts;
import com.doantotnghiep.repository.ContactsRepository;
import com.doantotnghiep.service.ContactsService;
import com.doantotnghiep.service.dto.ContactsDTO;
import com.doantotnghiep.service.dto.requestDTO.ContactsFilter;
import com.doantotnghiep.service.mapper.ContactsMapper;
import com.doantotnghiep.specification.ContactsSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ContactsServiceImpl implements ContactsService {
    private final ContactsRepository contactsRepository;
    private final ContactsMapper contactsMapper;

    public ContactsServiceImpl(ContactsRepository contactsRepository, ContactsMapper contactsMapper) {
        this.contactsRepository = contactsRepository;
        this.contactsMapper = contactsMapper;
    }
    @Override
    public Page<ContactsDTO> getAll(ContactsFilter filter, Pageable pageable) {
        return contactsRepository.findAll(new ContactsSpecification(filter), pageable).map(contactsMapper::toDto);
    }

    @Override
    public ContactsDTO save(ContactsDTO contactsDTO) {
        Contacts contacts = contactsMapper.toEntity(contactsDTO);
        return contactsMapper.toDto(contactsRepository.save(contacts));
    }

    @Override
    public ContactsDTO getContactsById(Long id) {
        return contactsMapper.toDto(contactsRepository.getById(id));
    }
}
