package com.doantotnghiep.momo.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 *
 */
@Builder
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TransactionCreateParam { //todo check builder
    private String partnerCode;
    private String partnerName;
    private String storeId;
    private String requestType;
    private String ipnUrl;
    private String redirectUrl;
    private String orderId;
    private Integer amount;
    private String lang;
    private String orderInfo;
    private String requestId;
    private String extraData;
    private String signature;
}
