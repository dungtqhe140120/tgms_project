package com.doantotnghiep.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
@Table(name = "gym_price")
public class GymPrice implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "register_days")
    private Long registerDays;

    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "has_pt")
    private Integer hasPT;

    @Column(name = "price")
    private Long price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRegisterDays() {
        return registerDays;
    }

    public void setRegisterDays(Long registerDays) {
        this.registerDays = registerDays;
    }

    public Integer getHasPT() {
        return hasPT;
    }

    public void setHasPT(Integer hasPT) {
        this.hasPT = hasPT;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
