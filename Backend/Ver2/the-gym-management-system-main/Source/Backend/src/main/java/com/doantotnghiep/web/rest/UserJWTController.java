package com.doantotnghiep.web.rest;

import com.doantotnghiep.domain.Authority;
import com.doantotnghiep.security.jwt.JWTFilter;
import com.doantotnghiep.security.jwt.TokenProvider;
import com.doantotnghiep.service.dto.AdminUserDTO;
import com.doantotnghiep.web.rest.vm.LoginVM;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
            loginVM.getUsername(),
            loginVM.getPassword()
        );

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.createToken(authentication, loginVM.isRememberMe());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        Set<Authority> authorities = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).map(authority -> {
            Authority auth = new Authority();
            auth.setName(authority);
            return auth;
        }).collect(Collectors.toSet());
        String login=authentication.getName();
        return new ResponseEntity<>(new JWTToken(jwt, authorities, login), httpHeaders, HttpStatus.OK);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;
        private Set<Authority> role;
        private String login;

        public JWTToken(String idToken, Set<Authority> role, String login) {
            this.idToken = idToken;
            this.role = role;
            this.login = login;
        }

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        @JsonProperty("role")
        Set<Authority> getRole() {
            return role;
        }

        @JsonProperty("login")
        String getLogin() {
            return login;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
