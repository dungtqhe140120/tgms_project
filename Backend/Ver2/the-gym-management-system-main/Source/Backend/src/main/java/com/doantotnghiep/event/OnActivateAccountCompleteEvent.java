package com.doantotnghiep.event;

import com.doantotnghiep.domain.User;
import org.springframework.context.ApplicationEvent;

/**
 *
 */
public class OnActivateAccountCompleteEvent extends ApplicationEvent {
    private final User user;

    public OnActivateAccountCompleteEvent(User user) {
        super(user);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
