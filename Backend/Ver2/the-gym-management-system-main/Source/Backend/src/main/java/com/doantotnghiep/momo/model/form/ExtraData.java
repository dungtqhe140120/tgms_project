package com.doantotnghiep.momo.model.form;

public record ExtraData(
    // Data maybe use in fureture like: bonus note, donate note
    String message,
    Long momoTransactionId
) {
}
