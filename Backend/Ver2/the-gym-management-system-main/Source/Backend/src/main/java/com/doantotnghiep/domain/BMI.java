package com.doantotnghiep.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * A BMI.
 */
@Entity
@Table(name = "bmi")
public class BMI implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @DecimalMin(value = "0.1")
    @Column(name = "weight", nullable = false)
    private Float weight;

    @NotNull
    @DecimalMin(value = "0.1")
    @Column(name = "height", nullable = false)
    private Float height;

    @Column(name="bmi_number")
    private Float bmi_number;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnoreProperties(value = {"BMI"}, allowSetters = true)
    private Trainee trainee;

    public BMI() {
    }

    public BMI(Long id, Float weight, Float height, Float bmi_number, LocalDateTime createdAt, Trainee trainee) {
        this.id = id;
        this.weight = weight;
        this.height = height;
        this.bmi_number = bmi_number;
        this.createdAt = createdAt;
        this.trainee = trainee;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public BMI id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getWeight() {
        return this.weight;
    }

    public BMI weight(Float weight) {
        this.setWeight(weight);
        return this;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getHeight() {
        return this.height;
    }

    public BMI height(Float height) {
        this.setHeight(height);
        return this;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Trainee getTrainee() {
        return trainee;
    }

    public void setTrainee(Trainee trainee) {
        this.trainee = trainee;
    }

    public Float getBmi_number() {
        return bmi_number;
    }

    public void setBmi_number(Float bmi_number) {
        this.bmi_number = bmi_number;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BMI)) {
            return false;
        }
        return id != null && id.equals(((BMI) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BMI{" +
            "id=" + id +
            ", weight=" + weight +
            ", height=" + height +
            ", trainee=" + trainee +
            '}';
    }
}
