package com.doantotnghiep.service.dto.display;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class DetailExerciseShowDTO implements Serializable {
    private Long id;
    private String title;
    private String content;
    private LocalDate dateShow;

    public DetailExerciseShowDTO(Long id, String title, String content, LocalDate dateShow) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.dateShow = dateShow;
    }

    public DetailExerciseShowDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getDateShow() {
        return dateShow;
    }

    public void setDateShow(LocalDate dateShow) {
        this.dateShow = dateShow;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetailExerciseShowDTO that = (DetailExerciseShowDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(title, that.title) && Objects.equals(content, that.content) && Objects.equals(dateShow, that.dateShow);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, content, dateShow);
    }

    @Override
    public String toString() {
        return "DetailExerciseShowDTO{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", content='" + content + '\'' +
            ", dateShow=" + dateShow +
            '}';
    }
}
