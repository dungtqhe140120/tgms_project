package com.doantotnghiep.service.mappers2;

import com.doantotnghiep.domain.GymCalendar;
import com.doantotnghiep.service.dto.GymCalendarDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public abstract class GymCalendarMapper2 {
    public static final GymCalendarMapper2 INSTANCE = Mappers.getMapper(GymCalendarMapper2.class);


    public abstract GymCalendar toEntity(GymCalendarDTO dto);

    public abstract List<GymCalendar> toEntities(List<GymCalendarDTO> listDTO);

    public abstract GymCalendarDTO toDTO(GymCalendar entity);

    public abstract List<GymCalendarDTO> toDTOs(List<GymCalendar> entities);
}
