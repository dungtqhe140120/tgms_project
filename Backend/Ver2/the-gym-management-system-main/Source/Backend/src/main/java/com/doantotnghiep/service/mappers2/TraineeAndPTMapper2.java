package com.doantotnghiep.service.mappers2;

import com.doantotnghiep.domain.TraineeAndPT;
import com.doantotnghiep.domain.TraineeAndPTDraft;
import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqCreateTraineeAndPT;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class TraineeAndPTMapper2 {
    public static final TraineeAndPTMapper2 INSTANCE = Mappers.getMapper(TraineeAndPTMapper2.class);


    @Mapping(target = "trainee.id", source = "traineeId")
    @Mapping(target = "personalTrainer.id", source = "ptId")
    @Mapping(target = "slotTime.id", source = "slotId")
    @Mapping(target = "registeredDate", source = "registerDate")
    public abstract TraineeAndPT toEntity(ReqCreateTraineeAndPT request);

    @Mapping(target = "traineeAndPtId", source = "id")
    @Mapping(target = "traineeId", source = "trainee.id")
    @Mapping(target = "ptId", source = "personalTrainer.id")
    @Mapping(target = "slotId", source = "slotTime.id")
    public abstract TraineeAndPTDTO toDTO(TraineeAndPT entity);

    public abstract TraineeAndPTDTO toDTO(TraineeAndPTDraft entityDraft);
}
