package com.doantotnghiep.service.dto;

import com.doantotnghiep.domain.Trainee;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.doantotnghiep.domain.BMI} entity.
 */
public class BMIDTO implements Serializable {

    private Long id;

    @NotNull
    @DecimalMin(value = "0.1")
    private Float weight;

    @NotNull
    @DecimalMin(value = "0.1")
    private Float height;

    private LocalDateTime createdAt;

    private Float bmi_number;
    private Trainee trainee;

    public Trainee getTrainee() {
        return trainee;
    }

    public void setTrainee(Trainee trainee) {
        this.trainee = trainee;
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getBmi_number() {
        return bmi_number;
    }

    public void setBmi_number(Float bmi_number) {
        this.bmi_number = bmi_number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BMIDTO)) {
            return false;
        }

        BMIDTO bMIDTO = (BMIDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, bMIDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BMIDTO{" +
            "id=" + getId() +
            ", weight=" + getWeight() +
            ", height=" + getHeight() +
            "}";
    }
}
