package com.doantotnghiep.repository;

import com.doantotnghiep.domain.RateCenter;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the RateCenter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RateCenterRepository extends JpaRepository<RateCenter, Long> {
    RateCenter findRateCenterByUserId(Long userId);
}
