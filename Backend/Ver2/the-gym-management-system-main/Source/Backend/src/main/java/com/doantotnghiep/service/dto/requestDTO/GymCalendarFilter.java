package com.doantotnghiep.service.dto.requestDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GymCalendarFilter {
    private Long traineeId;
    private LocalDate calendarDateFrom;
    private LocalDate calendarDateTo;
}
