package com.doantotnghiep.momo.model.form;

import lombok.Data;

/**
 *
 */
@Data
public class FormSignature {
    private String accessKey;
}
