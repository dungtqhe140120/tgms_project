package com.doantotnghiep.service.dto.display;

import java.io.Serializable;
import java.util.Objects;

public class CategoryDisplayDTO implements Serializable {

    private Long id;
    private String title;
    private String picture;

    public CategoryDisplayDTO() {
    }

    public CategoryDisplayDTO(Long id, String title, String picture) {
        this.id = id;
        this.title = title;
        this.picture = picture;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return picture;
    }

    public void setImage(String picture) {
        this.picture = picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDisplayDTO that = (CategoryDisplayDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(title, that.title) && Objects.equals(picture, that.picture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, picture);
    }

    @Override
    public String toString() {
        return "CategoryDisplayDTO{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", picture='" + picture + '\'' +
            '}';
    }
}
