package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.BMI;
import com.doantotnghiep.repository.BMIRepository;
import com.doantotnghiep.service.BMIService;
import com.doantotnghiep.service.dto.BMIDTO;
import com.doantotnghiep.service.mapper.BMIMapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link BMI}.
 */
@Service
@Transactional
public class BMIServiceImpl implements BMIService {

    private final Logger log = LoggerFactory.getLogger(BMIServiceImpl.class);

    private final BMIRepository bMIRepository;

    private final BMIMapper bMIMapper;

    public BMIServiceImpl(BMIRepository bMIRepository, BMIMapper bMIMapper) {
        this.bMIRepository = bMIRepository;
        this.bMIMapper = bMIMapper;
    }

    @Override
    public BMIDTO save(BMIDTO bMIDTO) {
        log.debug("Request to save BMI : {}", bMIDTO);
        bMIDTO.setCreatedAt(LocalDateTime.now());
        BMI bMI = bMIMapper.toEntity(bMIDTO);
        bMI = bMIRepository.save(bMI);
        return bMIMapper.toDto(bMI);
    }

    @Override
    public BMIDTO update(BMIDTO bMIDTO) {
        log.debug("Request to save BMI : {}", bMIDTO);
        BMI bMI = bMIMapper.toEntity(bMIDTO);
        bMI = bMIRepository.save(bMI);
        return bMIMapper.toDto(bMI);
    }

    @Override
    public Optional<BMIDTO> partialUpdate(BMIDTO bMIDTO) {
        log.debug("Request to partially update BMI : {}", bMIDTO);

        return bMIRepository
            .findById(bMIDTO.getId())
            .map(existingBMI -> {
                bMIMapper.partialUpdate(existingBMI, bMIDTO);

                return existingBMI;
            })
            .map(bMIRepository::save)
            .map(bMIMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BMIDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BMIS");
        return bMIRepository.findAll(pageable).map(bMIMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<BMIDTO> findOne(Long id) {
        log.debug("Request to get BMI : {}", id);
        return bMIRepository.findById(id).map(bMIMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete BMI : {}", id);
        bMIRepository.deleteById(id);
    }
}
