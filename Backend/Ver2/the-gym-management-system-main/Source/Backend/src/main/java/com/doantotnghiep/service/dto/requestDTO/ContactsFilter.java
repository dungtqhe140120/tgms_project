package com.doantotnghiep.service.dto.requestDTO;

import java.time.LocalDate;

public class ContactsFilter {
    private LocalDate createdAtFrom;
    private LocalDate createdAtTo;
    private String name;
    private String email;
    private Integer status;

    public LocalDate getCreatedAtFrom() {
        return createdAtFrom;
    }

    public void setCreatedAtFrom(LocalDate createdAtFrom) {
        this.createdAtFrom = createdAtFrom;
    }

    public LocalDate getCreatedAtTo() {
        return createdAtTo;
    }

    public void setCreatedAtTo(LocalDate createdAtTo) {
        this.createdAtTo = createdAtTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
