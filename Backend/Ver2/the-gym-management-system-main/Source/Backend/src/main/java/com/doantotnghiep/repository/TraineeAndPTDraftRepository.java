package com.doantotnghiep.repository;

import com.doantotnghiep.domain.TraineeAndPTDraft;
import com.doantotnghiep.service.resources.TraineeAndPTResources;
import com.doantotnghiep.service.resources.TraineeAndPtDraftInfResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

@Repository
public interface TraineeAndPTDraftRepository extends JpaRepository<TraineeAndPTDraft, Long> {
    Optional<TraineeAndPTDraft> getTraineeAndPTDraftByTraineeIdAndExpiredDateGreaterThanEqualAndConfirmStatus
        (Long traineeId, LocalDate currentDate, Integer status);

    @Query(value = "SELECT new com.doantotnghiep.service.resources.TraineeAndPtDraftInfResource(tap.id, p.fullname," +
        " pp.fullname, tap.expiredDate, tap.registeredDate, tap.type, tap.slotId, tap.confirmStatus," +
        " tap.confirmBy, tap.confirmDate, tap.reasonChange)" +
        " FROM TraineeAndPTDraft tap INNER JOIN Trainee t ON tap.traineeId = t.id" +
        " INNER JOIN Profile p ON t.profile.id = p.id" +
        " INNER JOIN PersonalTrainer pt ON tap.ptId = pt.id" +
        " INNER JOIN Profile pp ON pt.profile.id = pp.id" +
        " WHERE pp.fullname LIKE :ptName AND p.fullname LIKE :traineeName AND tap.confirmStatus IN :confirmStatus  AND tap.expiredDate > CURRENT_DATE")
    Page<TraineeAndPtDraftInfResource> getListByFilter(Pageable pageable, @Param("ptName") String ptName,
                                                              @Param("traineeName") String traineeName, @Param("confirmStatus") Set<Integer> confirmStatus);
}
