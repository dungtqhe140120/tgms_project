package com.doantotnghiep.service.dto;

import java.time.LocalDate;

import lombok.Data;

/**
 *
 */
@Data
public class HotFixTraineeAndPTDTO2 {
    private Long traineeAndPtId;

    private String type;

    private LocalDate expiredDate;

    private LocalDate registeredDate;

    private Long price;

    private Integer confirmStatus;

    private LocalDate confirmDate;

    private String confirmBy;

    private Long traineeId;

    private Long ptId;

    private String ptName;

    private Long slotId;

    private String imagePt;

    private Long transactionId;
}
