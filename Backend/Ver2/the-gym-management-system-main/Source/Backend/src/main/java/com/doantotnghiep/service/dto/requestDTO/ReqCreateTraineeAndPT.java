package com.doantotnghiep.service.dto.requestDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ReqCreateTraineeAndPT {
    private Long ptId;

    private Long traineeId;
    private String type;

    private Long slotId;

    private Long price;
    private LocalDate registerDate;

    @NotNull
    private Long dateRegister;

    private Long transactionId;
}
