package com.doantotnghiep.service;

import com.doantotnghiep.domain.Employee;
import com.doantotnghiep.service.dto.EmployeeDTO;

import java.util.List;
import java.util.Optional;

import com.doantotnghiep.service.dto.form.UpdateFcmForm;
import com.doantotnghiep.service.resources.EmployeeInfFullResources;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.Employee}.
 */
public interface EmployeeService {
    /**
     * Save a employee.
     *
     * @param employeeDTO the entity to save.
     * @return the persisted entity.
     */
    EmployeeDTO save(EmployeeDTO employeeDTO);

    /**
     * Partially updates a employee.
     *
     * @param employeeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<EmployeeDTO> partialUpdate(EmployeeDTO employeeDTO);

    /**
     * Get all the employees.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EmployeeDTO> findAll(Pageable pageable);

    List<EmployeeInfFullResources> findAllInf();

    /**
     * Get the "id" employee.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EmployeeDTO> findOne(Long id);

    /**
     * Delete the "id" employee.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Employee saveEmployee(Employee employee);

    List<Employee> getAll();

    void updateFcm(UpdateFcmForm form);
}
