package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.doantotnghiep.domain.PersonalTrainer} entity.
 */
public class PersonalTrainerDTO implements Serializable {

    private Long id;

    private String experience;

    private String short_description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonalTrainerDTO)) {
            return false;
        }

        PersonalTrainerDTO personalTrainerDTO = (PersonalTrainerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, personalTrainerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PersonalTrainerDTO{" +
            "id=" + getId() +
            ", experience='" + getExperience() + "'" +
            ", short_description='" + getShort_description() + "'" +
            "}";
    }
}
