package com.doantotnghiep.service.resources;

import org.springframework.data.domain.Page;

public class RateAboutMeResources {
    private Float startRate;
    private Page<TraineeRatePTResource> traineeRatePTResource;

    public Float getStartRate() {
        return startRate;
    }

    public void setStartRate(Float startRate) {
        this.startRate = startRate;
    }

    public Page<TraineeRatePTResource> getTraineeRatePTResource() {
        return traineeRatePTResource;
    }

    public void setTraineeRatePTResource(Page<TraineeRatePTResource> traineeRatePTResource) {
        this.traineeRatePTResource = traineeRatePTResource;
    }
}
