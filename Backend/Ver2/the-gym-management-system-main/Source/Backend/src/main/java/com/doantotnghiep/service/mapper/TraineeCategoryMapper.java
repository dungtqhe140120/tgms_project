package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.TraineeCategory;
import com.doantotnghiep.service.dto.TraineeCategoryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TraineeCategory} and its DTO {@link TraineeCategoryDTO}.
 */
@Mapper(componentModel = "spring")
public interface TraineeCategoryMapper extends EntityMapper<TraineeCategoryDTO, TraineeCategory> {}
