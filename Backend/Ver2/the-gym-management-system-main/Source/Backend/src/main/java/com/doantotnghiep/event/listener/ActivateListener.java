package com.doantotnghiep.event.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.doantotnghiep.domain.Employee;
import com.doantotnghiep.domain.Profile;
import com.doantotnghiep.event.OnActivateAccountCompleteEvent;
import com.doantotnghiep.repository.ProfileRepository;
import com.doantotnghiep.service.EmployeeService;
import com.doantotnghiep.service.FirebaseNotificationService;
import com.doantotnghiep.service.dto.firebase.Notice;
import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
@Slf4j
@RequiredArgsConstructor
public class ActivateListener implements ApplicationListener<OnActivateAccountCompleteEvent> {

    private final FirebaseNotificationService firebaseNotificationService;
    private final ProfileRepository profileRepository;

    @Override
    public void onApplicationEvent(OnActivateAccountCompleteEvent event) {

        List<Profile> profiles = profileRepository.findByUser_Authorities_NameIn(
            List.of("ROLE_ADMIN", "ROLE_EMPLOYEE")
        );
        Set<String> fcms = profiles.stream().map(Profile::getFcm)
            .filter(StringUtils::isNotBlank)
            .collect(Collectors.toSet());

        Notice notice = new Notice();
        notice.setSubject("Nguời tập mới được kích hoạt");
        notice.setContent(String.format("%s %s vừa mới kích hoạt tài khoản!",
            event.getUser().getFirstName(),
            event.getUser().getLastName()));
        notice.setImage(event.getUser().getImageUrl());
        notice.setRegistrationTokens(new ArrayList<>(fcms));
        notice.setData(new HashMap<>());
        firebaseNotificationService.sendNotification(notice);
    }
}
