package com.doantotnghiep.utils;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.doantotnghiep.service.dto.type.MomoPayType;
import com.doantotnghiep.service.dto.type.TransactionType;
import com.google.common.collect.ImmutableMap;
import lombok.experimental.UtilityClass;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 */
@UtilityClass
public class AppUtils {
    public static MomoPayType createMomoPayTypeByValue(String value) {
        switch (value){
            case "webApp" -> {
                return MomoPayType.WEB_APP;
            }
            case "app" -> {
                return MomoPayType.APP;
            }
            case "qr" -> {
                return MomoPayType.QR;
            }
            case "miniapp" -> {
                return MomoPayType.MINI_APP;
            }
            case "" -> {
                return MomoPayType.EMPTY;
            }
            default -> throw new IllegalStateException("Unexpected value:" + value);
        }
    }

    public static String generateRequestId(TransactionType transactionType) {
        return String.format("%s_%s", transactionType.name(), System.currentTimeMillis());
    }

    public static String generateTransactionInfo(String userName, TransactionType transactionType, String ptName, Integer amount) {
        return switch (transactionType) {
            case REGISTER_ROOM -> String.format("%s đăng ký tập với phòng với số tiền %d", userName, amount);
            case REGISTER_PT -> String.format("%s đăng ký tập với huấn luyện viên %s với số tiền %d", userName, ptName, amount);
            default -> throw new RuntimeException("Error when generate transaction info");
        };
    }

    public static String getBaseUrl(final HttpServletRequest request) {
        return ServletUriComponentsBuilder.fromRequestUri(request)
            .replacePath(null).build().toUriString();
    }
}
