package com.doantotnghiep.service.dto.display;

import java.io.Serializable;
import java.util.Objects;

public class CalendarPTDisplayDTO implements Serializable {
    private Long id;
    private String name;
    private String slotName;

    public CalendarPTDisplayDTO() {
    }

    public CalendarPTDisplayDTO(Long id, String name, String slotName) {
        this.id = id;
        this.name = name;
        this.slotName = slotName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalendarPTDisplayDTO that = (CalendarPTDisplayDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(slotName, that.slotName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, slotName);
    }

    @Override
    public String toString() {
        return "CalendarPTDisplayDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", slotName='" + slotName + '\'' +
            '}';
    }
}
