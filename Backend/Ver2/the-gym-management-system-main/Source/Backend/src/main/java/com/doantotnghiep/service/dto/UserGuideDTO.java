package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.doantotnghiep.domain.UserGuide} entity.
 */
public class UserGuideDTO implements Serializable {

    private Long id;

    private String title;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserGuideDTO)) {
            return false;
        }

        UserGuideDTO userGuideDTO = (UserGuideDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, userGuideDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserGuideDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", content='" + getContent() + "'" +
            "}";
    }
}
