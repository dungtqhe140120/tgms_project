package com.doantotnghiep.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A PersonalTrainer.
 */
@Entity
@Table(name = "personal_trainer")
public class PersonalTrainer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "experience")
    private String experience;

    @Column(name = "short_description")
    private String short_description;

    @Column(name = "created_date")
    private LocalDate created_date;

    @OneToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private Profile profile;

    @OneToMany(mappedBy = "personalTrainer",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "personalTrainer"}, allowSetters = true)
    private Set<TraineeAndPT> traineeAndPTS = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Long getId() {
        return this.id;
    }

    public PersonalTrainer id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExperience() {
        return this.experience;
    }

    public PersonalTrainer experience(String experience) {
        this.setExperience(experience);
        return this;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getShort_description() {
        return this.short_description;
    }

    public PersonalTrainer short_description(String short_description) {
        this.setShort_description(short_description);
        return this;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public Set<TraineeAndPT> getTrainerAndPT() {
        return this.traineeAndPTS;
    }

    public void setTraineeAndPTS(Set<TraineeAndPT> traineeAndPTS) {
        if (this.traineeAndPTS != null) {
            this.traineeAndPTS.forEach(i -> i.setPersonalTrainer(null));
        }
        if (traineeAndPTS != null) {
            traineeAndPTS.forEach(i -> i.setPersonalTrainer(this));
        }
        this.traineeAndPTS = traineeAndPTS;
    }

    public PersonalTrainer traineeAndPT(Set<TraineeAndPT> traineeAndPTS) {
        this.setTraineeAndPTS(traineeAndPTS);
        return this;
    }

    public PersonalTrainer addTraineeAndPT(TraineeAndPT traineeAndPT) {
        this.traineeAndPTS.add(traineeAndPT);
        traineeAndPT.setPersonalTrainer(this);
        return this;
    }

    public PersonalTrainer removeTraineeAndPT(TraineeAndPT traineeAndPT) {
        this.traineeAndPTS.remove(traineeAndPT);
        traineeAndPT.setPersonalTrainer(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonalTrainer)) {
            return false;
        }
        return id != null && id.equals(((PersonalTrainer) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PersonalTrainer{" +
            "id=" + getId() +
            ", experience='" + getExperience() + "'" +
            ", short_description='" + getShort_description() + "'" +
            "}";
    }
}
