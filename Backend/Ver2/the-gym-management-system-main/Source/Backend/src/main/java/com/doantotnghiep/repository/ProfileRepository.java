package com.doantotnghiep.repository;

import java.util.List;
import java.util.Set;

import com.doantotnghiep.domain.Authority;
import com.doantotnghiep.domain.Profile;
import com.doantotnghiep.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Profile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {


    @Query(value = "select * from jhi_profile where user_id=?1", nativeQuery = true)
    Profile getProfileByUserID(Long user_id);

    @Query(value = "select * from jhi_user a where a.email = ?1", nativeQuery = true)
    User findUserEmail(String email);

    Profile getProfilesById(Long id);

    @Query("SELECT p.fcm FROM Profile p JOIN User u ON p.user.id = u.id where u.authorities in :authorities")
    List<String> getFCM(@Param("authorities") Set<Authority> authorities);

    List<Profile> findByUser_Authorities_NameIn(List<String> names);
}
