package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.Employee;
import com.doantotnghiep.domain.Profile;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.repository.EmployeeRepository;
import com.doantotnghiep.service.EmployeeService;
import com.doantotnghiep.service.ProfileService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.EmployeeDTO;
import com.doantotnghiep.service.dto.form.UpdateFcmForm;
import com.doantotnghiep.service.mapper.EmployeeMapper;

import java.util.List;
import java.util.Optional;

import com.doantotnghiep.service.resources.EmployeeInfFullResources;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Employee}.
 */
@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final EmployeeMapper employeeMapper;

    private UserService userService;
    private final ProfileService profileService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserService getUserService() {
        return userService;
    }

    @Override
    public EmployeeDTO save(EmployeeDTO employeeDTO) {
        log.debug("Request to save EmployeeDTO : {}", employeeDTO);
        Employee employee = employeeMapper.toEntity(employeeDTO);
        employee = employeeRepository.save(employee);
        return employeeMapper.toDto(employee);
    }

    @Override
    public Optional<EmployeeDTO> partialUpdate(EmployeeDTO employeeDTO) {
        log.debug("Request to partially update Employee : {}", employeeDTO);

        return employeeRepository
            .findById(employeeDTO.getId())
            .map(existingEmployee -> {
                employeeMapper.partialUpdate(existingEmployee, employeeDTO);

                return existingEmployee;
            })
            .map(employeeRepository::save)
            .map(employeeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EmployeeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Employees");
        return employeeRepository.findAll(pageable).map(employeeMapper::toDto);
    }

    @Override
    public List<EmployeeInfFullResources> findAllInf() {
        return employeeRepository.getAllInfEmployees();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EmployeeDTO> findOne(Long id) {
        log.debug("Request to get Employee : {}", id);
        return employeeRepository.findById(id).map(employeeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Employee : {}", id);
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        log.debug("Request to delete Employee : {}", employee);
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    public void updateFcm(UpdateFcmForm form) {
        User user = userService.getCurrentUserLogin();
        Profile profileByUserID = profileService.findProfileByUserID(user.getId());
        profileByUserID.setFcm(form.fcm());
    }
}
