package com.doantotnghiep.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "trainee_and_pt_draft")
public class TraineeAndPTDraft implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "trainee_and_pt_id")
    private Long traineeAndPtId;

    @Column(name = "expired_date")
    private LocalDate expiredDate;

    @NotNull
    @Column(name = "registered_date", nullable = false)
    private LocalDate registeredDate;

    @Column(name = "price")
    private Long price;

    @Min(value = 0)
    @Max(value = 2)
    @Column(name = "confirmed")
    private Integer confirmStatus;

    @Column(name = "confirmed_date")
    private LocalDate confirmDate;

    @Column(name = "confirmed_by")
    private String confirmBy;

    @Column(name = "trainee_id")
    private Long traineeId;

    @Column(name = "pt_id")
    private Long ptId;

    @Column(name = "slot_time_id")
    private Long slotId;

    @Column(name = "reason_change")
    private String reasonChange;

    @Column(name = "transaction_id")
    private Long transactionId;
}
