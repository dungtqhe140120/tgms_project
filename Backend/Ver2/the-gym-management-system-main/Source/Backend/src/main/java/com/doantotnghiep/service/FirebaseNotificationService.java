package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.firebase.Notice;
import com.google.firebase.messaging.BatchResponse;

public interface FirebaseNotificationService {
    BatchResponse sendNotification(Notice notice);
}
