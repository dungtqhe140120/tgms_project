package com.doantotnghiep.repository;

import java.time.Instant;

import com.doantotnghiep.domain.MomoTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 */
public interface MomoTransactionRepository extends JpaRepository<MomoTransaction, Long> {
    @Query("UPDATE MomoTransaction " +
        "SET status= 'EXPIRED', message='Auto expired' " +
        "WHERE status IN (com.doantotnghiep.service.dto.type.MomoTransactionStatus.NEW, com.doantotnghiep.service.dto.type.MomoTransactionStatus.IN_PROGRESS) " +
        "AND createdDate <= :minusMinutes")
    @Modifying
    Integer updateStatusExpire(@Param("minusMinutes") Instant instant);
}
