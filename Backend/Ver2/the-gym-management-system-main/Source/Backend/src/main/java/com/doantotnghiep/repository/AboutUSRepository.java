package com.doantotnghiep.repository;

import com.doantotnghiep.domain.AboutUS;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AboutUS entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AboutUSRepository extends JpaRepository<AboutUS, Long> {}
