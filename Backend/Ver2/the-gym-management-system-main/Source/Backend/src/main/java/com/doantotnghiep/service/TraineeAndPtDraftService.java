package com.doantotnghiep.service;

import com.doantotnghiep.service.resources.TraineeAndPtDraftResource;

public interface TraineeAndPtDraftService {
    TraineeAndPtDraftResource save(TraineeAndPtDraftResource request);
}
