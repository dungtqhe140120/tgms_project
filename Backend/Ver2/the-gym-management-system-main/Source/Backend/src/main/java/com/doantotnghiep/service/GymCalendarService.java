package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.GymCalendarDTO;
import com.doantotnghiep.service.dto.requestDTO.GymCalendarConfirmRequest;
import com.doantotnghiep.service.dto.requestDTO.GymCalendarFilter;
import com.doantotnghiep.service.resources.GymCalendarResource;
import org.springframework.data.domain.Pageable;

public interface GymCalendarService {
    GymCalendarDTO save(GymCalendarDTO request);
    GymCalendarResource getCalendarTrainee(Pageable pageable, GymCalendarFilter filter);

    GymCalendarDTO confirm(GymCalendarConfirmRequest request, Long id);
}
