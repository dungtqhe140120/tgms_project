package com.doantotnghiep.service.resources;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TraineeAndPtDraftResource {
    private Long id;
    private String type;
    private Long traineeAndPtId;
    private LocalDate expiredDate;
    private LocalDate registeredDate;
    private Long price;
    private Integer confirmStatus;
    private LocalDate confirmDate;
    private String confirmBy;
    private Long traineeId;
    private Long ptId;
    private Long slotId;
    private String reasonChange;
    private Long transactionId;
}
