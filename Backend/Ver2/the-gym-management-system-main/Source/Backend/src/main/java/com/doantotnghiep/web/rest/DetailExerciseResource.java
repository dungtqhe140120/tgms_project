package com.doantotnghiep.web.rest;

import com.doantotnghiep.domain.DetailExercise;
import com.doantotnghiep.repository.DetailExerciseRepository;
import com.doantotnghiep.service.DetailExerciseService;
import com.doantotnghiep.service.dto.DetailExerciseDTO;
import com.doantotnghiep.service.dto.display.DetailContentDisplayDTO;
import com.doantotnghiep.service.dto.display.DetailExerciseDisplayDTO;
import com.doantotnghiep.service.dto.display.DetailExerciseShowDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqDetailExerciseDTO;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.DetailExercise}.
 */
@RestController
@RequestMapping("/api")
public class DetailExerciseResource {

    private final Logger log = LoggerFactory.getLogger(DetailExerciseResource.class);

    private static final String ENTITY_NAME = "detailExercise";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailExerciseService detailExerciseService;

    private final DetailExerciseRepository detailExerciseRepository;

    public DetailExerciseResource(DetailExerciseService detailExerciseService, DetailExerciseRepository detailExerciseRepository) {
        this.detailExerciseService = detailExerciseService;
        this.detailExerciseRepository = detailExerciseRepository;
    }

    /**
     * {@code POST  /detail-exercises} : Create a new detailExercise.
     *
     * @param detailExercise the detailExerciseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new detailExerciseDTO, or with status {@code 400 (Bad Request)} if the detailExercise has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/detail-exercises")
    public ResponseEntity<DetailExercise> createDetailExercise(@RequestBody DetailExercise detailExercise)
        throws URISyntaxException {
        log.debug("REST request to save DetailExercise : {}", detailExercise);
        if (detailExercise.getId() != null) {
            throw new BadRequestAlertException("A new detailExercise cannot already have an ID", ENTITY_NAME, "idexists");
        }
        detailExercise.setCreated_date(LocalDate.now());

        DetailExercise result = detailExerciseService.saveDetailExercise(detailExercise);
        return ResponseEntity
            .created(new URI("/api/detail-exercises/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * {@code PUT  /detail-exercises/:id} : Updates an existing detailExercise.
     *
     * @param id the id of the detailExerciseDTO to save.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailExerciseDTO,
     * or with status {@code 400 (Bad Request)} if the detailExerciseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the detailExerciseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/detail-exercises/{id}")
    public ResponseEntity<DetailExercise> updateDetailExercise(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ReqDetailExerciseDTO reqDetailExerciseDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DetailExercise : {}, {}", id, reqDetailExerciseDTO);

        if (!detailExerciseRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DetailExercise detailExercise= detailExerciseService.getDetailExerciseByID(id);

        detailExercise.setTitle(reqDetailExerciseDTO.getTitle());
        detailExercise.setImage(reqDetailExerciseDTO.getImage());

        DetailExercise result = detailExerciseService.saveDetailExercise(detailExercise);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, detailExercise.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /detail-exercises/:id} : Partial updates given fields of an existing detailExercise, field will ignore if it is null
     *
     * @param id the id of the detailExerciseDTO to save.
     * @param detailExerciseDTO the detailExerciseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailExerciseDTO,
     * or with status {@code 400 (Bad Request)} if the detailExerciseDTO is not valid,
     * or with status {@code 404 (Not Found)} if the detailExerciseDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the detailExerciseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/detail-exercises/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DetailExerciseDTO> partialUpdateDetailExercise(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DetailExerciseDTO detailExerciseDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DetailExercise partially : {}, {}", id, detailExerciseDTO);
        if (detailExerciseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, detailExerciseDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!detailExerciseRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DetailExerciseDTO> result = detailExerciseService.partialUpdate(detailExerciseDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, detailExerciseDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /detail-exercises} : get all the detailExercises.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailExercises in body.
     */
    @GetMapping("/detail-exercises")
    public ResponseEntity<List<DetailExerciseDTO>> getAllDetailExercises(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of DetailExercises");
        Page<DetailExerciseDTO> page = detailExerciseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /detail-exercises/:id} : get the "id" detailExercise.
     *
     * @param id the id of the detailExerciseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailExerciseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/detail-exercises/{id}")
    public ResponseEntity<DetailExerciseDTO> getDetailExercise(@PathVariable Long id) {
        log.debug("REST request to get DetailExercise : {}", id);
        Optional<DetailExerciseDTO> detailExerciseDTO = detailExerciseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailExerciseDTO);
    }

    /**
     * {@code DELETE  /detail-exercises/:id} : delete the "id" detailExercise.
     *
     * @param id the id of the detailExerciseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/detail-exercises/{id}")
    public ResponseEntity<Void> deleteDetailExercise(@PathVariable Long id) {
        log.debug("REST request to delete DetailExercise : {}", id);
        detailExerciseService.deleteDetailTraineeByID(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping("/detail-exercises/viewDetailExercise/{id}")
    public ResponseEntity<Page<DetailExerciseDisplayDTO>> getDetailExerciseDisplayDTO(@org.springdoc.api.annotations.ParameterObject Pageable pageable, @PathVariable("id") Long id) {
        log.debug("REST request to get a page of getDetailExerciseDisplayDTO");
        Page<DetailExerciseDisplayDTO> page=detailExerciseService.getDetailExerciseDisplayDTO(pageable, id);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/detail-exercises/getDetailExerciseShowDTOByID/{id}")
    public ResponseEntity<Page<DetailExerciseShowDTO>> getDetailExerciseShowDTO(@org.springdoc.api.annotations.ParameterObject Pageable pageable, @PathVariable("id") Long id) {
        log.debug("REST request to get a page of getDetailExerciseDisplayDTO");
        Page<DetailExerciseShowDTO> page=detailExerciseService.getDetailExerciseShowDTO(pageable, id);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @PutMapping("/detail-exercises/update/{id}")
    public ResponseEntity<DetailExercise> updateDetailExerciseDisplay(
        @PathVariable(value = "id", required = true) final Long id,
        @RequestBody DetailContentDisplayDTO detailContentDisplayDTO
    ) throws URISyntaxException {
        log.debug("REST request to update updateDetailExerciseDisplay : {}", id);

        if (!detailExerciseService.countDetailExerciseByID(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DetailExerciseDTO> detailExerciseDTO=detailExerciseService.findOne(id);
        DetailExerciseDTO detail=detailExerciseDTO.get();
        detail.setContent(detailContentDisplayDTO.getContent());
        DetailExerciseDTO update=detailExerciseService.update(detail);
        log.debug("content: "+detail.getContent());
        DetailExercise getID=detailExerciseService.getDetailExerciseByID(id);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .body(getID);
    }

    @GetMapping("/detail-exercises/getContent/{id}")
    public ResponseEntity<Object> getDetailExerciseContent(@PathVariable Long id) {
        log.debug("REST request to get getDetailExerciseContent : {}", id);
       DetailExercise detailExercise = detailExerciseService.getDetailExerciseByID(id);
       DetailContentDisplayDTO detailContentDisplayDTO=detailExerciseService.getDetailContentByID(detailExercise.getId());
       System.out.println(detailExercise.getContent());

        return ResponseEntity.ok().body(detailContentDisplayDTO);
    }
}
