package com.doantotnghiep.momo.retrofit;

import com.doantotnghiep.momo.config.MomoProperties;
import com.doantotnghiep.momo.exception.BodyError;
import com.doantotnghiep.momo.exception.MomoException;
import com.doantotnghiep.momo.model.TransactionConfirmParam;
import com.doantotnghiep.momo.model.TransactionConfirmResponse;
import com.doantotnghiep.momo.model.TransactionCreateParam;
import com.doantotnghiep.momo.model.TransactionCreateResponse;
import com.doantotnghiep.momo.model.form.FormSignatureConfirm;
import com.doantotnghiep.momo.model.form.FormSignatureCreate;
import com.doantotnghiep.momo.utils.Encoder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ResponseBody;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;

/**
 *
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MomoClient {
    public static final int MAINTENANCE_CODE = 503;
    private final MomoAPI momoAPI;
    private final MomoProperties momoProperties;
    private final Converter<ResponseBody, BodyError> converterMomoError;

    public TransactionCreateResponse sendRequestCreate(TransactionCreateParam transactionCreateParam)
        throws Exception {
        transactionCreateParam.setSignature(Encoder.generateSignature(transactionCreateParam, momoProperties, FormSignatureCreate.class));
        Call<TransactionCreateResponse> call = momoAPI.createTransaction(transactionCreateParam);
        Response<TransactionCreateResponse> execute = call.execute(); // Synchronous
        if (execute.isSuccessful()) {
            return execute.body();
        }
        if (execute.code() == MAINTENANCE_CODE) {
            throw new MomoException("Momo is overloaded or down for maintenance!");
        }
        BodyError convert = converterMomoError.convert(execute.errorBody());
        throw new MomoException(convert);
    }

    public TransactionConfirmResponse sendConfirmTransaction(TransactionConfirmParam transactionConfirmParam) throws Exception {
        transactionConfirmParam.setSignature(Encoder.generateSignature(transactionConfirmParam, momoProperties, FormSignatureConfirm.class));
        Call<TransactionConfirmResponse> call = momoAPI.confirmTransaction(transactionConfirmParam);
        Response<TransactionConfirmResponse> execute = call.execute(); // Synchronous
        if (execute.isSuccessful()) {
            return execute.body();
        }
        if (execute.code() == MAINTENANCE_CODE) {
            throw new MomoException("Momo is overloaded or down for maintenance!");
        }
        BodyError convert = converterMomoError.convert(execute.errorBody());
        throw new MomoException(convert);
    }
}
