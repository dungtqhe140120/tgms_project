package com.doantotnghiep.momo.model;

import lombok.Data;

/**
 *
 */
@Data
public class TransactionConfirmResponse {
    private String partnerCode;
    private String orderId;
    private String requestId;
    private String extraData;
    private Long amount;
    private Long transId;
    private String payType;
    private Integer resultCode;
    private Object[] refundTrans;
    private String message;
    private Long responseTime;
}
