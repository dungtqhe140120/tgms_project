package com.doantotnghiep.momo.model.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FormSignatureConfirm extends FormSignature {
    private String orderId;
    private String partnerCode;
    private String requestId;
}
