package com.doantotnghiep.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GymCalendarDTO {
    private Long id;
    private LocalDate calendarDate;
    private Long categoryId;
    private String categoryName;
    private Long createdBy;
    private Long traineeId;
    private String time;
    private Integer traineeConfirm;
    private Integer ptConfirm;
}
