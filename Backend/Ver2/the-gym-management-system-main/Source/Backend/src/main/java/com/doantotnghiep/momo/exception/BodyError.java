package com.doantotnghiep.momo.exception;

import lombok.Data;

/**
 *
 */
@Data
public class BodyError {
    private Long responseTime;
    private String message;
    private Integer resultCode;
    private Object subErrors;
}
