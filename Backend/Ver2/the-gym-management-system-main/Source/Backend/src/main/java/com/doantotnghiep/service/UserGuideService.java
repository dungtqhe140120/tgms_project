package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.UserGuideDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.UserGuide}.
 */
public interface UserGuideService {
    /**
     * Save a userGuide.
     *
     * @param userGuideDTO the entity to save.
     * @return the persisted entity.
     */
    UserGuideDTO save(UserGuideDTO userGuideDTO);

    /**
     * Updates a userGuide.
     *
     * @param userGuideDTO the entity to update.
     * @return the persisted entity.
     */
    UserGuideDTO update(UserGuideDTO userGuideDTO);

    /**
     * Partially updates a userGuide.
     *
     * @param userGuideDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<UserGuideDTO> partialUpdate(UserGuideDTO userGuideDTO);

    /**
     * Get all the userGuides.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserGuideDTO> findAll(Pageable pageable);

    /**
     * Get the "id" userGuide.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserGuideDTO> findOne(Long id);

    /**
     * Delete the "id" userGuide.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
