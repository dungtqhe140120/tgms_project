package com.doantotnghiep.service.resources;

import java.time.LocalDate;

public class TraineeRatePTResource {
    private String rateBy;
    private Float starRate;
    private String comment;
    private LocalDate createAt;

    public TraineeRatePTResource() {
    }

    public TraineeRatePTResource(String rateBy, Float starRate, String comment, LocalDate createAt) {
        this.rateBy = rateBy;
        this.starRate = starRate;
        this.comment = comment;
        this.createAt = createAt;
    }

    public TraineeRatePTResource(Float starRate, String comment, LocalDate createAt) {
        this.starRate = starRate;
        this.comment = comment;
        this.createAt = createAt;
    }

    public String getRateBy() {
        return rateBy;
    }

    public void setRateBy(String rateBy) {
        this.rateBy = rateBy;
    }

    public Float getStarRate() {
        return starRate;
    }

    public void setStarRate(Float starRate) {
        this.starRate = starRate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDate getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDate createAt) {
        this.createAt = createAt;
    }
}
