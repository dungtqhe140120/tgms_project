package com.doantotnghiep.web.rest;

import com.doantotnghiep.domain.Profile;
import com.doantotnghiep.domain.Trainee;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.repository.BMIRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.BMIService;
import com.doantotnghiep.service.ProfileService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.BMIDTO;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.BMI}.
 */
@RestController
@RequestMapping("/api")
public class BMIResource {

    private final Logger log = LoggerFactory.getLogger(BMIResource.class);

    private static final String ENTITY_NAME = "bMI";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BMIService bMIService;

    private final BMIRepository bMIRepository;
    private final UserService userService;
    private final TraineeRepository traineeRepository;

    private final ProfileService profileService;

    public BMIResource(BMIService bMIService, BMIRepository bMIRepository, UserService userService, TraineeRepository traineeRepository, ProfileService profileService) {
        this.bMIService = bMIService;
        this.bMIRepository = bMIRepository;
        this.userService = userService;
        this.traineeRepository = traineeRepository;
        this.profileService = profileService;
    }

    /**
     * {@code POST  /bmis} : Create a new bMI.
     *
     * @param bMIDTO the bMIDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bMIDTO, or with status {@code 400 (Bad Request)} if the bMI has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bmis")
    public ResponseEntity<BMIDTO> createBMI(@Valid @RequestBody BMIDTO bMIDTO) throws URISyntaxException {
        log.debug("REST request to save BMI : {}", bMIDTO);
        if (bMIDTO.getId() != null) {
            throw new BadRequestAlertException("A new bMI cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Long userId = userService.getCurrentUserLogin().getId();
        Long traineeId = traineeRepository.getTraineeId(userId);
        if (traineeId == null) {
            throw new BadRequestAlertException("Trainee not exit", ENTITY_NAME, "invalid");
        }
        Trainee trainee = new Trainee();
        trainee.setId(traineeId);
        bMIDTO.setTrainee(trainee);
        BMIDTO result = bMIService.save(bMIDTO);
        User user= userService.getCurrentUserLogin();
        Profile profile= profileService.findProfileByUserID(userId);
        profile.setFirst_login(0);
        profile=profileService.saveProfile(profile);
        return ResponseEntity
            .created(new URI("/api/bmis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bmis/:id} : Updates an existing bMI.
     *
     * @param id the id of the bMIDTO to save.
     * @param bMIDTO the bMIDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bMIDTO,
     * or with status {@code 400 (Bad Request)} if the bMIDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bMIDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bmis/{id}")
    public ResponseEntity<BMIDTO> updateBMI(@PathVariable(value = "id", required = false) final Long id, @Valid @RequestBody BMIDTO bMIDTO)
        throws URISyntaxException {
        log.debug("REST request to update BMI : {}, {}", id, bMIDTO);
        if (bMIDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, bMIDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!bMIRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        BMIDTO result = bMIService.update(bMIDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bMIDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /bmis/:id} : Partial updates given fields of an existing bMI, field will ignore if it is null
     *
     * @param id the id of the bMIDTO to save.
     * @param bMIDTO the bMIDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bMIDTO,
     * or with status {@code 400 (Bad Request)} if the bMIDTO is not valid,
     * or with status {@code 404 (Not Found)} if the bMIDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the bMIDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/bmis/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<BMIDTO> partialUpdateBMI(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody BMIDTO bMIDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update BMI partially : {}, {}", id, bMIDTO);
        if (bMIDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, bMIDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!bMIRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<BMIDTO> result = bMIService.partialUpdate(bMIDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bMIDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /bmis} : get all the bMIS.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bMIS in body.
     */
    @GetMapping("/bmis")
    public ResponseEntity<List<BMIDTO>> getAllBMIS(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of BMIS");
        Page<BMIDTO> page = bMIService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /bmis/:id} : get the "id" bMI.
     *
     * @param id the id of the bMIDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bMIDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bmis/{id}")
    public ResponseEntity<BMIDTO> getBMI(@PathVariable Long id) {
        log.debug("REST request to get BMI : {}", id);
        Optional<BMIDTO> bMIDTO = bMIService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bMIDTO);
    }

    /**
     * {@code DELETE  /bmis/:id} : delete the "id" bMI.
     *
     * @param id the id of the bMIDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bmis/{id}")
    public ResponseEntity<Void> deleteBMI(@PathVariable Long id) {
        log.debug("REST request to delete BMI : {}", id);
        bMIService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
