package com.doantotnghiep.momo.model.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FormSignatureCreate extends FormSignature {
    private Integer amount;
    private String extraData;
    private String ipnUrl;
    private String orderId;
    private String orderInfo;
    private String partnerCode;
    private String redirectUrl;
    private String requestId;
    private String requestType;
}
