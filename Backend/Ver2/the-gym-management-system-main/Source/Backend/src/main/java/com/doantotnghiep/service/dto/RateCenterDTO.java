package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.doantotnghiep.domain.RateCenter} entity.
 */
public class RateCenterDTO implements Serializable {

    private Long id;

    private String comment;

    private Float rate_star;

    private LocalDate created_at;

    private Long userId;

    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Float getRate_star() {
        return rate_star;
    }

    public void setRate_star(Float rate_star) {
        this.rate_star = rate_star;
    }

    public LocalDate getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDate created_at) {
        this.created_at = created_at;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RateCenterDTO)) {
            return false;
        }

        RateCenterDTO rateCenterDTO = (RateCenterDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, rateCenterDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RateCenterDTO{" +
            "id=" + getId() +
            ", comment='" + getComment() + "'" +
            ", rate_star=" + getRate_star() +
            ", created_at='" + getCreated_at() + "'" +
            "}";
    }
}
