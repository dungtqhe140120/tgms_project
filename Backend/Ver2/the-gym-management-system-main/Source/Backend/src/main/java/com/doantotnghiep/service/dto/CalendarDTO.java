package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.doantotnghiep.domain.Calendar} entity.
 */
public class CalendarDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate calendar_Date;

    @NotNull
    private String title;

    private String content;

    private LocalDate calendar_time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCalendar_Date() {
        return calendar_Date;
    }

    public void setCalendar_Date(LocalDate calendar_Date) {
        this.calendar_Date = calendar_Date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getCalendar_time() {
        return calendar_time;
    }

    public void setCalendar_time(LocalDate calendar_time) {
        this.calendar_time = calendar_time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CalendarDTO)) {
            return false;
        }

        CalendarDTO calendarDTO = (CalendarDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, calendarDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CalendarDTO{" +
            "id=" + getId() +
            ", calendar_Date='" + getCalendar_Date() + "'" +
            ", title='" + getTitle() + "'" +
            ", content='" + getContent() + "'" +
            ", calendar_time='" + getCalendar_time() + "'" +
            "}";
    }
}
