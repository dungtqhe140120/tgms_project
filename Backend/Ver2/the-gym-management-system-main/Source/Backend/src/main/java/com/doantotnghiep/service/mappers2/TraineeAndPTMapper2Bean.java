package com.doantotnghiep.service.mappers2;

import java.util.Objects;
import java.util.Optional;

import com.doantotnghiep.domain.Employee;
import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.domain.Trainee;
import com.doantotnghiep.repository.EmployeeRepository;
import com.doantotnghiep.repository.PersonalTrainerRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import com.doantotnghiep.service.resources.TraineeAndPTResources;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@Mapper(componentModel = "spring")
public abstract class TraineeAndPTMapper2Bean {

    @Autowired
    protected TraineeRepository traineeRepository;

    @Autowired
    protected PersonalTrainerRepository personalTrainerRepository;

    @Autowired
    protected EmployeeRepository employeeRepository;

    @Mapping(source = "traineeAndPtId", target = "id")
    @Mapping(source = "type", target = "packageRegister")
    @Mapping(source = "expiredDate", target = "expireDate")
    @Mapping(source = "registeredDate", target = "registerDate")
    @Mapping(source = "confirmDate", target = "confirmAt")
    @Mapping(source = "confirmBy", target = "confirmBy", qualifiedByName = "convertConfirmByName")
    @Mapping(source = "slotId", target = "slot")
    @Mapping(source = "traineeId", target = "traineeName", qualifiedByName = "convertToTraineeName")
    @Mapping(source = "ptId", target = "ptName", qualifiedByName = "convertToPtName")
    public abstract TraineeAndPTResources toResource(TraineeAndPTDTO dto);

    @Named("convertToTraineeName")
    String convertToTraineeName(Long traineeId) {
        Optional<Trainee> traineeByID = traineeRepository.findTraineeJoinFetch(traineeId);
        if (traineeByID.isPresent()) {
            return traineeByID.get().getProfile().getFullname();
        }
        return "";
    }

    @Named("convertToPtName")
    String convertToPtName(Long ptId) {
        Optional<PersonalTrainer> personalTrainerJoinFetch = personalTrainerRepository.findPersonalTrainerJoinFetch(ptId);
        if (personalTrainerJoinFetch.isPresent()) {
            return personalTrainerJoinFetch.get().getProfile().getFullname();
        }
        return "";
    }

    @Named("convertConfirmByName")
    @Transactional
    String convertConfirmByName(String confirmBy) {
        if (Objects.isNull(confirmBy)) {
            return "";
        }
        if ("1".equals(confirmBy)) {
            return "admin";
        }
        Optional<Employee> byId = employeeRepository.findByIdJoinFetch(Long.valueOf(confirmBy));
        if (byId.isPresent()) {
            return byId.get().getProfile().getFullname();
        }
        return "Không rõ";
    }
}
