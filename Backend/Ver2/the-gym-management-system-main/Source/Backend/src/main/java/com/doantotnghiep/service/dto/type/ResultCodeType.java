package com.doantotnghiep.service.dto.type;

/**
 *
 */
public enum ResultCodeType {
    SUCCESS("success"),
    ERROR("error");

    final String value;

    ResultCodeType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
