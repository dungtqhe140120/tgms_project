package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.ExerciseDTO;
import java.util.Optional;

import com.doantotnghiep.service.resources.ExercisesResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.Exercise}.
 */
public interface ExerciseService {
    /**
     * Save a exercise.
     *
     * @param exerciseDTO the entity to save.
     * @return the persisted entity.
     */
    ExerciseDTO save(ExerciseDTO exerciseDTO);

    /**
     * Partially updates a exercise.
     *
     * @param exerciseDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ExerciseDTO> partialUpdate(ExerciseDTO exerciseDTO);

    /**
     * Get all the exercises.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExerciseDTO> findAll(Pageable pageable);

    /**
     * Get all the exercises center.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExercisesResource> findExercisesCenter(Pageable pageable);

    /**
     * Get all the mine exercises.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExercisesResource> findMineExercises(Pageable pageable, Long userId);

    /**
     * Get the "id" exercise.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExerciseDTO> findOne(Long id);

    /**
     * Delete the "id" exercise.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


}
