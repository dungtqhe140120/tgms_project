package com.doantotnghiep.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RateCenter.
 */
@Entity
@Table(name = "rate_center")
public class RateCenter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "jhi_comment")
    private String comment;

    @Column(name = "rate_star")
    private Float rate_star;

    @Column(name = "created_at")
    private LocalDate created_at;
    @Column(name = "user_id")
    private Long userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RateCenter id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return this.comment;
    }

    public RateCenter comment(String comment) {
        this.setComment(comment);
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Float getRate_star() {
        return this.rate_star;
    }

    public RateCenter rate_star(Float rate_star) {
        this.setRate_star(rate_star);
        return this;
    }

    public void setRate_star(Float rate_star) {
        this.rate_star = rate_star;
    }

    public LocalDate getCreated_at() {
        return this.created_at;
    }

    public RateCenter created_at(LocalDate created_at) {
        this.setCreated_at(created_at);
        return this;
    }

    public void setCreated_at(LocalDate created_at) {
        this.created_at = created_at;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RateCenter)) {
            return false;
        }
        return id != null && id.equals(((RateCenter) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RateCenter{" +
            "id=" + getId() +
            ", comment='" + getComment() + "'" +
            ", rate_star=" + getRate_star() +
            ", created_at='" + getCreated_at() + "'" +
            "}";
    }
}
