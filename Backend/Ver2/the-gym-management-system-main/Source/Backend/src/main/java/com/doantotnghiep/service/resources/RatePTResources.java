package com.doantotnghiep.service.resources;

import org.springframework.data.domain.Page;

import java.time.LocalDate;

public class RatePTResources {
    private String namePT;
    private String phone;
    private Float startRate;
    private Integer gender;
    private LocalDate birthDay;
    private String avatarPT;

    private Page<TraineeRatePTResource> traineeRatePTResource;

    public RatePTResources(String namePT, String phone, Integer gender, LocalDate birthDay, String avatarPT) {
        this.namePT = namePT;
        this.phone = phone;
        this.gender = gender;
        this.birthDay = birthDay;
        this.avatarPT = avatarPT;
    }

    public RatePTResources() {
    }

    public Page<TraineeRatePTResource> getTraineeRatePTResource() {
        return traineeRatePTResource;
    }

    public void setTraineeRatePTResource(Page<TraineeRatePTResource> traineeRatePTResource) {
        this.traineeRatePTResource = traineeRatePTResource;
    }

    public String getAvatarPT() {
        return avatarPT;
    }

    public void setAvatarPT(String avatarPT) {
        this.avatarPT = avatarPT;
    }

    public String getNamePT() {
        return namePT;
    }

    public void setNamePT(String namePT) {
        this.namePT = namePT;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Float getStartRate() {
        return startRate;
    }

    public void setStartRate(Float startRate) {
        this.startRate = startRate;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }
}
