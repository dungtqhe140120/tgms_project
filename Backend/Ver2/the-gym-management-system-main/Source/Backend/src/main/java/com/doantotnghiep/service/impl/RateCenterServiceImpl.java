package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.RateCenter;
import com.doantotnghiep.repository.RateCenterRepository;
import com.doantotnghiep.service.RateCenterService;
import com.doantotnghiep.service.dto.RateCenterDTO;
import com.doantotnghiep.service.mapper.RateCenterMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link RateCenter}.
 */
@Service
@Transactional
public class RateCenterServiceImpl implements RateCenterService {

    private final Logger log = LoggerFactory.getLogger(RateCenterServiceImpl.class);

    private final RateCenterRepository rateCenterRepository;

    private final RateCenterMapper rateCenterMapper;

    public RateCenterServiceImpl(RateCenterRepository rateCenterRepository, RateCenterMapper rateCenterMapper) {
        this.rateCenterRepository = rateCenterRepository;
        this.rateCenterMapper = rateCenterMapper;
    }

    @Override
    public RateCenterDTO save(RateCenterDTO rateCenterDTO) {
        log.debug("Request to save RateCenter : {}", rateCenterDTO);
        RateCenter rateCenter = rateCenterMapper.toEntity(rateCenterDTO);
        rateCenter = rateCenterRepository.save(rateCenter);
        return rateCenterMapper.toDto(rateCenter);
    }

    @Override
    public RateCenterDTO update(RateCenterDTO rateCenterDTO) {
        log.debug("Request to save RateCenter : {}", rateCenterDTO);
        RateCenter rateCenter = rateCenterMapper.toEntity(rateCenterDTO);
        rateCenter = rateCenterRepository.save(rateCenter);
        return rateCenterMapper.toDto(rateCenter);
    }

    @Override
    public Optional<RateCenterDTO> partialUpdate(RateCenterDTO rateCenterDTO) {
        log.debug("Request to partially update RateCenter : {}", rateCenterDTO);

        return rateCenterRepository
            .findById(rateCenterDTO.getId())
            .map(existingRateCenter -> {
                rateCenterMapper.partialUpdate(existingRateCenter, rateCenterDTO);

                return existingRateCenter;
            })
            .map(rateCenterRepository::save)
            .map(rateCenterMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RateCenterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RateCenters");
        return rateCenterRepository.findAll(pageable).map(rateCenterMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RateCenterDTO> findOne(Long id) {
        log.debug("Request to get RateCenter : {}", id);
        return rateCenterRepository.findById(id).map(rateCenterMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RateCenter : {}", id);
        rateCenterRepository.deleteById(id);
    }
}
