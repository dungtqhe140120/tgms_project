package com.doantotnghiep.web.rest;

import com.doantotnghiep.domain.Category;
import com.doantotnghiep.domain.Trainee;
import com.doantotnghiep.domain.TraineeCategory;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.repository.TraineeCategoryRepository;
import com.doantotnghiep.service.CategoryService;
import com.doantotnghiep.service.TraineeCategoryService;
import com.doantotnghiep.service.TraineeService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.TraineeCategoryDTO;
import com.doantotnghiep.service.dto.display.CategoryTraineePTDisplayDTO;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.TraineeCategory}.
 */
@RestController
@RequestMapping("/api")
public class TraineeCategoryResource {

    private final Logger log = LoggerFactory.getLogger(TraineeCategoryResource.class);

    private static final String ENTITY_NAME = "traineeCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TraineeCategoryService traineeCategoryService;

    private final TraineeService traineeService;
    private final CategoryService categoryService;

    private final UserService userService;

    private final TraineeCategoryRepository traineeCategoryRepository;

    public TraineeCategoryResource(TraineeCategoryService traineeCategoryService, TraineeService traineeService, CategoryService categoryService, UserService userService, TraineeCategoryRepository traineeCategoryRepository) {
        this.traineeCategoryService = traineeCategoryService;
        this.traineeService = traineeService;
        this.categoryService = categoryService;
        this.userService = userService;
        this.traineeCategoryRepository = traineeCategoryRepository;
    }

    /**
     * {@code POST  /trainee-categories} : Create a new traineeCategory.
     *
     * @param traineeCategoryDTO the traineeCategoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new traineeCategoryDTO, or with status {@code 400 (Bad Request)} if the traineeCategory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/trainee-categories")
    public ResponseEntity<TraineeCategoryDTO> createTraineeCategory(@RequestBody TraineeCategoryDTO traineeCategoryDTO)
        throws URISyntaxException {
        log.debug("REST request to save TraineeCategory : {}", traineeCategoryDTO);
        if (traineeCategoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new traineeCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TraineeCategoryDTO result = traineeCategoryService.save(traineeCategoryDTO);
        return ResponseEntity
            .created(new URI("/api/trainee-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /trainee-categories/:id} : Updates an existing traineeCategory.
     *
     * @param id the id of the traineeCategoryDTO to save.
     * @param traineeCategoryDTO the traineeCategoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated traineeCategoryDTO,
     * or with status {@code 400 (Bad Request)} if the traineeCategoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the traineeCategoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/trainee-categories/{id}")
    public ResponseEntity<TraineeCategoryDTO> updateTraineeCategory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TraineeCategoryDTO traineeCategoryDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TraineeCategory : {}, {}", id, traineeCategoryDTO);
        if (traineeCategoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, traineeCategoryDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!traineeCategoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TraineeCategoryDTO result = traineeCategoryService.update(traineeCategoryDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeCategoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /trainee-categories/:id} : Partial updates given fields of an existing traineeCategory, field will ignore if it is null
     *
     * @param id the id of the traineeCategoryDTO to save.
     * @param traineeCategoryDTO the traineeCategoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated traineeCategoryDTO,
     * or with status {@code 400 (Bad Request)} if the traineeCategoryDTO is not valid,
     * or with status {@code 404 (Not Found)} if the traineeCategoryDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the traineeCategoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/trainee-categories/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TraineeCategoryDTO> partialUpdateTraineeCategory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TraineeCategoryDTO traineeCategoryDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update TraineeCategory partially : {}, {}", id, traineeCategoryDTO);
        if (traineeCategoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, traineeCategoryDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!traineeCategoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TraineeCategoryDTO> result = traineeCategoryService.partialUpdate(traineeCategoryDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeCategoryDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /trainee-categories} : get all the traineeCategories.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of traineeCategories in body.
     */
    @GetMapping("/trainee-categories")
    public ResponseEntity<List<TraineeCategoryDTO>> getAllTraineeCategories(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of TraineeCategories");
        Page<TraineeCategoryDTO> page = traineeCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /trainee-categories/:id} : get the "id" traineeCategory.
     *
     * @param id the id of the traineeCategoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the traineeCategoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/trainee-categories/{id}")
    public ResponseEntity<TraineeCategoryDTO> getTraineeCategory(@PathVariable Long id) {
        log.debug("REST request to get TraineeCategory : {}", id);
        Optional<TraineeCategoryDTO> traineeCategoryDTO = traineeCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(traineeCategoryDTO);
    }

    /**
     * {@code DELETE  /trainee-categories/:id} : delete the "id" traineeCategory.
     *
     * @param id the id of the traineeCategoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/trainee-categories/{id}")
    public ResponseEntity<Void> deleteTraineeCategory(@PathVariable Long id) {
        log.debug("REST request to delete TraineeCategory : {}", id);
        traineeCategoryService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping("/trainee-exercises/getExerciseTraineeByTrainee/{id}")
    public ResponseEntity<List<CategoryTraineePTDisplayDTO>> getCategoryTraineeByTrainee(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable, @PathVariable Long id
    ) {
        log.debug("REST request to get a page of getDetailExerciseTraineeByTrainee");
        Page<CategoryTraineePTDisplayDTO> page = traineeCategoryService.getCategoryTraineeByTrainee(pageable, id);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping("/trainee-categories/assignCategoryToTrainee")
    public ResponseEntity<TraineeCategory> assignTraineeCategory(
        @RequestParam(name = "trainee_id") Long trainee_id, @RequestParam (name = "category_id") Long category_id
    )throws URISyntaxException {
        log.debug("REST request to save assignTraineeCategory : {}", trainee_id);
        if (category_id == null || category_id==0) {
            throw new BadRequestAlertException("Please insert valid category_id", ENTITY_NAME, "idinvalid");
        }
        if (trainee_id == null || trainee_id==0) {
            throw new BadRequestAlertException("Please insert valid trainee_id", ENTITY_NAME, "idinvalid");
        }
        Trainee trainee =traineeService.findTraineeByID(trainee_id);
        Category category= categoryService.findCategoryByID(category_id);
        TraineeCategory traineeCategory=new TraineeCategory();
        traineeCategory.setCategory(category);
        traineeCategory.setTrainee(trainee);
        User user=userService.getCurrentUserLogin();
        traineeCategory.setAssign_by(user.getId());
        traineeCategory.setAssign_Date(LocalDate.now());
        traineeCategory.setModified_by(user.getId());
        traineeCategory.setModified_Date(LocalDate.now());
        traineeCategory.setModify_date(LocalDate.now());
        TraineeCategory result = traineeCategoryService.saveTraineeCategory(traineeCategory);
        return ResponseEntity
            .created(new URI("/api/trainee-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

}
