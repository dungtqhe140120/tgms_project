package com.doantotnghiep.service.resources;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TraineeAndPtDraftInfResource {
    private Long id;
    private String traineeName;
    private String ptName;
    private LocalDate expireDate;
    private LocalDate registerDate;
    private String packageRegister;
    private Long slot;
    private Integer confirmStatus;
    private String confirmBy;
    private LocalDate confirmAt;
    private String reasonChange;
}
