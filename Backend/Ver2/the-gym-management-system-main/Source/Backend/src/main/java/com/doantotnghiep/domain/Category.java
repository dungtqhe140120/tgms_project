package com.doantotnghiep.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * A Category.
 */
@Entity
@Table(name = "category")
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "image")
    private String image;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "created_by")
    private Long created_by;

    @Column(name = "created_date")
    private LocalDate created_date;

    @Column(name = "modify_by")
    private Long modify_by;

    @Column(name = "modify_date")
    private LocalDate modify_date;

    @Max(1)
    @Min(0)
    @Column(name = "isCenter")
    private Integer isCenter;


    @OneToMany(mappedBy = "category",fetch = FetchType.LAZY)
//    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "category"}, allowSetters = true)
    private Set<TraineeCategory> traineeCategories = new HashSet<>();


    @OneToMany(mappedBy = "category",fetch = FetchType.LAZY)
//    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "category"}, allowSetters = true)
    private Set<DetailExercise> detailExercises = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public Integer getIsCenter() {
        return isCenter;
    }

    public void setIsCenter(Integer isCenter) {
        this.isCenter = isCenter;
    }

    public Set<TraineeCategory> getTraineeCategories() {
        return traineeCategories;
    }

    public void setTraineeCategories(Set<TraineeCategory> traineeCategories) {
        this.traineeCategories = traineeCategories;
    }

    public Long getId() {
        return this.id;
    }

    public Category id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Category name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return this.image;
    }

    public Category image(String image) {
        this.setImage(image);
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public Category shortDescription(String shortDescription) {
        this.setShortDescription(shortDescription);
        return this;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Long getCreated_by() {
        return this.created_by;
    }

    public Category created_by(Long created_by) {
        this.setCreated_by(created_by);
        return this;
    }

    public void setCreated_by(Long created_by) {
        this.created_by = created_by;
    }

    public LocalDate getCreated_date() {
        return this.created_date;
    }

    public Category created_date(LocalDate created_date) {
        this.setCreated_date(created_date);
        return this;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public Set<DetailExercise> getDetailExercises() {
        return detailExercises;
    }

    public void setDetailExercises(Set<DetailExercise> detailExercises) {
        this.detailExercises = detailExercises;
    }

    public Long getModify_by() {
        return this.modify_by;
    }

    public Category modify_by(Long modify_by) {
        this.setModify_by(modify_by);
        return this;
    }

    public void setModify_by(Long modify_by) {
        this.modify_by = modify_by;
    }

    public LocalDate getModify_date() {
        return this.modify_date;
    }

    public Category modify_date(LocalDate modify_date) {
        this.setModify_date(modify_date);
        return this;
    }

    public void setModify_date(LocalDate modify_date) {
        this.modify_date = modify_date;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Category)) {
            return false;
        }
        return id != null && id.equals(((Category) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Category{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", image='" + getImage() + "'" +
            ", shortDescription='" + getShortDescription() + "'" +
            ", created_by=" + getCreated_by() +
            ", created_date='" + getCreated_date() + "'" +
            ", modify_by=" + getModify_by() +
            ", modify_date='" + getModify_date() + "'" +
            "}";
    }
}
