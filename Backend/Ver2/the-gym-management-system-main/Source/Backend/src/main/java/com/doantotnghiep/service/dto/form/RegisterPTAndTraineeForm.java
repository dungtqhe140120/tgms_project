package com.doantotnghiep.service.dto.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterPTAndTraineeForm {
    @Min(value = 1000, message = "Số tiền phải ít nhất 1000")
    private Integer amount;
    @NotNull
    private Long ptId;
    @NotNull
    private Long slotId;
    @NotBlank(message = "Redirect Url is mandatory")
    private String redirectUrl;
}
