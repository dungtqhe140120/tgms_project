package com.doantotnghiep.repository;

import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.domain.Trainee;
import com.doantotnghiep.service.dto.PersonalTrainerFullDTO;
import com.doantotnghiep.service.dto.TraineeFullDTO;
import com.doantotnghiep.service.dto.display.TraineeDisplayDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqTraineeFullDTO;
import com.doantotnghiep.service.dto.requestDTO.TraineeDetailDTO;
import com.doantotnghiep.service.resources.TraineeInfFullResources;
import com.doantotnghiep.service.resources.TraineeInfShortResource;
import com.doantotnghiep.service.resources.TraineeInfShortResource;
import liquibase.pro.packaged.Q;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data SQL repository for the Trainee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TraineeRepository extends JpaRepository<Trainee, Long> {
    @Query(value = "select new com.doantotnghiep.service.dto.TraineeFullDTO(t.id, t.short_Description, p.address, p.avatar, p.birthday, p.email, p.fullname, p.gender, p.is_active, p.modified_by, p.modified_date, p.objective, p.phone, p.first_login) from Trainee t inner join Profile p on p.id=t.profile.id where p.user.id= ?1")
    TraineeFullDTO getOneTraineeFullDTOByUser(Long user_id);

    @Query(value = "select  count(pt.id) from  Trainee  pt inner join Profile p  on p.id=pt.profile.id where p.user.id= ?1")
    Integer countTraineeByUserID(Long user_id);

    @Query(value = "select pt.* from Trainee pt inner join jhi_profile p on pt.profile_id=p.id inner join jhi_user u on u.id=p.user_id where u.id=?1", nativeQuery = true)
    Trainee getTraineeByUserID(Long user_id);

    @Query(value = "select new com.doantotnghiep.service.dto.requestDTO.ReqTraineeFullDTO(pt.id, pt.short_Description, p.address, p.avatar, p.birthday, p.email, p.fullname, p.gender, p.objective, p.phone) from Trainee  pt inner join Profile p on pt.profile.id=p.id inner join User u on u.id=p.user.id where u.id= ?1")
    ReqTraineeFullDTO getReqTraineeFullDTOByUserID(Long user_id);

    @Query(value = "SELECT distinct new com.doantotnghiep.service.resources.TraineeInfShortResource(t.id, p.fullname, p.phone) " +
        "FROM Trainee t INNER JOIN Profile p ON p.id = t.profile.id " +
        "INNER JOIN TraineeAndPT tpt ON t.id = tpt.trainee.id WHERE tpt.personalTrainer.id = :ptId AND tpt.expiredDate >= current_date " +
        "AND tpt.confirmStatus = :confirmStatus")
    List<TraineeInfShortResource> findTraineeOfPT(@Param("ptId") Long ptId, @Param("confirmStatus") Integer confirmStatus);

    @Query(value = "select new com.doantotnghiep.service.dto.display.TraineeDisplayDTO(t.id, p.fullname, p.avatar, p.gender, p.phone, p.email, p.address, u.createdDate, t.expired_date) from Profile p inner join Trainee t on p.id=t.profile.id inner join User u on p.user.id=u.id order by t.id desc ")
    Page<TraineeDisplayDTO> getAllTraineeDisplayDTO(Pageable pageable);

    @Query(value = "select new com.doantotnghiep.service.dto.display.TraineeDisplayDTO(t.id, p.fullname, p.avatar," +
        " p.gender, p.phone, p.email, p.address, u.createdDate, t.expired_date)" +
        " from Profile p inner join Trainee t on p.id=t.profile.id inner join User u on p.user.id=u.id" +
        " WHERE LOWER(p.fullname) LIKE LOWER(CONCAT('%', :traineeName,'%')) order by t.id desc")
    Page<TraineeDisplayDTO> getAllTraineeDisplayDTOByName(Pageable pageable, @Param("traineeName") String traineeName);

    @Query(value = "SELECT t.id " +
        "FROM Trainee t INNER JOIN Profile p ON p.id = t.profile.id " +
        "WHERE p.user.id = :userId ")
    Long getTraineeId(@Param("userId") Long userId);

    @Query(value = "select * from trainee where id=?1", nativeQuery = true)
    Trainee getTraineeByID(Long id);

    @Query("SELECT t FROM Trainee t JOIN FETCH t.profile WHERE t.id = :traineeId")
    Optional<Trainee> findTraineeJoinFetch(@Param("traineeId") Long traineeId);

    @Query(value = "SELECT new com.doantotnghiep.service.resources.TraineeInfFullResources(t.id, p.fullname, " +
        "p.avatar, p.gender, p.phone, p.email, " +
        "p.address, p.modified_date, t.expired_date, tpt.registeredDate, " +
        "tpt.expiredDate, tpt.type, tpt.personalTrainer.id, pf.fullname, " +
        "tpt.price, pf.avatar)" +
        "FROM Trainee t INNER JOIN Profile p ON p.id = t.profile.id " +
        "LEFT JOIN TraineeAndPT tpt ON t.id = tpt.trainee.id " +
        "LEFT JOIN Profile pf ON pf.id = tpt.personalTrainer.id " +
        "WHERE t.id = :traineeId ORDER BY t.expired_date DESC")
    Optional<TraineeInfFullResources> findFirstTraineeFullInf(@Param("traineeId") Long traineeId);
}
