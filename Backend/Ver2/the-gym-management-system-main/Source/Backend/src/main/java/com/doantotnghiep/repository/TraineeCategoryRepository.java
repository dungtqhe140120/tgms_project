package com.doantotnghiep.repository;

import com.doantotnghiep.domain.TraineeCategory;
import com.doantotnghiep.service.dto.display.CategoryTraineePTDisplayDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TraineeCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TraineeCategoryRepository extends JpaRepository<TraineeCategory, Long> {
    @Query(value = "select new com.doantotnghiep.service.dto.display.CategoryTraineePTDisplayDTO(e.id, e.name, p.fullname,  s.name, b.bmi_number, t.id ) from Category e inner join TraineeCategory te   on te.category.id=e.id inner join Trainee t on te.trainee.id=t.id inner join Profile p on t.profile.id=p.id \n" +
        "inner join BMI b on b.trainee.id=t.id inner join TraineeAndPT tp on t.id=tp.trainee.id inner join SlotTime s on s.id=tp.slotTime.id  where t.id=?1")
    Page<CategoryTraineePTDisplayDTO> getCategoryTraineeByTrainee(Pageable pageable, Long trainee_id);

}
