package com.doantotnghiep.service.dto.type;

/**
 *
 */
public enum MomoTransactionStatus {
    NEW,
    IN_PROGRESS,
    DONE,
    ERROR,
    CANCEL,
    UNKNOWN,
    EXPIRED
}
