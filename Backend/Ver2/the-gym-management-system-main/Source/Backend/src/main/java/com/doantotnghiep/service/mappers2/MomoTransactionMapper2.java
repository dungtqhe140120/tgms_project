package com.doantotnghiep.service.mappers2;

import java.util.Locale;

import com.doantotnghiep.domain.MomoTransaction;
import com.doantotnghiep.service.resources.MomoPTAndTraineeRegisterResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 *
 */
@Mapper(componentModel = "spring")
public abstract class MomoTransactionMapper2 {
    @Autowired
    protected MessageSource messageSource;

    @Mapping(source = "resultCode", target = "resultMessage", qualifiedByName = "resultConvert")
    public abstract MomoPTAndTraineeRegisterResource toResource(MomoTransaction entity);

    @Named("resultConvert")
    String resultConvert(Integer resultCode) {
        return messageSource.getMessage(
            "momo.result.code."+resultCode,
            null, Locale.forLanguageTag("vi")
        );
    }
}
