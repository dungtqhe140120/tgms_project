package com.doantotnghiep.service.resources;

import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.domain.Trainee;
import com.doantotnghiep.service.dto.type.MomoOrderType;
import com.doantotnghiep.service.dto.type.MomoTransactionStatus;
import lombok.Data;

/**
 *
 */
@Data
public class MomoPTAndTraineeRegisterResource {
    private Long id;
    private String transactionId;
    private Integer amount;
    private String transactionInfo;
    private String orderType;
    private String requestId;
    private String resultMessage;
    private String message;
    private Trainee trainee;
    private String status;
    private PersonalTrainer personalTrainer;
}
