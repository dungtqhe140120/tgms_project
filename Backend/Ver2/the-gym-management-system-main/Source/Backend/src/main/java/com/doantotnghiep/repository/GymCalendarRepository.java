package com.doantotnghiep.repository;

import com.doantotnghiep.domain.GymCalendar;
import com.doantotnghiep.service.resources.TraineeAndPTResources;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterator;

@Repository
public interface GymCalendarRepository extends JpaRepository<GymCalendar, Long>, JpaSpecificationExecutor<GymCalendar> {
    Optional<GymCalendar> getGymCalendarByCalendarDateAndTraineeIdAndCreatedBy(LocalDate calendarDate, Long traineeId, Long createdBy);

//    Page<GymCalendar> getGymCalendarsByOrderByCalendarDateDesc(Specification<GymCalendar> specification, Pageable pageable);
}
