package com.doantotnghiep.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.doantotnghiep.service.dto.type.MomoOrderType;
import com.doantotnghiep.service.dto.type.MomoPayType;
import com.doantotnghiep.service.dto.type.MomoTransactionStatus;
import com.doantotnghiep.service.dto.type.TransactionType;
import com.doantotnghiep.utils.AppUtils;
import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@Entity
@Getter
@Setter
public class MomoTransaction extends AbstractAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "transaction_id")
    private String transactionId; // equivalent to transactionId of momo

    @Column(nullable = false)
    private Integer amount;

    @Column
    private String transactionInfo;

    @Enumerated(EnumType.STRING)
    private MomoOrderType orderType;

    @Enumerated(EnumType.STRING)
    private MomoPayType payType;

    @Column(columnDefinition = "NVARCHAR2(1000)")
    private String responseBody;

    @Column(nullable = false)
    private String requestId;

    @Column
    private Integer resultCode;

    @Column
    private String message;

    @Column(nullable = false)
    private Long userId;

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private MomoTransactionStatus status;

    @Transient
    private String userName;

    @Transient
    private String ptName;

    @PrePersist
    public void prePersist() {
        setRequestId(AppUtils.generateRequestId(type));
        setTransactionInfo(
            AppUtils.generateTransactionInfo(userName, type, ptName, amount));
    }
}
