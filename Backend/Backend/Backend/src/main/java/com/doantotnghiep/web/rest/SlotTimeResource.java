package com.doantotnghiep.web.rest;

import com.doantotnghiep.repository.SlotTimeRepository;
import com.doantotnghiep.service.SlotTimeService;
import com.doantotnghiep.service.dto.SlotTimeDTO;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.SlotTime}.
 */
@RestController
@RequestMapping("/api")
public class SlotTimeResource {

    private final Logger log = LoggerFactory.getLogger(SlotTimeResource.class);

    private static final String ENTITY_NAME = "slotTime";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SlotTimeService slotTimeService;

    private final SlotTimeRepository slotTimeRepository;

    public SlotTimeResource(SlotTimeService slotTimeService, SlotTimeRepository slotTimeRepository) {
        this.slotTimeService = slotTimeService;
        this.slotTimeRepository = slotTimeRepository;
    }

    /**
     * {@code POST  /slot-times} : Create a new slotTime.
     *
     * @param slotTimeDTO the slotTimeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new slotTimeDTO, or with status {@code 400 (Bad Request)} if the slotTime has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/slot-times")
    public ResponseEntity<SlotTimeDTO> createSlotTime(@RequestBody SlotTimeDTO slotTimeDTO) throws URISyntaxException {
        log.debug("REST request to save SlotTime : {}", slotTimeDTO);
        if (slotTimeDTO.getId() != null) {
            throw new BadRequestAlertException("A new slotTime cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SlotTimeDTO result = slotTimeService.save(slotTimeDTO);
        return ResponseEntity
            .created(new URI("/api/slot-times/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /slot-times/:id} : Updates an existing slotTime.
     *
     * @param id the id of the slotTimeDTO to save.
     * @param slotTimeDTO the slotTimeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated slotTimeDTO,
     * or with status {@code 400 (Bad Request)} if the slotTimeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the slotTimeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/slot-times/{id}")
    public ResponseEntity<SlotTimeDTO> updateSlotTime(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SlotTimeDTO slotTimeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update SlotTime : {}, {}", id, slotTimeDTO);
        if (slotTimeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, slotTimeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!slotTimeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SlotTimeDTO result = slotTimeService.update(slotTimeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, slotTimeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /slot-times/:id} : Partial updates given fields of an existing slotTime, field will ignore if it is null
     *
     * @param id the id of the slotTimeDTO to save.
     * @param slotTimeDTO the slotTimeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated slotTimeDTO,
     * or with status {@code 400 (Bad Request)} if the slotTimeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the slotTimeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the slotTimeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/slot-times/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SlotTimeDTO> partialUpdateSlotTime(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SlotTimeDTO slotTimeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update SlotTime partially : {}, {}", id, slotTimeDTO);
        if (slotTimeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, slotTimeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!slotTimeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SlotTimeDTO> result = slotTimeService.partialUpdate(slotTimeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, slotTimeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /slot-times} : get all the slotTimes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of slotTimes in body.
     */
    @GetMapping("/slot-times")
    public ResponseEntity<Page<SlotTimeDTO>> getAllSlotTimes(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of SlotTimes");
        Page<SlotTimeDTO> page = slotTimeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    /**
     * {@code GET  /slot-times/:id} : get the "id" slotTime.
     *
     * @param id the id of the slotTimeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the slotTimeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/slot-times/{id}")
    public ResponseEntity<SlotTimeDTO> getSlotTime(@PathVariable Long id) {
        log.debug("REST request to get SlotTime : {}", id);
        Optional<SlotTimeDTO> slotTimeDTO = slotTimeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(slotTimeDTO);
    }

    /**
     * {@code DELETE  /slot-times/:id} : delete the "id" slotTime.
     *
     * @param id the id of the slotTimeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/slot-times/{id}")
    public ResponseEntity<Void> deleteSlotTime(@PathVariable Long id) {
        log.debug("REST request to delete SlotTime : {}", id);
        slotTimeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
