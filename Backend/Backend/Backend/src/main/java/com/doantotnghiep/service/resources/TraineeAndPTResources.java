package com.doantotnghiep.service.resources;

import java.time.LocalDate;

public class TraineeAndPTResources {
    private Long id;
    private String traineeName;
    private String ptName;
    private LocalDate expireDate;
    private LocalDate registerDate;
    private String packageRegister;
    private Long slot;
    private Integer confirmStatus;
    private String confirmBy;
    private LocalDate confirmAt;

    public TraineeAndPTResources(Long id, String traineeName, String ptName, LocalDate expireDate, LocalDate registerDate,
                                 String packageRegister, Long slot, Integer confirmStatus, String confirmBy, LocalDate confirmAt) {
        this.id = id;
        this.traineeName = traineeName;
        this.ptName = ptName;
        this.expireDate = expireDate;
        this.registerDate = registerDate;
        this.packageRegister = packageRegister;
        this.slot = slot;
        this.confirmStatus = confirmStatus;
        this.confirmBy = confirmBy;
        this.confirmAt = confirmAt;
    }

    public TraineeAndPTResources() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTraineeName() {
        return traineeName;
    }

    public void setTraineeName(String traineeName) {
        this.traineeName = traineeName;
    }

    public String getPtName() {
        return ptName;
    }

    public void setPtName(String ptName) {
        this.ptName = ptName;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    public LocalDate getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(LocalDate registerDate) {
        this.registerDate = registerDate;
    }

    public String getPackageRegister() {
        return packageRegister;
    }

    public void setPackageRegister(String packageRegister) {
        this.packageRegister = packageRegister;
    }

    public Long getSlot() {
        return slot;
    }

    public void setSlot(Long slot) {
        this.slot = slot;
    }

    public Integer getConfirmStatus() {
        return confirmStatus;
    }

    public void setConfirmStatus(Integer confirmStatus) {
        this.confirmStatus = confirmStatus;
    }

    public String getConfirmBy() {
        return confirmBy;
    }

    public void setConfirmBy(String confirmBy) {
        this.confirmBy = confirmBy;
    }

    public LocalDate getConfirmAt() {
        return confirmAt;
    }

    public void setConfirmAt(LocalDate confirmAt) {
        this.confirmAt = confirmAt;
    }
}
