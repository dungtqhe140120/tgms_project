package com.doantotnghiep.repository;

import com.doantotnghiep.domain.GymPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GymPriceRepository extends JpaRepository<GymPrice, Long> {
    GymPrice getGymPricesByRegisterDays(Long registerDays);

    @Query(value = "select gr.registerDays  from GymPrice gr group by gr.registerDays")
    List<Long> getRegisterDays();


}
