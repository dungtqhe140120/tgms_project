package com.doantotnghiep.service.impl;

import com.doantotnghiep.repository.BMIRepository;
import com.doantotnghiep.repository.NewsRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.service.HomeService;
import com.doantotnghiep.service.mappers2.BMIMapper2;
import com.doantotnghiep.service.resources.BMIResource;
import com.doantotnghiep.service.resources.HomeTraineeEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class HomeServiceImpl implements HomeService {
    private final Logger log = LoggerFactory.getLogger(HomeServiceImpl.class);
    private final NewsRepository newsRepository;
    private final BMIRepository bmiRepository;

    private final TraineeAndPTRepository traineeAndPTRepository;

    private static final Float BALANCE_NORMAL_LESS = 18.5f;
    private static final Float BALANCE_NORMAL_MAX = 22.9f;

    public HomeServiceImpl(NewsRepository newsRepository, BMIRepository bmiRepository, TraineeAndPTRepository traineeAndPTRepository) {
        this.bmiRepository = bmiRepository;
        this.newsRepository = newsRepository;
        this.traineeAndPTRepository = traineeAndPTRepository;
    }

    @Override
    public HomeTraineeEntity getHomeTrainee(Pageable pageable, Long traineeId) {
        var news = newsRepository.findAllNewsDes(pageable);
        var bmi = bmiRepository.findTopByTraineeIdOrderByCreatedAtDesc(traineeId);
        HomeTraineeEntity homeTraineeEntity = new HomeTraineeEntity();
        if (news != null) {
            homeTraineeEntity.setNewsResources(news);
        }

        homeTraineeEntity.setPtId(traineeAndPTRepository.getPtId(traineeId));

        BMIResource bmiResource = BMIMapper2.INSTANCE.toResource(bmi.get());
        if (bmiResource != null) {
            String status = "Normal";
            if (bmiResource.getBmi() < BALANCE_NORMAL_LESS) {
                status = "Thin";
            }
            if (bmiResource.getBmi() > BALANCE_NORMAL_MAX) {
                status = "Overweight";
            }
            bmiResource.setStatus(status);
            homeTraineeEntity.setBmiResource(bmiResource);
        }

        return homeTraineeEntity;
    }
}
