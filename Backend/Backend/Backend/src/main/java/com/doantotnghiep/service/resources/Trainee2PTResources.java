package com.doantotnghiep.service.resources;

import java.time.LocalDate;

public class Trainee2PTResources {
    private Long traineeId;
    private String traineeName;
    private LocalDate expireDate;
    private String packagePT;
    private Long slot;
    private String ratePT;
    private Float rateStar;
    private LocalDate rateAt;
    private String avatarTrainee;

    public Trainee2PTResources() {
    }

    public Trainee2PTResources(Long traineeId, String traineeName, LocalDate expireDate, String packagePT, Long slot, String ratePT, Float rateStar, LocalDate rateAt, String avatarTrainee) {
        this.traineeId = traineeId;
        this.traineeName = traineeName;
        this.expireDate = expireDate;
        this.packagePT = packagePT;
        this.slot = slot;
        this.ratePT = ratePT;
        this.rateStar = rateStar;
        this.rateAt = rateAt;
        this.avatarTrainee = avatarTrainee;
    }

    public String getAvatarTrainee() {
        return avatarTrainee;
    }

    public void setAvatarTrainee(String avatarTrainee) {
        this.avatarTrainee = avatarTrainee;
    }

    public Long getTraineeId() {
        return traineeId;
    }

    public void setTraineeId(Long traineeId) {
        this.traineeId = traineeId;
    }

    public String getTraineeName() {
        return traineeName;
    }

    public void setTraineeName(String traineeName) {
        this.traineeName = traineeName;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    public String getPackagePT() {
        return packagePT;
    }

    public void setPackagePT(String packagePT) {
        this.packagePT = packagePT;
    }

    public Long getSlot() {
        return slot;
    }

    public void setSlot(Long slot) {
        this.slot = slot;
    }

    public String getRatePT() {
        return ratePT;
    }

    public void setRatePT(String ratePT) {
        this.ratePT = ratePT;
    }

    public Float getRateStar() {
        return rateStar;
    }

    public void setRateStar(Float rateStar) {
        this.rateStar = rateStar;
    }

    public LocalDate getRateAt() {
        return rateAt;
    }

    public void setRateAt(LocalDate rateAt) {
        this.rateAt = rateAt;
    }
}
