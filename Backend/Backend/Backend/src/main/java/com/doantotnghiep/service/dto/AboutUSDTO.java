package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.doantotnghiep.domain.AboutUS} entity.
 */
public class AboutUSDTO implements Serializable {

    private Long id;

    private String logo;

    private String content;

    private LocalDate date_incorporation;

    private String phoneNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getDate_incorporation() {
        return date_incorporation;
    }

    public void setDate_incorporation(LocalDate date_incorporation) {
        this.date_incorporation = date_incorporation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AboutUSDTO)) {
            return false;
        }

        AboutUSDTO aboutUSDTO = (AboutUSDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, aboutUSDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AboutUSDTO{" +
            "id=" + getId() +
            ", logo='" + getLogo() + "'" +
            ", content='" + getContent() + "'" +
            ", date_incorporation='" + getDate_incorporation() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            "}";
    }
}
