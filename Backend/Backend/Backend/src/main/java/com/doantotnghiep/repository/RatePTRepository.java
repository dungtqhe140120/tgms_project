package com.doantotnghiep.repository;

import com.doantotnghiep.domain.RatePT;
import com.doantotnghiep.service.resources.RatePTResources;
import com.doantotnghiep.service.resources.TraineeRatePTResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the RatePT entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RatePTRepository extends JpaRepository<RatePT, Long> {
    RatePT findRatePTByTraineeIdAndPtId(Long traineeId, Long ptId);


    @Query(value = "SELECT new com.doantotnghiep.service.resources.TraineeRatePTResource(p.fullname, rt.rateStar, rt.comment, rt.created_at) " +
        " from RatePT  rt inner join Trainee tn on rt.traineeId = tn.id" +
        " inner join Profile p on tn.profile.id = p.id where rt.ptId = :ptId")
    Page<TraineeRatePTResource> findAllByPT(Pageable pageable, @Param("ptId") Long ptId);

    @Query(value = "SELECT new com.doantotnghiep.service.resources.RatePTResources(p.fullname, p.phone, p.gender, p.birthday, p.avatar) " +
        " from PersonalTrainer pt inner join Profile p on pt.profile.id = p.id where pt.id = :ptId")
    RatePTResources findPTById(@Param("ptId") Long ptId);

    @Query(value = "SELECT AVG(rp.rateStar) " +
        " from RatePT rp where rp.ptId = :ptId")
    Float avgStarOfPT(@Param("ptId") Long ptId);

    @Query(value = "SELECT new com.doantotnghiep.service.resources.TraineeRatePTResource(rt.rateStar, rt.comment, rt.created_at) " +
        " from RatePT  rt inner join Trainee tn on rt.traineeId = tn.id" +
        " inner join Profile p on tn.profile.id = p.id where rt.ptId = :ptId")
    Page<TraineeRatePTResource> findAllByUserPtId(Pageable pageable, @Param("ptId") Long ptId);
}
