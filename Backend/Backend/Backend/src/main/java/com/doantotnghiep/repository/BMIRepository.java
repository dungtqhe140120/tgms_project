package com.doantotnghiep.repository;

import com.doantotnghiep.domain.BMI;
import com.doantotnghiep.service.resources.NewsResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data SQL repository for the BMI entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BMIRepository extends JpaRepository<BMI, Long> {
//    @Query(value = "SELECT new BMI(n.id, n.weight, n.height, n.createdAt, n.bmi_number) " +
//        "FROM BMI n WHERE n.trainee.id = :id ")
    Optional<BMI> findTopByTraineeIdOrderByCreatedAtDesc(Long traineeId);
}
