package com.doantotnghiep.service.dto.response;

public class MetaData {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
