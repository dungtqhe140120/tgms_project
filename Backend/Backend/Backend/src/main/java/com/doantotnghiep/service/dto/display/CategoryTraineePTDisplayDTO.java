package com.doantotnghiep.service.dto.display;

import java.io.Serializable;
import java.util.Objects;

public class CategoryTraineePTDisplayDTO  implements Serializable {
    private Long exID;
    private String exTitle;
    private String traineeName;
    private String slotName;
    private Float bmiNumber;

    private Long trainee_id;

    public CategoryTraineePTDisplayDTO(Long exID, String exTitle, String traineeName, String slotName, Float bmiNumber, Long trainee_id) {
        this.exID = exID;
        this.exTitle = exTitle;
        this.traineeName = traineeName;
        this.slotName = slotName;
        this.bmiNumber = bmiNumber;
        this.trainee_id = trainee_id;
    }

    public CategoryTraineePTDisplayDTO() {
    }

    public Long getExID() {
        return exID;
    }

    public void setExID(Long exID) {
        this.exID = exID;
    }

    public String getExTitle() {
        return exTitle;
    }

    public void setExTitle(String exTitle) {
        this.exTitle = exTitle;
    }

    public String getTraineeName() {
        return traineeName;
    }

    public void setTraineeName(String traineeName) {
        this.traineeName = traineeName;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public Float getBmiNumber() {
        return bmiNumber;
    }

    public void setBmiNumber(Float bmiNumber) {
        this.bmiNumber = bmiNumber;
    }

    public Long getTrainee_id() {
        return trainee_id;
    }

    public void setTrainee_id(Long trainee_id) {
        this.trainee_id = trainee_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryTraineePTDisplayDTO that = (CategoryTraineePTDisplayDTO) o;
        return Objects.equals(exID, that.exID) && Objects.equals(exTitle, that.exTitle) && Objects.equals(traineeName, that.traineeName) && Objects.equals(slotName, that.slotName) && Objects.equals(bmiNumber, that.bmiNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exID, exTitle, traineeName, slotName, bmiNumber);
    }

    @Override
    public String toString() {
        return "CategoryTraineePTDisplayDTO{" +
            "exID=" + exID +
            ", exTitle='" + exTitle + '\'' +
            ", traineeName='" + traineeName + '\'' +
            ", slotName='" + slotName + '\'' +
            ", bmiNumber=" + bmiNumber +
            '}';
    }
}
