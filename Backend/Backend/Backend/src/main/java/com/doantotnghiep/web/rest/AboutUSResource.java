package com.doantotnghiep.web.rest;

import com.doantotnghiep.repository.AboutUSRepository;
import com.doantotnghiep.service.AboutUSService;
import com.doantotnghiep.service.dto.AboutUSDTO;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.AboutUS}.
 */
@RestController
@RequestMapping("/api")
public class AboutUSResource {

    private final Logger log = LoggerFactory.getLogger(AboutUSResource.class);

    private static final String ENTITY_NAME = "aboutUS";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AboutUSService aboutUSService;

    private final AboutUSRepository aboutUSRepository;

    public AboutUSResource(AboutUSService aboutUSService, AboutUSRepository aboutUSRepository) {
        this.aboutUSService = aboutUSService;
        this.aboutUSRepository = aboutUSRepository;
    }

    /**
     * {@code POST  /aboutuses} : Create a new aboutUS.
     *
     * @param aboutUSDTO the aboutUSDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aboutUSDTO, or with status {@code 400 (Bad Request)} if the aboutUS has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/aboutuses")
    public ResponseEntity<AboutUSDTO> createAboutUS(@RequestBody AboutUSDTO aboutUSDTO) throws URISyntaxException {
        log.debug("REST request to save AboutUS : {}", aboutUSDTO);
        if (aboutUSDTO.getId() != null) {
            throw new BadRequestAlertException("A new aboutUS cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AboutUSDTO result = aboutUSService.save(aboutUSDTO);
        return ResponseEntity
            .created(new URI("/api/aboutuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /aboutuses/:id} : Updates an existing aboutUS.
     *
     * @param id the id of the aboutUSDTO to save.
     * @param aboutUSDTO the aboutUSDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aboutUSDTO,
     * or with status {@code 400 (Bad Request)} if the aboutUSDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aboutUSDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/aboutuses/{id}")
    public ResponseEntity<AboutUSDTO> updateAboutUS(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AboutUSDTO aboutUSDTO
    ) throws URISyntaxException {
        log.debug("REST request to update AboutUS : {}, {}", id, aboutUSDTO);
        if (aboutUSDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, aboutUSDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!aboutUSRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AboutUSDTO result = aboutUSService.update(aboutUSDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, aboutUSDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /aboutuses/:id} : Partial updates given fields of an existing aboutUS, field will ignore if it is null
     *
     * @param id the id of the aboutUSDTO to save.
     * @param aboutUSDTO the aboutUSDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aboutUSDTO,
     * or with status {@code 400 (Bad Request)} if the aboutUSDTO is not valid,
     * or with status {@code 404 (Not Found)} if the aboutUSDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the aboutUSDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/aboutuses/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AboutUSDTO> partialUpdateAboutUS(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AboutUSDTO aboutUSDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update AboutUS partially : {}, {}", id, aboutUSDTO);
        if (aboutUSDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, aboutUSDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!aboutUSRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AboutUSDTO> result = aboutUSService.partialUpdate(aboutUSDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, aboutUSDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /aboutuses} : get all the aboutuses.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aboutuses in body.
     */
    @GetMapping("/aboutuses")
    public ResponseEntity<List<AboutUSDTO>> getAllAboutuses(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Aboutuses");
        Page<AboutUSDTO> page = aboutUSService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /aboutuses/:id} : get the "id" aboutUS.
     *
     * @param id the id of the aboutUSDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aboutUSDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/aboutuses/{id}")
    public ResponseEntity<AboutUSDTO> getAboutUS(@PathVariable Long id) {
        log.debug("REST request to get AboutUS : {}", id);
        Optional<AboutUSDTO> aboutUSDTO = aboutUSService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aboutUSDTO);
    }

    /**
     * {@code DELETE  /aboutuses/:id} : delete the "id" aboutUS.
     *
     * @param id the id of the aboutUSDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/aboutuses/{id}")
    public ResponseEntity<Void> deleteAboutUS(@PathVariable Long id) {
        log.debug("REST request to delete AboutUS : {}", id);
        aboutUSService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
