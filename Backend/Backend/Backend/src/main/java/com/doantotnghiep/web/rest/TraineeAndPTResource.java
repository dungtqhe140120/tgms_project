package com.doantotnghiep.web.rest;

import com.doantotnghiep.repository.EmployeeRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.TraineeAndPTService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import com.doantotnghiep.service.dto.display.CalendarPTDisplayDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqCreateTraineeAndPT;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.dto.requestDTO.TraineeAndPTFilter;
import com.doantotnghiep.service.resources.TraineeAndPTResources;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.TraineeAndPT}.
 */
@RestController
@RequestMapping("/api")
public class TraineeAndPTResource {

    private final Logger log = LoggerFactory.getLogger(TraineeAndPTResource.class);

    private static final String ENTITY_NAME = "traineeAndPT";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TraineeAndPTService traineeAndPTService;

    private final TraineeAndPTRepository traineeAndPTRepository;

    private final TraineeRepository traineeRepository;

    private final EmployeeRepository employeeRepository;

    private final UserService userService;

    public TraineeAndPTResource(TraineeAndPTService traineeAndPTService, TraineeAndPTRepository traineeAndPTRepository,
                                TraineeRepository traineeRepository, UserService userService, EmployeeRepository employeeRepository) {
        this.traineeAndPTService = traineeAndPTService;
        this.traineeAndPTRepository = traineeAndPTRepository;
        this.traineeRepository = traineeRepository;
        this.userService = userService;
        this.employeeRepository = employeeRepository;
    }

    @PostMapping("/trainee-and-pts")
    public ResponseEntity<TraineeAndPTDTO> createTraineeAndPT(
        @Valid @RequestBody ReqCreateTraineeAndPT request
    )
        throws URISyntaxException {
        log.debug("REST request to save TraineeAndPT : {}", request);
        Long userId = userService.getCurrentUserLogin().getId();
        Long traineeId = traineeRepository.getTraineeId(userId);
        request.setTraineeId(traineeId);
        request.setType(request.getDateRegister().toString());
        TraineeAndPTDTO result = traineeAndPTService.saveV2(request);
        return ResponseEntity
            .created(new URI("/api/trainee-and-pts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /trainee-and-pts/:id} : Updates an existing traineeAndPT.
     *
     * @param id              the id of the traineeAndPTDTO to save.
     * @param traineeAndPTDTO the traineeAndPTDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated traineeAndPTDTO,
     * or with status {@code 400 (Bad Request)} if the traineeAndPTDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the traineeAndPTDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/trainee-and-pts/{id}")
    public ResponseEntity<TraineeAndPTDTO> updateTraineeAndPT(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TraineeAndPTDTO traineeAndPTDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TraineeAndPT : {}, {}", id, traineeAndPTDTO);
        if (traineeAndPTDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, traineeAndPTDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!traineeAndPTRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TraineeAndPTDTO result = traineeAndPTService.save(traineeAndPTDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeAndPTDTO.getId().toString()))
            .body(result);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_EMPLOYEE')")
    @PutMapping("/trainee-and-pts/{id}/confirm")
    public ResponseEntity<TraineeAndPTDTO> confirmTraineeAndPT(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TraineeAndPTDTO traineeAndPTDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TraineeAndPT : {}, {}", id, traineeAndPTDTO);
        if (traineeAndPTDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, traineeAndPTDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!traineeAndPTRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        Long userId = userService.getCurrentUserLogin().getId();
        Long confirmBy = employeeRepository.getEmployeeId(userId);
        if (confirmBy == null) {
            confirmBy = 1l;
        }
        TraineeAndPTDTO result = traineeAndPTService.confirm(traineeAndPTDTO, confirmBy);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeAndPTDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /trainee-and-pts/:id} : Partial updates given fields of an existing traineeAndPT, field will ignore if it is null
     *
     * @param id              the id of the traineeAndPTDTO to save.
     * @param traineeAndPTDTO the traineeAndPTDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated traineeAndPTDTO,
     * or with status {@code 400 (Bad Request)} if the traineeAndPTDTO is not valid,
     * or with status {@code 404 (Not Found)} if the traineeAndPTDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the traineeAndPTDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/trainee-and-pts/{id}", consumes = {"application/json", "application/merge-patch+json"})
    public ResponseEntity<TraineeAndPTDTO> partialUpdateTraineeAndPT(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody TraineeAndPTDTO traineeAndPTDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update TraineeAndPT partially : {}, {}", id, traineeAndPTDTO);
        if (traineeAndPTDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, traineeAndPTDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!traineeAndPTRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TraineeAndPTDTO> result = traineeAndPTService.partialUpdate(traineeAndPTDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, traineeAndPTDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /trainee-and-pts} : get all the traineeAndPTS.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of traineeAndPTS in body.
     */
    @GetMapping("/trainee-and-pts")
    public ResponseEntity<List<TraineeAndPTDTO>> getAllTraineeAndPTS(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of TraineeAndPTS");
        Page<TraineeAndPTDTO> page = traineeAndPTService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/trainee-and-pts/filters")
    public ResponseEntity<Page<TraineeAndPTResources>> getAllTraineeAndPTSFilter(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        TraineeAndPTFilter filter) {
        Page<TraineeAndPTResources> page = traineeAndPTService.getListTraineeAndPTByFilter(filter, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    /**
     * {@code GET  /trainee-and-pts/:id} : get the "id" traineeAndPT.
     *
     * @param id the id of the traineeAndPTDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the traineeAndPTDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/trainee-and-pts/{id}")
    public ResponseEntity<TraineeAndPTDTO> getTraineeAndPT(@PathVariable Long id) {
        log.debug("REST request to get TraineeAndPT : {}", id);
        Optional<TraineeAndPTDTO> traineeAndPTDTO = traineeAndPTService.findOne(id);
        return ResponseUtil.wrapOrNotFound(traineeAndPTDTO);
    }

    /**
     * {@code DELETE  /trainee-and-pts/:id} : delete the "id" traineeAndPT.
     *
     * @param id the id of the traineeAndPTDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/trainee-and-pts/{id}")
    public ResponseEntity<Void> deleteTraineeAndPT(@PathVariable Long id) {
        log.debug("REST request to delete TraineeAndPT : {}", id);
        traineeAndPTService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping("/trainees-and-pt/slot/{slotId}")
    public ResponseEntity<List<ReqPTFullDTO>> getFreePTs(
        @RequestParam(value = "register-date") LocalDate registerDate,
        @PathVariable Long slotId
    ) {
        log.debug("REST request to get a list PT have free slot");
        List<ReqPTFullDTO> pTsFreeSlot = traineeAndPTService.getPTsFreeSlot(slotId, registerDate);
        return ResponseEntity.ok().body(pTsFreeSlot);
    }

    @GetMapping("/trainee-and-pts/getCalendarPT/{id}")
    public ResponseEntity<Page<CalendarPTDisplayDTO>> getTraineeAndPT(@org.springdoc.api.annotations.ParameterObject Pageable pageable, @PathVariable Long id, @RequestParam("date") String date
    ) {
        log.debug("REST request to get CalendarPTDisplayDTO : {}", id);
        List<CalendarPTDisplayDTO> ls = traineeAndPTService.getListCalendarPTDisplayDTO(pageable, id, date);
        log.debug("dateFind: " + date);
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), ls.size());
        Page<CalendarPTDisplayDTO> page = new PageImpl<>(ls.subList(start, end), pageable, ls.size());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }
}
