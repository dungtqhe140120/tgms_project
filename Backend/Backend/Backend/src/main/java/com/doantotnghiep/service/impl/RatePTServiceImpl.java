package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.RatePT;
import com.doantotnghiep.repository.PersonalTrainerRepository;
import com.doantotnghiep.repository.RatePTRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.RatePTService;
import com.doantotnghiep.service.dto.RatePTDTO;
import com.doantotnghiep.service.mapper.RatePTMapper;
import com.doantotnghiep.service.resources.RateAboutMeResources;
import com.doantotnghiep.service.resources.RatePTResources;
import com.doantotnghiep.service.resources.TraineeRatePTResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link RatePT}.
 */
@Service
@Transactional
public class RatePTServiceImpl implements RatePTService {

    private final Logger log = LoggerFactory.getLogger(RatePTServiceImpl.class);

    private final RatePTRepository ratePTRepository;

    private final RatePTMapper ratePTMapper;

    private TraineeRepository traineeRepository;

    private TraineeAndPTRepository traineeAndPTRepository;

    private PersonalTrainerRepository personalTrainerRepository;

    public RatePTServiceImpl(RatePTRepository ratePTRepository, RatePTMapper ratePTMapper,
                             TraineeRepository traineeRepository, TraineeAndPTRepository traineeAndPTRepository,
                             PersonalTrainerRepository personalTrainerRepository) {
        this.ratePTRepository = ratePTRepository;
        this.ratePTMapper = ratePTMapper;
        this.traineeRepository = traineeRepository;
        this.traineeAndPTRepository = traineeAndPTRepository;
        this.personalTrainerRepository = personalTrainerRepository;
    }

    @Override
    public RatePTDTO save(RatePTDTO ratePTDTO) {
        log.debug("Request to save RatePT : {}", ratePTDTO);
        RatePT ratePT = ratePTMapper.toEntity(ratePTDTO);
        ratePT = ratePTRepository.save(ratePT);
        return ratePTMapper.toDto(ratePT);
    }

    @Override
    public RatePTDTO update(RatePTDTO ratePTDTO) {
        log.debug("Request to save RatePT : {}", ratePTDTO);
        RatePT ratePT = ratePTMapper.toEntity(ratePTDTO);
        ratePT = ratePTRepository.save(ratePT);
        return ratePTMapper.toDto(ratePT);
    }

    @Override
    public Optional<RatePTDTO> partialUpdate(RatePTDTO ratePTDTO) {
        log.debug("Request to partially update RatePT : {}", ratePTDTO);

        return ratePTRepository
            .findById(ratePTDTO.getId())
            .map(existingRatePT -> {
                ratePTMapper.partialUpdate(existingRatePT, ratePTDTO);

                return existingRatePT;
            })
            .map(ratePTRepository::save)
            .map(ratePTMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RatePTDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RatePTS");
        return ratePTRepository.findAll(pageable).map(ratePTMapper::toDto);
    }

    @Override
    public RatePTResources findAllByPT(Pageable pageable, Long userId) {
        Long traineeId = traineeRepository.getTraineeId(userId);
        Long ptId = traineeAndPTRepository.getPtId(traineeId);
        if (ptId == null) {
            return new RatePTResources();
        }
        var ratePTResource = ratePTRepository.findPTById(ptId);
        Float avgStar = ratePTRepository.avgStarOfPT(ptId);
        ratePTResource.setStartRate(avgStar);
        var traineeRatePTResources = ratePTRepository.findAllByPT(pageable, ptId);
        ratePTResource.setTraineeRatePTResource(traineeRatePTResources);
        return ratePTResource;
    }

    @Override
    public RateAboutMeResources findByPT(Pageable pageable, Long userPTId) {
        RateAboutMeResources rateAboutMeResources = new RateAboutMeResources();
        Long ptId = personalTrainerRepository.getPTId(userPTId);

        Float avgStar = ratePTRepository.avgStarOfPT(ptId);
        rateAboutMeResources.setStartRate(avgStar);

        var traineeRatePTResources =  ratePTRepository.findAllByUserPtId(pageable, ptId);
        rateAboutMeResources.setTraineeRatePTResource(traineeRatePTResources);

        return rateAboutMeResources;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RatePTDTO> findOne(Long id) {
        log.debug("Request to get RatePT : {}", id);
        return ratePTRepository.findById(id).map(ratePTMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RatePT : {}", id);
        ratePTRepository.deleteById(id);
    }
}
