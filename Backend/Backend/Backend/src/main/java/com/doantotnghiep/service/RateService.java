package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.RateCenterDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqRate;
import com.doantotnghiep.service.dto.response.ResponseRate;

public interface RateService {
    ResponseRate save(ReqRate reqRate, Long userId);

    RateCenterDTO saveRateCenter(RateCenterDTO request, Long userId);

    RateCenterDTO getRateCenter(Long userId);

    ReqRate getRateByUserId(Long userId);
}
