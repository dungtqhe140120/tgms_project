package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.RatePTDTO;
import com.doantotnghiep.service.resources.RateAboutMeResources;
import com.doantotnghiep.service.resources.RatePTResources;
import com.doantotnghiep.service.resources.TraineeRatePTResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.RatePT}.
 */
public interface RatePTService {
    /**
     * Save a ratePT.
     *
     * @param ratePTDTO the entity to save.
     * @return the persisted entity.
     */
    RatePTDTO save(RatePTDTO ratePTDTO);

    /**
     * Updates a ratePT.
     *
     * @param ratePTDTO the entity to update.
     * @return the persisted entity.
     */
    RatePTDTO update(RatePTDTO ratePTDTO);

    /**
     * Partially updates a ratePT.
     *
     * @param ratePTDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RatePTDTO> partialUpdate(RatePTDTO ratePTDTO);

    /**
     * Get all the ratePTS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RatePTDTO> findAll(Pageable pageable);

    RatePTResources findAllByPT(Pageable pageable, Long userId);

    RateAboutMeResources findByPT(Pageable pageable, Long userPTId);

    /**
     * Get the "id" ratePT.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RatePTDTO> findOne(Long id);

    /**
     * Delete the "id" ratePT.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
