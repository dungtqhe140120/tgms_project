package com.doantotnghiep.repository;

import com.doantotnghiep.domain.DetailExercise;
import com.doantotnghiep.service.dto.display.DetailContentDisplayDTO;
import com.doantotnghiep.service.dto.display.DetailExerciseDisplayDTO;
import com.doantotnghiep.service.dto.display.DetailExerciseShowDTO;
import liquibase.pro.packaged.Q;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data SQL repository for the DetailExercise entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetailExerciseRepository extends JpaRepository<DetailExercise, Long> {

    @Query(value = "select new com.doantotnghiep.service.dto.display.DetailExerciseDisplayDTO(d.id, d.image, d.title) from DetailExercise  d inner join Category c on d.category.id=c.id where c.id=?1")
    Page<DetailExerciseDisplayDTO> getDetailExerciseDisplayDTO(Pageable pageable, Long category_id);

    @Query(value = "select new com.doantotnghiep.service.dto.display.DetailExerciseShowDTO(a.id, a.title,a.content, a.created_date) from DetailExercise a where a.id=?1")
    Page<DetailExerciseShowDTO> getDetailExerciseShowDTO(Pageable pageable, Long id);

    @Query(value = "select count(1) from detail_exercise where id=?1", nativeQuery = true)
    Integer countDetailExerciseByID(Long id);

    @Query(value = "select * from detail_exercise where id=?1", nativeQuery = true)
    DetailExercise getDetailExerciseByID(Long id);

    @Modifying
    @Transactional
    @Query(value = "delete from detail_exercise where id=?1", nativeQuery = true)
    void deleteDetailExerciseByID(Long id);

    @Query(value = "select new com.doantotnghiep.service.dto.display.DetailContentDisplayDTO(d.content) from DetailExercise d where id=?1")
    DetailContentDisplayDTO detailContent(Long id);

}
