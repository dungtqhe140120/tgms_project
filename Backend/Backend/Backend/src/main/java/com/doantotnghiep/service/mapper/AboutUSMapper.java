package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.AboutUS;
import com.doantotnghiep.service.dto.AboutUSDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AboutUS} and its DTO {@link AboutUSDTO}.
 */
@Mapper(componentModel = "spring")
public interface AboutUSMapper extends EntityMapper<AboutUSDTO, AboutUS> {}
