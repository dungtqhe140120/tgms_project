package com.doantotnghiep.service.mappers2;

import com.doantotnghiep.domain.Exercise;
import com.doantotnghiep.service.resources.ExercisesResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public abstract class ExercisesMapper2 {

    public static final ExercisesMapper2 INSTANCE = Mappers.getMapper(ExercisesMapper2.class);


    @Mapping(target = "picture", source = "image")
    public abstract ExercisesResource toResource(Exercise entity);

    public abstract List<ExercisesResource> toResources(List<Exercise> exercises);
}
