package com.doantotnghiep.repository;

import com.doantotnghiep.domain.News;
import com.doantotnghiep.service.dto.NewsDTO;
import com.doantotnghiep.service.dto.TraineeDTO;
import com.doantotnghiep.service.resources.NewsResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the News entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsRepository extends JpaRepository<News, Long> {
    @Query(value = "select * from news where is_display=1 and ROWNUM <= 3 order by created_date", nativeQuery = true)
    List<News> getThreeLastestNew();

    @Query(value = "SELECT new com.doantotnghiep.service.resources.NewsResource(n.id, n.title, n.image, n.content, n.short_description, n.created_date) " +
        "FROM News n WHERE n.is_display = 1 ORDER BY n.created_date DESC")
    Page<NewsResource> findAllNewsDes(Pageable pageable);

}
