package com.doantotnghiep.service.dto.display;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class NewsDisplayDTO implements Serializable {

    private Long id;

    @NotNull
    private String title;

    @NotNull
    private String image;

    private LocalDate created_date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewsDisplayDTO that = (NewsDisplayDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(title, that.title) && Objects.equals(image, that.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, image);
    }

    @Override
    public String toString() {
        return "NewsDisplayDTO{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", image='" + image + '\'' +
            '}';
    }
}
