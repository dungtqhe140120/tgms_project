package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.News;
import com.doantotnghiep.repository.NewsRepository;
import com.doantotnghiep.service.NewsService;
import com.doantotnghiep.service.dto.NewsDTO;
import com.doantotnghiep.service.dto.display.NewsDisplayDTO;
import com.doantotnghiep.service.mapper.NewsMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.doantotnghiep.service.mapper.displayMapper.NewsDisplayMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link News}.
 */
@Service
@Transactional
public class NewsServiceImpl implements NewsService {

    private final Logger log = LoggerFactory.getLogger(NewsServiceImpl.class);

    private final NewsRepository newsRepository;

    private final NewsMapper newsMapper;

    private final NewsDisplayMapper newsDisplayMapper;

    public NewsServiceImpl(NewsRepository newsRepository, NewsMapper newsMapper, NewsDisplayMapper newsDisplayMapper) {
        this.newsRepository = newsRepository;
        this.newsMapper = newsMapper;
        this.newsDisplayMapper = newsDisplayMapper;
    }

    @Override
    public NewsDTO save(NewsDTO newsDTO) {
        log.debug("Request to save News : {}", newsDTO);
        News news = newsMapper.toEntity(newsDTO);
        news = newsRepository.save(news);
        return newsMapper.toDto(news);
    }

    @Override
    public Optional<NewsDTO> partialUpdate(NewsDTO newsDTO) {
        log.debug("Request to partially update News : {}", newsDTO);

        return newsRepository
            .findById(newsDTO.getId())
            .map(existingNews -> {
                newsMapper.partialUpdate(existingNews, newsDTO);

                return existingNews;
            })
            .map(newsRepository::save)
            .map(newsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<NewsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all News");
        return newsRepository.findAll(pageable).map(newsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<NewsDTO> findOne(Long id) {
        log.debug("Request to get News : {}", id);
        return newsRepository.findById(id).map(newsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete News : {}", id);
        newsRepository.deleteById(id);
    }

    @Override
    public List<NewsDisplayDTO> findThreeLastestNews() {
        log.debug("Request to findThreeLastestNews");
        return newsRepository.getThreeLastestNew().stream().map(newsDisplayMapper::toDto).collect(Collectors.toList());
    }
}
