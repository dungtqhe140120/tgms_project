package com.doantotnghiep.service.dto.response;

import com.doantotnghiep.service.dto.requestDTO.ReqRate;

public class ResponseRate {
    private MetaData metaData;
    private ReqRate reqRate;

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public ReqRate getReqRate() {
        return reqRate;
    }

    public void setReqRate(ReqRate reqRate) {
        this.reqRate = reqRate;
    }
}
