package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.DetailExercise;
import com.doantotnghiep.service.dto.DetailExerciseDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DetailExercise} and its DTO {@link DetailExerciseDTO}.
 */
@Mapper(componentModel = "spring")
public interface DetailExerciseMapper extends EntityMapper<DetailExerciseDTO, DetailExercise> {}
