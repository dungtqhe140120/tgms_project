package com.doantotnghiep.service.resources;

import java.time.LocalDate;

public class NewsResource {
    private Long id;
    private String title;
    private String image;
    private String content;
    private String shortDescription;
    private LocalDate createdDate;

    public NewsResource() {
    }

    public NewsResource(Long id, String title, String image, String content, String shortDescription, LocalDate createdDate) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.content = content;
        this.shortDescription = shortDescription;
        this.createdDate = createdDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }
}
