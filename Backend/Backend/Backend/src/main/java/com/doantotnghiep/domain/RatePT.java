package com.doantotnghiep.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RatePT.
 */
@Entity
@Table(name = "ratept")
public class RatePT implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "jhi_comment")
    private String comment;

    @Column(name = "rate_star")
    private Float rateStar;

    @Column(name = "created_at")
    private LocalDate created_at;

    //    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
//    @JsonIgnoreProperties(value = {"ratePT"}, allowSetters = true)
    @Column(name = "trainee_id")
    private Long traineeId;

    //    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
//    @JsonIgnoreProperties(value = {"ratePT"}, allowSetters = true)
    @Column(name = "personal_trainer_id")
    private Long ptId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RatePT id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return this.comment;
    }

    public RatePT comment(String comment) {
        this.setComment(comment);
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Float getRateStar() {
        return this.rateStar;
    }

    public RatePT rateStar(Float rateStar) {
        this.setRateStar(rateStar);
        return this;
    }

    public void setRateStar(Float rateStar) {
        this.rateStar = rateStar;
    }

    public LocalDate getCreated_at() {
        return this.created_at;
    }

    public RatePT created_at(LocalDate created_at) {
        this.setCreated_at(created_at);
        return this;
    }

    public void setCreated_at(LocalDate created_at) {
        this.created_at = created_at;
    }

    public Long getTraineeId() {
        return traineeId;
    }

    public void setTraineeId(Long traineeId) {
        this.traineeId = traineeId;
    }

    public Long getPtId() {
        return ptId;
    }

    public void setPtId(Long ptId) {
        this.ptId = ptId;
    }
// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RatePT)) {
            return false;
        }
        return id != null && id.equals(((RatePT) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RatePT{" +
            "id=" + getId() +
            ", comment='" + getComment() + "'" +
            ", rateStar=" + getRateStar() +
            ", created_at='" + getCreated_at() + "'" +
            "}";
    }
}
