package com.doantotnghiep.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Profile.
 */
@Entity
@Table(name = "jhi_profile")
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "fullname", length = 255, nullable = false)
    private String fullname;

    @Size(min = 1, max = 255)
    @Column(name = "address", length = 255)
    private String address;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "phone", length = 50, nullable = false, unique = true)
    private String phone;

    @Column(name = "objective")
    private String objective;

    @Column(name = "birthday")
    private LocalDate birthday;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "email", length = 50, nullable = false, unique = true)
    private String email;

    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "gender")
    private Integer gender;

    @Column(name = "avatar")
    private String avatar;

    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "is_active")
    private Integer is_active;

    @NotNull
    @Column(name = "modified_by", nullable = false)
    private Long modified_by;

    @NotNull
    @Column(name = "modified_date", nullable = false)
    private LocalDate modified_date;


    @Min(value = 0)
    @Max(value = 1)
    @NotNull
    @Column(name = "first_login", nullable = false)
    private Integer first_login;

    @OneToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @NotNull
    @JoinColumn(unique = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Profile id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullname() {
        return this.fullname;
    }

    public Profile fullname(String fullname) {
        this.setFullname(fullname);
        return this;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return this.address;
    }

    public Profile address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return this.phone;
    }

    public Profile phone(String phone) {
        this.setPhone(phone);
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getObjective() {
        return this.objective;
    }

    public Profile objective(String objective) {
        this.setObjective(objective);
        return this;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public LocalDate getBirthday() {
        return this.birthday;
    }

    public Profile birthday(LocalDate birthday) {
        this.setBirthday(birthday);
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return this.email;
    }

    public Profile email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return this.gender;
    }

    public Profile gender(Integer gender) {
        this.setGender(gender);
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public Profile avatar(String avatar) {
        this.setAvatar(avatar);
        return this;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getIs_active() {
        return this.is_active;
    }

    public Profile is_active(Integer is_active) {
        this.setIs_active(is_active);
        return this;
    }

    public void setIs_active(Integer is_active) {
        this.is_active = is_active;
    }

    public Long getModified_by() {
        return this.modified_by;
    }

    public Profile modified_by(Long modified_by) {
        this.setModified_by(modified_by);
        return this;
    }

    public void setModified_by(Long modified_by) {
        this.modified_by = modified_by;
    }

    public LocalDate getModified_date() {
        return this.modified_date;
    }

    public Profile modified_date(LocalDate modified_date) {
        this.setModified_date(modified_date);
        return this;
    }

    public void setModified_date(LocalDate modified_date) {
        this.modified_date = modified_date;
    }

    public Integer getFirst_login() {
        return first_login;
    }

    public void setFirst_login(Integer first_login) {
        this.first_login = first_login;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Profile)) {
            return false;
        }
        return id != null && id.equals(((Profile) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Profile{" +
            "id=" + getId() +
            ", fullname='" + getFullname() + "'" +
            ", address='" + getAddress() + "'" +
            ", phone='" + getPhone() + "'" +
            ", objective='" + getObjective() + "'" +
            ", birthday='" + getBirthday() + "'" +
            ", email='" + getEmail() + "'" +
            ", gender=" + getGender() +
            ", avatar='" + getAvatar() + "'" +
            ", is_active=" + getIs_active() +
            ", modified_by=" + getModified_by() +
            ", modified_date='" + getModified_date() + "'" +
                ", first_login='" + getFirst_login() + "'" +
            "}";
    }
}
