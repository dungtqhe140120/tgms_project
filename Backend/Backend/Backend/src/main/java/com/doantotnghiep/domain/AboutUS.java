package com.doantotnghiep.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A AboutUS.
 */
@Entity
@Table(name = "aboutus")
public class AboutUS implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "logo")
    private String logo;

    @Column(name = "content")
    private String content;

    @Column(name = "date_incorporation")
    private LocalDate date_incorporation;

    @Column(name = "phone_number")
    private String phoneNumber;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public AboutUS id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogo() {
        return this.logo;
    }

    public AboutUS logo(String logo) {
        this.setLogo(logo);
        return this;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getContent() {
        return this.content;
    }

    public AboutUS content(String content) {
        this.setContent(content);
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getDate_incorporation() {
        return this.date_incorporation;
    }

    public AboutUS date_incorporation(LocalDate date_incorporation) {
        this.setDate_incorporation(date_incorporation);
        return this;
    }

    public void setDate_incorporation(LocalDate date_incorporation) {
        this.date_incorporation = date_incorporation;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public AboutUS phoneNumber(String phoneNumber) {
        this.setPhoneNumber(phoneNumber);
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AboutUS)) {
            return false;
        }
        return id != null && id.equals(((AboutUS) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AboutUS{" +
            "id=" + getId() +
            ", logo='" + getLogo() + "'" +
            ", content='" + getContent() + "'" +
            ", date_incorporation='" + getDate_incorporation() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            "}";
    }
}
