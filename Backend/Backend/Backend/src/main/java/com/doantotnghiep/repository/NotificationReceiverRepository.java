package com.doantotnghiep.repository;

import com.doantotnghiep.domain.NotificationReceiver;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the NotificationReceiver entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationReceiverRepository extends JpaRepository<NotificationReceiver, Long> {}
