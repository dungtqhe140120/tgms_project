package com.doantotnghiep.service.resources;

import java.time.LocalDate;

public class EmployeeInfFullResources {
    private Long id;
    private LocalDate joinDate;
    private String address;
    private String avatar;
    private LocalDate birthday;
    private String email;
    private String fullName;
    private Integer gender;
    private String objective;
    private String phone;

    public EmployeeInfFullResources() {
    }

    public EmployeeInfFullResources(Long id, LocalDate joinDate, String address, String avatar, LocalDate birthday, String email, String fullName, Integer gender, String objective, String phone) {
        this.id = id;
        this.joinDate = joinDate;
        this.address = address;
        this.avatar = avatar;
        this.birthday = birthday;
        this.email = email;
        this.fullName = fullName;
        this.gender = gender;
        this.objective = objective;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(LocalDate joinDate) {
        this.joinDate = joinDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
