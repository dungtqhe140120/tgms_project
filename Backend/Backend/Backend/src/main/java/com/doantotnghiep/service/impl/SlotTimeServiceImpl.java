package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.SlotTime;
import com.doantotnghiep.repository.SlotTimeRepository;
import com.doantotnghiep.service.SlotTimeService;
import com.doantotnghiep.service.dto.SlotTimeDTO;
import com.doantotnghiep.service.mapper.SlotTimeMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link SlotTime}.
 */
@Service
@Transactional
public class SlotTimeServiceImpl implements SlotTimeService {

    private final Logger log = LoggerFactory.getLogger(SlotTimeServiceImpl.class);

    private final SlotTimeRepository slotTimeRepository;

    private final SlotTimeMapper slotTimeMapper;

    public SlotTimeServiceImpl(SlotTimeRepository slotTimeRepository, SlotTimeMapper slotTimeMapper) {
        this.slotTimeRepository = slotTimeRepository;
        this.slotTimeMapper = slotTimeMapper;
    }

    @Override
    public SlotTimeDTO save(SlotTimeDTO slotTimeDTO) {
        log.debug("Request to save SlotTime : {}", slotTimeDTO);
        SlotTime slotTime = slotTimeMapper.toEntity(slotTimeDTO);
        slotTime = slotTimeRepository.save(slotTime);
        return slotTimeMapper.toDto(slotTime);
    }

    @Override
    public SlotTimeDTO update(SlotTimeDTO slotTimeDTO) {
        log.debug("Request to save SlotTime : {}", slotTimeDTO);
        SlotTime slotTime = slotTimeMapper.toEntity(slotTimeDTO);
        slotTime = slotTimeRepository.save(slotTime);
        return slotTimeMapper.toDto(slotTime);
    }

    @Override
    public Optional<SlotTimeDTO> partialUpdate(SlotTimeDTO slotTimeDTO) {
        log.debug("Request to partially update SlotTime : {}", slotTimeDTO);

        return slotTimeRepository
            .findById(slotTimeDTO.getId())
            .map(existingSlotTime -> {
                slotTimeMapper.partialUpdate(existingSlotTime, slotTimeDTO);

                return existingSlotTime;
            })
            .map(slotTimeRepository::save)
            .map(slotTimeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SlotTimeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SlotTimes");
        return slotTimeRepository.findAll(pageable).map(slotTimeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SlotTimeDTO> findOne(Long id) {
        log.debug("Request to get SlotTime : {}", id);
        return slotTimeRepository.findById(id).map(slotTimeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete SlotTime : {}", id);
        slotTimeRepository.deleteById(id);
    }
}
