package com.doantotnghiep.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A TraineeAndPT.
 */
@Entity
@Table(name = "trainee_andpt")
public class TraineeAndPT implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "expired_date")
    private LocalDate expiredDate;

    @NotNull
    @Column(name = "registered_date", nullable = false)
    private LocalDate registeredDate;

    @Column(name = "price")
    private Long price;

    @Min(value = 0)
    @Max(value = 2)
    @Column(name = "confirmed")
    private Integer confirm_status;

    @Column(name = "confirmed_date")
    private LocalDate confirm_date;

    @Column(name = "confirmed_by")
    private String confirm_by;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnoreProperties(value = {"TraineeAndPT"}, allowSetters = true)
    private Trainee trainee;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnoreProperties(value = {"TraineeAndPT"}, allowSetters = true)
    private PersonalTrainer personalTrainer;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JsonIgnoreProperties(value = {"TraineeAndPT"}, allowSetters = true)
    private SlotTime slotTime;

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Trainee getTrainee() {
        return trainee;
    }

    public void setTrainee(Trainee trainee) {
        this.trainee = trainee;
    }

    public PersonalTrainer getPersonalTrainer() {
        return personalTrainer;
    }

    public void setPersonalTrainer(PersonalTrainer personalTrainer) {
        this.personalTrainer = personalTrainer;
    }

    public Long getId() {
        return this.id;
    }

    public TraineeAndPT id(Long id) {
        this.setId(id);
        return this;
    }

    public SlotTime getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(SlotTime slotTime) {
        this.slotTime = slotTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public TraineeAndPT type(String type) {
        this.setType(type);
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDate getExpired_date() {
        return this.expiredDate;
    }

    public TraineeAndPT expired_date(LocalDate expired_date) {
        this.setExpired_date(expired_date);
        return this;
    }

    public void setExpired_date(LocalDate expired_date) {
        this.expiredDate = expired_date;
    }

    public LocalDate getRegistered_Date() {
        return this.registeredDate;
    }

    public TraineeAndPT registered_Date(LocalDate registered_Date) {
        this.setRegistered_Date(registered_Date);
        return this;
    }

    public void setRegistered_Date(LocalDate registered_Date) {
        this.registeredDate = registered_Date;
    }

//    public String getSlot() {
//        return slot;
//    }
//
//    public void setSlot(String slot) {
//        this.slot = slot;
//    }

    public Integer getConfirm_status() {
        return confirm_status;
    }

    public void setConfirm_status(Integer confirm_status) {
        this.confirm_status = confirm_status;
    }

    public LocalDate getConfirm_date() {
        return confirm_date;
    }

    public void setConfirm_date(LocalDate confirm_date) {
        this.confirm_date = confirm_date;
    }

    public String getConfirm_by() {
        return confirm_by;
    }

    public void setConfirm_by(String confirm_by) {
        this.confirm_by = confirm_by;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TraineeAndPT)) {
            return false;
        }
        return id != null && id.equals(((TraineeAndPT) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "TraineeAndPT{" +
            "id=" + id +
            ", type='" + type + '\'' +
            ", expired_date=" + expiredDate +
            ", registered_Date=" + registeredDate +
//            ", slot='" + slot + '\'' +
            ", confirm_status=" + confirm_status +
            ", confirm_date=" + confirm_date +
            ", confirm_by='" + confirm_by + '\'' +
            ", trainee=" + trainee +
            ", personalTrainer=" + personalTrainer +
            '}';
    }
}
