package com.doantotnghiep.service;

import com.doantotnghiep.domain.TraineeCategory;
import com.doantotnghiep.service.dto.TraineeCategoryDTO;
import java.util.Optional;

import com.doantotnghiep.service.dto.display.CategoryTraineePTDisplayDTO;
import com.doantotnghiep.service.dto.display.ExerciseTraineePTDisplayDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.TraineeCategory}.
 */
public interface TraineeCategoryService {
    /**
     * Save a traineeCategory.
     *
     * @param traineeCategoryDTO the entity to save.
     * @return the persisted entity.
     */
    TraineeCategoryDTO save(TraineeCategoryDTO traineeCategoryDTO);

    /**
     * Updates a traineeCategory.
     *
     * @param traineeCategoryDTO the entity to update.
     * @return the persisted entity.
     */
    TraineeCategoryDTO update(TraineeCategoryDTO traineeCategoryDTO);

    /**
     * Partially updates a traineeCategory.
     *
     * @param traineeCategoryDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TraineeCategoryDTO> partialUpdate(TraineeCategoryDTO traineeCategoryDTO);

    /**
     * Get all the traineeCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TraineeCategoryDTO> findAll(Pageable pageable);

    /**
     * Get the "id" traineeCategory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TraineeCategoryDTO> findOne(Long id);

    /**
     * Delete the "id" traineeCategory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Page<CategoryTraineePTDisplayDTO> getCategoryTraineeByTrainee(Pageable pageable, Long trainee_id);

    TraineeCategory saveTraineeCategory(TraineeCategory traineeCategory);
}
