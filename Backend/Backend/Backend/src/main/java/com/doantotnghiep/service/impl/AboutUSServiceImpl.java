package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.AboutUS;
import com.doantotnghiep.repository.AboutUSRepository;
import com.doantotnghiep.service.AboutUSService;
import com.doantotnghiep.service.dto.AboutUSDTO;
import com.doantotnghiep.service.mapper.AboutUSMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link AboutUS}.
 */
@Service
@Transactional
public class AboutUSServiceImpl implements AboutUSService {

    private final Logger log = LoggerFactory.getLogger(AboutUSServiceImpl.class);

    private final AboutUSRepository aboutUSRepository;

    private final AboutUSMapper aboutUSMapper;

    public AboutUSServiceImpl(AboutUSRepository aboutUSRepository, AboutUSMapper aboutUSMapper) {
        this.aboutUSRepository = aboutUSRepository;
        this.aboutUSMapper = aboutUSMapper;
    }

    @Override
    public AboutUSDTO save(AboutUSDTO aboutUSDTO) {
        log.debug("Request to save AboutUS : {}", aboutUSDTO);
        AboutUS aboutUS = aboutUSMapper.toEntity(aboutUSDTO);
        aboutUS = aboutUSRepository.save(aboutUS);
        return aboutUSMapper.toDto(aboutUS);
    }

    @Override
    public AboutUSDTO update(AboutUSDTO aboutUSDTO) {
        log.debug("Request to save AboutUS : {}", aboutUSDTO);
        AboutUS aboutUS = aboutUSMapper.toEntity(aboutUSDTO);
        aboutUS = aboutUSRepository.save(aboutUS);
        return aboutUSMapper.toDto(aboutUS);
    }

    @Override
    public Optional<AboutUSDTO> partialUpdate(AboutUSDTO aboutUSDTO) {
        log.debug("Request to partially update AboutUS : {}", aboutUSDTO);

        return aboutUSRepository
            .findById(aboutUSDTO.getId())
            .map(existingAboutUS -> {
                aboutUSMapper.partialUpdate(existingAboutUS, aboutUSDTO);

                return existingAboutUS;
            })
            .map(aboutUSRepository::save)
            .map(aboutUSMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AboutUSDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Aboutuses");
        return aboutUSRepository.findAll(pageable).map(aboutUSMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AboutUSDTO> findOne(Long id) {
        log.debug("Request to get AboutUS : {}", id);
        return aboutUSRepository.findById(id).map(aboutUSMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete AboutUS : {}", id);
        aboutUSRepository.deleteById(id);
    }
}
