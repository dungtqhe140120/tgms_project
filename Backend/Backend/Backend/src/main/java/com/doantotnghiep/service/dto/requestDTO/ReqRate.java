package com.doantotnghiep.service.dto.requestDTO;

import com.doantotnghiep.service.dto.RateCenterDTO;
import com.doantotnghiep.service.dto.RatePTDTO;

public class ReqRate {
    private RatePTDTO ratePTDTO;
    private RateCenterDTO rateCenterDTO;

    public RatePTDTO getRatePTDTO() {
        return ratePTDTO;
    }

    public void setRatePTDTO(RatePTDTO ratePTDTO) {
        this.ratePTDTO = ratePTDTO;
    }

    public RateCenterDTO getRateCenterDTO() {
        return rateCenterDTO;
    }

    public void setRateCenterDTO(RateCenterDTO rateCenterDTO) {
        this.rateCenterDTO = rateCenterDTO;
    }
}
