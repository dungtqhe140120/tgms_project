package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.doantotnghiep.domain.Exercise} entity.
 */
public class ExerciseDTO implements Serializable {

    private Long id;

    @NotNull
    private String title;

    private String short_Description;

    @NotNull
    private String content;

    private Long created_by;

    private Long updated_by;

    private LocalDate created_date;

    private LocalDate update_date;

    @NotNull
    private String image;

    private Integer is_Active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShort_Description() {
        return short_Description;
    }

    public void setShort_Description(String short_Description) {
        this.short_Description = short_Description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(Long created_by) {
        this.created_by = created_by;
    }

    public Long getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(Long updated_by) {
        this.updated_by = updated_by;
    }

    public LocalDate getCreated_date() {
        return created_date;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public LocalDate getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(LocalDate update_date) {
        this.update_date = update_date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getIs_Active() {
        return is_Active;
    }

    public void setIs_Active(Integer is_Active) {
        this.is_Active = is_Active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExerciseDTO)) {
            return false;
        }

        ExerciseDTO exerciseDTO = (ExerciseDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, exerciseDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExerciseDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", short_Description='" + getShort_Description() + "'" +
            ", content='" + getContent() + "'" +
            ", created_by=" + getCreated_by() +
            ", updated_by=" + getUpdated_by() +
            ", created_date='" + getCreated_date() + "'" +
            ", update_date='" + getUpdate_date() + "'" +
            ", image='" + getImage() + "'" +
            ", is_Active=" + getIs_Active() +
            "}";
    }
}
