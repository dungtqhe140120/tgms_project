package com.doantotnghiep.service.mappers2;

import com.doantotnghiep.domain.Exercise;
import com.doantotnghiep.service.resources.ExercisesResource;
import com.doantotnghiep.service.resources.HomeTraineeEntity;
import com.doantotnghiep.service.resources.HomeTraineeResource;
import com.doantotnghiep.service.resources.NewsResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper
public abstract class HomeMapper2 {

    public static final HomeMapper2 INSTANCE = Mappers.getMapper(HomeMapper2.class);


    public abstract HomeTraineeResource toResource(HomeTraineeEntity entity);

    public abstract List<HomeTraineeResource> toResources(List<HomeTraineeEntity> exercises);

}
