package com.doantotnghiep.service.mappers2;

import com.doantotnghiep.domain.Exercise;
import com.doantotnghiep.domain.TraineeAndPT;
import com.doantotnghiep.service.dto.requestDTO.ReqCreateTraineeAndPT;
import com.doantotnghiep.service.resources.ExercisesResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public abstract class TraineeAndPTMapper2 {
    public static final TraineeAndPTMapper2 INSTANCE = Mappers.getMapper(TraineeAndPTMapper2.class);


    @Mapping(target = "trainee.id", source = "traineeId")
    @Mapping(target = "personalTrainer.id", source = "ptId")
    @Mapping(target = "slotTime.id", source = "slotId")
    @Mapping(target = "registered_Date", source = "registerDate")
    public abstract TraineeAndPT toEntity(ReqCreateTraineeAndPT request);
}
