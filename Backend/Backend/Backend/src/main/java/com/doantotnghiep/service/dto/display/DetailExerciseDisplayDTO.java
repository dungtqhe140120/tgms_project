package com.doantotnghiep.service.dto.display;

import java.io.Serializable;
import java.util.Objects;

public class DetailExerciseDisplayDTO implements Serializable {
    private Long id;
    private String image;
    private String title;

    public DetailExerciseDisplayDTO(Long id, String image, String title) {
        this.id = id;
        this.image = image;
        this.title = title;
    }

    public DetailExerciseDisplayDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetailExerciseDisplayDTO that = (DetailExerciseDisplayDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(image, that.image) && Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, image, title);
    }

    @Override
    public String toString() {
        return "DetailExerciseDisplayDTO{" +
            "id=" + id +
            ", image='" + image + '\'' +
            ", title='" + title + '\'' +
            '}';
    }
}
