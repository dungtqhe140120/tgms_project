package com.doantotnghiep.web.rest;

import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.HomeService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.mappers2.HomeMapper2;
import com.doantotnghiep.service.resources.HomeTraineeEntity;
import com.doantotnghiep.service.resources.HomeTraineeResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class HomeController {

    private static final String ENTITY_NAME = "Home";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HomeService homeService;

    private final TraineeRepository traineeRepository;
    private final UserService userService;

    public HomeController(HomeService homeService, TraineeRepository traineeRepository, UserService userService) {
        this.homeService = homeService;
        this.traineeRepository = traineeRepository;
        this.userService = userService;
    }

    @PreAuthorize("hasRole('ROLE_TRAINEE')")
    @GetMapping("/home-trainee")
    public ResponseEntity<HomeTraineeResource> getHome(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        Long userId = userService.getCurrentUserLogin().getId();
        Long traineeId = traineeRepository.getTraineeId(userId);
        HomeTraineeEntity homeTraineeEntity = homeService.getHomeTrainee(pageable, traineeId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), homeTraineeEntity.getNewsResources());
        return ResponseEntity.ok().headers(headers).body(HomeMapper2.INSTANCE.toResource(homeTraineeEntity));
    }

}
