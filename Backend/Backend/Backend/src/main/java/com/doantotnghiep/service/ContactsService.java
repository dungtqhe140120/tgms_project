package com.doantotnghiep.service;

import com.doantotnghiep.domain.Contacts;
import com.doantotnghiep.service.dto.ContactsDTO;
import com.doantotnghiep.service.dto.requestDTO.ContactsFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ContactsService {
    Page<ContactsDTO> getAll(ContactsFilter filter, Pageable pageable);
    ContactsDTO save(ContactsDTO contactsDTO);
    ContactsDTO getContactsById(Long id);
}
