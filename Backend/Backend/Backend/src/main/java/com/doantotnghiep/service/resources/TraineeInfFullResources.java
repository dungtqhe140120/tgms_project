package com.doantotnghiep.service.resources;

import java.time.LocalDate;

public class TraineeInfFullResources {
    private Long id;
    private String fullName;
    private String avatar;
    private Integer gender;
    private String phone;
    private String email;
    private String address;
    private LocalDate createdAt;
    private LocalDate expiredAccountAt;
    private LocalDate registerAt;
    private LocalDate expiredRegisterPTAt;
    private String numberDayRegister;
    private Long ptId;
    private String fullNamePt;
    private Long pricePT;
    private String avatarPT;

    public TraineeInfFullResources(Long id, String fullName, String avatar, Integer gender, String phone, String email,
                                   String address, LocalDate createdAt, LocalDate expiredAccountAt, LocalDate registerAt,
                                   LocalDate expiredRegisterPTAt, String numberDayRegister, Long ptId, String fullNamePt,
                                   Long pricePT, String avatarPT) {
        this.id = id;
        this.fullName = fullName;
        this.avatar = avatar;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.createdAt = createdAt;
        this.expiredAccountAt = expiredAccountAt;
        this.registerAt = registerAt;
        this.expiredRegisterPTAt = expiredRegisterPTAt;
        this.numberDayRegister = numberDayRegister;
        this.ptId = ptId;
        this.fullNamePt = fullNamePt;
        this.pricePT = pricePT;
        this.avatarPT = avatarPT;
    }

    public TraineeInfFullResources() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getExpiredAccountAt() {
        return expiredAccountAt;
    }

    public void setExpiredAccountAt(LocalDate expiredAccountAt) {
        this.expiredAccountAt = expiredAccountAt;
    }

    public LocalDate getRegisterAt() {
        return registerAt;
    }

    public void setRegisterAt(LocalDate registerAt) {
        this.registerAt = registerAt;
    }

    public LocalDate getExpiredRegisterPTAt() {
        return expiredRegisterPTAt;
    }

    public void setExpiredRegisterPTAt(LocalDate expiredRegisterPTAt) {
        this.expiredRegisterPTAt = expiredRegisterPTAt;
    }

    public String getNumberDayRegister() {
        return numberDayRegister;
    }

    public void setNumberDayRegister(String numberDayRegister) {
        this.numberDayRegister = numberDayRegister;
    }

    public Long getPtId() {
        return ptId;
    }

    public void setPtId(Long ptId) {
        this.ptId = ptId;
    }

    public String getFullNamePt() {
        return fullNamePt;
    }

    public void setFullNamePt(String fullNamePt) {
        this.fullNamePt = fullNamePt;
    }

    public Long getPricePT() {
        return pricePT;
    }

    public void setPricePT(Long pricePT) {
        this.pricePT = pricePT;
    }

    public String getAvatarPT() {
        return avatarPT;
    }

    public void setAvatarPT(String avatarPT) {
        this.avatarPT = avatarPT;
    }
}
