package com.doantotnghiep.repository;

import com.doantotnghiep.domain.UserGuide;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the UserGuide entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserGuideRepository extends JpaRepository<UserGuide, Long> {}
