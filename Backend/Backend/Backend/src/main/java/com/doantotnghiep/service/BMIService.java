package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.BMIDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.BMI}.
 */
public interface BMIService {
    /**
     * Save a bMI.
     *
     * @param bMIDTO the entity to save.
     * @return the persisted entity.
     */
    BMIDTO save(BMIDTO bMIDTO);

    /**
     * Updates a bMI.
     *
     * @param bMIDTO the entity to update.
     * @return the persisted entity.
     */
    BMIDTO update(BMIDTO bMIDTO);

    /**
     * Partially updates a bMI.
     *
     * @param bMIDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<BMIDTO> partialUpdate(BMIDTO bMIDTO);

    /**
     * Get all the bMIS.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BMIDTO> findAll(Pageable pageable);

    /**
     * Get the "id" bMI.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BMIDTO> findOne(Long id);

    /**
     * Delete the "id" bMI.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
