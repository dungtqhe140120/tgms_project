package com.doantotnghiep.repository;

import com.doantotnghiep.domain.Employee;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.resources.EmployeeInfFullResources;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the Employee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query(value = "SELECT t.id " +
        "FROM Employee t INNER JOIN Profile p ON p.id = t.profile.id " +
        "WHERE p.user.id = :userId ")
    Long getEmployeeId(@Param("userId") Long userId);


    @Query(value = "select new com.doantotnghiep.service.resources.EmployeeInfFullResources(em.id, em.join_date," +
        " p.address, p.avatar, p.birthday, p.email, p.fullname, p.gender, p.objective, p.phone)" +
        " from Employee  em inner join Profile p on em.profile.id=p.id order by em.id desc")
    List<EmployeeInfFullResources> getAllInfEmployees();
}
