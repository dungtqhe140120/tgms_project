package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.Contacts;
import com.doantotnghiep.service.dto.ContactsDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ContactsMapper extends EntityMapper<ContactsDTO, Contacts>{
}
