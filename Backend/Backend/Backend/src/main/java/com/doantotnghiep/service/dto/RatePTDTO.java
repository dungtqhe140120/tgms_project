package com.doantotnghiep.service.dto;

import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.domain.Trainee;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.doantotnghiep.domain.RatePT} entity.
 */
public class RatePTDTO implements Serializable {

    private Long id;

    private String comment;

    private Float rateStar;

    private LocalDate created_at;

    private Long traineeId;

    private Long ptId;

    public Long getId() {
        return id;
    }

    public Long getTraineeId() {
        return traineeId;
    }

    public void setTraineeId(Long traineeId) {
        this.traineeId = traineeId;
    }

    public Long getPtId() {
        return ptId;
    }

    public void setPtId(Long ptId) {
        this.ptId = ptId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Float getRateStar() {
        return rateStar;
    }

    public void setRateStar(Float rateStar) {
        this.rateStar = rateStar;
    }

    public LocalDate getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDate created_at) {
        this.created_at = created_at;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RatePTDTO)) {
            return false;
        }

        RatePTDTO ratePTDTO = (RatePTDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, ratePTDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RatePTDTO{" +
            "id=" + getId() +
            ", comment='" + getComment() + "'" +
            ", rateStar=" + getRateStar() +
            ", created_at='" + getCreated_at() + "'" +
            "}";
    }
}
