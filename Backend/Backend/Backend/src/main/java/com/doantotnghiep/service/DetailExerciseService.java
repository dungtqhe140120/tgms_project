package com.doantotnghiep.service;

import com.doantotnghiep.domain.DetailExercise;
import com.doantotnghiep.service.dto.DetailExerciseDTO;
import java.util.Optional;

import com.doantotnghiep.service.dto.display.DetailContentDisplayDTO;
import com.doantotnghiep.service.dto.display.DetailExerciseDisplayDTO;
import com.doantotnghiep.service.dto.display.DetailExerciseShowDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.DetailExercise}.
 */
public interface DetailExerciseService {
    /**
     * Save a detailExercise.
     *
     * @param detailExerciseDTO the entity to save.
     * @return the persisted entity.
     */
    DetailExerciseDTO save(DetailExerciseDTO detailExerciseDTO);

    /**
     * Updates a detailExercise.
     *
     * @param detailExerciseDTO the entity to update.
     * @return the persisted entity.
     */
    DetailExerciseDTO update(DetailExerciseDTO detailExerciseDTO);

    /**
     * Partially updates a detailExercise.
     *
     * @param detailExerciseDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DetailExerciseDTO> partialUpdate(DetailExerciseDTO detailExerciseDTO);

    /**
     * Get all the detailExercises.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DetailExerciseDTO> findAll(Pageable pageable);

    /**
     * Get the "id" detailExercise.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DetailExerciseDTO> findOne(Long id);

    /**
     * Delete the "id" detailExercise.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Page<DetailExerciseDisplayDTO> getDetailExerciseDisplayDTO(Pageable pageable, Long category_id);

    Page<DetailExerciseShowDTO> getDetailExerciseShowDTO(Pageable pageable, Long id);

    DetailExercise saveDetailExercise(DetailExercise detailExercise);

    Boolean countDetailExerciseByID(Long id);
    DetailExercise getDetailExerciseByID(Long id);

    void deleteDetailTraineeByID(Long id);

    DetailContentDisplayDTO getDetailContentByID(Long id);
}
