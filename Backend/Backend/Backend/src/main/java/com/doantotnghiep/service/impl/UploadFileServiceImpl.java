package com.doantotnghiep.service.impl;

import com.doantotnghiep.service.UploadFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@Transactional
public class UploadFileServiceImpl implements UploadFileService {

    @Value("${upload.dir}")
    String uploadDir;
    @Value("${upload.prefix}")
    String uploadPrefix;
    @Value("${link_img}")
    String link_img;

    private final Logger log = LoggerFactory.getLogger(UploadFileServiceImpl.class);
    @Override
    public String uploadFile(MultipartFile multipartFile, String name) {
        try {
            Path root = Paths.get(uploadDir);
            Path resolve = root.resolve(name);
            Files.createDirectories(root);
            Files.copy(multipartFile.getInputStream(), resolve);
            log.info("dang up file image");
        } catch (Exception e) {
            System.out.println("Error");
        }
        return link_img+"/"+uploadPrefix+"/"+ name;
    }
}
