package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.SlotTime;
import com.doantotnghiep.service.dto.SlotTimeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link SlotTime} and its DTO {@link SlotTimeDTO}.
 */
@Mapper(componentModel = "spring")
public interface SlotTimeMapper extends EntityMapper<SlotTimeDTO, SlotTime> {}
