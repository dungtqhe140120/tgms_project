package com.doantotnghiep.web.rest.errors;

public class InvalidEmailException extends BadRequestAlertException{

    private static final long serialVersionUID = 1L;

    public InvalidEmailException() {
        super(ErrorConstants.DEFAULT_TYPE, "Invalid email! Email not match with user! ", "userManagement", "emailnotmatch");
    }
}
