package com.doantotnghiep.service.dto.requestDTO;

import java.io.Serializable;
import java.util.Objects;

public class ReqDetailExerciseDTO implements Serializable {
    private String title;
    private String image;

    public ReqDetailExerciseDTO(String title, String image) {
        this.title = title;
        this.image = image;
    }

    public ReqDetailExerciseDTO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReqDetailExerciseDTO that = (ReqDetailExerciseDTO) o;
        return Objects.equals(title, that.title) && Objects.equals(image, that.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, image);
    }

    @Override
    public String toString() {
        return "ReqDetailExerciseDTO{" +
            "title='" + title + '\'' +
            ", image='" + image + '\'' +
            '}';
    }
}
