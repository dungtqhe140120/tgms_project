package com.doantotnghiep.web.rest;

import com.doantotnghiep.repository.RateCenterRepository;
import com.doantotnghiep.service.RateCenterService;
import com.doantotnghiep.service.dto.RateCenterDTO;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.doantotnghiep.domain.RateCenter}.
 */
@RestController
@RequestMapping("/api")
public class RateCenterResource {

    private final Logger log = LoggerFactory.getLogger(RateCenterResource.class);

    private static final String ENTITY_NAME = "rateCenter";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RateCenterService rateCenterService;

    private final RateCenterRepository rateCenterRepository;

    public RateCenterResource(RateCenterService rateCenterService, RateCenterRepository rateCenterRepository) {
        this.rateCenterService = rateCenterService;
        this.rateCenterRepository = rateCenterRepository;
    }

    /**
     * {@code POST  /rate-centers} : Create a new rateCenter.
     *
     * @param rateCenterDTO the rateCenterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new rateCenterDTO, or with status {@code 400 (Bad Request)} if the rateCenter has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rate-centers")
    public ResponseEntity<RateCenterDTO> createRateCenter(@RequestBody RateCenterDTO rateCenterDTO) throws URISyntaxException {
        log.debug("REST request to save RateCenter : {}", rateCenterDTO);
        if (rateCenterDTO.getId() != null) {
            throw new BadRequestAlertException("A new rateCenter cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RateCenterDTO result = rateCenterService.save(rateCenterDTO);
        return ResponseEntity
            .created(new URI("/api/rate-centers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rate-centers/:id} : Updates an existing rateCenter.
     *
     * @param id the id of the rateCenterDTO to save.
     * @param rateCenterDTO the rateCenterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated rateCenterDTO,
     * or with status {@code 400 (Bad Request)} if the rateCenterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the rateCenterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rate-centers/{id}")
    public ResponseEntity<RateCenterDTO> updateRateCenter(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RateCenterDTO rateCenterDTO
    ) throws URISyntaxException {
        log.debug("REST request to update RateCenter : {}, {}", id, rateCenterDTO);
        if (rateCenterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, rateCenterDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!rateCenterRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RateCenterDTO result = rateCenterService.update(rateCenterDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, rateCenterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /rate-centers/:id} : Partial updates given fields of an existing rateCenter, field will ignore if it is null
     *
     * @param id the id of the rateCenterDTO to save.
     * @param rateCenterDTO the rateCenterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated rateCenterDTO,
     * or with status {@code 400 (Bad Request)} if the rateCenterDTO is not valid,
     * or with status {@code 404 (Not Found)} if the rateCenterDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the rateCenterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/rate-centers/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RateCenterDTO> partialUpdateRateCenter(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RateCenterDTO rateCenterDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update RateCenter partially : {}, {}", id, rateCenterDTO);
        if (rateCenterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, rateCenterDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!rateCenterRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RateCenterDTO> result = rateCenterService.partialUpdate(rateCenterDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, rateCenterDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /rate-centers} : get all the rateCenters.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of rateCenters in body.
     */
    @GetMapping("/rate-centers")
    public ResponseEntity<List<RateCenterDTO>> getAllRateCenters(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of RateCenters");
        Page<RateCenterDTO> page = rateCenterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /rate-centers/:id} : get the "id" rateCenter.
     *
     * @param id the id of the rateCenterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the rateCenterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rate-centers/{id}")
    public ResponseEntity<RateCenterDTO> getRateCenter(@PathVariable Long id) {
        log.debug("REST request to get RateCenter : {}", id);
        Optional<RateCenterDTO> rateCenterDTO = rateCenterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(rateCenterDTO);
    }

    /**
     * {@code DELETE  /rate-centers/:id} : delete the "id" rateCenter.
     *
     * @param id the id of the rateCenterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rate-centers/{id}")
    public ResponseEntity<Void> deleteRateCenter(@PathVariable Long id) {
        log.debug("REST request to delete RateCenter : {}", id);
        rateCenterService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
