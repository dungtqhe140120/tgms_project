package com.doantotnghiep.service;

import com.doantotnghiep.domain.GymPrice;
import com.doantotnghiep.service.dto.GymPriceDTO;

import java.time.LocalDate;
import java.util.List;

public interface GymPriceService {
    GymPriceDTO save(GymPriceDTO gymPrice);

    GymPriceDTO getGymPrice(Long registerDays, LocalDate registerDate, Long auserId);

    List<Long> getRegisterDays();
}
