package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.Exercise;
import com.doantotnghiep.repository.ExerciseRepository;
import com.doantotnghiep.service.ExerciseService;
import com.doantotnghiep.service.dto.ExerciseDTO;
import com.doantotnghiep.service.mapper.ExerciseMapper;
import com.doantotnghiep.service.mappers2.ExercisesMapper2;
import com.doantotnghiep.service.resources.ExercisesResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Exercise}.
 */
@Service
@Transactional
public class ExerciseServiceImpl implements ExerciseService {

    private final Logger log = LoggerFactory.getLogger(ExerciseServiceImpl.class);

    private final ExerciseRepository exerciseRepository;

    private final ExerciseMapper exerciseMapper;

    private static final Long CENTER_ID = 1L;

    public ExerciseServiceImpl(ExerciseRepository exerciseRepository, ExerciseMapper exerciseMapper) {
        this.exerciseRepository = exerciseRepository;
        this.exerciseMapper = exerciseMapper;
    }

    //check role lai
    /*
    neu role nhan vien thi createdBy=1
    neu PT thì createdby=id cua nhan vien
     */
    @Override
    public ExerciseDTO save(ExerciseDTO exerciseDTO) {
        log.debug("Request to save Exercise : {}", exerciseDTO);
        Exercise exercise = exerciseMapper.toEntity(exerciseDTO);
        exercise = exerciseRepository.save(exercise);
        return exerciseMapper.toDto(exercise);
    }

    @Override
    public Optional<ExerciseDTO> partialUpdate(ExerciseDTO exerciseDTO) {
        log.debug("Request to partially update Exercise : {}", exerciseDTO);

        return exerciseRepository
            .findById(exerciseDTO.getId())
            .map(existingExercise -> {
                exerciseMapper.partialUpdate(existingExercise, exerciseDTO);

                return existingExercise;
            })
            .map(exerciseRepository::save)
            .map(exerciseMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ExerciseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Exercises");
        return exerciseRepository.findAll(pageable).map(exerciseMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ExercisesResource> findExercisesCenter(Pageable pageable) {
        log.debug("Request to get all Exercises Center");
        return exerciseRepository.findByCreatedBy(pageable, CENTER_ID).map(ExercisesMapper2.INSTANCE::toResource);
    }

    @Override
    public Page<ExercisesResource> findMineExercises(Pageable pageable, Long userId) {
        log.debug("Request to get all mine  Exercises");
        return exerciseRepository.findByCreatedBy(pageable, userId).map((ExercisesMapper2.INSTANCE::toResource));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ExerciseDTO> findOne(Long id) {
        log.debug("Request to get Exercise : {}", id);
        return exerciseRepository.findById(id).map(exerciseMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Exercise : {}", id);
        exerciseRepository.deleteById(id);
    }
}
