package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.RatePT;
import com.doantotnghiep.service.dto.RatePTDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link RatePT} and its DTO {@link RatePTDTO}.
 */
@Mapper(componentModel = "spring")
public interface RatePTMapper extends EntityMapper<RatePTDTO, RatePT> {}
