package com.doantotnghiep.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Exercise.
 */
@Entity
@Table(name = "exercise")
public class Exercise implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "short_description")
    private String short_Description;

    @NotNull
    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "created_by")
    private Long createdBy;

    @NotNull
    @Column(name = "updated_by", nullable = false)
    private Long updatedBy;


    @Column(name = "created_date", nullable = false)
    private LocalDate created_date;

    @Column(name = "update_date", nullable = false)
    private LocalDate update_date;

    @NotNull
    @Column(name = "image", nullable = false)
    private String image;

    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "is_active", nullable = false)
    private Integer is_Active;

    @OneToMany(mappedBy = "exercise",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "exercise"}, allowSetters = true)
    private Set<TraineeExercise> traineeExercises = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Exercise id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public Exercise title(String title) {
        this.setTitle(title);
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShort_Description() {
        return this.short_Description;
    }

    public Exercise short_Description(String short_Description) {
        this.setShort_Description(short_Description);
        return this;
    }

    public void setShort_Description(String short_Description) {
        this.short_Description = short_Description;
    }

    public String getContent() {
        return this.content;
    }

    public Exercise content(String content) {
        this.setContent(content);
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreated_by() {
        return this.createdBy;
    }

    public Exercise created_by(Long created_by) {
        this.setCreated_by(created_by);
        return this;
    }

    public void setCreated_by(Long created_by) {
        this.createdBy = created_by;
    }

    public Long getUpdated_by() {
        return this.updatedBy;
    }

    public Exercise updated_by(Long updated_by) {
        this.setUpdated_by(updated_by);
        return this;
    }

    public void setUpdated_by(Long updated_by) {
        this.updatedBy = updated_by;
    }

    public LocalDate getCreated_date() {
        return this.created_date;
    }

    public Exercise created_date(LocalDate created_date) {
        this.setCreated_date(created_date);
        return this;
    }

    public void setCreated_date(LocalDate created_date) {
        this.created_date = created_date;
    }

    public LocalDate getUpdate_date() {
        return this.update_date;
    }

    public Exercise update_date(LocalDate update_date) {
        this.setUpdate_date(update_date);
        return this;
    }

    public void setUpdate_date(LocalDate update_date) {
        this.update_date = update_date;
    }

    public String getImage() {
        return this.image;
    }

    public Exercise image(String image) {
        this.setImage(image);
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getIs_Active() {
        return this.is_Active;
    }

    public Exercise is_Active(Integer is_Active) {
        this.setIs_Active(is_Active);
        return this;
    }

    public Set<TraineeExercise> getTraineeExercises() {
        return this.traineeExercises;
    }

    public void setTraineeExercises(Set<TraineeExercise> traineeExercises) {
        if (this.traineeExercises != null) {
            this.traineeExercises.forEach(i -> i.setExercise(null));
        }
        if (traineeExercises != null) {
            traineeExercises.forEach(i -> i.setExercise(this));
        }
        this.traineeExercises = traineeExercises;
    }

    public Exercise traineeExercise(Set<TraineeExercise> traineeExercises) {
        this.setTraineeExercises(traineeExercises);
        return this;
    }

    public Exercise addTraineeExercise(TraineeExercise traineeExercise) {
        this.traineeExercises.add(traineeExercise);
        traineeExercise.setExercise(this);
        return this;
    }

    public Exercise removeTraineeExercise(TraineeExercise traineeExercise) {
        this.traineeExercises.remove(traineeExercise);
        traineeExercise.setExercise(null);
        return this;
    }

    public void setIs_Active(Integer is_Active) {
        this.is_Active = is_Active;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Exercise)) {
            return false;
        }
        return id != null && id.equals(((Exercise) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Exercise{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", short_Description='" + getShort_Description() + "'" +
            ", content='" + getContent() + "'" +
            ", created_by=" + getCreated_by() +
            ", updated_by=" + getUpdated_by() +
            ", created_date='" + getCreated_date() + "'" +
            ", update_date='" + getUpdate_date() + "'" +
            ", image='" + getImage() + "'" +
            ", is_Active=" + getIs_Active() +
            "}";
    }
}
