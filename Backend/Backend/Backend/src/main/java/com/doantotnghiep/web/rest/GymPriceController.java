package com.doantotnghiep.web.rest;

import com.doantotnghiep.service.GymPriceService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.GymPriceDTO;
import com.doantotnghiep.web.rest.errors.BadRequestAlertException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api")
public class GymPriceController {
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private static final String ENTITY_NAME = "GymPrice";

    private final GymPriceService gymPriceService;
    private final UserService userService;

    public GymPriceController(GymPriceService gymPriceService, UserService userService) {
        this.userService = userService;
        this.gymPriceService = gymPriceService;
    }

    @PostMapping("/gym-prices")
    public ResponseEntity<GymPriceDTO> createGymPrice(@Valid @RequestBody GymPriceDTO gymPriceDTO) throws URISyntaxException {
        if (gymPriceDTO.getId() != null) {
            throw new BadRequestAlertException("A new gym_price cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GymPriceDTO result = gymPriceService.save(gymPriceDTO);
        return ResponseEntity
            .created(new URI("/api/gym-prices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/gym-prices/register-days")
    public ResponseEntity<List<Long>> getRegisterDays(
    ) {
        List<Long> listRegisterDays = gymPriceService.getRegisterDays();
        return ResponseEntity.ok().body(listRegisterDays);
    }

    @GetMapping("/gym-prices/prices")
    public ResponseEntity<GymPriceDTO> getPrices(
        @RequestParam(value = "registerDays") Long registerDays,
        @RequestParam(value = "registerDate") LocalDate registerDate
    ) {
        if (registerDate.isBefore(LocalDate.now())) {
            throw new BadRequestAlertException("You can't choose register date before current time", ENTITY_NAME, "register date invalid");
        }
        Long userId = userService.getCurrentUserLogin().getId();
        GymPriceDTO gymPriceDTO = gymPriceService.getGymPrice(registerDays, registerDate, userId);
        return ResponseEntity.ok().body(gymPriceDTO);
    }

}
