package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.doantotnghiep.domain.TraineeAndPT} entity.
 */
public class TraineeAndPTDTO implements Serializable {

    private Long id;

    private String type;

    private LocalDate expiredDate;

//    private String slot;

//    @NotNull
    private LocalDate registeredDate;

    private Integer confirm_status;

    private LocalDate confirm_date;

    private String confirm_by;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDate getExpired_date() {
        return expiredDate;
    }

    public void setExpired_date(LocalDate expired_date) {
        this.expiredDate = expired_date;
    }

    public LocalDate getRegistered_Date() {
        return registeredDate;
    }

    public void setRegistered_Date(LocalDate registered_Date) {
        this.registeredDate = registered_Date;
    }

//    public String getSlot() {
//        return slot;
//    }
//
//    public void setSlot(String slot) {
//        this.slot = slot;
//    }

    public Integer getConfirm_status() {
        return confirm_status;
    }

    public void setConfirm_status(Integer confirm_status) {
        this.confirm_status = confirm_status;
    }

    public LocalDate getConfirm_date() {
        return confirm_date;
    }

    public void setConfirm_date(LocalDate confirm_date) {
        this.confirm_date = confirm_date;
    }

    public String getConfirm_by(String confirm_by) {
        return this.confirm_by;
    }

    public void setConfirm_by(String confirm_by) {
        this.confirm_by = confirm_by;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TraineeAndPTDTO)) {
            return false;
        }

        TraineeAndPTDTO traineeAndPTDTO = (TraineeAndPTDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, traineeAndPTDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "TraineeAndPTDTO{" +
            "id=" + id +
            ", type='" + type + '\'' +
            ", expired_date=" + expiredDate +
            ", registered_Date=" + registeredDate +
//            ", slot='" + slot + '\'' +
            ", confirm_status=" + confirm_status +
            ", confirm_date=" + confirm_date +
            ", confirm_by='" + confirm_by + '\'' +
            '}';
    }
}
