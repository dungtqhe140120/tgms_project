package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.DetailExercise;
import com.doantotnghiep.repository.DetailExerciseRepository;
import com.doantotnghiep.service.DetailExerciseService;
import com.doantotnghiep.service.dto.DetailExerciseDTO;
import com.doantotnghiep.service.dto.display.DetailContentDisplayDTO;
import com.doantotnghiep.service.dto.display.DetailExerciseDisplayDTO;
import com.doantotnghiep.service.dto.display.DetailExerciseShowDTO;
import com.doantotnghiep.service.mapper.DetailExerciseMapper;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link DetailExercise}.
 */
@Service
@Transactional
public class DetailExerciseServiceImpl implements DetailExerciseService {

    private final Logger log = LoggerFactory.getLogger(DetailExerciseServiceImpl.class);

    private final DetailExerciseRepository detailExerciseRepository;

    private final DetailExerciseMapper detailExerciseMapper;

    public DetailExerciseServiceImpl(DetailExerciseRepository detailExerciseRepository, DetailExerciseMapper detailExerciseMapper) {
        this.detailExerciseRepository = detailExerciseRepository;
        this.detailExerciseMapper = detailExerciseMapper;
    }

    @Override
    public DetailExerciseDTO save(DetailExerciseDTO detailExerciseDTO) {
        log.debug("Request to save DetailExercise : {}", detailExerciseDTO);
        DetailExercise detailExercise = detailExerciseMapper.toEntity(detailExerciseDTO);
        detailExercise = detailExerciseRepository.save(detailExercise);
        return detailExerciseMapper.toDto(detailExercise);
    }

    @Override
    public DetailExerciseDTO update(DetailExerciseDTO detailExerciseDTO) {
        log.debug("Request to save DetailExercise : {}", detailExerciseDTO);
        DetailExercise detailExercise = detailExerciseMapper.toEntity(detailExerciseDTO);
        detailExercise = detailExerciseRepository.save(detailExercise);
        return detailExerciseMapper.toDto(detailExercise);
    }

    @Override
    public Optional<DetailExerciseDTO> partialUpdate(DetailExerciseDTO detailExerciseDTO) {
        log.debug("Request to partially update DetailExercise : {}", detailExerciseDTO);

        return detailExerciseRepository
            .findById(detailExerciseDTO.getId())
            .map(existingDetailExercise -> {
                detailExerciseMapper.partialUpdate(existingDetailExercise, detailExerciseDTO);

                return existingDetailExercise;
            })
            .map(detailExerciseRepository::save)
            .map(detailExerciseMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DetailExerciseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DetailExercises");
        return detailExerciseRepository.findAll(pageable).map(detailExerciseMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DetailExerciseDTO> findOne(Long id) {
        log.debug("Request to get DetailExercise : {}", id);
        return detailExerciseRepository.findById(id).map(detailExerciseMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DetailExercise : {}", id);
        detailExerciseRepository.deleteById(id);
    }

    @Override
    public Page<DetailExerciseDisplayDTO> getDetailExerciseDisplayDTO(Pageable pageable, Long category_id) {
        log.debug("Request to getDetailExerciseDisplayDTO : {}", category_id);
        return detailExerciseRepository.getDetailExerciseDisplayDTO(pageable, category_id);
    }

    @Override
    public Page<DetailExerciseShowDTO> getDetailExerciseShowDTO(Pageable pageable, Long id) {
        log.debug("Request to getDetailExerciseShowDTO : {}", id);
        return detailExerciseRepository.getDetailExerciseShowDTO(pageable, id);
    }

    @Override
    public DetailExercise saveDetailExercise(DetailExercise detailExercise) {
        log.debug("Request to saveDetailExercise : {}");
        return detailExerciseRepository.save(detailExercise);
    }

    @Override
    public Boolean countDetailExerciseByID(Long id) {
        log.debug("Request to countDetailExerciseByID : {}",id);
        return detailExerciseRepository.countDetailExerciseByID(id) > 0 ? true : false;
    }

    @Override
    public DetailExercise getDetailExerciseByID(Long id) {
        log.debug("Request to getDetailExerciseByID : {}",id);
        return detailExerciseRepository.getDetailExerciseByID(id);
    }

    @Override
    public void deleteDetailTraineeByID(Long id) {
        log.debug("Request to deleteDetailTraineeByID : {}",id);
        detailExerciseRepository.deleteDetailExerciseByID(id);
    }

    @Override
    public DetailContentDisplayDTO getDetailContentByID(Long id) {
        log.debug("Request to getDetailContentByID : {}",id);
        DetailContentDisplayDTO detailContentDisplayDTO=detailExerciseRepository.detailContent(id);
        log.debug("detailContentDisplayDTO: "+ detailContentDisplayDTO.getContent());
        return detailExerciseRepository.detailContent(id);
    }
}
