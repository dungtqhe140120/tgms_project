package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.RateCenter;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.repository.RateCenterRepository;
import com.doantotnghiep.repository.RatePTRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.service.RateService;
import com.doantotnghiep.service.dto.RateCenterDTO;
import com.doantotnghiep.service.dto.RatePTDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqRate;
import com.doantotnghiep.service.dto.response.MetaData;
import com.doantotnghiep.service.dto.response.ResponseRate;
import com.doantotnghiep.service.mapper.RateCenterMapper;
import com.doantotnghiep.service.mapper.RatePTMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@Transactional
public class RateServiceImpl implements RateService {
    private final RateCenterRepository rateCenterRepository;

    private final RateCenterMapper rateCenterMapper;

    private final RatePTRepository ratePTRepository;

    private final TraineeAndPTRepository traineeAndPTRepository;

    private final TraineeRepository traineeRepository;

    private final RatePTMapper ratePTMapper;

    public RateServiceImpl(RatePTRepository ratePTRepository, RatePTMapper ratePTMapper,
                           RateCenterRepository rateCenterRepository, RateCenterMapper rateCenterMapper,
                           TraineeAndPTRepository traineeAndPTRepository, TraineeRepository traineeRepository) {
        this.ratePTRepository = ratePTRepository;
        this.ratePTMapper = ratePTMapper;
        this.rateCenterRepository = rateCenterRepository;
        this.rateCenterMapper = rateCenterMapper;
        this.traineeAndPTRepository = traineeAndPTRepository;
        this.traineeRepository = traineeRepository;
    }

    @Override
    public ResponseRate save(ReqRate reqRate, Long userId) {
        ResponseRate responseRate = new ResponseRate();
        ReqRate reqRate1 = new ReqRate();

        try {
            if (reqRate.getRateCenterDTO() != null) {
                reqRate.getRateCenterDTO().setUserId(userId);
                reqRate.getRateCenterDTO().setCreated_at(LocalDate.now());
                var rateCenter = rateCenterRepository.save(rateCenterMapper.toEntity(reqRate.getRateCenterDTO()));
                reqRate1.setRateCenterDTO(rateCenterMapper.toDto(rateCenter));
            }

            if (reqRate.getRatePTDTO() != null) {
                Long traineeId = traineeRepository.getTraineeId(userId);
                if (traineeId != null) {
                    Long ptId = traineeAndPTRepository.getPtId(traineeId);
                    if (ptId != null) {
                        reqRate.getRatePTDTO().setTraineeId(traineeId);

                        reqRate.getRatePTDTO().setPtId(ptId);

                        reqRate.getRatePTDTO().setCreated_at(LocalDate.now());
                        var ratePT = ratePTRepository.save(ratePTMapper.toEntity(reqRate.getRatePTDTO()));
                        reqRate1.setRatePTDTO(ratePTMapper.toDto(ratePT));

                    } else {
                        MetaData metaData = new MetaData();
                        metaData.setMessage("Hiện tại bạn không có PT nên đánh giá PT không thành công!");
                        responseRate.setMetaData(metaData);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        responseRate.setReqRate(reqRate1);
        return responseRate;
    }

    @Override
    public RateCenterDTO getRateCenter(Long userId) {
        RateCenterDTO rateCenterDTO = new RateCenterDTO();
        var rateCenterDTO1 = rateCenterMapper.toDto(rateCenterRepository.findRateCenterByUserId(userId));
        return rateCenterDTO1 == null ? rateCenterDTO : rateCenterDTO1;
    }

    @Override
    public RateCenterDTO saveRateCenter(RateCenterDTO request, Long userId) {
        request.setUserId(userId);
        request.setCreated_at(LocalDate.now());
        RateCenter rateCenter = new RateCenter();
        try {
            rateCenter = rateCenterRepository.save(rateCenterMapper.toEntity(request));
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        return rateCenterMapper.toDto(rateCenter);
    }

    @Override
    public ReqRate getRateByUserId(Long userId) {
        ReqRate resp = new ReqRate();

        {
            RateCenterDTO rateCenterDTO = rateCenterMapper.toDto(rateCenterRepository.findRateCenterByUserId(userId));
            if (rateCenterDTO != null) {
                resp.setRateCenterDTO(rateCenterDTO);
            }
        }

        {
            Long traineeId = traineeRepository.getTraineeId(userId);
            if (traineeId != null) {
                Long ptId = traineeAndPTRepository.getPtId(traineeId);
                if (ptId != null) {
                    RatePTDTO ratePTDTO = ratePTMapper.toDto(ratePTRepository.findRatePTByTraineeIdAndPtId(traineeId, ptId));
                    if (ratePTDTO != null) {
                        resp.setRatePTDTO(ratePTDTO);
                    }
                }

            }
        }
        return resp;
    }
}
