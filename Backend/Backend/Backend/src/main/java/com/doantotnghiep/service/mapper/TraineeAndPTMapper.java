package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.TraineeAndPT;
import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TraineeAndPT} and its DTO {@link TraineeAndPTDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TraineeAndPTMapper extends EntityMapper<TraineeAndPTDTO, TraineeAndPT> {}
