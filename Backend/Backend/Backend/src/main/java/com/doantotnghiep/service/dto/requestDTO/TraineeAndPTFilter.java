package com.doantotnghiep.service.dto.requestDTO;

import org.springframework.util.CollectionUtils;

import java.util.Set;

public class TraineeAndPTFilter {
    private String namePT;
    private String nameTrainee;
    private Set<Integer> statusConfirm;

    public String getNamePT() {
        return namePT;
    }

    public void setNamePT(String namePT) {
        this.namePT = namePT;
    }

    public String getNameTrainee() {
        return nameTrainee;
    }

    public void setNameTrainee(String nameTrainee) {
        this.nameTrainee = nameTrainee;
    }

    public Set<Integer> getStatusConfirm() {
        return statusConfirm;
    }

    public void setStatusConfirm(Set<Integer> statusConfirm) {
        this.statusConfirm = statusConfirm;
    }

    public TraineeAndPTFilter convertFilter() {
        TraineeAndPTFilter traineeAndPTFilter = new TraineeAndPTFilter();
        traineeAndPTFilter.setNamePT(this.namePT == null ? "%%" : ("%" + this.namePT + "%"));
        traineeAndPTFilter.setNameTrainee(this.nameTrainee == null ? "%%" : ("%" + this.nameTrainee + "%"));
        traineeAndPTFilter.setStatusConfirm(CollectionUtils.isEmpty(this.statusConfirm) ? Set.of(0, 1, 2) : this.statusConfirm);
        return traineeAndPTFilter;
    }
}
