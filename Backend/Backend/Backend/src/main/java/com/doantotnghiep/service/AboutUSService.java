package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.AboutUSDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.AboutUS}.
 */
public interface AboutUSService {
    /**
     * Save a aboutUS.
     *
     * @param aboutUSDTO the entity to save.
     * @return the persisted entity.
     */
    AboutUSDTO save(AboutUSDTO aboutUSDTO);

    /**
     * Updates a aboutUS.
     *
     * @param aboutUSDTO the entity to update.
     * @return the persisted entity.
     */
    AboutUSDTO update(AboutUSDTO aboutUSDTO);

    /**
     * Partially updates a aboutUS.
     *
     * @param aboutUSDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<AboutUSDTO> partialUpdate(AboutUSDTO aboutUSDTO);

    /**
     * Get all the aboutuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AboutUSDTO> findAll(Pageable pageable);

    /**
     * Get the "id" aboutUS.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AboutUSDTO> findOne(Long id);

    /**
     * Delete the "id" aboutUS.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
