package com.doantotnghiep.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.doantotnghiep.domain.NotificationReceiver} entity.
 */
public class NotificationReceiverDTO implements Serializable {

    private Long id;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NotificationReceiverDTO)) {
            return false;
        }

        NotificationReceiverDTO notificationReceiverDTO = (NotificationReceiverDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, notificationReceiverDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NotificationReceiverDTO{" +
            "id=" + getId() +
            ", status=" + getStatus() +
            "}";
    }
}
