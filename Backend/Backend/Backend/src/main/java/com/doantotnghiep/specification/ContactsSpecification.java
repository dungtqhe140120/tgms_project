package com.doantotnghiep.specification;

import com.doantotnghiep.domain.Contacts;
import com.doantotnghiep.service.dto.requestDTO.ContactsFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ContactsSpecification implements Specification<Contacts> {
    private final ContactsFilter filter;

    public ContactsSpecification(ContactsFilter filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<Contacts> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        final List<Predicate> predicates = new ArrayList<>();

        if (!StringUtils.isBlank(filter.getEmail())) {
            predicates.add(root.get("email").in(filter.getEmail()));
        }

        if (!StringUtils.isBlank(filter.getName())) {
            predicates.add(root.get("name").in(filter.getName()));
        }

        if (filter.getStatus() != null) {
            predicates.add(root.get("status").in(filter.getStatus()));
        }

        if (filter.getCreatedAtFrom() != null) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), filter.getCreatedAtFrom()));
        }

        if (filter.getCreatedAtTo() != null) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("createdAt"), filter.getCreatedAtTo()));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

}
