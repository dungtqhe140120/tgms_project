package com.doantotnghiep.service;

import com.doantotnghiep.domain.PersonalTrainer;
import com.doantotnghiep.service.dto.PersonalTrainerDTO;
import com.doantotnghiep.service.dto.PersonalTrainerFullDTO;
import com.doantotnghiep.service.dto.requestDTO.PersonalTraineeUpdateDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.resources.Trainee2PTResources;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.PersonalTrainer}.
 */
public interface PersonalTrainerService {
    /**
     * Save a personalTrainer.
     *
     * @param personalTrainerDTO the entity to save.
     * @return the persisted entity.
     */
    PersonalTrainerDTO save(PersonalTrainerDTO personalTrainerDTO);

    /**
     * Partially updates a personalTrainer.
     *
     * @param personalTrainerDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PersonalTrainerDTO> partialUpdate(PersonalTrainerDTO personalTrainerDTO);

    /**
     * Get all the personalTrainers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PersonalTrainerDTO> findAll(Pageable pageable);

    /**
     * Get the "id" personalTrainer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PersonalTrainerDTO> findOne(Long id);

    /**
     * Delete the "id" personalTrainer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    PersonalTrainer savePersonalTrainer(PersonalTrainer personalTrainer);

    PersonalTrainerFullDTO getOnePersonalTrainerFullDTOByUser(Long user_id);

    ReqPTFullDTO getOneReqPTFullDTOByUser(Long user_id);

    Boolean getExistPT(Long user_id);

    PersonalTrainer findPTByUserID(Long user_id);

    Page<ReqPTFullDTO> getPTInfFulls(Pageable pageable);

    Page<Trainee2PTResources> getCurrentTraineeOfPT(Long ptId, Pageable pageable);

    Optional<PersonalTraineeUpdateDTO> getPTById(Long id);

    void updateFullPT(PersonalTraineeUpdateDTO request);
}
