package com.doantotnghiep.service.mapper;

import com.doantotnghiep.domain.NotificationReceiver;
import com.doantotnghiep.service.dto.NotificationReceiverDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link NotificationReceiver} and its DTO {@link NotificationReceiverDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NotificationReceiverMapper extends EntityMapper<NotificationReceiverDTO, NotificationReceiver> {}
