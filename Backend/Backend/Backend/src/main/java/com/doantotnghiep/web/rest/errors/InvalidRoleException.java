package com.doantotnghiep.web.rest.errors;

public class InvalidRoleException extends BadRequestAlertException{

    private static final long serialVersionUID = 1L;

    public InvalidRoleException() {
        super(ErrorConstants.INVALID_ROLE, "Invalid Role!", "userManagement", "invalidRole");
    }
}
