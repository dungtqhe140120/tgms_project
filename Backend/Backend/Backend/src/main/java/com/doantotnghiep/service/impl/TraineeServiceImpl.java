package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.Profile;
import com.doantotnghiep.domain.Trainee;
import com.doantotnghiep.domain.User;
import com.doantotnghiep.repository.ProfileRepository;
import com.doantotnghiep.repository.TraineeRepository;
import com.doantotnghiep.repository.UserRepository;
import com.doantotnghiep.service.TraineeService;
import com.doantotnghiep.service.dto.PersonalTrainerFullDTO;
import com.doantotnghiep.service.dto.TraineeDTO;
import com.doantotnghiep.service.dto.TraineeFullDTO;
import com.doantotnghiep.service.dto.display.TraineeDisplayDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqTraineeFullDTO;
import com.doantotnghiep.service.dto.requestDTO.TraineeDetailDTO;
import com.doantotnghiep.service.mapper.TraineeMapper;

import java.util.Objects;
import java.util.Optional;

import com.doantotnghiep.service.resources.TraineeInfFullResources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service
 * Implementation
 * for
 * managing
 * {@link
 * Trainee}.
 */
@Service
@Transactional
public class TraineeServiceImpl implements TraineeService {

    private final Logger log = LoggerFactory.getLogger(TraineeServiceImpl.class);

    private final TraineeRepository traineeRepository;

    private final TraineeMapper traineeMapper;

    public TraineeServiceImpl(TraineeRepository traineeRepository, TraineeMapper traineeMapper) {
        this.traineeRepository = traineeRepository;
        this.traineeMapper = traineeMapper;
    }

    @Override
    public TraineeDTO save(TraineeDTO traineeDTO) {
        log.debug("Request to save TraineeDTO : {}", traineeDTO);
        Trainee trainee = traineeMapper.toEntity(traineeDTO);
        trainee = traineeRepository.save(trainee);
        return traineeMapper.toDto(trainee);
    }

    @Override
    public Optional<TraineeDTO> partialUpdate(TraineeDTO traineeDTO) {
        log.debug("Request to partially update Trainee : {}", traineeDTO);

        return traineeRepository
            .findById(traineeDTO.getId())
            .map(existingTrainee -> {
                traineeMapper.partialUpdate(existingTrainee, traineeDTO);

                return existingTrainee;
            })
            .map(traineeRepository::save)
            .map(traineeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TraineeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Trainees");
        return traineeRepository.findAll(pageable).map(traineeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TraineeDTO> findOne(Long id) {
        log.debug("Request to get Trainee : {}", id);
        return traineeRepository.findById(id).map(traineeMapper::toDto);
    }

    @Override
    public Optional<TraineeInfFullResources> findFullInfTrainee(Long traineeId) {
        return traineeRepository.findFirstTraineeFullInf(traineeId);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Trainee : {}", id);
        traineeRepository.deleteById(id);
    }

    @Override
    public Trainee saveTrainee(Trainee trainee) {
        log.debug("Request to delete Trainee : {}", trainee);
        return traineeRepository.save(trainee);
    }

    @Override
    public TraineeFullDTO getOneTraineeFullDTOByUser(Long user_id) {
        log.debug("Request to getOneTraineeFullDTOByUser : {}", user_id);
        return traineeRepository.getOneTraineeFullDTOByUser(user_id);
    }

    @Override
    public Boolean getExistTrainee(Long user_id) {
        log.info("count PT by user: " + traineeRepository.countTraineeByUserID(user_id));
        if (traineeRepository.countTraineeByUserID(user_id) != 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Trainee findTraineeByUserID(Long user_id) {
        log.debug("Request to findTraineeByUserID : {}", user_id);
        return traineeRepository.getTraineeByUserID(user_id);
    }

    @Override
    public ReqTraineeFullDTO getOneReqTraineeFullDTOByUser(Long user_id) {
        log.debug("Request to getOneReqTraineeFullDTOByUser : {}", user_id);
        return traineeRepository.getReqTraineeFullDTOByUserID(user_id);
    }

    @Override
    public Page<TraineeDisplayDTO> getAllListTraineeDisplayDTO(Pageable pageable) {
        log.debug("Request to getAllListTraineeDisplayDTO : {}");
        return traineeRepository.getAllTraineeDisplayDTO(pageable);
    }

    @Override
    public Page<TraineeDisplayDTO> getAllTraineeDisplayDTOByName(Pageable pageable, String traineeName) {
        return traineeRepository.getAllTraineeDisplayDTOByName(pageable, traineeName);
    }

    @Override
    public Trainee findTraineeByID(Long id) {
        log.debug("Request to findTraineeByID : {}", id);
        return traineeRepository.getTraineeByID(id);
    }

    public TraineeDetailDTO getTraineeDetailDto(Long id) {
        Trainee traineeByID = traineeRepository.getTraineeByID(id);
        return convertTraineeEntityToDto(traineeByID);
    }

    private TraineeDetailDTO convertTraineeEntityToDto(Trainee traineeByID) {
        Profile profile = traineeByID.getProfile();
        User user = profile.getUser();
        TraineeDetailDTO traineeDetailDTO = new TraineeDetailDTO(
            traineeByID.getId(),
            user.getLogin(),
            user.getFirstName(),
            user.getLastName(),
            user.getEmail(),
            user.getImageUrl(),
            profile.getGender(),
            profile.getBirthday(),
            profile.getPhone(),
            profile.getAddress(),
            traineeByID.getExpired_date()
        );
        return traineeDetailDTO;
    }

    @Override
    public TraineeDetailDTO updateTraineeDetail(Long id, TraineeDetailDTO traineeDetailDTO) {
        Trainee trainee = traineeRepository.getTraineeByID(id);
        if (Objects.isNull(trainee)) {
            throw new RuntimeException(String.format("Cannot find Trainee by id: %s", id));
        }
        Profile profile = trainee.getProfile();
        profile.setGender(traineeDetailDTO.getGender());
        profile.setBirthday(traineeDetailDTO.getBirthDay());
        profile.setPhone(traineeDetailDTO.getPhone());
        profile.setAddress(traineeDetailDTO.getAddress());

        User user = profile.getUser();
        user.setFirstName(traineeDetailDTO.getFirstName());
        user.setLastName(traineeDetailDTO.getLastName());
        user.setEmail(traineeDetailDTO.getEmail());
        user.setImageUrl(traineeDetailDTO.getImageUrl());

        trainee.setExpired_date(traineeDetailDTO.getExpireDate());
        Trainee trainee1 = traineeRepository.save(trainee);
        return convertTraineeEntityToDto(trainee1);
    }
}
