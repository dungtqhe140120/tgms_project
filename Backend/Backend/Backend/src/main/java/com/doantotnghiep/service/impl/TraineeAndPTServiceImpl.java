package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.TraineeAndPT;
import com.doantotnghiep.repository.PersonalTrainerRepository;
import com.doantotnghiep.repository.TraineeAndPTRepository;
import com.doantotnghiep.service.TraineeAndPTService;
import com.doantotnghiep.service.UserService;
import com.doantotnghiep.service.dto.TraineeAndPTDTO;
import com.doantotnghiep.service.dto.display.CalendarPTDisplayDTO;
import com.doantotnghiep.service.dto.requestDTO.ReqCreateTraineeAndPT;
import com.doantotnghiep.service.dto.requestDTO.ReqPTFullDTO;
import com.doantotnghiep.service.dto.requestDTO.TraineeAndPTFilter;
import com.doantotnghiep.service.mapper.TraineeAndPTMapper;
import com.doantotnghiep.service.mappers2.TraineeAndPTMapper2;
import com.doantotnghiep.service.resources.TraineeAndPTResources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link TraineeAndPT}.
 */
@Service
@Transactional
public class TraineeAndPTServiceImpl implements TraineeAndPTService {

    private final Logger log = LoggerFactory.getLogger(TraineeAndPTServiceImpl.class);

    private final TraineeAndPTRepository traineeAndPTRepository;

    private final TraineeAndPTMapper traineeAndPTMapper;

    private final PersonalTrainerRepository personalTrainerRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public TraineeAndPTServiceImpl(TraineeAndPTRepository traineeAndPTRepository, TraineeAndPTMapper traineeAndPTMapper, PersonalTrainerRepository personalTrainerRepository) {
        this.traineeAndPTRepository = traineeAndPTRepository;
        this.traineeAndPTMapper = traineeAndPTMapper;
        this.personalTrainerRepository = personalTrainerRepository;
    }

    @Override
    public TraineeAndPTDTO save(TraineeAndPTDTO traineeAndPTDTO) {
        log.debug("Request to save TraineeAndPT : {}", traineeAndPTDTO);
        TraineeAndPT traineeAndPT = traineeAndPTMapper.toEntity(traineeAndPTDTO);
        traineeAndPT = traineeAndPTRepository.save(traineeAndPT);
        return traineeAndPTMapper.toDto(traineeAndPT);
    }

    @Override
    public TraineeAndPTDTO saveV2(ReqCreateTraineeAndPT request) {
        log.debug("Request to save TraineeAndPT : {}", request);
        TraineeAndPT traineeAndPT = TraineeAndPTMapper2.INSTANCE.toEntity(request);
        traineeAndPT.setExpired_date(traineeAndPT.getRegistered_Date().plusDays(request.getDateRegister()));
        traineeAndPT.setConfirm_status(0);
        traineeAndPT = traineeAndPTRepository.save(traineeAndPT);
        return traineeAndPTMapper.toDto(traineeAndPT);
    }

    @Override
    public Optional<TraineeAndPTDTO> partialUpdate(TraineeAndPTDTO traineeAndPTDTO) {
        log.debug("Request to partially update TraineeAndPT : {}", traineeAndPTDTO);

        return traineeAndPTRepository
            .findById(traineeAndPTDTO.getId())
            .map(existingTraineeAndPT -> {
                traineeAndPTMapper.partialUpdate(existingTraineeAndPT, traineeAndPTDTO);

                return existingTraineeAndPT;
            })
            .map(traineeAndPTRepository::save)
            .map(traineeAndPTMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TraineeAndPTDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TraineeAndPTS");
        return traineeAndPTRepository.findAll(pageable).map(traineeAndPTMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TraineeAndPTDTO> findOne(Long id) {
        log.debug("Request to get TraineeAndPT : {}", id);
        return traineeAndPTRepository.findById(id).map(traineeAndPTMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TraineeAndPT : {}", id);
        traineeAndPTRepository.deleteById(id);
    }

    @Override
    public List<CalendarPTDisplayDTO> getListCalendarPTDisplayDTO(Pageable pageable, Long pt_id, String date) {
//        return traineeAndPTRepository.getListCalendarPTDisplayDTO(pageable, pt_id, date);
        StringBuilder sql = new StringBuilder("select b.id, p.fullname , st.name as slot from trainee_andpt a inner join trainee b " +
            "on a.trainee_id=b.id inner join jhi_profile p on p.id=b.profile_id inner join slot_time st on st.id=a.slot_time_id inner " +
            "join personal_trainer pt on a.personal_trainer_id=pt.id \n" +
            "where  pt.id=? and to_date(?,'dd-mm-yyyy') >= a.registered_date " +
            "and to_date(?,'dd-mm-yyyy') <= a.expired_Date and a.confirmed=1  " +
            " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");
        log.info("final sql: " + sql.toString());
        return jdbcTemplate.query(sql.toString(),new Object[]{pt_id, date, date, pageable.getPageNumber() * pageable.getPageSize(), pageable.getPageSize()}, (rs, rowNum) ->
            new CalendarPTDisplayDTO(
                rs.getLong(1),
                rs.getString(2),
                rs.getString(3)
            ));
    }

    @Override
    public TraineeAndPTDTO confirm(TraineeAndPTDTO traineeAndPTDTO, Long confirmBy) {

        TraineeAndPT traineeAndPT = traineeAndPTRepository.getById(traineeAndPTDTO.getId());
        traineeAndPT.setConfirm_by(confirmBy.toString());
        traineeAndPT.setConfirm_date(LocalDate.now());
        traineeAndPT.setConfirm_status(traineeAndPTDTO.getConfirm_status());
        return traineeAndPTMapper.toDto(traineeAndPTRepository.save(traineeAndPT));
    }

    @Override
    public List<ReqPTFullDTO> getPTsFreeSlot(Long slotId, LocalDate registerDate) {
        List<TraineeAndPT> listPTsNotFreeSlot = traineeAndPTRepository.findTraineeAndPTSBySlotTimeIdAndExpiredDateIsAfter(slotId, registerDate);
        List<Long> ptIds = listPTsNotFreeSlot.stream().map(traineeAndPT -> traineeAndPT.getPersonalTrainer().getId()).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(ptIds)){
            return personalTrainerRepository.getPTs();
        }
        return personalTrainerRepository.getPTsFreeSlot(ptIds);
    }

    @Override
    public Page<TraineeAndPTResources> getListTraineeAndPTByFilter(TraineeAndPTFilter traineeAndPTFilter, Pageable pageable) {
        var traineeAndPTFilter1 = traineeAndPTFilter.convertFilter();
        return traineeAndPTRepository.getListTraineeByFilter(pageable, traineeAndPTFilter1.getNamePT(),
            traineeAndPTFilter1.getNameTrainee(), traineeAndPTFilter1.getStatusConfirm());
    }
}
