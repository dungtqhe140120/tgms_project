package com.doantotnghiep.repository;

import com.doantotnghiep.domain.SlotTime;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the SlotTime entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SlotTimeRepository extends JpaRepository<SlotTime, Long> {}
