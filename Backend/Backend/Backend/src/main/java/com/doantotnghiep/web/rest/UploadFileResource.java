package com.doantotnghiep.web.rest;

import com.doantotnghiep.service.UploadFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@RestController
@RequestMapping("/api")
public class UploadFileResource {

    private final UploadFileService uploadFileService;

    private final Logger log = LoggerFactory.getLogger(UploadFileResource.class);

    public UploadFileResource(UploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    @PostMapping("/uploadImage")
    public ResponseEntity create(@RequestParam MultipartFile file, Principal principal) throws Exception {
        String name;
        log.info(file.getOriginalFilename());

        long time = System.currentTimeMillis();
        if (file.getContentType().equals("image/png")) {
            name = time + ".png";
        }else if (file.getContentType().equals("image/jpeg")) {
            name = time + ".jpeg";
        }else if (file.getContentType().equals("video/quicktime")) {
            name = time + ".mov";
        } else if (file.getContentType().contains("mp4")) {
            name = time + ".mp4";
        }else if (file.getContentType().toLowerCase().contains("mov")) {
            name = time + ".mov";
        } else {
            return new ResponseEntity<Object>("Please choose other file!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        String linkImage = uploadFileService.uploadFile(file, name);
        if (linkImage.equals("ErrorFTP"))
            return new ResponseEntity<Object>("Error FTP", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<Object>(linkImage, HttpStatus.OK);
    }

    //chua check dieu kien
    @PostMapping("/uploadMultipleFile")
    public List<String> uploads(@RequestBody MultipartFile[] files, Principal principal) throws Exception {
        List<String> urls = new ArrayList<>();
        for (MultipartFile file : files) {
            urls.add(uploadFileService.uploadFile(file, file.getOriginalFilename()));
        }
        return urls;
    }

}
