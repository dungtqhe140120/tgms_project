package com.doantotnghiep.service;

import com.doantotnghiep.service.dto.NotificationReceiverDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.doantotnghiep.domain.NotificationReceiver}.
 */
public interface NotificationReceiverService {
    /**
     * Save a notificationReceiver.
     *
     * @param notificationReceiverDTO the entity to save.
     * @return the persisted entity.
     */
    NotificationReceiverDTO save(NotificationReceiverDTO notificationReceiverDTO);

    /**
     * Partially updates a notificationReceiver.
     *
     * @param notificationReceiverDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<NotificationReceiverDTO> partialUpdate(NotificationReceiverDTO notificationReceiverDTO);

    /**
     * Get all the notificationReceivers.
     *
     * @return the list of entities.
     */
    List<NotificationReceiverDTO> findAll();

    /**
     * Get the "id" notificationReceiver.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NotificationReceiverDTO> findOne(Long id);

    /**
     * Delete the "id" notificationReceiver.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
