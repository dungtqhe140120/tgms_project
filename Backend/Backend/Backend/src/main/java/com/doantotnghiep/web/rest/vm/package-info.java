/**
 * View Models used by Spring MVC REST controllers.
 */
package com.doantotnghiep.web.rest.vm;
