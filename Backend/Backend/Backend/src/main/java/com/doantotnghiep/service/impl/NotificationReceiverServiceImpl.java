package com.doantotnghiep.service.impl;

import com.doantotnghiep.domain.NotificationReceiver;
import com.doantotnghiep.repository.NotificationReceiverRepository;
import com.doantotnghiep.service.NotificationReceiverService;
import com.doantotnghiep.service.dto.NotificationReceiverDTO;
import com.doantotnghiep.service.mapper.NotificationReceiverMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link NotificationReceiver}.
 */
@Service
@Transactional
public class NotificationReceiverServiceImpl implements NotificationReceiverService {

    private final Logger log = LoggerFactory.getLogger(NotificationReceiverServiceImpl.class);

    private final NotificationReceiverRepository notificationReceiverRepository;

    private final NotificationReceiverMapper notificationReceiverMapper;

    public NotificationReceiverServiceImpl(
        NotificationReceiverRepository notificationReceiverRepository,
        NotificationReceiverMapper notificationReceiverMapper
    ) {
        this.notificationReceiverRepository = notificationReceiverRepository;
        this.notificationReceiverMapper = notificationReceiverMapper;
    }

    @Override
    public NotificationReceiverDTO save(NotificationReceiverDTO notificationReceiverDTO) {
        log.debug("Request to save NotificationReceiver : {}", notificationReceiverDTO);
        NotificationReceiver notificationReceiver = notificationReceiverMapper.toEntity(notificationReceiverDTO);
        notificationReceiver = notificationReceiverRepository.save(notificationReceiver);
        return notificationReceiverMapper.toDto(notificationReceiver);
    }

    @Override
    public Optional<NotificationReceiverDTO> partialUpdate(NotificationReceiverDTO notificationReceiverDTO) {
        log.debug("Request to partially update NotificationReceiver : {}", notificationReceiverDTO);

        return notificationReceiverRepository
            .findById(notificationReceiverDTO.getId())
            .map(existingNotificationReceiver -> {
                notificationReceiverMapper.partialUpdate(existingNotificationReceiver, notificationReceiverDTO);

                return existingNotificationReceiver;
            })
            .map(notificationReceiverRepository::save)
            .map(notificationReceiverMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<NotificationReceiverDTO> findAll() {
        log.debug("Request to get all NotificationReceivers");
        return notificationReceiverRepository
            .findAll()
            .stream()
            .map(notificationReceiverMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<NotificationReceiverDTO> findOne(Long id) {
        log.debug("Request to get NotificationReceiver : {}", id);
        return notificationReceiverRepository.findById(id).map(notificationReceiverMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete NotificationReceiver : {}", id);
        notificationReceiverRepository.deleteById(id);
    }
}
