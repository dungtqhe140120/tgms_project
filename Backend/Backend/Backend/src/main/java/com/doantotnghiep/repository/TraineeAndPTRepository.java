package com.doantotnghiep.repository;

import com.doantotnghiep.domain.TraineeAndPT;
import com.doantotnghiep.service.resources.Trainee2PTResources;
import com.doantotnghiep.service.resources.TraineeAndPTResources;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * Spring Data SQL repository for the TraineeAndPT entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TraineeAndPTRepository extends JpaRepository<TraineeAndPT, Long> {
    List<TraineeAndPT> findTraineeAndPTSBySlotTimeIdAndExpiredDateIsAfter(Long slotId, LocalDate currentDate);

//    @Query(value = "select new com.doantotnghiep.service.dto.display.CalendarPTDisplayDTO (a.id, p.fullname , st.name )" +
//        "from TraineeAndPT a inner join Trainee b on a.trainee.id=b.id inner join Profile p on p.id=b.profile.id inner join" +
//        " SlotTime st on st.id=a.slotTime.id inner join PersonalTrainer pt on a.personalTrainer.id=pt.id where  pt.id=?1 and " +
//        " a.registered_Date <= to_date(?2,'dd-mm-yyyy')  and to_date(?2,'dd-mm-yyyy') <= a.expired_date and a.confirmed=1")
//    Page<CalendarPTDisplayDTO> getListCalendarPTDisplayDTO(Pageable pageable,Long id,  String date);

    @Query(value = "SELECT t.personalTrainer.id " +
        "FROM TraineeAndPT t  " +
        "WHERE t.trainee.id = :traineeId AND t.expiredDate > current_date ")
    Long getPtId(@Param("traineeId") Long traineeId);

    @Query(value = "SELECT t.expiredDate " +
        "FROM TraineeAndPT t  " +
        "WHERE t.trainee.id = :traineeId AND t.expiredDate > :registerDate")
    List<LocalDate> getExpireDayByTraineeIdAndRegistered_Date(@Param("traineeId") Long traineeId, @Param("registerDate") LocalDate registerDate);

    @Query(value = "SELECT new com.doantotnghiep.service.resources.Trainee2PTResources(t.id, p.fullname," +
        " tap.expiredDate, tap.type, tap.slotTime.id, rp.comment, rp.rateStar, rp.created_at, p.avatar)" +
        " FROM TraineeAndPT tap INNER JOIN Trainee t ON tap.trainee.id = t.id" +
        " INNER JOIN Profile p ON t.profile.id = p.id" +
        " LEFT JOIN RatePT rp ON t.id = rp.traineeId" +
        " WHERE tap.personalTrainer.id = :ptId AND tap.expiredDate > CURRENT_DATE" +
        " ORDER BY rp.created_at DESC ")
    Page<Trainee2PTResources> getCurrentListTraineeByPTId(Pageable pageable, @Param("ptId") Long ptId);

    @Query(value = "SELECT new com.doantotnghiep.service.resources.TraineeAndPTResources(tap.id, p.fullname," +
        " pp.fullname, tap.expiredDate, tap.registeredDate, tap.type, tap.slotTime.id, tap.confirm_status," +
        " tap.confirm_by, tap.confirm_date)" +
        " FROM TraineeAndPT tap INNER JOIN Trainee t ON tap.trainee.id = t.id" +
        " INNER JOIN Profile p ON t.profile.id = p.id" +
        " INNER JOIN PersonalTrainer pt ON tap.personalTrainer.id = pt.id" +
        " INNER JOIN Profile pp ON pt.profile.id = pp.id" +
        " WHERE pp.fullname LIKE :ptName AND p.fullname LIKE :traineeName AND tap.confirm_status IN :confirmStatus  AND tap.expiredDate > CURRENT_DATE")
    Page<TraineeAndPTResources> getListTraineeByFilter(Pageable pageable, @Param("ptName") String ptName,
                                                       @Param("traineeName") String traineeName, @Param("confirmStatus") Set<Integer> confirmStatus);
}
