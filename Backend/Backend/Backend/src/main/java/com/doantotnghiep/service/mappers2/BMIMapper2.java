package com.doantotnghiep.service.mappers2;

import com.doantotnghiep.domain.BMI;
import com.doantotnghiep.domain.Exercise;
import com.doantotnghiep.service.resources.BMIResource;
import com.doantotnghiep.service.resources.ExercisesResource;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class BMIMapper2 {
    public static final BMIMapper2 INSTANCE = Mappers.getMapper(BMIMapper2.class);


    @Mapping(target = "bmi", source = "bmi_number")
    public abstract BMIResource toResource(BMI entity);
}
