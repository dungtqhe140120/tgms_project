package com.doantotnghiep.service.dto.requestDTO;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class ReqCreateTraineeAndPT {
    private Long ptId;

    private Long traineeId;
    private String type;

    private Long slotId;

    private Long price;
    private LocalDate registerDate;

    @NotNull
    private Long dateRegister;

    public LocalDate getRegisterDate() {
        return registerDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRegisterDate(LocalDate registerDate) {
        this.registerDate = registerDate;
    }

    public Long getPtId() {
        return ptId;
    }

    public void setPtId(Long ptId) {
        this.ptId = ptId;
    }

    public Long getTraineeId() {
        return traineeId;
    }

    public void setTraineeId(Long traineeId) {
        this.traineeId = traineeId;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public Long getDateRegister() {
        return dateRegister;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public void setDateRegister(Long dateRegister) {
        this.dateRegister = dateRegister;
    }
}
